﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pharmacogenetics.Core.ViewModels.PharmaViewModels.ViewModels
{
   public class ExeclAssayAlleleTranslator
    {



       public string ACell { set; get; }
       public string BCell { set; get; }
       public string CCell { set; get; }
       public string DCell { set; get; }
       public string ECell { set; get; }
       public string FCell { set; get; }
       public string GCell { set; get; }
       public string HCell { set; get; }
       public string ICell { set; get; }
       public string JCell { set; get; }
       public string kCell { set; get; }
       public string LCell { set; get; }
       public string MCell { set; get; }
       public string NCell { set; get; }
       public string OCell { set; get; }
       public string PCell { set; get; }



    }




      public class GeneRows 
      {

          string GeneName {get;set;}
          List<GeneRowsDetails>  GeneRowsDetailList {get;set;}

     }



     public class GeneRowsDetails 
      {

          string AssaySymbol {get;set;}
          string snpRef {get;set;}
          string GeneSymbol {get;set;}
          string AssayIdName {get;set;}

     }






}
