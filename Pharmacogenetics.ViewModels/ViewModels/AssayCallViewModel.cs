﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pharmacogenetics.Core.ViewModels.PharmaViewModels.ViewModels
{
    public class AssayCallViewModel
    {
        public AssayCallViewModel()
        {
        }
        public AssayCallViewModel(int assayId, int callId, string assayName, string callName)
        {
            this.assayId = assayId;
            this.callId = callId;
            this.assayName = assayName;
            this.callName = callName;
        }

        public int assayId;
        public int callId;
        public string assayName;
        public string callName;
    }
}
