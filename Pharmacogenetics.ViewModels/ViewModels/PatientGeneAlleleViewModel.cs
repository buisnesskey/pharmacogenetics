﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pharmacogenetics.Core.ViewModels.PharmaViewModels.ViewModels
{
    public class PatientGeneAlleleViewModel
    {
        public PatientGeneAlleleViewModel()
        {
            this.geneAllele = new List<GeneAlleleViewModel>();
        }
        public int patientId;
        public string mrn;
        public string patientName;
        public List<GeneAlleleViewModel> geneAllele;
    }
}
