﻿using Pharmacogenetics.BaseClasses.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Pharmacogenetics.Core.ViewModels.PharmaViewModels.ViewModels
{
    public class PatientViewModel : BaseViewModel
    {
        public PatientViewModel()
        {
            this.PatientLab = new List<PatientLabViewModel>();
            this.PatientTranslator = new List<PatientTranslatorViewModel>();
        }

        public int PatientID { get; set; }
        public int MedicalID { get; set; }
        public string PatientName { get; set; }
        public byte DeleteStatus { get; set; }
        public System.DateTime CreateDate { get; set; }
        public Nullable<System.DateTime> ModifyDate { get; set; }
        public Nullable<System.DateTime> DeleteDate { get; set; }

        public virtual List<PatientLabViewModel> PatientLab { get; set; }
        public virtual List<PatientTranslatorViewModel> PatientTranslator { get; set; }
    }
}
