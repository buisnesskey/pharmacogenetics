//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Pharmacogenetics.Core.ViewModels.PharmaViewModels.ViewModels
{
    using System;
    using System.Collections.Generic;
    
    public class GeneViewModel
    {
        public GeneViewModel()
        {
            this.AssayList = new List<AssayViewModel>();
            this.PatientLab = new List<PatientLabViewModel>();
            this.TranslatorMapping = new List<TranslatorMappingViewModel>();
        }
    
        public int GeneID { get; set; }
        public string GeneSymbol { get; set; }
        public byte DeleteStatus { get; set; }
        public System.DateTime CreateDate { get; set; }
        public Nullable<System.DateTime> ModifyDate { get; set; }
        public Nullable<System.DateTime> DeleteDate { get; set; }
    
        public virtual List<AssayViewModel> AssayList { get; set; }
        public virtual List<PatientLabViewModel> PatientLab { get; set; }
        public virtual List<TranslatorMappingViewModel> TranslatorMapping { get; set; }
    }
}
