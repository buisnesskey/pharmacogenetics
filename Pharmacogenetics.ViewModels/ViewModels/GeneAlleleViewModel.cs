﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pharmacogenetics.Core.ViewModels.PharmaViewModels.ViewModels
{
    public class GeneAlleleViewModel
    {
        public GeneAlleleViewModel()
        {
        }
        public GeneAlleleViewModel(int geneId,int alleleId,string geneName,string alleleName)
        {
            this.geneId=geneId;
            this.alleleId=alleleId;
            this.geneName=geneName;
            this.alleleName= alleleName;
        }
    
        public int geneId;
        public int alleleId;
        public string geneName;
        public string alleleName;
    }
}
