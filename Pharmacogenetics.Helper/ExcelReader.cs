﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Pharmacogenetics.Helper
{
    public class ExcelReader
    {
        /// <summary>
        /// Read the sheet file and return the result as DataTable.The allows types of the file are (xls,xlsx and csv)
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public DataTable ReadExcelFile(string path)
        {
            #region read excel
            DataTable dt = new DataTable();
            DataTable dt2 = new DataTable();
            using (OleDbConnection conn = new OleDbConnection())
            {
                //get the connection string for excel lib to read the sheet depend in the file type (xls,xlsx and csv)
                string xlsxConnectionString = ConfigurationManager.AppSettings["xlsxConnectionString"];
                string xlsConnectionString = ConfigurationManager.AppSettings["xlsConnectionString"];
                string csvConnectionString = ConfigurationManager.AppSettings["csvConnectionString"];

                //inject the file name in the excel connection string depend in the file type (xls,xlsx and csv)
                string Import_FileName = path;
                string fileExtension = Path.GetExtension(Import_FileName);
                if (fileExtension == ".xls")
                    conn.ConnectionString = string.Format(xlsConnectionString, Import_FileName);//"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Import_FileName + ";" + "Extended Properties='Excel 8.0;HDR=YES;'";
                if (fileExtension == ".xlsx")
                    conn.ConnectionString = string.Format(xlsxConnectionString, Import_FileName);//"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + Import_FileName + ";" + "Extended Properties='Excel 12.0 Xml;HDR=YES;'";
                if (fileExtension == ".csv")
                    conn.ConnectionString = string.Format(csvConnectionString, Path.GetDirectoryName(path));

                if (fileExtension == ".csv")
                {
                    //read line by line if the file CSV
                    dt = ReadCsvFile(path);
                }
                else
                {
                    //Else: using excel queris
                    using (OleDbCommand comm = new OleDbCommand())
                    {
                        conn.Open();
                        dt2 = conn.GetSchema("Tables");
                        string firstSheet = dt2.Rows[0]["TABLE_NAME"].ToString();

                        comm.CommandText = "Select * from [" + firstSheet + "]";
                        comm.Connection = conn;
                        using (OleDbDataAdapter da = new OleDbDataAdapter())
                        {
                            da.SelectCommand = comm;
                            da.Fill(dt);
                        }
                    }
                }
            }
            #endregion
            return dt;
        }
        /// <summary>
        /// Read CSV file and return the result as DataTable.
        /// </summary>
        /// <param name="fileNamepath"></param>
        /// <returns></returns>
        public DataTable ReadCsvFile(string fileNamepath)
        {
            DataTable dtCsv = new DataTable();
            string Fulltext;
            using (StreamReader sr = new StreamReader(fileNamepath))
            {
                while (!sr.EndOfStream)
                {
                    //read full file text  
                    Fulltext = sr.ReadToEnd().ToString(); 
                    //Split by line to rows
                    string[] rows = Regex.Split(Fulltext, "\r\n");
                    
                    for (int i = 0; i < rows.Count() - 1; i++)
                    {
                        //split each row with comma to get individual values  
                        string[] rowValues = rows[i].Split(','); 
                        {
                            if (i == 0)
                            {
                                for (int j = 0; j < rowValues.Count(); j++)
                                {
                                    //add headers  
                                    dtCsv.Columns.Add(rowValues[j]); 
                                }
                            }
                            else
                            {
                                DataRow dr = dtCsv.NewRow();
                                for (int k = 0; k < dtCsv.Columns.Count; k++)
                                {
                                    if (!string.IsNullOrWhiteSpace(rowValues[k]))
                                        dr[k] = rowValues[k].ToString();
                                }
                                dtCsv.Rows.Add(dr); //add other rows  
                            }
                        }
                    }
                }
            }
            return dtCsv;
        }


        public DataTable ReadExcelFileForTranslator(string path  )
        {
            #region read excel
            DataTable dt = new DataTable();
            DataTable dt2 = new DataTable();
            using (OleDbConnection conn = new OleDbConnection())
            {
                string xlsxConnectionString = ConfigurationManager.AppSettings["xlsxConnectionString"];
                string xlsConnectionString = ConfigurationManager.AppSettings["xlsConnectionString"];
                string csvConnectionString = ConfigurationManager.AppSettings["csvConnectionString"];



                string Import_FileName = path;
                string fileExtension = Path.GetExtension(Import_FileName);
                if (fileExtension == ".xls")
                    conn.ConnectionString = string.Format(xlsConnectionString, Import_FileName);//"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Import_FileName + ";" + "Extended Properties='Excel 8.0;HDR=YES;'";
                if (fileExtension == ".xlsx")
                    conn.ConnectionString = string.Format(xlsxConnectionString, Import_FileName);//"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + Import_FileName + ";" + "Extended Properties='Excel 12.0 Xml;HDR=YES;'";
                if (fileExtension == ".csv")
                     conn.ConnectionString = string.Format(csvConnectionString, Path.GetDirectoryName(path));
                // conn.ConnectionString = string.Format(hardCodedConnectionString, Path.GetDirectoryName(path));


                //oConn.Open "Provider=Microsoft.Jet.OLEDB.4.0;" & _"Data Source=" & App.Path & "\Results\Orders1.xls;" & _"Extended Properties=""Excel 8.0;HDR=NO;"""
//value="Provider=Microsoft.Jet.OleDb.4.0; Data Source={0};Extended Properties='Text;HDR=YES;FMT=Delimited;ImportMixedTypes=Text;IMEX=1;'"
                using (OleDbCommand comm = new OleDbCommand())
                {
                    conn.Open();
                    dt2 = conn.GetSchema("Tables");


                    string firstSheet = "";
                    if (dt2.Rows.Count == 1)
                    {
                        firstSheet = dt2.Rows[0]["TABLE_NAME"].ToString();
                    }
                    else
                    {
                        firstSheet = dt2.Rows[1]["TABLE_NAME"].ToString();
                    }
                    if (fileExtension == ".xlsx" || fileExtension == ".xls")
                    { 
                    comm.CommandText = "Select * from [" + firstSheet + "]";
                    }
                    if (fileExtension == ".csv")
                    { 
                        comm.CommandText = "SELECT * FROM [" + Path.GetFileName(path) + "]";
                    }
                    comm.Connection = conn;

                    using (OleDbDataAdapter da = new OleDbDataAdapter())
                    {
                        da.SelectCommand = comm;
                        da.Fill(dt);
                        return dt;
                    }
                }
            }
            #endregion
         //   return dt;

            // if (fileExtension == ".csv")

            // {
            #region Method 1

            //string CSVFilePathName = path;
            //string[] Lines = File.ReadAllLines(CSVFilePathName);
            //string[] Fields;
            //Fields = Lines[0].Split(new char[] { ',' });
            //int Cols = Fields.GetLength(0);
            ////1st row must be column names; force lower case to ensure matching later on.
            //for (int i = 0; i < Cols; i++)
            //dt.Columns.Add(Fields[i].ToLower(), typeof(string));
            //DataRow Row;
            //for (int i = 1; i < Lines.GetLength(0); i++)
            //{
            //Fields = Lines[i].Split(new char[] { ',' });
            //Row = dt.NewRow();
            //for (int f = 0; f < Cols; f++)

            //{
            //try
            //{
            //if (Fields.Length>f) Row[f] = Fields[f];
            //}
            //catch (Exception ex)
            //{

            //string ss = ex.Message;
            //}
            //}
            //dt.Rows.Add(Row);
            //}


            //return dt;

            #endregion

            #region Feedback Issues (Why you read the file two times???)
            //You can remove the perv. one
            #endregion

            #region Method2

            //List<string[]> parsedData = new List<string[]>();
            //try
            //{
            //    using (StreamReader readFile = new StreamReader(path))
            //    {
            //        string line;
            //        string[] row;

            //        while ((line = readFile.ReadLine()) != null)
            //        {
            //            row = line.Split(',');
            //            parsedData.Add(row);
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    string ee = ex.Message;
            //}

            //var data= parsedData;

            //DataTable newdt = ConvertListToDataTable(parsedData);
            //return newdt;//dt;

            #endregion

            // }


        }

        static DataTable ConvertListToDataTable(List<string[]> list)
        {
            // New table.
            DataTable table = new DataTable();

            // Get max columns.
            int columns = 0;
            foreach (var array in list)
            {
                if (array.Length > columns)
                {
                    columns = array.Length;
                }
            }

            // Add columns.
            for (int i = 0; i < columns; i++)
            {
                table.Columns.Add();
            }

            // Add rows.
            foreach (var array in list)
            {
                table.Rows.Add(array);
            }

            return table;
        }



     

        public DataTable ReadExcelFileForPatientCopyNumber(string path)
        {
            #region read excel
            DataTable dt = new DataTable();
            DataTable dt2 = new DataTable();
            using (OleDbConnection conn = new OleDbConnection())
            {
                string xlsxConnectionString = ConfigurationManager.AppSettings["xlsxConnectionString"];
                string xlsConnectionString = ConfigurationManager.AppSettings["xlsConnectionString"];
                string csvConnectionString = ConfigurationManager.AppSettings["csvConnectionString"];
                string fileExtension = Path.GetExtension(path);
                if (fileExtension == ".xls")
                    conn.ConnectionString = string.Format(xlsConnectionString, path);
                        //"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Import_FileName + ";" + "Extended Properties='Excel 8.0;HDR=YES;'";
                if (fileExtension == ".xlsx")
                    conn.ConnectionString = string.Format(xlsxConnectionString, path);
                        //"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + Import_FileName + ";" + "Extended Properties='Excel 12.0 Xml;HDR=YES;'";
                if (fileExtension == ".csv")
                    conn.ConnectionString = string.Format(csvConnectionString, Path.GetDirectoryName(path));
                if (fileExtension == ".csv")
                {
                    dt = ReadCsvPatientCopyNumberFile(path);
                }
                else
                {
                    using (OleDbCommand comm = new OleDbCommand())
                    {
                        conn.Open();
                        dt2 = conn.GetSchema("Tables");
                        string firstSheet = dt2.Rows[0]["TABLE_NAME"].ToString();

                        comm.CommandText = "Select * from [" + firstSheet + "]";
                        comm.Connection = conn;
                        using (OleDbDataAdapter da = new OleDbDataAdapter())
                        {
                            da.SelectCommand = comm;
                            da.Fill(dt);
                        }
                        bool headerExist = false;
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            if (dt.Rows[i].ItemArray.Contains("Sample Name") && dt.Rows[i].ItemArray.Contains("Target") &&
                                dt.Rows[i].ItemArray.Contains("CN Predicted"))
                            {
                                headerExist = true;
                                for (int j = 0; j <= i; j++)
                                {
                                    if (j == i)
                                    {
                                        for (int k = 0; k < dt.Rows[0].ItemArray.Length; k++)
                                        {
                                            dt.Columns[k].ColumnName = dt.Rows[0].ItemArray[k].ToString();
                                        }
                                    }
                                    dt.Rows.RemoveAt(0);
                                }
                            }
                        }
                        if (!headerExist)
                        {
                            dt = null;
                        }
                    }
                }
            }
            #endregion
            return dt;
        }

        public DataTable ReadCsvPatientCopyNumberFile(string path)
        {
            DataTable dtCsv = new DataTable();
            using (StreamReader sr = new StreamReader(path))
            {
                while (!sr.EndOfStream)
                {
                    string fullText = sr.ReadToEnd(); //read full file text  
                    string[] rows = Regex.Split(fullText, "\r\n");
                    int headerIndex=Array.FindIndex(rows, f => f.Contains("Sample Name")&& f.Contains("Target")&& f.Contains("CN Predicted"));
                    if (headerIndex != -1&& dtCsv!=null)
                    {
                        for (int i = headerIndex; i < rows.Count() - 1; i++)
                        {
                            string[] rowValues = rows[i].Split(',');
                            //split each row with comma to get individual values  
                            {
                                if (i == headerIndex)
                                {
                                    for (int j = 0; j < rowValues.Count(); j++)
                                    {
                                        dtCsv.Columns.Add(rowValues[j]); //add headers  
                                    }
                                }
                                else
                                {
                                    if (rowValues.Length == dtCsv.Columns.Count)
                                    {
                                        DataRow dr = dtCsv.NewRow();
                                        for (int k = 0; k < dtCsv.Columns.Count; k++)
                                        {
                                            if (!string.IsNullOrWhiteSpace(rowValues[k]))
                                                dr[k] = rowValues[k].ToString();
                                        }
                                        dtCsv.Rows.Add(dr); //add other rows  
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        dtCsv = null;
                    }
                }
            }
            return dtCsv;
        }



        #region Review

        public DataTable ReadExcelFileForEmployeesData(string path, List<string> englishColumnHeaders, List<string> arabicColumnHeaders, List<string> status, string uploadStatusKey)
        {
            #region read excel 
            DataTable dt = new DataTable();
            DataTable dt2 = new DataTable();
            try
            {
                using (OleDbConnection conn = new OleDbConnection())
                {
                    string xlsxConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 12.0;IMEX=1;HDR=NO;TypeGuessRows=0;ImportMixedTypes=Text;'";
                    string xlsConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties='Excel 8.0;HDR=NO;ImportMixedTypes=Text;'";
                    //string csvConnectionString ="Provider=Microsoft.Jet.OleDb.4.0; Data Source={0};Extended Properties='Text;HDR=YES;FMT=Delimited;ImportMixedTypes=Text;IMEX=1;'";
                    string fileExtension = Path.GetExtension(path);
                    if (fileExtension == ".xls")
                        conn.ConnectionString = string.Format(xlsConnectionString, path);
                    //"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Import_FileName + ";" + "Extended Properties='Excel 8.0;HDR=YES;'";
                    if (fileExtension == ".xlsx")
                        conn.ConnectionString = string.Format(xlsxConnectionString, path);
                    //"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + Import_FileName + ";" + "Extended Properties='Excel 12.0 Xml;HDR=YES;'";
                    //if (fileExtension == ".csv")
                    //    conn.ConnectionString = string.Format(csvConnectionString, Path.GetDirectoryName(path));

                    //if (fileExtension == ".csv")
                    //{
                    //    dt = ReadCsvEmployeesDataFile(path);
                    //}
                    if (fileExtension == ".xls" || fileExtension == ".xlsx")
                    {
                        using (OleDbCommand comm = new OleDbCommand())
                        {
                            conn.Open();
                            dt2 = conn.GetSchema("Tables");
                            List<string> sheetsList = new List<string>();
                            for (int i = 0; i < dt2.Rows.Count; i++)
                            {
                                sheetsList.Add(dt2.Rows[i]["TABLE_NAME"].ToString());
                            }
                            string firstSheet = sheetsList.Find(x => x.Trim().ToLower() == "final" + "$");
                            if (!string.IsNullOrWhiteSpace(firstSheet))
                            {
                                comm.CommandText = "Select * from [" + firstSheet + "]";
                                comm.Connection = conn;
                                using (OleDbDataAdapter da = new OleDbDataAdapter())
                                {
                                    da.SelectCommand = comm;
                                    da.Fill(dt);
                                }
                                bool headerExist = false;
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    if (!headerExist)
                                    {
                                        bool allEnglishColHeaderFound = englishColumnHeaders.TrueForAll(ch => dt.Rows[i].ItemArray.Any(x => x.ToString().Trim().ToLower() == ch.Trim().ToLower()));
                                        bool allArabicColHeaderFound = arabicColumnHeaders.TrueForAll(ch => dt.Rows[i].ItemArray.Any(x => x.ToString().Trim() == ch.Trim()));
                                        if (allEnglishColHeaderFound || allArabicColHeaderFound)
                                        {
                                            List<string> columnHeaders;
                                            if (allEnglishColHeaderFound)
                                            {
                                                columnHeaders = englishColumnHeaders;
                                            }
                                            else
                                            {
                                                columnHeaders = arabicColumnHeaders;
                                            }
                                            headerExist = true;
                                            for (int j = 0; j <= i; j++)
                                            {
                                                if (j == i)
                                                {
                                                    for (int k = 0; k < dt.Rows[0].ItemArray.Length; k++)
                                                    {
                                                        if (!string.IsNullOrWhiteSpace(dt.Rows[0].ItemArray[k].ToString()) && !dt.Columns.Contains(dt.Rows[0].ItemArray[k].ToString().Trim().ToLower()) && columnHeaders.Any(ch => dt.Rows[0].ItemArray[k].ToString().Trim().ToLower() == ch.Trim().ToLower()))
                                                        {
                                                            dt.Columns[k].ColumnName = dt.Rows[0].ItemArray[k].ToString().Trim().ToLower();
                                                        }
                                                        else
                                                        {
                                                            if (dt.Columns.Contains(dt.Rows[0].ItemArray[k].ToString().Trim().ToLower()) && columnHeaders.Any(ch => dt.Rows[0].ItemArray[k].ToString().Trim().ToLower() == ch.Trim().ToLower()))
                                                            {
                                                                //IDWebsiteAppService.updateSessionStatus(status, "The \"Final\" sheet contains another column with the same header \"" +
                                                                //    dt.Rows[0].ItemArray[k].ToString() + "\" and the system will insert only the data in the first column of this header",
                                                                //    uploadStatusKey);
                                                            }
                                                            dt.Columns.RemoveAt(k);
                                                            k--;
                                                        }
                                                    }
                                                }
                                                dt.Rows.RemoveAt(0);
                                            }
                                        }
                                    }
                                }
                                if (!headerExist)
                                {
                                    dt = null;
                                    string allHeaders = "";
                                    foreach (var ch in englishColumnHeaders)
                                    {
                                        allHeaders += "\"" + ch + "\"" + ", ";
                                    }
                                    if (allHeaders.Length != 0)
                                    {
                                        allHeaders = allHeaders.Remove(allHeaders.Length - 2, 2);
                                    }

                                    allHeaders += " or ";

                                    foreach (var ch in arabicColumnHeaders)
                                    {
                                        allHeaders += "\"" + ch + "\"" + ", ";
                                    }
                                    if (allHeaders.Length != 0)
                                    {
                                        allHeaders = allHeaders.Remove(allHeaders.Length - 2, 2);
                                    }
                                    //IDWebsiteAppService.updateSessionStatus(status, "The \"Final\" sheet must contain the following column headers: " + allHeaders, uploadStatusKey);
                                }
                            }
                            else
                            {
                                //IDWebsiteAppService.updateSessionStatus(status, "There is no \"Final\" sheet in the Excel file", uploadStatusKey);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //IDWebsiteAppService.updateSessionStatus(status, "Error", uploadStatusKey);
                //ErrorLogging.LogError(ex.Message, ex, false);
            }

            #endregion
            return dt;
        }


        #endregion
    }
}
