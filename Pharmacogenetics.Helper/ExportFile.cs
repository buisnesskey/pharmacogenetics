﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace Pharmacogenetics.Helper
{
    public class ExportFile
    {
        /// <summary>
        /// Export CSV File
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="dataTable"></param>
        /// /// <param name="response"></param>
        public static void ExportToFileCSV(string fileName, DataTable dataTable, HttpResponseBase response)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(String.Join(",",
                from DataColumn c in dataTable.Columns
                select c.ColumnName
            )).Append("\n");

            //Append data from datatable
            builder.Append(string.Join("\n",
                from DataRow row in dataTable.Rows
                select String.Join("\n",
                    String.Join(",", row.ItemArray)
                )
            ));

            response.ClearContent();
            response.Buffer = true;
            response.ContentType = "application/CSV";
            response.AddHeader("content-disposition", "attachment; filename="+fileName+".csv");
            response.Charset = "";
            response.Write(builder.ToString());
            response.Flush();
            response.End();
        }

        /// <summary>
        /// Export XLs File
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="dataTable"></param>
        /// /// <param name="response"></param>
        public static void ExportToFileXLS(string fileName, DataTable dataTable, HttpResponseBase response)
        {
            GridView gv = new GridView();
            gv.DataSource = dataTable;
            gv.DataBind();
            response.ClearContent();
            response.Buffer = true;
            response.AddHeader("content-disposition", "attachment; filename=" + fileName + ".xls");
            response.ContentType = "application/ms-excel";
            response.Charset = "";
            StringWriter objStringWriter = new StringWriter();
            HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
            gv.RenderControl(objHtmlTextWriter);
            response.Output.Write(objStringWriter.ToString());
            response.Flush();
            response.End();
        }


        /// <summary>
        ///  Generate Excel sheet  
        /// </summary>
        /// <param name="dataTable">Table of Data</param>
        /// <param name="excelSheetName">the name of the excel sheet</param>
        /// <returns>string</returns>
        public static bool GenerateExcelSheetWithoutDownload(DataTable dataTable, string exportingSheetPath, out string exportingFileName)
        {
            #region Validate the parameters and Generate the excel sheet
            bool returnValue = false;
            exportingFileName = "";// Guid.NewGuid().ToString() + ".xlsx";

            try
            {



                #region Write stream to the file
                MemoryStream ms = DataToExcel(dataTable);
                byte[] blob = ms.ToArray();
                if (blob != null)
                {
                    using (MemoryStream inStream = new MemoryStream(blob))
                    {
                        FileStream fs = new FileStream(exportingSheetPath, FileMode.Create);
                        inStream.WriteTo(fs);
                        fs.Close();
                    }
                }
                //FileStream fs = new FileStream(excelSheetPath, FileMode.OpenOrCreate);
                //ms.WriteTo(fs);
                //fs.Close();
                ms.Close();
                returnValue = true;
                #endregion

            }
            catch (Exception ex)
            {
                #region Log The Exception
                //Log(LogEnum.LogWriteType.File, LogEnum.LogMethodType.Method, GetType().FullName + "." + MethodBase.GetCurrentMethod().Name, ex);
                #endregion
            }
            return returnValue;
            #endregion
        }


        /// <summary>
        /// Export data table to sheet and write it to memory stream
        /// </summary>
        /// <param name="dt">DataTable data to be exported</param>
        /// <returns>Memory stream</returns>
        public static MemoryStream DataToExcel(DataTable dt)
        {
            MemoryStream ms = new MemoryStream();
            using (dt)
            {
                //IWorkbook workbook = new HSSFWorkbook();//Create an excel Workbook
                IWorkbook workbook = new XSSFWorkbook();//Create an excel Workbook
                ISheet sheet = workbook.CreateSheet();//Create a work table in the table
                IRow headerRow = sheet.CreateRow(0); //To add a row in the table
                foreach (DataColumn column in dt.Columns)
                    headerRow.CreateCell(column.Ordinal).SetCellValue(column.Caption);
                int rowIndex = 1;

                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        IRow dataRow = sheet.CreateRow(rowIndex);
                        foreach (DataColumn column in dt.Columns)
                        {
                            dataRow.CreateCell(column.Ordinal).SetCellValue(row[column].ToString());
                        }
                        rowIndex++;
                    }
                }
                workbook.Write(ms);
                ms.Flush();
                //ms.Position = 0;
            }
            return ms;
        }

    }
}
