//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Pharmacogenetics.Entities.MainModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class Allele
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Allele()
        {
            this.PatientTranslator = new HashSet<PatientTranslator>();
            this.PhenoTypeRecommendation = new HashSet<PhenoTypeRecommendation>();
            this.Translator = new HashSet<Translator>();
        }
    
        public int AlleleID { get; set; }
        public string AlleleSymbol { get; set; }
        public byte DeleteStatus { get; set; }
        public System.DateTime CreateDate { get; set; }
        public Nullable<System.DateTime> ModifyDate { get; set; }
        public Nullable<System.DateTime> DeleteDate { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PatientTranslator> PatientTranslator { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PhenoTypeRecommendation> PhenoTypeRecommendation { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Translator> Translator { get; set; }
    }
}
