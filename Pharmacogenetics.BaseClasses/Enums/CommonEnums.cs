﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pharmacogenetics.BaseClasses.Enums
{
    public class CommonEnums
    {
        public enum DeleteStatus
        {
            NotDeleted = 1,
            Deleted = 2
        }
        public enum Language
        {
            English = 2,
            Arabic = 1
        }
        public enum ComponentEnum
        {

        }
        public enum SettingEnum
        {
        }
        public enum ActionEnum
        {
            View = 1,
            Add = 2,
            Edit = 3,
            Delete = 4
        }
        public enum PrivilegeEnum
        { }

        public enum SortDirection
        {
            Ascending = 1,
            Descending = 2
        }
        public enum UserTypeEnum
        {
            Administrator = 1,
            User = 2
        }

        public enum PositionEnums
        {
            Left = 1,
            Right = 2,
            Single = 3
        }

        public enum ProjectFilesEnums
        {
            fileTranslator = 1,
            filePatient = 2,
            fileCopyNumber = 3,
        }

        public enum MessageTypeEnum
        {
            Success = 1,
            Error = 2,
            Warning = 3,
        }


        public enum CallTypeEnum
        {
            Normal = 1,
            CopyNumber = 2,
        }
    }
}
