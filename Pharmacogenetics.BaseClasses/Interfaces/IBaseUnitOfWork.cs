﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pharmacogenetics.BaseClasses.Interfaces
{
    public interface IBaseUnitOfWork : IDisposable
    {
        int Commit();
    }
}
