﻿using Pharmacogenetics.Entities.MainModel;
//using Alterna.Data.Entities.AlternaEntityService.IRepositories;
using Pharmacogenetics.EntityService.Repositories;
//using Pharmacogenetics.Data.Entities.PharmaEntitySvcInterface.IBases;
//using Pharmacogenetics.Data.Entities.PharmaEntitySvcInterface.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pharmacogenetics.EntityService.IBases;

namespace Pharmacogenetics.EntityService.Bases
{
    public class UnitOfWork //: IUnitOfWork
    {
        #region Common Properties
        private DbContext AlternaDbContext { get; set; }
        #endregion

        #region Constructors
        public UnitOfWork()
        {
            AlternaDbContext = new PharmacoGeneticsDBEntities();
            // Avoid load navigation properties
            AlternaDbContext.Configuration.LazyLoadingEnabled = false;
            AlternaDbContext.Database.CommandTimeout = 360000;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Commit changes of QuickWinDbContext
        /// </summary>
        /// <returns></returns>
        public int Commit()
        {
            //try
            //{
              int x = AlternaDbContext.SaveChanges();
              return x;
            //}
            //catch (Exception ex)
            //{
            //
            //    //AlternaDbContext.Database.CurrentTransaction.Rollback();
            //    System.IO.File.AppendAllText("D:\\Logs\\Alterna\\Services\\Windows\\Alterna.Services.MailParsing\\2016_11_20_log.txt", ex.Message);
            //    if (ex.InnerException != null)
            //        System.IO.File.AppendAllText("D:\\Logs\\Alterna\\Services\\Windows\\Alterna.Services.MailParsing\\2016_11_20_log.txt", ex.InnerException.Message);
            //    return -1;
            //}

        }
        /// <summary>
        /// Dispose the UnitOfWork and QuickWinDbContext objects
        /// </summary>
        public void Dispose()
        {
            AlternaDbContext.Dispose();
        }



        /// <summary>
        /// For StoredProcedure
        ///
        /// </summary>


        public void ExecuteStoredProcedure(string storedName , List<System.Data.SqlClient.SqlParameter> SqlParameterList)
        {

            string FullStoredName = storedName;
            string parmaName = SqlParameterList[0].ParameterName;

            AlternaDbContext.Database.ExecuteSqlCommand(storedName, SqlParameterList);

        }

        public int ExecWithStoreProcedure(string query, params object[] parameters)
        {
            return AlternaDbContext.Database.ExecuteSqlCommand("EXEC " + query ,parameters);
        }

        public int ExecSQLCommand(string query, params object[] parameters)
        {
            return AlternaDbContext.Database.ExecuteSqlCommand(query);
        }
        #endregion

        #region Repositories
        public PatientRepository Patient
        {
            get { return new PatientRepository(AlternaDbContext); }
        }
        public PatientLabRepository PatientLab
        {
            get { return new PatientLabRepository(AlternaDbContext); }
        }
        public AlleleRepository Allele
        {
            get { return new AlleleRepository(AlternaDbContext); }
        }
        public AssayRepository Assay
        {
            get { return new AssayRepository(AlternaDbContext); }
        }
        public CallRepository Call
        {
            get { return new CallRepository(AlternaDbContext); }
        }
        public GeneRepository Gene
        {
            get { return new GeneRepository(AlternaDbContext); }
        }
        public TranslatorRepository Translator
        {
            get { return new TranslatorRepository(AlternaDbContext); }
        }
        public PatientTranslatorRepository PatientTranslator
        {
            get { return new PatientTranslatorRepository(AlternaDbContext); }
        }
        public ProjectRepository Project
        {
            get { return new ProjectRepository(AlternaDbContext); }
        }

        public FileRepository File
        {
            get { return new FileRepository(AlternaDbContext); }
        }


        public RecommendationRepository Recommendation
        {
            get { return new RecommendationRepository(AlternaDbContext); }
        }


        public PhenoTypeRecommendationRepository PhenoTypeRecommendation
        {
            get { return new PhenoTypeRecommendationRepository(AlternaDbContext); }
        }


        public PhenoTypeRepository PhenoType
        {
            get { return new PhenoTypeRepository(AlternaDbContext); }
        }


        public GeneDrugMatrixRepository GeneDrugMatrix
        {
            get { return new GeneDrugMatrixRepository(AlternaDbContext); }
        }

        public GeneMatrixRepository GeneMatrix
        {
            get { return new GeneMatrixRepository(AlternaDbContext); }
        }


        public DrugRepository Drug
        {
            get { return new DrugRepository(AlternaDbContext); }
        }





        #endregion
    }
}
