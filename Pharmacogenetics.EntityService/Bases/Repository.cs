﻿using Pharmacogenetics.BaseClasses.Classes;
//using Pharmacogenetics.Data.Entities.PharmaEntitySvcInterface.IBases;
using Pharmacogenetics.EntityService.IBases;
//using Alterna.Data.Entities.AlternaEntityService.IRepositories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
//using System.Web.UI.WebControls;

namespace Pharmacogenetics.EntityService.Bases
{
    public class Repository<T> where T : class
    {
        #region Properties
        protected DbContext DbContext { get; set; }

        protected DbSet<T> DbSet { get; set; }


        #endregion

        #region Constructor
        public Repository(DbContext dbContext)
        {
            if (dbContext == null)
                throw new ArgumentNullException("dbContext");
            DbContext = dbContext;
            DbSet = DbContext.Set<T>();
        }
        #endregion

        #region Get All Data Methods
        public virtual IQueryable<T> GetAll()
        {
            return DbSet;
        }


        public virtual IQueryable<T> GetAll(string includeTable)
        {
            return DbSet.Include(includeTable);
        }




        /// <summary>
        /// Get All data sorted by lambda expression
        /// </summary>
        /// <typeparam name="TKey">Type of sorting key or property</typeparam>
        /// <param name="sortingExpression">lambda expression for sorting ex: T => T.Key</param>
        /// <returns>IOrderedQueryable<typeparamref name="T"/></returns>
        public IQueryable<T> GetAllSorted<TKey>(Expression<Func<T, TKey>> sortingExpression)
        {
            return DbSet.OrderBy<T, TKey>(sortingExpression);
        }

        public IQueryable<T> GetWhere(System.Linq.Expressions.Expression<Func<T, bool>> filter = null, string includeProperties = "")
        {
            IQueryable<T> query = DbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }
            query = includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                .Aggregate(query, (current, includeProperty) => current.Include(includeProperty));

            return query;
        }

        public IQueryable<T> GetWhere(IQueryable<T> query, string includeProperties = "")
        {

            query = includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                .Aggregate(query, (current, includeProperty) => current.Include(includeProperty));

            return query;
        }






        public bool GetAny(System.Linq.Expressions.Expression<Func<T, bool>> filter = null)
        {
            IQueryable<T> query = DbSet;
            bool result = false;
            if (filter != null)
            {
                result = query.Any(filter);
            }
            return result;
        }

        public T CustomFirstOrDefault(System.Linq.Expressions.Expression<Func<T, bool>> filter = null)
        {
            if (filter != null)
            {
                return DbSet.FirstOrDefault(filter);
            }
            return null;
        }

        public bool IsExist(System.Linq.Expressions.Expression<Func<T, bool>> filter = null)
        {
            IQueryable<T> query = DbSet;
            bool isExist = false;
            if (filter != null)
            {
                isExist = query.Any(filter);
            }
            return isExist;
        }
        public IQueryable<T> GetWhere<TKey>(Expression<Func<T, TKey>> sortingExpression, Pharmacogenetics.BaseClasses.Enums.CommonEnums.SortDirection sortDir, System.Linq.Expressions.Expression<Func<T, bool>> filter = null, string includeProperties = "")
        {
            IQueryable<T> query = DbSet;
            #region Apply filter conditions on the DBSet
            if (filter != null)
            {
                query = query.Where(filter);
            }
            #endregion
            #region Apply sorting on the DBSet
            switch (sortDir)
            {
                case Pharmacogenetics.BaseClasses.Enums.CommonEnums.SortDirection.Ascending:
                    query = query.OrderBy<T, TKey>(sortingExpression);
                    break;
                case Pharmacogenetics.BaseClasses.Enums.CommonEnums.SortDirection.Descending:
                    query = query.OrderByDescending<T, TKey>(sortingExpression);
                    break;
                default:
                    break;
            }
            #endregion
            query = includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                .Aggregate(query, (current, includeProperty) => current.Include(includeProperty));

            return query;
        }

        #endregion

        #region Get one record
        public virtual T GetById(int id)
        {
            //return DbSet.FirstOrDefault(PredicateBuilder.GetByIdPredicate<T>(id));
            return DbSet.Find(id);
        }

        public virtual T GetById(long id)
        {
            //return DbSet.FirstOrDefault(PredicateBuilder.GetByIdPredicate<T>(id));
            return DbSet.Find(id);
        }

        #endregion

        #region CRUD Methods
        public virtual bool Insert(T entity)
        {
            bool returnVal = false;
            DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
            if (dbEntityEntry.State != EntityState.Detached)
            {
                dbEntityEntry.State = EntityState.Added;
            }
            else
            {
                DbSet.Add(entity);
            }
            returnVal = true;
            return returnVal;
        }

        public virtual void InsertList(List<T> entityList)
        {
            DbSet.AddRange(entityList);
        }

        public virtual void Update(T entity)
        {
            DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
            if (dbEntityEntry.State == EntityState.Detached)
            {
                DbSet.Attach(entity);
            }
            dbEntityEntry.State = EntityState.Modified;
        }

        public virtual void UpdateList(List<T> entityList)
        {
            foreach (T item in entityList)
            {
                Update(item);
            }
        }

        public virtual void Delete(T entity)
        {
            DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
            if (dbEntityEntry.State != EntityState.Deleted)
            {
                dbEntityEntry.State = EntityState.Deleted;
            }
            else
            {
                DbSet.Attach(entity);
                DbSet.Remove(entity);
            }
        }
        public virtual void DeleteList(List<T> entityList)
        {
            foreach (T item in entityList)
            {
                Delete(item);
            }
        }

        public virtual void Delete(int id)
        {
            var entity = GetById(id);
            if (entity == null) return; // not found; assume already deleted.
            Delete(entity);
        }

        #endregion

        #region Get Data in Range - Take - Skip

        /// <summary>
        /// Get range of data sorted by lambda expression
        /// </summary>
        /// <typeparam name="TKey">Type of sorting key or property</typeparam>
        /// <param name="skip">number of records to be skipped </param>
        /// <param name="take">number of records to be taken</param>
        /// <param name="sortingExpression">lambda expression for sorting ex: T => T.Key</param>
        /// <param name="sortDir">Sorting direction </param>
        /// <returns>IQueryable<T></returns>
        public virtual IQueryable<T> GetPage<TKey>(int skipCount, int takeCount, Expression<Func<T, TKey>> sortingExpression, Pharmacogenetics.BaseClasses.Enums.CommonEnums.SortDirection sortDir)
        {
            IQueryable<T> queryResult = null;
            switch (sortDir)
            {
                case Pharmacogenetics.BaseClasses.Enums.CommonEnums.SortDirection.Ascending:
                    if (skipCount == 0)
                        queryResult = DbSet.OrderBy<T, TKey>(sortingExpression).Take(takeCount);
                    else
                        queryResult = DbSet.OrderBy<T, TKey>(sortingExpression).Skip(skipCount).Take(takeCount);
                    break;
                case Pharmacogenetics.BaseClasses.Enums.CommonEnums.SortDirection.Descending:
                    if (skipCount == 0)
                        queryResult = DbSet.OrderByDescending<T, TKey>(sortingExpression).Take(takeCount);
                    else
                        queryResult = DbSet.OrderByDescending<T, TKey>(sortingExpression).Skip(skipCount).Take(takeCount);
                    break;
                default:
                    break;
            }
            return queryResult;

        }
        public virtual IQueryable<T> GetPage<TKey>(int skipCount, int takeCount)
        {
            IQueryable<T> queryResult = null;
            if (skipCount == 0)
                queryResult = DbSet.Take(takeCount);
            else
                queryResult = DbSet.Skip(skipCount).Take(takeCount);
            return queryResult;

        }

        /// <summary>
        /// Get range of filtered data sorted by lambda expression
        /// </summary>
        /// <typeparam name="TKey">Type of sorting key or property</typeparam>
        /// <param name="skip">number of records to be skipped </param>
        /// <param name="take">number of records to be taken</param>
        /// <param name="sortingExpression">lambda expression for sorting ex: T => T.Key</param>
        /// <param name="filter">lambda expression for filtering ex: T => T.Key == value</param>
        /// <param name="sortDir">Sorting direction </param>
        /// <param name="includeString">navigation properties separated by comma to be included</param>
        /// <returns>IQueryable<T></returns>
        public virtual IQueryable<T> GetPageWhere<TKey>(int skipCount, int takeCount, Expression<Func<T, TKey>> sortingExpression, System.Linq.Expressions.Expression<Func<T, bool>> filter, Pharmacogenetics.BaseClasses.Enums.CommonEnums.SortDirection sortDir, string includeString)
        {
            IQueryable<T> queryResult = null;

            switch (sortDir)
            {
                case Pharmacogenetics.BaseClasses.Enums.CommonEnums.SortDirection.Ascending:
                    if (skipCount == 0)
                        queryResult = GetWhere(filter, includeString).OrderBy<T, TKey>(sortingExpression).Take(takeCount);
                    else
                        queryResult = GetWhere(filter, includeString).OrderBy<T, TKey>(sortingExpression).Skip(skipCount).Take(takeCount);
                    break;
                case Pharmacogenetics.BaseClasses.Enums.CommonEnums.SortDirection.Descending:
                    if (skipCount == 0)
                        queryResult = GetWhere(filter, includeString).OrderByDescending<T, TKey>(sortingExpression).Take(takeCount);
                    else
                        queryResult = GetWhere(filter, includeString).OrderByDescending<T, TKey>(sortingExpression).Skip(skipCount).Take(takeCount);
                    break;
                default:
                    break;
            }
            return queryResult;
        }
        /// <summary>
        /// Get range of filtered data sorted by lambda expression
        /// </summary>
        /// <typeparam name="TKey">Type of sorting key or property</typeparam>
        /// <param name="skip">number of records to be skipped </param>
        /// <param name="take">number of records to be taken</param>
        /// <param name="sortingExpression">lambda expression for sorting ex: T => T.Key</param>
        /// <param name="filter">lambda expression for filtering ex: T => T.Key == value</param>
        /// <param name="sortDir">Sorting direction </param>
        /// <param name="includeString">navigation properties separated by comma to be included</param>
        /// <returns>IQueryable<T></returns>
        public virtual IQueryable<T> GetPageWhere<TKey>(int skipCount, int takeCount, Expression<Func<T, TKey>> sortingExpression, IQueryable<T> query, Pharmacogenetics.BaseClasses.Enums.CommonEnums.SortDirection sortDir, string includeString)
        {
            IQueryable<T> queryResult = null;

            switch (sortDir)
            {
                case Pharmacogenetics.BaseClasses.Enums.CommonEnums.SortDirection.Ascending:
                    if (skipCount == 0)
                        queryResult = GetWhere(query, includeString).OrderBy<T, TKey>(sortingExpression).Take(takeCount);
                    else
                        queryResult = GetWhere(query, includeString).OrderBy<T, TKey>(sortingExpression).Skip(skipCount).Take(takeCount);
                    break;
                case Pharmacogenetics.BaseClasses.Enums.CommonEnums.SortDirection.Descending:
                    if (skipCount == 0)
                        queryResult = GetWhere(query, includeString).OrderByDescending<T, TKey>(sortingExpression).Take(takeCount);
                    else
                        queryResult = GetWhere(query, includeString).OrderByDescending<T, TKey>(sortingExpression).Skip(skipCount).Take(takeCount);
                    break;
                default:
                    break;
            }
            return queryResult;
        }

        /// <summary>
        /// Get All Data But By Sorting Direction.
        /// </summary>
        /// <typeparam name="TKey">Type Of Sorting</typeparam>
        /// <param name="sortingExpression"> Lambad Expression For Sorting</param>
        /// <param name="filter"> lambda Expreesioon For Filter </param>
        /// <param name="sortDir"> Sorting Asc or Des</param>
        /// <returns></returns>
        public virtual IQueryable<T> GetAllOrderBy<TKey>(Expression<Func<T, TKey>> sortingExpression, Expression<Func<T, bool>> filter, Pharmacogenetics.BaseClasses.Enums.CommonEnums.SortDirection sortDir)
        {
            IQueryable<T> queryResult = null;

            switch (sortDir)
            {
                case Pharmacogenetics.BaseClasses.Enums.CommonEnums.SortDirection.Ascending:
                    queryResult = GetWhere(filter).OrderBy<T, TKey>(sortingExpression);
                    break;
                case Pharmacogenetics.BaseClasses.Enums.CommonEnums.SortDirection.Descending:
                    queryResult = GetWhere(filter).OrderByDescending<T, TKey>(sortingExpression);
                    break;
                default:
                    break;
            }
            return queryResult;
        }

        /// <summary>
        /// Get All Data But By Sorting Direction.
        /// </summary>
        /// <typeparam name="TKey">Type Of Sorting</typeparam>
        /// <param name="sortingExpression"> Lambad Expression For Sorting</param>
        /// <param name="filter"> lambda Expreesioon For Filter </param>
        /// <param name="sortDir"> Sorting Asc or Des</param>
        /// <returns></returns>
        public virtual IQueryable<T> GetAllOrderBy<TKey>(Expression<Func<T, TKey>> sortingExpression, Expression<Func<T, bool>> filter, Pharmacogenetics.BaseClasses.Enums.CommonEnums.SortDirection sortDir, string includeString)
        {
            IQueryable<T> queryResult = null;

            switch (sortDir)
            {
                case Pharmacogenetics.BaseClasses.Enums.CommonEnums.SortDirection.Ascending:
                    queryResult = GetWhere(filter, includeString).OrderBy<T, TKey>(sortingExpression);
                    break;
                case Pharmacogenetics.BaseClasses.Enums.CommonEnums.SortDirection.Descending:
                    queryResult = GetWhere(filter, includeString).OrderByDescending<T, TKey>(sortingExpression);
                    break;
                default:
                    break;
            }
            return queryResult;
        }




        #endregion

        #region Get Count Methods
        /// <summary>
        /// Get the count of rows
        /// </summary>
        /// <returns></returns>
        public virtual int GetCount()
        {
            return DbSet.Count();
        }

        /// <summary>
        /// Get the count of filtered rows
        /// </summary>
        /// <returns></returns>
        public virtual int GetCount(Expression<Func<T, bool>> filter)
        {
            return GetWhere(filter).Count();
        }


        public virtual int GetCount(IQueryable<T> query)
        {
            return GetWhere(query).Count();
        }
        #endregion

        #region Log
        #region Config
        private string LogApiName
        {
            get
            {
                string apiName = "";
                if (!string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["LogApi"]))
                {
                    apiName = ConfigurationManager.AppSettings["LogApi"];
                }

                return apiName;
            }
        }
        private string LogApiHost
        {
            get
            {
                string apiHost = "";
                if (!string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["LogApiHost"]))
                {
                    apiHost = ConfigurationManager.AppSettings["LogApiHost"];
                }

                return apiHost;
            }
        }
        #endregion
        #endregion


        #region new methods implementation
        public virtual IQueryable<T> GetAll(params string[] includeTable)
        {
            IQueryable<T> query = DbSet;
            foreach (var inc in includeTable)
                query = query.Include(inc);
            return query;
        }
        public IQueryable<T> GetWhere(IQueryable<T> query, params string[] includeProperties)
        {
        
            foreach (var inc in includeProperties)
                query = query.Include(inc);
            //query = includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
            //    .Aggregate(query, (current, includeProperty) => current.Include(includeProperty));
        
            return query;
        }
        public IQueryable<T> GetWhere(System.Linq.Expressions.Expression<Func<T, bool>> filter = null, params string[] includeProperties)
        {
            IQueryable<T> query = DbSet;
        
            if (filter != null)
            {
                query = query.Where(filter);
            }
            foreach (var inc in includeProperties)
                query = query.Include(inc);
            //query = includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
            //    .Aggregate(query, (current, includeProperty) => current.Include(includeProperty));
        
            return query;
        }
        public virtual IQueryable<T> GetPageWhere<TKey>(int skipCount, int takeCount, Expression<Func<T, TKey>> sortingExpression, System.Linq.Expressions.Expression<Func<T, bool>> filter, Pharmacogenetics.BaseClasses.Enums.CommonEnums.SortDirection sortDir, params string[] includeString)
        {
            IQueryable<T> queryResult = null;
        
            switch (sortDir)
            {
                case Pharmacogenetics.BaseClasses.Enums.CommonEnums.SortDirection.Ascending:
                    if (skipCount == 0)
                        queryResult = GetWhere(filter, includeString).OrderBy<T, TKey>(sortingExpression).Take(takeCount);
                    else
                        queryResult = GetWhere(filter, includeString).OrderBy<T, TKey>(sortingExpression).Skip(skipCount).Take(takeCount);
                    break;
                case Pharmacogenetics.BaseClasses.Enums.CommonEnums.SortDirection.Descending:
                    if (skipCount == 0)
                        queryResult = GetWhere(filter, includeString).OrderByDescending<T, TKey>(sortingExpression).Take(takeCount);
                    else
                        queryResult = GetWhere(filter, includeString).OrderByDescending<T, TKey>(sortingExpression).Skip(skipCount).Take(takeCount);
                    break;
                default:
                    break;
            }
            return queryResult;
        }
        public virtual IQueryable<T> GetPageWhere<TKey>(int skipCount, int takeCount, Expression<Func<T, TKey>> sortingExpression, IQueryable<T> query, Pharmacogenetics.BaseClasses.Enums.CommonEnums.SortDirection sortDir, params string[] includeString)
        {
            IQueryable<T> queryResult = null;
        
            switch (sortDir)
            {
                case Pharmacogenetics.BaseClasses.Enums.CommonEnums.SortDirection.Ascending:
                    if (skipCount == 0)
                        queryResult = GetWhere(query, includeString).OrderBy<T, TKey>(sortingExpression).Take(takeCount);
                    else
                        queryResult = GetWhere(query, includeString).OrderBy<T, TKey>(sortingExpression).Skip(skipCount).Take(takeCount);
                    break;
                case Pharmacogenetics.BaseClasses.Enums.CommonEnums.SortDirection.Descending:
                    if (skipCount == 0)
                        queryResult = GetWhere(query, includeString).OrderByDescending<T, TKey>(sortingExpression).Take(takeCount);
                    else
                        queryResult = GetWhere(query, includeString).OrderByDescending<T, TKey>(sortingExpression).Skip(skipCount).Take(takeCount);
                    break;
                default:
                    break;
            }
            return queryResult;
        }
        public virtual IQueryable<T> GetAllOrderBy<TKey>(Expression<Func<T, TKey>> sortingExpression, Expression<Func<T, bool>> filter, Pharmacogenetics.BaseClasses.Enums.CommonEnums.SortDirection sortDir, params string[] includeString)
        {
            IQueryable<T> queryResult = null;
        
            switch (sortDir)
            {
                case Pharmacogenetics.BaseClasses.Enums.CommonEnums.SortDirection.Ascending:
                    queryResult = GetWhere(filter, includeString).OrderBy<T, TKey>(sortingExpression);
                    break;
                case Pharmacogenetics.BaseClasses.Enums.CommonEnums.SortDirection.Descending:
                    queryResult = GetWhere(filter, includeString).OrderByDescending<T, TKey>(sortingExpression);
                    break;
                default:
                    break;
            }
            return queryResult;
        }
        #endregion
    }
}
