﻿using Pharmacogenetics.BaseClasses.Interfaces;
using Pharmacogenetics.Entities.MainModel;
using Pharmacogenetics.EntityService.Repositories;
//using Alterna.Data.Entities.AlternaEntityServiceInterface.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pharmacogenetics.EntityService.IBases
{
    public interface IUnitOfWork : IBaseUnitOfWork
    {
        #region Repositories
        PatientRepository Patient { get; }
        PatientLabRepository PatientLab { get; }
        AlleleRepository Allele { get; }
        AssayRepository Assay { get; }
        CallRepository Call { get; }
        GeneRepository Gene { get; }
        TranslatorRepository Translator { get; }
        PatientTranslatorRepository PatientTranslator { get; }
        ProjectRepository Project { get; }
        FileRepository File { get; }
        #endregion
    }
}
