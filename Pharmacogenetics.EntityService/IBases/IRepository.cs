﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Pharmacogenetics.EntityService.IBases
{
    public interface IRepository<T> where T : class
    {
        IQueryable<T> GetAll();

        IQueryable<T> GetAllSorted<TKey>(Expression<Func<T, TKey>> sortingExpression);

        IQueryable<T> GetWhere(Expression<Func<T, bool>> filter = null, string includeProperties = "");
        IQueryable<T> GetWhere(IQueryable<T> query, string includeProperties = "");
        T GetById(int entityId);

        bool Insert(T entity);
        void InsertList(List<T> entity);
        void Update(T entity);
        void UpdateList(List<T> entity);

        void Delete(T entity);

        void Delete(int entityId);


        IQueryable<T> GetPage<TKey>(int skipCount, int takeCount, Expression<Func<T, TKey>> sortingExpression, Pharmacogenetics.BaseClasses.Enums.CommonEnums.SortDirection sortDir);
        IQueryable<T> GetPage<TKey>(int skipCount, int takeCount);

        IQueryable<T> GetPageWhere<TKey>(int skipCount, int takeCount, Expression<Func<T, TKey>> sortingExpression, Expression<Func<T, bool>> filter, Pharmacogenetics.BaseClasses.Enums.CommonEnums.SortDirection sortDir, string includeString);
        IQueryable<T> GetPageWhere<TKey>(int skipCount, int takeCount, Expression<Func<T, TKey>> sortingExpression, IQueryable<T> query, Pharmacogenetics.BaseClasses.Enums.CommonEnums.SortDirection sortDir, string includeString);
        IQueryable<T> GetAllOrderBy<TKey>(Expression<Func<T, TKey>> sortingExpression, Expression<Func<T, bool>> filter, Pharmacogenetics.BaseClasses.Enums.CommonEnums.SortDirection sortDir, string includeString);
        IQueryable<T> GetAllOrderBy<TKey>(Expression<Func<T, TKey>> sortingExpression, Expression<Func<T, bool>> filter, Pharmacogenetics.BaseClasses.Enums.CommonEnums.SortDirection sortDir);

        #region new methods
        //IQueryable<T> GetAll(params string[] includeTable);
        //IQueryable<T> GetWhere(Expression<Func<T, bool>> filter = null, params string[] includeProperties = "");
        //IQueryable<T> GetWhere(IQueryable<T> query, params string[] includeProperties = "");
        //IQueryable<T> GetPageWhere<TKey>(int skipCount, int takeCount, Expression<Func<T, TKey>> sortingExpression, Expression<Func<T, bool>> filter, Pharmacogenetics.BaseClasses.Enums.CommonEnums.SortDirection sortDir, params string[] includeString);
        //IQueryable<T> GetPageWhere<TKey>(int skipCount, int takeCount, Expression<Func<T, TKey>> sortingExpression, IQueryable<T> query, Pharmacogenetics.BaseClasses.Enums.CommonEnums.SortDirection sortDir, params string[] includeString);
        //IQueryable<T> GetAllOrderBy<TKey>(Expression<Func<T, TKey>> sortingExpression, Expression<Func<T, bool>> filter, Pharmacogenetics.BaseClasses.Enums.CommonEnums.SortDirection sortDir, params string[] includeString); 
        #endregion
    }
}
