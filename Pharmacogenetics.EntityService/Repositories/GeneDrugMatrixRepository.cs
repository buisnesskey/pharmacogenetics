﻿using Pharmacogenetics.Entities.MainModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pharmacogenetics.EntityService.Bases;
using System.Linq.Expressions;
//using Pharmacogenetics.Data.Entities.PharmaEntitySvcInterface.Interfaces;

namespace Pharmacogenetics.EntityService.Repositories
{
    
    public class GeneDrugMatrixRepository : Repository<GeneDrugMatrix>//, IGeneRepository
    {
        public GeneDrugMatrixRepository(DbContext dbContext) : base(dbContext) { }
    }
}
