﻿using Pharmacogenetics.Entities.MainModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pharmacogenetics.EntityService.Bases;
using System.Linq.Expressions;
//using Pharmacogenetics.Data.Entities.PharmaEntitySvcInterface.Interfaces;

namespace Pharmacogenetics.EntityService.Repositories
{
    public class PatientRepository : Repository<Patient>//, IPatientRepository
    {
        public PatientRepository(DbContext dbContext): base(dbContext){}

        public List<Patient> GetAllPatients(string fullName, string phoneNumber, byte UserType, int jtStartIndex, int jtPageSize, string jtSorting, out int totalCount)
        {
            List<Patient> usersViewModelList = new List<Patient>();
            totalCount = new int();
            try
            {
                #region Filter
                var q = GetAll();
                //if (!string.IsNullOrWhiteSpace(fullName))
                //    q = q.Where(x => x.FullName.Contains(fullName));
                //if (!string.IsNullOrWhiteSpace(phoneNumber))
                //    q = q.Where(x => x.PrimaryPhoneNumber.Contains(phoneNumber));
                //if (UserType != new byte())
                //    q = q.Where(x => x.UserTypeEnumId == UserType);
                //#endregion
                //#region sorting
                //string[] sortingExpression = jtSorting.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                //Alterna.Bases.BaseClasses.Enums.CommonEnums.SortDirection sortingDir = sortingExpression[1] == "DESC" ? Alterna.Bases.BaseClasses.Enums.CommonEnums.SortDirection.Descending : Alterna.Bases.BaseClasses.Enums.CommonEnums.SortDirection.Ascending;
                //
                //switch (sortingExpression[0])
                //{
                //    case "FullName":
                //        usersViewModelList = GetPageWhere(jtStartIndex, jtPageSize, x => x.FullName, q, sortingDir, "").ToList();
                //        break;
                //    default:
                //        usersViewModelList = GetPageWhere(jtStartIndex, jtPageSize, x => x.UserId, q, sortingDir, "").ToList();
                //        break;
                //}
                #endregion

                totalCount = GetCount(q);
            }
            catch (Exception ex)
            {
 
            }
            return usersViewModelList;
        }


    }
}
