﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Pharmacogenetics.Website.Models.ViewModels
{
    public class PatientTrainDataViewModel
    {
        //[Required]
        [DataType(DataType.Upload)]
        public HttpPostedFileBase FileUpload { get; set; }
    }
}
