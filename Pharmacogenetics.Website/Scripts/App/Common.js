﻿
function ShowDeleteConfirmation(header, msgBody, elementId, okBtnCallBack) {
    $("#popUpUserHeader").text(header);
    $("#msgBody").text(msgBody);
    $("#deleteConfirmation").modal("show");

    // cleare previous event handler
    $("#btnOkDelete").unbind('click');
    $("#btnOkDelete").bind('click', function (id) { okBtnCallBack(elementId); });
}


function ShowOperationResultMessage(message, messageType) {
    debugger;
    $('#divClientMessagePharma').attr('class', ''); //without paramters to remove all css classes
    switch (messageType) {
        case MessageTypeEnum.Success:
            $('#divClientMessagePharma').addClass('container first_wrapper bg-success text-success');
            break;
        case MessageTypeEnum.Error:
            $('#divClientMessagePharma').addClass('container first_wrapper bg-danger text-danger');
            break;
        case MessageTypeEnum.Warning:
            $('#divClientMessagePharma').addClass('container first_wrapper bg-warning text-warning');
            break;
        default:
            //default code block
    }
    $('#messageClientText').text(message);
    //$('#divMessagePharma').show();
    $('#divClientMessagePharma').fadeIn().delay(5000).fadeOut();
}

// enum to define the message types
var MessageTypeEnum =
    {
        Success: 1,
        Error: 2,
        Warning: 3
    }


// enum to define the message types
var MessageTypeStringQouteEnum =
    {
        Success: "OK",
        Error: "ERROR",
        Warning: "WARNING"
    }
// close the success, error, or warning messsage
function CloseMessage(divMessagePharma) {
    $('#' + divMessagePharma).stop().fadeOut("slow");
}

// Scroll to top pixelNumber which are passed in parater
function Scroll(pixelNumber) {
    $("body").animate({
        scrollTop: pixelNumber // space from the first pixel of the page
    }, 300); // 300 is the speed of animation
}


function clearMessage() {
    $('#alertContent').remove();
}

function CloseDeleteConfirmation() {
    $("#deleteConfirmation").modal("hide");
}

function ShowMessageOnPopup(message, messageType, msgElementId, txtElementId) {
    $('#' + msgElementId).attr('class', ''); //without paramters to remove all css classes
    switch (messageType) {
        case MessageTypeEnum.Success:
            $('#' + msgElementId).addClass('first_wrapper bg-success text-success');
            break;
        case MessageTypeEnum.Error:
            $('#' + msgElementId).addClass('first_wrapper bg-danger text-danger');
            break;
        case MessageTypeEnum.Warning:
            $('#' + msgElementId).addClass('first_wrapper bg-warning text-warning');
            break;
        default:
            //default code block
    }
    $('#' + msgElementId).attr('style', 'margin: 6px; padding: 6px;');
    $('#' + txtElementId).attr('style', 'margin: 10px 0 10px;font-size: 18px; font-weight: bold;');
    $('#' + txtElementId).text(message);
    //$('#' + msgElementId).show();
    $('#' + msgElementId).fadeIn().delay(5000).fadeOut();
}

function DrawList(data, controlIdToAppend) {
    var result = "";
    for (var i = 0; i < data.DataSource.length; i++) {
        result += "<option value='" + data.DataSource[i].Value + "'>" + data.DataSource[i].Text + "</option>";
    }

    if (result != "")
        $("#" + controlIdToAppend).html(result);
    else
        $("#" + controlIdToAppend).html('');
}
