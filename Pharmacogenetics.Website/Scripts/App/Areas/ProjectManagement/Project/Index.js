﻿var flagStatusShow = false;
var idleInterval = null;
var dataFile = null;
var dataFiles = [];

var fileControls = [];
var fileTranslator = {
    id: "fileTranslator",
    text: "Drag and drop Translator file here",
    textAfterDrag: "Translator file",
};
fileControls.push(fileTranslator);
var filePatient = {
    id: "filePatient",
    text: "Drag and drop Patients file here",
    textAfterDrag: "Patients file"
};
fileControls.push(filePatient);
var fileCopyNumber = {
    id: "fileCopyNumber",
    text: "Drag and drop Copy Number file here",
    textAfterDrag: "Copy Number file"
};
fileControls.push(fileCopyNumber);


$(document).ready(function () {
    fileControls.forEach(function (fileControl) {
        $('input#' + fileControl.id).fileuploader({
            changeInput: '<div class="fileuploader-input">' +
                              '<div class="fileuploader-input-inner">' +
                                  '<img src="' + imageUploadPath + '">' +
                                  '<h3 class="fileuploader-input-caption"><span>' + fileControl.text + '</span></h3>' +
                                  '<p>or</p>' +
                                  '<div class="fileuploader-input-button"><span>Browse Files</span></div>' +
                              '</div>' +
                          '</div>',
            theme: 'dragdrop',
            extensions: ['txt', 'csv', 'xls', 'xlsx'],
            upload: {
                //url: 'php/ajax_upload_file.php',
                data: null,
                type: 'POST',
                enctype: 'multipart/form-data',
                start: true,
                synchron: true,
                beforeSend: null,
                onSuccess: function (result, item) {
                    //debugger;
                    //var data = JSON.parse(result);
                    //
                    //// if success
                    //if (data.isSuccess && data.files[0]) {
                    //    item.name = data.files[0].name;
                    //}
                    //
                    //// if warnings
                    //if (data.hasWarnings) {
                    //    for (var warning in data.warnings) {
                    //        alert(data.warnings);
                    //    }
                    //
                    //    item.html.removeClass('upload-successful').addClass('upload-failed');
                    //    // go out from success function by calling onError function
                    //    // in this case we have a animation there
                    //    // you can also response in PHP with 404
                    //    return this.onError ? this.onError(item) : null;
                    //}

                    item.html.find('.column-actions').append('<a class="fileuploader-action fileuploader-action-remove fileuploader-action-success" title="Remove"><i></i></a>');
                    setTimeout(function () {
                        item.html.find('.progress-bar2').fadeOut(400);
                    }, 400);
                },
                onError: function (item) {
                    var progressBar = item.html.find('.progress-bar2');

                    if (progressBar.length > 0) {
                        progressBar.find('span').html(0 + "%");
                        progressBar.find('.fileuploader-progressbar .bar').width(0 + "%");
                        item.html.find('.progress-bar2').fadeOut(400);
                    }

                    item.upload.status != 'cancelled' && item.html.find('.fileuploader-action-retry').length == 0 ? item.html.find('.column-actions').prepend(
                        '<a class="fileuploader-action fileuploader-action-retry" title="Retry"><i></i></a>'
                    ) : null;
                },
                onProgress: function (data, item) {
                    //debugger;
                    dataFile = item.file;
                    dataFiles.push({ fileType: fileControl.id, fileObject: item.file });

                    var progressBar = item.html.find('.progress-bar2');

                    if (progressBar.length > 0) {
                        progressBar.show();
                        progressBar.find('span').html(data.percentage + "%");
                        progressBar.find('.fileuploader-progressbar .bar').width(data.percentage + "%");
                    }
                    //$(".div" + fileControl.id + " .fileuploader-items-list li").eq(1).remove();
                },
                onComplete: null,
            },
            onRemove: function (item) {
               // debugger;
                var indexesFiles = $.map(dataFiles, function(obj, index) {
                    if (obj.fileType == fileControl.id && obj.fileObject.name == item.name) {
                        return index;
                    }
                });
                indexesFiles.forEach(function (indexFile) {
                    dataFiles.splice(indexFile, 1);
                });
                $(".div" + fileControl.id + " .fileuploader-items-list li").remove();
                //index = dataFiles.findIndex(x => x.prop2=="yutu");
                //var result = $.grep(dataFiles, function (e) { return e.fileType == fileControl.id; });
                //if (result.length == 1) {
                //    var tt = result[0];
                //    // access the foo property using result[0].foo
                //} else {
                //    // multiple items found
                //}
                //
                //var indexFile = dataFiles.indexOf(fileControl.id);
                //if (indexFile > -1) {
                //    dataFiles.splice(indexFile, 1);
                //}
                //$(".div" + fileControl.id + " .fileuploader-items-list li").remove();
                //$.post('./php/ajax_remove_file.php', {
                //    file: item.name
                //});
            },
            captions: {
                feedback: fileControl.textAfterDrag,
                feedback2: fileControl.textAfterDrag,
                drop: fileControl.textAfterDrag
            },
        });
    });
});

$("#frmUploadProjectFiles").submit(function (e) {
    e.preventDefault();
    debugger;
    
    //var fileInput = document.getElementById('FileUpload');
    //var files = fileInput.files;
    //var myID = 3; //uncomment this to make sure the ajax URL works
    //var form = $(this);
    var data = new FormData($(this)[0]);
    dataFiles.forEach(function (fileControl) {
        data.append(fileControl.fileType, fileControl.fileObject);
    });
    $.validator.unobtrusive.parse(data);
    //var rr2 = $.validator.unobtrusive.parse($(this));
    //var rr3 = $.validator.unobtrusive.parse($(this)[0]);
    //var rr4 = $.validator.unobtrusive.parse('#frmUploadProjectFiles');
    var form = $(this);
    if (form.valid()) {
        $('#btnCreateProject').prop('disabled', true);
        //startLoading('ajaxBusy1');
        $.ajax({
            type: "Post",
            upload_max_filesize : "25M" ,             
            url: actionUpload,
            contentType: false,
            processData: false,
            data: data,
            async: true,
            beforeSend: function (XMLHttpRequest)
            {
                flagStatusShow = true;
                var idleInterval = setInterval(timerIncrement, 4000); // 1 minute
            },
            success: function (result) {
                if (result.Status == MessageTypeEnum.Success) { 
                    flagStatusShow = true;
                    idleInterval = setInterval(function () { timerIncrement(result.uploadStatusKey); }, 1000); // 1 minute
                    parsingMessageDivInterval = setInterval(function () { timerParsingMessageDivScrollIncrement(); }, 1000); // 1 minute
                    //ShowOperationResultMessage(result.Message, result.Status);
                    ShowOperationResultMessage(result.Message, result.Status);
                    //$('#btnCreateProject').prop('disabled', true);
                } else {
                    //ShowOperationResultMessage(result.Message, result.Status);
                }
            },
            error: function (xhr, status, p3, p4)
            {
                debugger;
                flagStatusShow = false;
                var err = "Error " + " " + status + " " + p3 + " " + p4;
                if (xhr.responseText && xhr.responseText[0] == "{")
                    err = JSON.parse(xhr.responseText).Message;

                console.log(err);
                if (xhr.status == 500)
                    alert("please make sure that file size must be less that 25 M")
                else
                    alert(err)

            }
        });
    }
});


function timerIncrement(uploadStatusKey) {
    //debugger;
    $.ajax({
        type: "GET",
        url: actionUploadStatus + "/" + uploadStatusKey,
        contentType: false,
        processData: false,
        data: uploadStatusKey,
        //async: true,
        success: function (result) {
            if (flagStatusShow) {
                if (result.breifStatus.length > 0)
                    stopLoading('ajaxBusy1');
                $("#messageParsing").html(result.breifStatus.join("<br />"));
                //$("#messageParsing").text(result.join("<br />"));
                console.log(result);
                if (result.breifStatus[result.breifStatus.length - 1] == "Error" || result.breifStatus[result.breifStatus.length - 1] == "Complete") {
                    clearInterval(idleInterval);
                    clearInterval(parsingMessageDivInterval);
                    flagStatusShow = false;
                    //$("#detialsDivMessage").show();
                    //$("#messageInnerParsing").html(result.logStatus.join("<br />"));
                    //$('#btnCreateProject').prop('disabled', false);
                }
            }
        },
        error: function (xhr, status, p3, p4) {
            var err = "Error " + " " + status + " " + p3 + " " + p4;
            if (xhr.responseText && xhr.responseText[0] == "{")
                err = JSON.parse(xhr.responseText).Message;
            console.log(err);
        }
    });
}


function timerParsingMessageDivScrollIncrement()
{
    var elem = document.getElementById('messageParsing');
    elem.scrollTop = elem.scrollHeight;
    console.log("scroll top: " + elem.scrollTop);
    console.log("scroll down: " + elem.scrollHeight);
}