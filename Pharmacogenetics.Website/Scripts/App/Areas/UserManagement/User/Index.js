﻿$(document).ready(function () {
    $('#divJtableUsers').jtable({
        title: '',
        paging: true,
        pageSize: 10,
        sorting: true,
        defaultSorting: 'UserId Asc',
        actions:
            {
                listAction: JtableList // '~/PoolManagement/GetPoolRoleSearchReslt'
            },
        fields: {
            UserId: {
                key: true,
                create: false,
                edit: false,
                list: false
            },
            FullName: {
                title: 'FullName',
                type: 'text',
                sorting: true,
            },
            PrimaryPhoneNumber: {
                title: 'PrimaryPhoneNumber',
                type: 'text',
                sorting: true,
            },
            Edit:
                    {
                        title: 'Edit',
                        width: '3%',
                        sorting: false,
                        display: function (data) {
                                var btn = $('<button type="button" id="btnUpdateUser" class="btn btn-sm btn-default"> Edit </button>').click(function () {
                                    currentPool = data.record.UserId;
                                    UpdateUser(data.record.UserId);
                                });
                                return btn;
                        }
                    },
            Delete:
                       {
                           title: 'Delete',
                           width: '3%',
                           sorting: false,
                           display: function (data) {
                                   var btn = $('<button type="button" id="btnDeletePool" class="btn btn-sm btn-danger"> Delete</button>').click(function () {
                                       ShowDeleteConfirmation("Delete Confirmation", "Are you sure you want to delete this pool ?", data.record.PoolId, DeletePool);
                                   });
                                   return btn;
                           }
                       },
        },
        recordsLoaded: function (event, data) {
            // hide accountid            
            //$("#PoolRoleDiv .table th:eq(0)").hide();
            //$("#PoolRoleDiv .table .tohide").hide();
            //$("#PoolRoleDiv .table .tohide").parent().hide();
        }
    });
    $('.jtable').addClass('table');
    Search();
});

function Search() {
    var form = $('#frmSearchUser');

    $('#divJtableUsers').jtable('load', {
        model: form.serializeObject(),
        //userId: currentUserId,
        //poolSearchType: $('input[name=PoolSearchType]:checked').val(),
        //poolId: currentPool
    });
}

$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

function AddUser() {
    $('#divAddUserModal').load(AddActionUrl, function (response, status, xhr) {
        $('#divAddUserModal').modal('show');
        $.validator.unobtrusive.parse($('#frmAddUser'));
        $.ajaxSetup({ async: false });
        $("#frmAddUser").submit(function (e) {
            e.preventDefault();  // prevent standard form submission                      
            var form = $(this);
            if (form.valid()) {
                AddUserLogic();
            }
        });
    });
}
function AddUserLogic() {
    var form = $('#frmAddUser');
    $.ajax({
        url: AddActionUrl,
        type: 'POST',
        dataType: 'json',
        data: form.serialize(),
        cache: false,
        success: function (data) {
            if (data.Result == MessageTypeEnum.Success) {
                $('#divAddUserModal').modal("hide");
                ShowOperationResultMessage(data.Message, data.Result);
                CloseDeleteConfirmation();
                Search();
            } else {
                ShowMessageOnPopup(data.Message, data.Result, "saveMsg", "saveMessageText");
            }
        },
        error: function (xhr, status, thrownError) {
            ShowMessageOnPopup(thrownError, MessageTypeEnum.Error, "saveMsg", "saveMessageText");
        }
    });
}
function UpdateUser(userId) {
    //var editUrl = EditActionUrl + "?guardSiteContractId=" + guardSiteContractId
    var updateUrl = UpdateActionUrl + "/" + userId
    $('#divUpdateUserModal').load(updateUrl, function (response, status, xhr) {
        $('#divUpdateUserModal').modal('show');
        $.validator.unobtrusive.parse($('#frmUpdateUser'));
        $.ajaxSetup({ async: false });
        $("#frmUpdateUser").submit(function (e) {
            e.preventDefault();  // prevent standard form submission
            var form = $(this);
            if (form.valid()) {
                UpdateUserLogic(updateUrl);
            }
        });
    });
}
function UpdateUserLogic(updateUrl) {
    var form = $('#frmUpdateUser');
    $.ajax({
        url: updateUrl,
        type: 'POST',
        dataType: 'json',
        data: form.serialize(),
        cache: false,
        success: function (data) {
            if (data.Result == MessageTypeEnum.Success) {
                $('#divUpdateUserModal').modal("hide");
                ShowOperationResultMessage(data.Message, data.Result);
                CloseDeleteConfirmation();
                Search();
            } else {
                ShowMessageOnPopup(data.Message, data.Result, "saveMsg", "saveMessageText");
            }
        },
        error: function (xhr, status, thrownError) {
            ShowMessageOnPopup(thrownError, MessageTypeEnum.Error, "saveMsg", "saveMessageText");
        }
    });
}

