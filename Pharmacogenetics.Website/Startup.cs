﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Pharmacogenetics.Website.Startup))]
namespace Pharmacogenetics.Website
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
