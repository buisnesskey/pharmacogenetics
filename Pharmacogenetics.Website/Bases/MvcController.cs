﻿using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pharmacogenetics.Website.Bases
{
    //[Authorize]
    public class MvcController : Controller
    {
        public bool ShowException
        {
            get
            {
                string value = ConfigurationManager.AppSettings["showException"];
                bool showException = false;
                if (bool.TryParse(value, out showException))
                    return showException;
                return false;
            }
        }
        public static log4net.ILog Log { get; set; }
        public ILog log = log4net.LogManager.GetLogger(typeof(MvcController));

        #region Handle Exceptions
        protected override void OnException(ExceptionContext filterContext)
        {
            Exception actionEx = filterContext.Exception;
            if (!ShowException)
            {
                try
                {
                    if (filterContext.ExceptionHandled)
                    {
                        return;
                    }
                    #region Log Exception
                    string controller = filterContext.RouteData.Values["controller"].ToString();
                    string action = filterContext.RouteData.Values["action"].ToString();

                    var method = controller + " / " + action;//MethodBase.GetCurrentMethod();  
                    //Log(LogEnum.Process.GeneralException, LogEnum.LogWriteType.File, LogEnum.LogMethodType.Method, method, ex);
                    #endregion

                    ViewData["Error"] = ViewData["Error"] + "\nError " + actionEx.Message;
                    log.Error(actionEx);

                    filterContext.Result = new ViewResult
                    {
                        ViewName = "~/Views/Shared/Error.cshtml"
                    };
                    filterContext.ExceptionHandled = true;
                }
                catch (Exception ex)
                {
                    #region Log Exception
                    ViewData["Error"] = ViewData["Error"] + "\nError " + ex.Message;
                    log.Error(ex);
                    //Log(LogEnum.Process.GeneralException, LogEnum.LogWriteType.File, LogEnum.LogMethodType.Method, MethodBase.GetCurrentMethod().Name, ex);
                    #endregion
                }
            }
            else
                log.Error(actionEx);
        }
        #endregion
        
    }
}