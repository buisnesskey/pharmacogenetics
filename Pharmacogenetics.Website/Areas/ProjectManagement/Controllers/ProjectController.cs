﻿using Pharmacogenetics.Business.AppServices;
using System.Collections.Generic;
using System.Web.Mvc;
using Pharmacogenetics.Website.Bases;
using Pharmacogenetics.Models.ViewModels;
using System;
using System.Net;
using System.IO;
using System.Threading;
using System.Web;
using System.Linq;
using Pharmacogenetics.BaseClasses.Enums;
using Pharmacogenetics.Website.Areas.TranslatorManagement;


namespace Pharmacogenetics.Website.Areas.ProjectManagement.Controllers
{
    public class ProjectController : MvcController
    {
        // GET: ProjectManagement/Patient
        public ActionResult Index()
        {
            return View();
        }



        #region Project Page

        ProjectAppService pAS = new ProjectAppService();
        public ActionResult ProjectPage()
        {
            List<ProjectViewModel> projectsList = pAS.GetAllProjects();
            return View(projectsList);
        }



        #endregion

        [HttpGet]
        public ActionResult Create()
        {
            ProjectViewModel model = new ProjectViewModel();
            return View();
        }
        [HttpPost]
        public ActionResult Create(ProjectViewModel model)
        {
            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];
                if (file != null && file.ContentLength > 0)
                {

                }
            }
            return View(model);
        }
        [HttpPost]
        public ActionResult CreateJson(ProjectViewModel model)
        {
            List<string> status = new List<string>();
            //to make unique id for each operation (each session)
            string uploadStatusKey = Guid.NewGuid().ToString();
            try
            {
                bool translatorFileUploadFlag = false;
                bool patientFileUploadFlag = false;
                bool filesUploadFlag = false;
                //check if the request contain any files
                if (Request.Files.Count > 0)
                {
                    ProjectAppService projectAppService = new ProjectAppService();
                    model.File = new List<FileViewModel>();

                    int j = 0;
                    //loop for each file
                    foreach (string fileTypeName in Request.Files.AllKeys)
                    {
                        var file = Request.Files[j];
                        //check if the size of file is valid
                        if (file != null && file.ContentLength > 0)
                        {
                            //set upload path
                            var uploadDir = "~/uploads";
                            //check if the path is exist else create it
                            if (!Directory.Exists(Server.MapPath(uploadDir)))
                                Directory.CreateDirectory(Server.MapPath(uploadDir));
                            //create file and concatinate the unique id with it's name
                            var filePath = Path.Combine(Server.MapPath(uploadDir), "Project-" + uploadStatusKey + "-" + file.FileName);
                            //var fileUrl = Path.Combine(uploadDir, file.FileName);
                            file.SaveAs(filePath);


                            //check the type of the file
                            if (CommonEnums.ProjectFilesEnums.fileTranslator.ToString().Equals(fileTypeName))
                            {
                                model.File.Add(new FileViewModel()
                                {
                                    FilePath = filePath,
                                    FileType = (byte)CommonEnums.ProjectFilesEnums.fileTranslator,
                                });
                                //flag to make sure at least one translator file is uploaded
                                translatorFileUploadFlag = true;
                            }
                            if (CommonEnums.ProjectFilesEnums.filePatient.ToString().Equals(fileTypeName))
                            {
                                model.File.Add(new FileViewModel()
                                {
                                    FilePath = filePath,
                                    FileType = (byte)CommonEnums.ProjectFilesEnums.filePatient,
                                });
                                //flag to make sure at least one paitent file is uploaded
                                patientFileUploadFlag = true;
                            }
                            if (CommonEnums.ProjectFilesEnums.fileCopyNumber.ToString().Equals(fileTypeName))
                            {
                                model.File.Add(new FileViewModel()
                                {
                                    FilePath = filePath,
                                    FileType = (byte)CommonEnums.ProjectFilesEnums.fileCopyNumber,
                                });
                            }
                        }
                        j++;
                    }

                    #region translator upload (Khaled section)
                    var filePathTranslatorList = model.File.Where(x => x.FileType == (byte)CommonEnums.ProjectFilesEnums.fileTranslator).Select(x => x.FilePath).ToList();

                    int i = 0;
                    foreach (var fileTranslator in filePathTranslatorList)
                    {
                        Random rand = new Random();
                        
                        string filePathTranslator = fileTranslator;//model.File.Where(x => x.FileType == (byte)CommonEnums.ProjectFilesEnums.fileTranslator).Select(x => x.FilePath).FirstOrDefault();

                        var uploadDirectory = "~/uploads";
                        var fileparts = filePathTranslator.Split('.');
                        var extension = fileparts[fileparts.Length - 1];


                        string newFileName = "";
                        string newFilePathAndName = "";

                        do
                        {
                            newFileName = "Bi-Allele" + DateTime.Now.ToFileTime() + rand.Next() + "." + extension;
                            newFilePathAndName = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(uploadDirectory), newFileName);
                        }
                        while (System.IO.File.Exists(newFilePathAndName));

                        //var newFileName = "Bi-Allele" + DateTime.Now.ToFileTime() + rand.Next() + "." + extension;
                        //var newFilePathAndName = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(uploadDirectory), newFileName);


                        System.IO.File.Move(filePathTranslator, newFilePathAndName);

                        model.File.Where(f => f.FileType == (byte)CommonEnums.ProjectFilesEnums.fileTranslator).ToList()[i].FilePath = newFilePathAndName;

                        //(fu => fu.FilePath = newFilePathAndName);
                        i++;
                    }
                    #endregion

                    //calculate the final flag flag to make sure at least one paitent and one translator file is uploaded
                    filesUploadFlag = translatorFileUploadFlag && patientFileUploadFlag;
                    if (filesUploadFlag)
                    {
                        bool createProjectStatus = projectAppService.CreateProject(model);
                        //display in the log the status of project creation
                        if (createProjectStatus)
                            updateSessionStatus(status, "Project's Information Created successfully", uploadStatusKey);
                        if (createProjectStatus)
                        {
                            //cretae thread for parsing, analysis, comparing and storing the data
                            HttpContext ctx = System.Web.HttpContext.Current;
                            Thread t = new Thread(new ThreadStart(() =>
                            {
                                System.Web.HttpContext.Current = ctx;
                                ProcessFiles(model, uploadStatusKey, status);
                            }));
                            t.Start();
                        }
                        else
                            return Json(new { Status = CommonEnums.MessageTypeEnum.Error, uploadStatusKey = uploadStatusKey, Message = "Project create failed!" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { Status = CommonEnums.MessageTypeEnum.Error, uploadStatusKey = uploadStatusKey, Message = "Please upload valid Translator file and Patient file to create the project!" }, JsonRequestBehavior.AllowGet);
                    return Json(new { Status = CommonEnums.MessageTypeEnum.Success, uploadStatusKey = uploadStatusKey }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("No Files , Upload failed");
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                updateSessionStatus(status, ex.Message, uploadStatusKey);
                updateSessionStatus(status, "Error", uploadStatusKey);
                return Json("Upload failed");
            }
            return Json("File uploaded successfully");
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            ProjectAppService projectAppService = new ProjectAppService();
            ProjectViewModel model = projectAppService.GetProject(id);
            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(ProjectViewModel model)
        {
            return View(model);
        }

        public ActionResult EditJson(ProjectViewModel model)
        {
            List<string> status = new List<string>();
            string uploadStatusKey = Guid.NewGuid().ToString();
            try
            {
                ProjectAppService projectAppService = new ProjectAppService();
                bool editProjectStatus = projectAppService.EditProject(model);
                if (editProjectStatus)
                    updateSessionStatus(status, "Project's Information Updated successfully", uploadStatusKey);
                bool filesUploadFlag = false;
                if (Request.Files.Count > 0)
                {
                    model.File = new List<FileViewModel>();



                    int filesWithContentCount = 0;
                    foreach (string fileTypeName in Request.Files.AllKeys)
                    {
                        var file = Request.Files[fileTypeName];

                        if (file != null && file.ContentLength > 0)
                        {
                            filesWithContentCount++;
                            var uploadDir = "~/uploads";
                            if (!Directory.Exists(Server.MapPath(uploadDir)))
                                Directory.CreateDirectory(Server.MapPath(uploadDir));
                            var filePath = Path.Combine(Server.MapPath(uploadDir), "Project-" + uploadStatusKey + "-" + file.FileName);
                            var fileUrl = Path.Combine(uploadDir, file.FileName);
                            file.SaveAs(filePath);

                            if (CommonEnums.ProjectFilesEnums.fileTranslator.ToString().Equals(fileTypeName))
                            {
                                model.File.Add(new FileViewModel()
                                {
                                    FilePath = filePath,
                                    FileType = (byte)CommonEnums.ProjectFilesEnums.fileTranslator,
                                });
                                filesUploadFlag = true;
                            }
                            if (CommonEnums.ProjectFilesEnums.filePatient.ToString().Equals(fileTypeName))
                            {
                                model.File.Add(new FileViewModel()
                                {
                                    FilePath = filePath,
                                    FileType = (byte)CommonEnums.ProjectFilesEnums.filePatient,
                                });
                                filesUploadFlag = true;
                            }
                            if (CommonEnums.ProjectFilesEnums.fileCopyNumber.ToString().Equals(fileTypeName))
                            {
                                model.File.Add(new FileViewModel()
                                {
                                    FilePath = filePath,
                                    FileType = (byte)CommonEnums.ProjectFilesEnums.fileCopyNumber,
                                });
                                filesUploadFlag = true;
                            }
                        }
                    }
                    if (filesWithContentCount == 0)
                    {
                        updateSessionStatus(status, string.Format("no files uploaded on server  , please try again"), uploadStatusKey);
                    }
                    if (filesUploadFlag)
                    {
                        HttpContext ctx = System.Web.HttpContext.Current;
                        Thread t = new Thread(new ThreadStart(() =>
                        {
                            System.Web.HttpContext.Current = ctx;
                            ProcessFiles(model, uploadStatusKey, status);
                        }));
                        t.Start();
                    }
                    else
                    {
                        updateSessionStatus(status, string.Format("Complete"), uploadStatusKey);
                    }
                    //return Json(new { uploadStatusKey = uploadStatusKey }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { Status = CommonEnums.MessageTypeEnum.Success, Message = "Project's Information Updated successfully", uploadStatusKey = uploadStatusKey }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                updateSessionStatus(status, ex.Message, uploadStatusKey);
                updateSessionStatus(status, "Error", uploadStatusKey);
                return Json("Project update failed!");

            }
        }

        /// <summary>
        /// action to parsing, analysis, comparing and storing the project's data files 
        /// </summary>
        /// <param name="projectViewModel"></param>
        /// <param name="uploadStatusKey"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public JsonResult ProcessFiles(ProjectViewModel projectViewModel, string uploadStatusKey, List<string> status)
        {
            bool operationStatus = false;
            try
            {
                AppServiceManager AppServiceManager = new AppServiceManager();
                operationStatus = AppServiceManager.ProjectAppService.CreateProjectDetails(projectViewModel, uploadStatusKey, status);
                operationStatus = true;
            }
            catch (Exception ex)
            {
                updateSessionStatus(status, ex.Message, uploadStatusKey);
                updateSessionStatus(status, "Error", uploadStatusKey);
            }
            return Json(operationStatus, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult CreateStatusJson(string id)
        {
            List<string> status = new List<string>();
            List<string> logStatus = new List<string>();
            try
            {
                log.Info(DateTime.Now + " UploadTrainDataStatusJson");
                if (System.Web.HttpContext.Current.Application["listStatus" + id] != null)
                {
                    status = (List<string>)System.Web.HttpContext.Current.Application["listStatus" + id];
                }
                if (System.Web.HttpContext.Current.Application["listStatusInner" + id] != null)
                {
                    logStatus = (List<string>)System.Web.HttpContext.Current.Application["listStatusInner" + id];
                }
            }
            catch (Exception)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(null, JsonRequestBehavior.AllowGet);
            }
            return Json(new { breifStatus = status, logStatus = logStatus }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult DeleteProject(int projectId)
        {
            bool deleteProjectStatus = false;
            try
            {
                ProjectAppService projectAppService = new ProjectAppService();
                deleteProjectStatus = projectAppService.DeleteProject(projectId);
                if (deleteProjectStatus)
                    TempData["Success"] = string.Join("<br />", "Project deleted successfully");
                else
                    ViewData["Error"] = ViewData["Error"] + "\nError " + string.Join("<br />", "Project deleted Fail");
            }
            catch (Exception ex)
            {
                ViewData["Error"] = ViewData["Error"] + "\nError " + string.Join("<br />", ex.Message);
            }
            return RedirectToAction("ProjectPage");
        }

        #region private methods
        public void updateSessionStatus(List<string> status, string currentStatus, string uploadStatusKey, bool removeLastRecord = false)
        {
            if (removeLastRecord)
                status.RemoveAt(status.Count - 1);
            status.Add(currentStatus);
            System.Web.HttpContext.Current.Application["listStatus" + uploadStatusKey] = status;
        }

        public void updateInnerSessionStatus(List<string> status, string currentStatus, string uploadStatusKey)
        {
            System.Web.HttpContext.Current.Application["listStatusInner" + uploadStatusKey] = status;
        }
        #endregion
    }
}