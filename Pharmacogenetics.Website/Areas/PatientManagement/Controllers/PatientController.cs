﻿using Pharmacogenetics.Business.AppServices;
using Pharmacogenetics.Business.Interfaces;
using Pharmacogenetics.Website.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pharmacogenetics.Helper;
using Pharmacogenetics.Website.Bases;
using Pharmacogenetics.Models.ViewModels;

namespace Pharmacogenetics.Website.Areas.PatientManagement.Controllers
{
    public class PatientController : MvcController
    {
        // GET: PatientManagement/Patient
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult GetUsers(PatientViewModel model, int jtStartIndex = new int(), int jtPageSize = new int(), string jtSorting = null)
        {
            JsonResult returnedResult = Json(new { Result = "ERROR", Message = "Error" });
            int totalCount = new int();
            AppServiceManager appServiceManager = new AppServiceManager();
            //UserManagementAppService userAppService = new UserManagementAppService();
            var result = appServiceManager.PatientAppService.GetAllPatients(model, jtStartIndex, jtPageSize, jtSorting, out totalCount);
            returnedResult = Json(new { Result = "OK", Records = result, TotalRecordCount = totalCount });
            return returnedResult;
        }
        public ActionResult Insert()
        {
            var model = new PatientViewModel();
            return PartialView("_Add", model);
        }
        [HttpPost]
        public ActionResult Insert(PatientViewModel model)
        {
            return PartialView("_Add", model);
        }
        public ActionResult Update(int? id)
        {
            PatientAppService patientAppService = new PatientAppService();
            var model = patientAppService.GetPatient(id.Value);
            return PartialView("_Update", model);
        }
        [HttpPost]
        public ActionResult Update(PatientViewModel model)
        {
            PatientAppService patientAppService = new PatientAppService();
            //var model = userAppService.GetUser(model.Value);
            return PartialView("_Update", model);
        }
        public ActionResult UploadTrainData()
        {
            PatientTrainDataViewModel patientTrainDataViewModel = new PatientTrainDataViewModel();
            return View(patientTrainDataViewModel);
        }
        [HttpPost]
        public ActionResult UploadTrainData(PatientTrainDataViewModel model)
        {
            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {
                    // var uploadDir = "~/uploads";
                    // if (!Directory.Exists(Server.MapPath(uploadDir)))
                    //     Directory.CreateDirectory(Server.MapPath(uploadDir));
                    // var filePath = Path.Combine(Server.MapPath(uploadDir), file.FileName);
                    // var fileUrl = Path.Combine(uploadDir, file.FileName);
                    // file.SaveAs(filePath);
                    // PatientAppService patientAppService = new PatientAppService();
                    // List<string> status = new List<string>();
                    // bool operationStatus = false;
                    // List<PatientResultViewModel> patientList = patientAppService.ParsePatientData(filePath, status, "");
                    // //patientAppService.StoreData(patientList, status, new List<string>(), "");
                    // int patientIter = 0;
                    // //foreach (var curPatient in patientList)
                    // //{
                    // //    patientIter++;
                    // //    //updateSessionStatus(status, string.Format("Check patient {0} {1}/{2} records", curPatient.PatientID, patientIter, patientList.Count));
                    // //    patientAppService.StorePatientData(curPatient, status,new List<string>(), "");
                    // //}
                    // operationStatus = true;
                    // if (operationStatus)
                    //     TempData["Success"] = string.Join("<br />", status);
                    // else
                    //     ViewData["Error"] = ViewData["Error"] + "\nError " + string.Join("<br />", status);
                    // 
                    // //var fileName = Path.GetFileName(file.FileName);
                    // //var path = Path.Combine(Server.MapPath("~/Images/"), fileName);
                    // //file.SaveAs(path);
                }
            }
            return View(model);
        }
        [HttpPost]
        public JsonResult UploadTrainDataJson()
        {
            try
            {
                if (Request.Files.Count > 0)
                {
                    var file = Request.Files[0];

                    if (file != null && file.ContentLength > 0)
                    {
                        var uploadDir = "~/uploads";
                        if (!Directory.Exists(Server.MapPath(uploadDir)))
                            Directory.CreateDirectory(Server.MapPath(uploadDir));
                        var filePath = Path.Combine(Server.MapPath(uploadDir), file.FileName);
                        var fileUrl = Path.Combine(uploadDir, file.FileName);
                        file.SaveAs(filePath);


                        string uploadStatusKey = Guid.NewGuid().ToString();

                        HttpContext ctx = System.Web.HttpContext.Current;
                        Thread t = new Thread(new ThreadStart(() =>
                        {
                            System.Web.HttpContext.Current = ctx;
                            ProcessFile(filePath, uploadStatusKey, ctx);
                        }));
                        t.Start();
                        //Thread thread = new Thread(() => ProcessFile(filePath, System.Web.HttpContext.Current));
                        //thread.Start();

                        //ProcessFile(filePath, uploadStatusKey, ctx);

                        //Thread newThread;
                        //newThread = new System.Threading.Thread(new ParameterizedThreadStart(ProcessFile));
                        //newThread.Start(filePath, System.Web.HttpContext.Current);
                        //
                        //Task.Run(() => ProcessFile(filePath, System.Web.HttpContext.Current));
                        return Json(new { filePath = filePath, uploadStatusKey = uploadStatusKey }, JsonRequestBehavior.AllowGet);


                        //
                        //var fileName = Path.GetFileName(file.FileName);
                        //var path = Path.Combine(Server.MapPath("~/Images/"), fileName);
                        //file.SaveAs(path);
                    }
                }
            }
            catch (Exception)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json("Upload failed");
            }
            return Json("File uploaded successfully");
        }
        [HttpPost]
        public JsonResult ProcessFile(string filePath, string uploadStatusKey, HttpContext ctx)
        {
            bool operationStatus = false;
            List<string> status = new List<string>();
            try
            {
                PatientAppService patientAppService = new PatientAppService();
                System.Web.HttpContext.Current.Application.Add("listStatus" + uploadStatusKey, new List<string>());

                List<PatientResultViewModel> patientList = patientAppService.ParsePatientData(filePath, status, uploadStatusKey);
                updateSessionStatus(status, "Analysis patients", ctx, uploadStatusKey);
                updateSessionStatus(status, "Analysis patients", ctx, uploadStatusKey);
                List<string> allStatusLog = new List<string>();
                List<string> statusLog = new List<string>();
                patientAppService.StoreData(patientList, statusLog, status, uploadStatusKey, new ProjectViewModel(), new Business.Containers.ProjectEntities());

                operationStatus = true;

                #region Execute Comparing
                if (operationStatus)
                {
                    PatientTranslatorService patientTranslatorService = new PatientTranslatorService();
                    //operationStatus = patientTranslatorService.ExecuteComparing(status, ctx, uploadStatusKey);
                }
                #endregion

                updateSessionStatus(status, string.Format("Complete"), ctx, uploadStatusKey);
                operationStatus = true;
            }
            catch (Exception ex)
            {
                updateSessionStatus(status, ex.Message, ctx, uploadStatusKey);
                updateSessionStatus(status, "Error", ctx, uploadStatusKey);
            }
            return Json(operationStatus, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult UploadTrainDataStatusJson(string id)
        {
            List<string> status = new List<string>();
            List<string> logStatus = new List<string>();
            try
            {
                log.Info(DateTime.Now + " UploadTrainDataStatusJson");
                if (System.Web.HttpContext.Current.Application["listStatus" + id] != null)
                {
                    status = (List<string>)System.Web.HttpContext.Current.Application["listStatus" + id];
                }
                if (System.Web.HttpContext.Current.Application["listStatusInner" + id] != null)
                {
                    logStatus = (List<string>)System.Web.HttpContext.Current.Application["listStatusInner" + id];
                }
            }
            catch (Exception)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(null, JsonRequestBehavior.AllowGet);
            }
            return Json(new { breifStatus = status, logStatus = logStatus }, JsonRequestBehavior.AllowGet);
        }


        #region  khaled area

        //[HttpPost]
        //public JsonResult TestPatientTranslator(string filePath, string uploadStatusKey, HttpContext ctx)
        //{
        //    bool operationStatus = true;
        //    List<string> status = new List<string>();
        //    List<string> allStatusLog = new List<string>();

        //    if (operationStatus)
        //    {
        //        updateSessionStatus(status, string.Format("Translator Stage"), ctx, uploadStatusKey);
        //        PatientTranslatorService PatientTranslatorService = new PatientTranslatorService();
        //        int PatientTranslatorInsertedCounts = 0;
        //        int PatientTranslatorUpdatedCounts = 0;

        //        operationStatus = PatientTranslatorService.Result(out PatientTranslatorInsertedCounts, out PatientTranslatorUpdatedCounts);
        //        updateSessionStatus(status, string.Format("{0} new records in Patient Translator  ", PatientTranslatorInsertedCounts), ctx, uploadStatusKey);
        //        updateSessionStatus(status, string.Format("{0} updated records in Patient Translator  ", PatientTranslatorUpdatedCounts), ctx, uploadStatusKey);

        //        updateSessionStatus(status, string.Format("Complete"), ctx, uploadStatusKey);
        //    }
        //    updateInnerSessionStatus(allStatusLog, string.Format(""), ctx, uploadStatusKey);
        //    operationStatus = true;
        //    return Json(operationStatus, JsonRequestBehavior.AllowGet);
        //}

        //[HttpPost]
        //public JsonResult UploadTrainDataJson()
        //{
        //    try
        //    {
        //        if (Request.Files.Count > 0)
        //        {
        //            var file = Request.Files[0];

        //            if (file != null && file.ContentLength > 0)
        //            {
        //                var uploadDir = "~/uploads";
        //                if (!Directory.Exists(Server.MapPath(uploadDir)))
        //                    Directory.CreateDirectory(Server.MapPath(uploadDir));
        //                var filePath = Path.Combine(Server.MapPath(uploadDir), file.FileName);
        //                var fileUrl = Path.Combine(uploadDir, file.FileName);
        //                file.SaveAs(filePath);


        //                string uploadStatusKey = Guid.NewGuid().ToString();
        //                HttpContext ctx = System.Web.HttpContext.Current;
        //                Thread t = new Thread(new ThreadStart(() =>
        //                {
        //                    System.Web.HttpContext.Current = ctx;
        //                    // ProcessFile(filePath, uploadStatusKey, ctx);
        //                    TestPatientTranslator(filePath, uploadStatusKey, ctx);
        //                }));
        //                t.Start();
        //                //Thread thread = new Thread(() => ProcessFile(filePath, System.Web.HttpContext.Current));
        //                //thread.Start();

        //                //Thread newThread;
        //                //newThread = new System.Threading.Thread(new ParameterizedThreadStart(ProcessFile));
        //                //newThread.Start(filePath, System.Web.HttpContext.Current);
        //                //
        //                //Task.Run(() => ProcessFile(filePath, System.Web.HttpContext.Current));
        //                return Json(new { filePath = filePath, uploadStatusKey = uploadStatusKey }, JsonRequestBehavior.AllowGet);


        //                //
        //                //var fileName = Path.GetFileName(file.FileName);
        //                //var path = Path.Combine(Server.MapPath("~/Images/"), fileName);
        //                //file.SaveAs(path);
        //            }
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        Response.StatusCode = (int)HttpStatusCode.BadRequest;
        //        return Json("Upload failed");
        //    }
        //    return Json("File uploaded successfully");
        //}

        #endregion



        #region Patient Output
        PatientAppService pAS = new PatientAppService();
        public ActionResult PatientOutput(string projectId)
        {
            List<SelectItemViewModel> projectsList = pAS.GetAllProjects();
            projectsList.Insert(0, new SelectItemViewModel() { key = "0", value = "All" });
            ViewBag.ProjectsList = projectsList;
            List<SelectItemViewModel> patientsList = pAS.GetAllPatients();
            patientsList.Insert(0, new SelectItemViewModel() { key = "0", value = "All" });
            ViewBag.PatientsList = patientsList;
            List<SelectItemViewModel> genesList = pAS.GetAllGenes();
            genesList.Insert(0, new SelectItemViewModel() { key = "0", value = "All" });
            ViewBag.GenesList = genesList;
            List<SelectItemViewModel> allelesList = pAS.GetAllSingleAlleles();
            allelesList.Insert(0, new SelectItemViewModel() { key = "All", value = "All" });
            ViewBag.AllelesList = allelesList;
            ViewData["cbAlleleUndefined"] = false;
            ViewData["cbAlleleProbability"] = false;
            ViewBag.ProjectsListSelectedItem = projectId;
            List<PatientGeneAlleleViewModel> patients = null;
            if (projectId != null)
            {
                int projectIdInteger = 0;
                int.TryParse(projectId, out projectIdInteger);
                patients = pAS.GetPatientGenesAndAlleles(
                    new List<int>() { projectIdInteger }, null, null,
                    false, false, null, null);
            }
            ViewBag.Patients = patients;
            return View();
        }
        public ActionResult PatientOutputReport(string projectId)
        {
            Session["projectId"] = projectId;
            List<SelectItemViewModel> patientsList = pAS.GetAllPatients();
            patientsList.Insert(0, new SelectItemViewModel() { key = "0", value = "All" });
            ViewBag.PatientsList = patientsList;
            return View();
        }
        [HttpPost]
        public JsonResult FilterByPatientInReport(List<string> ddlPatientId)
        {
            int projectId = 0;
            int.TryParse(Session["projectId"] as string, out projectId);
            List<int> patientIdList = ddlPatientId != null
                ? ddlPatientId.Select(i => Convert.ToInt32(i)).ToList()
                : null;
            List<PatientGeneAlleleViewModel> patients = null;
            object p = null;
            if ((ddlPatientId != null) && projectId != 0)
            {
                patients = pAS.GetPatientGenesAndAlleles(new List<int>() { projectId }, patientIdList, null, false, false, null, null);
                if (patients != null)
                {
                    p = patients.Select(
                        i =>
                            new
                            {
                                patientName = i.patientName,
                                geneCount = i.geneAllele.Count(),
                                definedAllele =
                                i.geneAllele.Count(d => d.alleleName != "-" && d.alleleName != "UND" && !d.probability),
                                undefinedAllele =
                                i.geneAllele.Count(x => x.alleleName == "UND" || x.alleleName == "-" || x.probability)
                            }).ToList();
                }
            }
            return Json(new
            {
                Success = true,
                model =
                Json(p),
                PartialViewHtml =
                RenderPartialToStringExtensions.RenderPartialToStringMethod(this.ControllerContext,
                    "PatientGeneAlleleGridPartialReport", patients)
            });
        }
        [HttpPost]
        public ActionResult FilterByPatientAndGene(List<string> ddlProjectId, List<string> ddlPatientId, List<string> ddlGeneId, bool cbAlleleUndefined, bool cbAlleleProbability, List<string> ddlAllele1Id, List<string> ddlAllele2Id)
        {
            List<string> status = new List<string>();
            List<string> logStatus = new List<string>();
            List<PatientGeneAlleleViewModel> patients = null;

            try
            {
                //int projectId = 0;
                //int.TryParse(Session["projectId"] as string, out projectId);
                List<int> projectIdList = ddlProjectId != null ? ddlProjectId.Select(i => Convert.ToInt32(i)).ToList() : null;
                List<int> patientIdList = ddlPatientId != null ? ddlPatientId.Select(i => Convert.ToInt32(i)).ToList() : null;
                List<int> geneIdList = ddlGeneId != null ? ddlGeneId.Select(i => Convert.ToInt32(i)).ToList() : null;
            
                if (ddlProjectId != null || ddlPatientId != null || ddlGeneId != null || cbAlleleUndefined || cbAlleleProbability ||
                    ddlAllele1Id != null || ddlAllele2Id != null)
                {
                    patients = pAS.GetPatientGenesAndAlleles(projectIdList, patientIdList, geneIdList, cbAlleleUndefined, cbAlleleProbability, ddlAllele1Id, ddlAllele2Id);
                }

            }
            catch (Exception ex)
            {
                updateSessionStatus(status, ex.Message, null, "");
                updateSessionStatus(status, "Error", null, "");
            }
            return PartialView("PatientGeneAlleleGridPartial", patients);

        }

        [HttpPost]
        public ActionResult ShowAssaysAndCallsOfPatient(int projectId, int patientId, int geneId)
        {
            List<AssayCallViewModel> assaysAndCalls = pAS.GetAssaysAndCalls(projectId, patientId, geneId);
            return PartialView("PatientAssayCallGridPartial", assaysAndCalls);
        }


        [HttpPost]
        public ActionResult PatientGenoTypeRecommendation(int projectId, int patientId, int geneId)
        {

            GenoTypeRecommendatoinService recommendationService = new GenoTypeRecommendatoinService();
            List<PatientGenoTypeRecommendationViewModel> PatientGenoTypeRecommendationViewModelList = recommendationService.GetGenoTypeRecommendatoinList(projectId, patientId, geneId);
            return PartialView("PatientGenoTypeRecommendationPartial", PatientGenoTypeRecommendationViewModelList);
        }

        [HttpPost]
        public ActionResult LoadData()
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            //Find Order Column
            var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
            int recordsTotal = 0;
            List<AssayCallViewModel> data = (List<AssayCallViewModel>)TempData["PatientListAssayCall"];
            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult ExportPatientsGenesAllelesToExcelExec(List<string> ddlProjectId, List<string> ddlPatientId, List<string> ddlGeneId, bool cbAlleleUndefined, bool cbAlleleProbability, List<string> ddlAllele1Id, List<string> ddlAllele2Id)
        {
            //int projectId = 0;
            //int.TryParse(Session["projectId"] as string, out projectId);
            List<int> projectIdList = ddlProjectId != null ? ddlProjectId.Select(i => Convert.ToInt32(i)).ToList() : null;
            List<int> patientIdList = ddlPatientId != null ? ddlPatientId.Select(i => Convert.ToInt32(i)).ToList() : null;
            List<int> geneIdList = ddlGeneId != null ? ddlGeneId.Select(i => Convert.ToInt32(i)).ToList() : null;
            List<PatientGeneAlleleViewModel> patients = null;
            if (ddlProjectId != null || ddlPatientId != null || ddlGeneId != null || cbAlleleUndefined || cbAlleleProbability ||
                ddlAllele1Id != null || ddlAllele2Id != null)
            {
                patients = pAS.GetPatientGenesAndAlleles(projectIdList, patientIdList, geneIdList, cbAlleleUndefined, cbAlleleProbability, ddlAllele1Id, ddlAllele2Id);
            }
            TempData["PatientListGeneAllele"] = patients;
            return Json(new { success = true, responseText = "Your message successfuly sent!" }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ExportPatientsGenesAllelesToExcelDownload()
        {
            List<PatientGeneAlleleViewModel> listGeneAllele = (List<PatientGeneAlleleViewModel>)TempData["PatientListGeneAllele"];
            if (listGeneAllele != null)
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("Project Name", typeof(string));
                dt.Columns.Add("Patient", typeof(string));
                dt.Columns.Add("Gene", typeof(string));
                dt.Columns.Add("Allele", typeof(string));
                dt.Columns.Add("Copy Number", typeof(string));
                dt.Columns.Add("Probability", typeof(string));

                if (listGeneAllele != null)
                {
                    foreach (PatientGeneAlleleViewModel patient in listGeneAllele)
                    {
                        foreach (GeneAlleleViewModel geneAllele in patient.geneAllele)
                        {

                            if (geneAllele.probability)
                            {
                                dt.Rows.Add(geneAllele.projectName, patient.patientName, geneAllele.geneName,
                                    "\"[" + geneAllele.alleleName + "]\"", geneAllele.copyNumber, geneAllele.probability);
                            }
                            else
                            {
                                dt.Rows.Add(geneAllele.projectName, patient.patientName, geneAllele.geneName,
                                    geneAllele.alleleName, geneAllele.copyNumber, geneAllele.probability);
                            }
                        }
                    }
                }
                ExportFile.ExportToFileCSV("PatientsGeneAllele", dt, Response);
            }
            return PartialView("PatientGeneAlleleGridPartial", listGeneAllele);
        }

        public ActionResult ExportPatientsGenesAllelesToExcel()
        {
            List<PatientGeneAlleleViewModel> listGeneAllele = (List<PatientGeneAlleleViewModel>)TempData["PatientListGeneAllele"];
            if (listGeneAllele != null)
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("Project Name", typeof(string));
                dt.Columns.Add("Patient", typeof(string));
                dt.Columns.Add("Gene", typeof(string));
                dt.Columns.Add("Allele", typeof(string));
                dt.Columns.Add("Copy Number", typeof(string));
                dt.Columns.Add("Probability", typeof(string));

                if (listGeneAllele != null)
                {
                    foreach (PatientGeneAlleleViewModel patient in listGeneAllele)
                    {
                        foreach (GeneAlleleViewModel geneAllele in patient.geneAllele)
                        {

                            if (geneAllele.probability)
                            {
                                dt.Rows.Add(geneAllele.projectName, patient.patientName, geneAllele.geneName,
                                    "\"[" + geneAllele.alleleName + "]\"", geneAllele.copyNumber, geneAllele.probability);
                            }
                            else
                            {
                                dt.Rows.Add(geneAllele.projectName, patient.patientName, geneAllele.geneName,
                                    geneAllele.alleleName, geneAllele.copyNumber, geneAllele.probability);
                            }
                        }
                    }
                }
                ExportFile.ExportToFileCSV("PatientsGeneAllele", dt, Response);
            }
            return PartialView("PatientGeneAlleleGridPartial", listGeneAllele);
        }

        public ActionResult ExportPatientsGeneAssaysCallsToExcel(string projectId,string patientId,string geneId)
        {
            int projectIdNum;
            int.TryParse(projectId, out projectIdNum);
            int patientIdNum;
            int.TryParse(patientId, out patientIdNum);
            int geneIdNum;
            int.TryParse(geneId, out geneIdNum);
            List<AssayCallViewModel> listAssayCall = null;
            if (projectIdNum != 0 && patientIdNum != 0 && geneIdNum != 0)
            {
                listAssayCall = pAS.GetAssaysAndCalls(projectIdNum, patientIdNum, geneIdNum);
                DataTable dt = new DataTable();
                dt.Columns.Add("Assay", typeof(string));
                dt.Columns.Add("RsId", typeof(string));
                dt.Columns.Add("Call", typeof(string));

                if (listAssayCall != null)
                {
                    foreach (AssayCallViewModel gene in listAssayCall)
                    {
                        dt.Rows.Add(gene.assayName, gene.rsId, gene.callName);
                    }
                }
                ExportFile.ExportToFileCSV("GeneAssaysCalls", dt, Response);
            }
            return PartialView("PatientAssayCallGridPartial", listAssayCall);
        }



        public ActionResult ExportPatientRecommendationList(string projectId, string patientId, string geneId)
        {
            int projectIdNum;
            int.TryParse(projectId, out projectIdNum);
            int patientIdNum;
            int.TryParse(patientId, out patientIdNum);
            int geneIdNum;
            int.TryParse(geneId, out geneIdNum);



            GenoTypeRecommendatoinService recommendationService = new GenoTypeRecommendatoinService();
            List<PatientGenoTypeRecommendationViewModel> PatientGenoTypeRecommendationViewModelList= new List<PatientGenoTypeRecommendationViewModel> ();
            try
            {
                if (projectIdNum != 0 && patientIdNum != 0 && geneIdNum != 0)
                {
                    PatientGenoTypeRecommendationViewModelList = recommendationService.GetGenoTypeRecommendatoinList(projectIdNum, patientIdNum, geneIdNum);
                    DataTable dt = new DataTable();
                  

                    if (PatientGenoTypeRecommendationViewModelList != null)
                    {

                        object[] valuesColmn = new object[2 +PatientGenoTypeRecommendationViewModelList[0].RecommendationList.Count];
                        valuesColmn = BuildColumn(PatientGenoTypeRecommendationViewModelList[0]);
                        for (int i = 0; i < valuesColmn.Count(); i++)
                        {
                            dt.Columns.Add(valuesColmn[i].ToString());
                        }

                        foreach (var RecommendationItem in PatientGenoTypeRecommendationViewModelList)
                        {

                            object[] values = new object[2 + RecommendationItem.RecommendationList.Count];
                            values = BuildRow(RecommendationItem);
                            dt.Rows.Add(values);
                        }
                    }
                    ExportFile.ExportToFileCSV("PatientGenoTypeRecommendationList", dt, Response);
                }
            }
            catch (Exception ex)
            {
                string ss = ex.Message;
            }
            return PartialView("PatientGenoTypeRecommendationPartial", PatientGenoTypeRecommendationViewModelList);
        }


        private object[] BuildColumn(PatientGenoTypeRecommendationViewModel recommendationItem)
        {
            object[] values = new object[2 + recommendationItem.RecommendationList.Count];
            values[0] = "Gene Name";
            values[1] = "Allele Name";
            for (int i = 0; i < recommendationItem.RecommendationList.Count; i++)
            {
                values[i+2] = recommendationItem.RecommendationList[i].RecommendationKey;
            }
            return values;
        }

        private object[] BuildRow(PatientGenoTypeRecommendationViewModel recommendationItem)
        {
            object[] values = new object[2 + recommendationItem.RecommendationList.Count];
            values[0] = recommendationItem.GeneName;
            values[1] = recommendationItem.AlleleName;
            for (int i = 0; i < recommendationItem.RecommendationList.Count; i++)
            {
            values[i+2] = recommendationItem.RecommendationList[i].RecommendationValue;
            }
            return values;          
        }

        public ActionResult ExportReportToExcel()
        {
            List<PatientGeneAlleleViewModel> listReportTableData = (List<PatientGeneAlleleViewModel>)TempData["ReportTableData"];
            DataTable dt = new DataTable();

            dt.Columns.Add("Patient", typeof(string));
            dt.Columns.Add("Number of Genes", typeof(string));
            dt.Columns.Add("Number of Defined Alleles", typeof(string));
            dt.Columns.Add("Number of Undefined Alleles", typeof(string));
            if (listReportTableData != null)
            {
                foreach (PatientGeneAlleleViewModel patient in listReportTableData)
                {
                    dt.Rows.Add(patient.patientName, patient.geneAllele.Count,
                        patient.geneAllele.Count(x => x.alleleName != "UND" && x.alleleName != "-"),
                        patient.geneAllele.Count(x => x.alleleName == "UND" || x.alleleName == "-"));
                }
            }
            ExportFile.ExportToFileCSV("PatientsReport", dt, Response);
            return PartialView("PatientGeneAlleleGridPartialReport", listReportTableData);
        }
        #endregion


        #region Upload Copy Number Data
        public ActionResult UploadCopyNumberData()
        {
            return View();
        }
        [HttpPost]
        public JsonResult UploadCopyNumberDataJson(int projectId)
        {
            try
            {
                if (Request.Files.Count > 0)
                {
                    var file = Request.Files[0];
                    string uploadStatusKey = Guid.NewGuid().ToString();
                    if (file != null && file.ContentLength > 0)
                    {
                        var uploadDir = "~/uploads";
                        if (!Directory.Exists(Server.MapPath(uploadDir)))
                            Directory.CreateDirectory(Server.MapPath(uploadDir));
                        var filePath = Path.Combine(Server.MapPath(uploadDir), "CopyNumber-" + DateTime.Now.ToFileTime() + "-" + file.FileName);
                        //var fileUrl = Path.Combine(uploadDir, file.FileName);
                        file.SaveAs(filePath);
                        HttpContext ctx = System.Web.HttpContext.Current;
                        new Thread(() =>
                        {
                            System.Web.HttpContext.Current = ctx;
                            ProcessCopyNumberFile(projectId, filePath, uploadStatusKey, ctx);
                        }).Start();
                        return Json(new { filePath, uploadStatusKey }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json("Upload failed");
            }
            return Json("File uploaded successfully");
        }
        [HttpPost]
        public JsonResult ProcessCopyNumberFile(int projectId, string filePath, string uploadStatusKey, HttpContext ctx)
        {
            bool operationStatus = false;
            List<string> status = new List<string>();
            try
            {
                PatientAppService patientAppService = new PatientAppService();
                System.Web.HttpContext.Current.Application.Add("listStatus" + uploadStatusKey, new List<string>());
                List<PatientCopyNumberAnalysisViewModel> patientCopyNumberList = patientAppService.ParseCopyNumberData(filePath, status, uploadStatusKey);
                patientAppService.StoreCopyNumberData(projectId, null, patientCopyNumberList, status, uploadStatusKey);
                operationStatus = true;
            }
            catch (Exception ex)
            {
                updateSessionStatus(status, "Error", ctx, uploadStatusKey);
                updateSessionStatus(status, ex.Message, ctx, uploadStatusKey);
            }
            return Json(operationStatus, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult UploadCopyNumberDataStatusJson(string id)
        {
            List<string> status = new List<string>();
            List<string> logStatus = new List<string>();
            try
            {
                log.Info(DateTime.Now + " UploadTrainDataStatusJson");
                if (System.Web.HttpContext.Current.Application["listStatus" + id] != null)
                {
                    status = (List<string>)System.Web.HttpContext.Current.Application["listStatus" + id];
                }
                if (System.Web.HttpContext.Current.Application["listStatusInner" + id] != null)
                {
                    logStatus = (List<string>)System.Web.HttpContext.Current.Application["listStatusInner" + id];
                }
            }
            catch (Exception)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(null, JsonRequestBehavior.AllowGet);
            }
            return Json(new { breifStatus = status, logStatus = logStatus }, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region update Session Status  Methods
        public void updateSessionStatus(List<string> status, string currentStatus, HttpContext ctx, string uploadStatusKey, bool removeLastRecord = false)
        {
            if (removeLastRecord)
                status.RemoveAt(status.Count - 1);
            status.Add(currentStatus);
            System.Web.HttpContext.Current.Application["listStatus" + uploadStatusKey] = status;
        }
        public void updateInnerSessionStatus(List<string> status, string currentStatus, HttpContext ctx, string uploadStatusKey)
        {
            System.Web.HttpContext.Current.Application["listStatusInner" + uploadStatusKey] = status;
        }
        #endregion
    }
}