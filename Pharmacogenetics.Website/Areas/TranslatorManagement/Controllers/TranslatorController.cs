﻿using Pharmacogenetics.Business.AppServices;
using Pharmacogenetics.Business.Interfaces;
using Pharmacogenetics.Website.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pharmacogenetics.Helper;
using Pharmacogenetics.Website.Bases;
using Pharmacogenetics.Models.ViewModels;

namespace Pharmacogenetics.Website.Areas.TranslatorManagement.Controllers
{
    public class TranslatorController : MvcController
    {
        // GET: TranslatorManagement/Translator
        public ActionResult Index()
        {
            return View();
        }

        //public ActionResult UploadTranslatorData()
        //{
        //    TranslatorDataViewModel TranslatorDataViewModel = new TranslatorDataViewModel();
        //    return View(TranslatorDataViewModel);
        //}
        public ActionResult UploadTranslatorData()
        { 
            return View();
        }

        [HttpPost]
        public JsonResult UploadTranslatorDataJson()
        {
            try
            {
                if (Request.Files.Count > 0)
                {
                    var file = Request.Files[0];

                    if (file != null && file.ContentLength > 0)
                    {


                        var uploadDir = "~/uploads";
                        if (!Directory.Exists(Server.MapPath(uploadDir)))
                            Directory.CreateDirectory(Server.MapPath(uploadDir));
                        var filePath = Path.Combine(Server.MapPath(uploadDir), file.FileName);
                        var fileUrl = Path.Combine(uploadDir, file.FileName);
                        file.SaveAs(filePath);


                        var  newFileName = "Bi-Allele" + DateTime.Now.ToFileTime() + ".csv";
                        var newFilePathAndName = Path.Combine(Server.MapPath(uploadDir), newFileName);


                        System.IO.File.Move(filePath, newFilePathAndName);

                        string uploadStatusKey = Guid.NewGuid().ToString();
                        HttpContext ctx = System.Web.HttpContext.Current;
                        List<string> status = new List<string>();
                        new Thread(() =>
                        {
                            System.Web.HttpContext.Current = ctx;

                          //  PatientTranslatorService patientTranslatorService = new PatientTranslatorService();
                          //bool  operationStatus = patientTranslatorService.ExecuteComparing(status, ctx, uploadStatusKey);

                            //TranslatorManager translatorManager = new TranslatorManager();
                            //translatorManager.ProcessFile(newFilePathAndName, uploadStatusKey, ctx, null, ref status);

                        }).Start();
                        return Json(new { newFilePathAndName, uploadStatusKey }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json("Upload failed");
            }
            return Json("File uploaded successfully");
        }

        [HttpPost]



        [HttpGet]
        public JsonResult UploadTranslatorDataStatusJson(string id)
        {
                List<string> status = new List<string>();
                List<string> logStatus = new List<string>();
            try
            {
                log.Info(DateTime.Now + " UploadTranslatorDataStatusJson");
                if (System.Web.HttpContext.Current.Application["listStatus" + id] != null)
                {
                    status = (List<string>)System.Web.HttpContext.Current.Application["listStatus" + id];
                }
                if (System.Web.HttpContext.Current.Application["listStatusInner" + id] != null)
                {
                    logStatus = (List<string>)System.Web.HttpContext.Current.Application["listStatusInner" + id];
                }
            }
            catch (Exception)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(null, JsonRequestBehavior.AllowGet);
            }
            return Json(new { breifStatus = status, logStatus = logStatus }, JsonRequestBehavior.AllowGet);
        }



        #region Translator Output

        TranslatorAppService tAS = new TranslatorAppService();

        public ActionResult TranslatorOutput(string projectId)
        {
            List<SelectItemViewModel> projectsList = tAS.GetAllProjects();
            projectsList.Insert(0, new SelectItemViewModel() { key = "0", value = "All" });
            ViewBag.ProjectsList = projectsList;
            List<SelectItemViewModel> genesList = tAS.GetAllGenes();
            genesList.Insert(0, new SelectItemViewModel() { key = "0", value = "All" });
            ViewBag.GenesList = genesList;
            List<SelectItemViewModel> allelesList = tAS.GetAllSingleAlleles();
            allelesList.Insert(0, new SelectItemViewModel() { key = "All", value = "All" });
            ViewBag.AllelesList = allelesList;
            ViewBag.ProjectsListSelectedItem = projectId;
            List<TranslatorGeneAlleleViewModel> genesAlleles = null;
            if (projectId != null)
            {
                int projectIdInteger = 0;
                int.TryParse(projectId, out projectIdInteger);
                genesAlleles = tAS.GetGenesAndAlleles(
                    new List<int>() { projectIdInteger }, null, null, null);
            }
            ViewBag.GenesAlleles = genesAlleles;
            return View();
        }

        public ActionResult FilterByGeneAndAllele(List<string> ddlProjectId, List<string> ddlGeneId, List<string> ddlAllele1Id, List<string> ddlAllele2Id)
        {
            List<int> projectIdList = ddlProjectId != null ? ddlProjectId.Select(i => Convert.ToInt32(i)).ToList() : null;
            List<int> geneIdList = ddlGeneId != null ? ddlGeneId.Select(i => Convert.ToInt32(i)).ToList() : null;
            List<TranslatorGeneAlleleViewModel> genesAlleles = null;
            if (ddlProjectId != null || ddlGeneId != null || ddlAllele1Id != null || ddlAllele2Id != null)
            {
                genesAlleles = tAS.GetGenesAndAlleles(projectIdList, geneIdList, ddlAllele1Id, ddlAllele2Id);
            }
            return PartialView("TranslatorGeneAlleleGridPartial", genesAlleles);
        }

        [HttpPost]
        public ActionResult ShowAssaysAndCallsOfTranslator(int projectId, int geneId,int alleleId)
        {
            List<AssayCallViewModel> assaysAndCalls = tAS.GetAssaysAndCalls(projectId,geneId,alleleId);
            return PartialView("TranslatorAssayCallGridPartial", assaysAndCalls);
        }
        public ActionResult ExportTranslatorGenesAllelesToExcel()
        {
            List<TranslatorGeneAlleleViewModel> listGeneAllele = (List<TranslatorGeneAlleleViewModel>)TempData["TranslatorListGeneAllele"];
            if (listGeneAllele != null)
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("Project Name", typeof(string));
                dt.Columns.Add("Gene", typeof(string));
                dt.Columns.Add("Allele", typeof(string));

                if (listGeneAllele != null)
                {
                    foreach (TranslatorGeneAlleleViewModel geneAllele in listGeneAllele)
                    {
                        dt.Rows.Add(geneAllele.projectName, geneAllele.geneName, geneAllele.alleleName);
                    }
                }
                ExportFile.ExportToFileCSV("TranslatorsGeneAllele", dt, Response);
            }
            return PartialView("TranslatorGeneAlleleGridPartial", listGeneAllele);
        }

        public ActionResult ExportTranslatorGeneAssaysCallsToExcel(string projectId, string geneId,string alleleId)
        {
            int projectIdNum;
            int.TryParse(projectId, out projectIdNum);
            int geneIdNum;
            int.TryParse(geneId, out geneIdNum);
            int alleleIdNum;
            int.TryParse(alleleId, out alleleIdNum);
            List<AssayCallViewModel> listAssayCall = null;
            if (projectIdNum != 0 && geneIdNum != 0)
            {
                listAssayCall = tAS.GetAssaysAndCalls(projectIdNum, geneIdNum, alleleIdNum);
                DataTable dt = new DataTable();
                dt.Columns.Add("Assay", typeof(string));
                dt.Columns.Add("RsId", typeof(string));
                dt.Columns.Add("Call", typeof(string));
                if (listAssayCall != null)
                {
                    foreach (AssayCallViewModel gene in listAssayCall)
                    {
                        dt.Rows.Add(gene.assayName, gene.rsId, gene.callName);
                    }
                }
                ExportFile.ExportToFileCSV("GeneAssaysCalls", dt, Response);
            }
            return PartialView("TranslatorAssayCallGridPartial", listAssayCall);
        }

        #endregion



        #region Translator Methods
        #endregion

    }
}