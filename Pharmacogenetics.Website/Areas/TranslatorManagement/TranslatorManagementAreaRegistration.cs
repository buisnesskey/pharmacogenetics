﻿using System.Web.Mvc;

namespace Pharmacogenetics.Website.Areas.TranslatorManagement
{
    public class TranslatorManagementAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "TranslatorManagement";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "TranslatorManagement_default",
                "TranslatorManagement/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}