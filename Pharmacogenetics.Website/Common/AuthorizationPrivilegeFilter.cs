﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web;
using System.Web.Http.Filters;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

namespace Pharmacogenetics.Website.Common
{
    public class AuthorizationPrivilegeFilter : AuthorizeAttribute
    {
        public Pharmacogenetics.BaseClasses.Enums.CommonEnums.ActionEnum Action { get; set; }
        public Pharmacogenetics.BaseClasses.Enums.CommonEnums.PrivilegeEnum View { get; set; }
        #region old code
        //public bool ISAlarmReport { get; set; }
        //public bool ISChartReport { get; set; } 
        #endregion
        private DirectoryInfo LogDirectoryInfo
        {
            get;
            set;
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            #region old code
            //UnitOfWork unitOfWork = new UnitOfWork();
            //unitOfWork.MainDirectoryInfo = LogDirectoryInfo;
            ////filterContext.Controller.ViewBag.Alert = "TEST";
            //try
            //{
            //    NNOCBaseController currentBaseController = filterContext.Controller as NNOCBaseController;
            //    LogDirectoryInfo = currentBaseController.LogDirectory;
            //    if (filterContext.HttpContext.User.Identity.IsAuthenticated)
            //    {
            //        User currentUser = unitOfWork.User.GetUserByUsername(filterContext.HttpContext.User.Identity.Name);
            //        if (currentUser != null)
            //        {
            //            if (ISAlarmReport)
            //            {
            //                SetAlarmReportView(filterContext);
            //            }
            //            else if (ISChartReport)
            //                SetAlarmReportView(filterContext, ReportViewType.Chart);

            //            #region Check current user authorization
            //            bool authorized = unitOfWork.UserPrivilege.HasPrivilege(currentUser.UserId, (int)Action, (int)View);
            //            if (authorized)
            //            {
            //                #region Set the viewBag values
            //                UserAction userActions = unitOfWork.User.GetUserActions(currentUser.UserId, View);
            //                filterContext.Controller.ViewBag.Add = userActions.Add;
            //                filterContext.Controller.ViewBag.Edit = userActions.Edit;
            //                filterContext.Controller.ViewBag.Delete = userActions.Delete;
            //                #endregion
            //                // return to complete the action executing
            //                return;
            //            }
            //            #endregion
            //        }
            //    }
            //    #region Go to login page if user not logged in OR controller not of type NNOCBaseController, because there is no user to authorize him
            //    HandleUnauthorizedRequest(filterContext);
            //    #endregion
            //}
            //catch (Exception exception)
            //{
            //    var method = MethodBase.GetCurrentMethod();
            //    var methodFullNameWithParams = string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray()));
            //    ExceptionHandler.WriteLog(exception, methodFullNameWithParams);
            //}
            //finally
            //{
            //    unitOfWork.Dispose();
            //}
            //base.OnAuthorization(filterContext); 
            #endregion
        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            #region old code
            //try
            //{
            //    base.HandleUnauthorizedRequest(filterContext);
            //    if (filterContext.HttpContext.Request.IsAjaxRequest())
            //    {
            //        #region Handle ajax requests cases, but should be handled from client side code
            //        ExceptionHandler.WriteMessage(LogDirectoryInfo, LogTypeEnumList.Warning, "Unauthorized user try to access the system using ajax", "AuthorizationPrivilegeFilter.OnAuthorization");
            //        FormsAuthentication.SignOut();
            //        filterContext.HttpContext.Response.StatusCode = 401;
            //        filterContext.HttpContext.Response.SuppressContent = true;
            //        filterContext.HttpContext.Response.End();
            //        #endregion
            //    }
            //    else
            //    {
            //        ExceptionHandler.WriteMessage(LogDirectoryInfo, LogTypeEnumList.Warning, "Unauthorized user try to access the system", "AuthorizationPrivilegeFilter.OnAuthorization");
            //        FormsAuthentication.SignOut();
            //        RedirectToLogin(filterContext, filterContext.HttpContext.Request.RawUrl);
            //    }
            //}
            //catch (Exception exception)
            //{
            //    var method = MethodBase.GetCurrentMethod();
            //    var methodFullNameWithParams = string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray()));
            //    ExceptionHandler.WriteLog(exception, methodFullNameWithParams);
            //} 
            #endregion
        }

        /// <summary>
        /// Redirect send context to login page
        /// </summary>
        /// <param name="context"></param>
        /// <param name="returnURL"></param>
        private void RedirectToLogin(AuthorizationContext context, string returnURL = null)
        {
            #region old code
            //try
            //{
            //    RouteValueDictionary routValueDictionary = new RouteValueDictionary(new
            //    {
            //        controller = "User",
            //        action = "Login",
            //        area = "Security"
            //    });
            //    if (!string.IsNullOrWhiteSpace(returnURL))
            //    {
            //        routValueDictionary.Add("returnUrl", context.HttpContext.Request.RawUrl.ToString());
            //    }
            //    ActionResult actionResult = new RedirectToRouteResult(new RouteValueDictionary(routValueDictionary));
            //    context.Result = actionResult;
            //}
            //catch (Exception exception)
            //{
            //    #region Exception logging
            //    var method = MethodBase.GetCurrentMethod();
            //    var methodFullNameWithParams = string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray()));
            //    ExceptionHandler.WriteLog(exception, methodFullNameWithParams);
            //    #endregion
            //} 
            #endregion
        }

    }
}