﻿using System.Web;
using System.Web.Optimization;

namespace Pharmacogenetics.Website
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery-ui-{version}.js",
                        "~/Scripts/jtable/jquery.jtable.js"
                        ));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            //            "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                //"~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Scripts/jtable/themes/basic/jtable_basic.css",
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/common").Include(
                        "~/Scripts/App/Common.js"));




            bundles.Add(new StyleBundle("~/Content/css/pharma").Include(
                      "~/Content/themes/phrma/css/style.css",
                      "~/Content/themes/phrma/vendors/drag-drop/src/jquery.fileuploader.css",
                      "~/Content/themes/phrma/vendors/drag-drop/css/jquery.fileuploader-theme-dragdrop.css"));

            bundles.Add(new ScriptBundle("~/bundles/js/pharma").Include(
                //"~/Content/themes/phrma/js/jquery.js",
                        "~/Content/themes/phrma/vendors/drag-drop/src/jquery.fileuploader.min.js",
                        "~/Content/themes/phrma/vendors/drag-drop/js/custom.js"
                        ));

            bundles.Add(new StyleBundle("~/Content/css/pharma/datatables").Include(
                      //"~/Content/themes/phrma/css/style.css",
                      "~/Content/themes/phrma/vendors/DataTables-1.10.7/media/css/jquery.dataTables.min.css",
                      "~/Content/themes/phrma/vendors/DataTables-1.10.7/examples/resources/syntax/shCore.css",
                      "~/Content/themes/phrma/vendors/DataTables-1.10.7/examples/resources/demo.css"));

            bundles.Add(new ScriptBundle("~/bundles/js/pharma/datatables").Include(
                //"~/Content/themes/phrma/js/jquery.js",
                        "~/Content/themes/phrma/vendors/DataTables-1.10.7/media/js/jquery.dataTables.js",
                        "~/Content/themes/phrma/vendors/DataTables-1.10.7/examples/resources/syntax/shCore.js",
                        "~/Content/themes/phrma/vendors/DataTables-1.10.7/examples/resources/demo.js"
                        ));
        }
    }
}
