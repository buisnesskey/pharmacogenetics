﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Filters;

namespace Pharmacogenetics.Website.CustomFilters
{
    public class Authenticationfilter : ActionFilterAttribute, IAuthenticationFilter
    {
        public void OnAuthentication(AuthenticationContext filterContext)
        {
        }
        public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
        {
            var user = filterContext.HttpContext.User;
            if (user != null)
            {
                filterContext.Result = new HttpUnauthorizedResult();
                var helper = new UrlHelper(filterContext.RequestContext);
                var url = helper.Action("Index", "Setting");
                filterContext.Result = new RedirectResult(url);
            }
        }
    }
}