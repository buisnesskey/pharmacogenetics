﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pharmacogenetics.Models.ViewModels
{
    public class FileViewModel
    {
        public FileViewModel()
        {
        }

        public int FileID { get; set; }
        public string FilePath { get; set; }
        public byte FileType { get; set; }
        public byte DeleteStatus { get; set; }
        public System.DateTime CreateDate { get; set; }
        public Nullable<System.DateTime> ModifyDate { get; set; }
        public Nullable<System.DateTime> DeleteDate { get; set; }
        public int ProjectID { get; set; }

        public virtual ProjectViewModel Project { get; set; }
    }
}
