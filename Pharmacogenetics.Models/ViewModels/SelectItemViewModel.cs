﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pharmacogenetics.Models.ViewModels
{
    public class SelectItemViewModel
    {
       public string key { set; get; }
       public string value { set; get; }
    }
}
