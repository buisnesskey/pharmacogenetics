﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pharmacogenetics.Models.ViewModels
{
    public class PatientGenoTypeRecommendationViewModel
    {

        public string projectName { get; set; }
        public string patientName { get; set; }
        public string GeneName { get; set; }
        public string AlleleName { get; set; }


        public int projectID { get; set; }
        public int patientID { get; set; }
        public int GeneID { get; set; }
        public int AlleleID { get; set; }



        public string PhenoType { get; set; }
        public int PhenoTypeID { get; set; }


        public List<RecommendationViewModel> RecommendationList { get; set; }


    }


    public class RecommendationViewModel 
            {

        public string RecommendationKey { get; set; }
        public string RecommendationValue { get; set; }

    }




}