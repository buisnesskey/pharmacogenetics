﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pharmacogenetics.Models.ViewModels.ImportViewModels
{
    public class TrainingAnalysisViewModel
    {
        public string AssayName {set;get;}
        public string AssayID {set;get;}
        public string GeneSymbol {set;get;}
        public string NCBISNPReference {set;get;}
        public string SampleID {set;get;}
        public string Call {set;get;}
        public string Manual {set;get;}
        public string Quality {set;get;}
        public string VIC_Rn {set;get;}
        public string FAM_Rn {set;get;}
        public string ROX {set;get;}
        public string Task {set;get;}
        public string Gender {set;get;}
        public string Population {set;get;}
        public string Well {set;get;}
        public string ExperimentName {set;get;}
        public string PlateBarcode {set;get;}
        public string Annotation {set;get;}
        public string LowROXIntensity {set;get;}
        public string NTCFAMIntensityHigh {set;get;}
        public string NTCVICIntensityHigh {set;get;}
        public string GenotypeQualityLow {set;get;}
        public string FailedControl {set;get;}
        public string ReferenceSampleDiscordance {set;get;}
        public string ReplicateSampleDiscordance {set;get;}
        public string Chromosome {set;get;}
        public string Position {set;get;}
    }
}
