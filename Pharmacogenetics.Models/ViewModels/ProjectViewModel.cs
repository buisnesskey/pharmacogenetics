﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pharmacogenetics.Models.ViewModels
{
    public class ProjectViewModel
    {
        public ProjectViewModel()
        {
        }

        public int ProjectID { get; set; }
        [DisplayName("Name")]
        [Required]
        public string ProjectName { get; set; }
        public System.DateTime CreateDate { get; set; }
        [DisplayName("Description")]
        public string ProjectDescription { get; set; }
        [DisplayName("Notes")]
        public string ProjectNotes { get; set; }
        public byte DeleteStatus { get; set; }
        public Nullable<System.DateTime> ModifyDate { get; set; }
        public Nullable<System.DateTime> DeleteDate { get; set; }
        public virtual List<FileViewModel> File { get; set; }
        public virtual List<PatientLabViewModel> PatientLab { get; set; }
        public virtual List<PatientTranslatorViewModel> PatientTranslator { get; set; }
        public virtual List<TranslatorViewModel> Translator { get; set; }

    }
}
