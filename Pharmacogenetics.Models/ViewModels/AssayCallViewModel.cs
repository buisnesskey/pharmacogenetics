﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pharmacogenetics.Models.ViewModels
{
    public class AssayCallViewModel
    {
        public AssayCallViewModel()
        {
        }
        public AssayCallViewModel(string assayName, string rsId ,string callName)
        {
            this.assayName = assayName;
            this.rsId = rsId;
            this.callName = callName;
        }
        
        public string assayName;
        public string rsId;
        public string callName;
    }
}
