﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pharmacogenetics.Models.ViewModels
{
   public class ExeclAssayAlleleTranslator
    {



        #region A
        public string ACell { set; get; }
        public string BCell { set; get; }
        public string CCell { set; get; }
        public string DCell { set; get; }
        public string ECell { set; get; }
        public string FCell { set; get; }
        public string GCell { set; get; }
        public string HCell { set; get; }
        public string ICell { set; get; }
        public string JCell { set; get; }
        public string kCell { set; get; }
        public string LCell { set; get; }
        public string MCell { set; get; }
        public string NCell { set; get; }
        public string OCell { set; get; }
        public string PCell { set; get; }
        public string QCell { set; get; }
        public string RCell { set; get; }
        public string SCell { set; get; }
        public string TCell { set; get; }
        public string UCell { set; get; }
        public string VCell { set; get; }
        public string WCell { set; get; }
        public string XCell { set; get; }
        public string YCell { set; get; }
        public string ZCell { set; get; }

        #endregion


        #region AA
        public string AACell { set; get; }
        public string ABCell { set; get; }
        public string ACCell { set; get; }
        public string ADCell { set; get; }
        public string AECell { set; get; }
        public string AFCell { set; get; }
        public string AGCell { set; get; }
        public string AHCell { set; get; }
        public string AICell { set; get; }
        public string AJCell { set; get; }
        public string AkCell { set; get; }
        public string ALCell { set; get; }
        public string AMCell { set; get; }
        public string ANCell { set; get; }
        public string AOCell { set; get; }
        public string APCell { set; get; }
        public string AQCell { set; get; }
        public string ARCell { set; get; }
        public string ASCell { set; get; }
        public string ATCell { set; get; }
        public string AUCell { set; get; }
        public string AVCell { set; get; }
        public string AWCell { set; get; }
        public string AXCell { set; get; }
        public string AYCell { set; get; }
        public string AZCell { set; get; }
        #endregion


        #region B
        public string BACell { set; get; }
        public string BBCell { set; get; }
        public string BCCell { set; get; }
        public string BDCell { set; get; }
        public string BECell { set; get; }
        public string BFCell { set; get; }
        public string BGCell { set; get; }
        public string BHCell { set; get; }
        public string BICell { set; get; }
        public string BJCell { set; get; }
        public string BkCell { set; get; }
        public string BLCell { set; get; }
        public string BMCell { set; get; }
        public string BNCell { set; get; }
        public string BOCell { set; get; }
        public string BPCell { set; get; }
        public string BQCell { set; get; }
        public string BRCell { set; get; }
        public string BSCell { set; get; }
        public string BTCell { set; get; }
        public string BUCell { set; get; }
        public string BVCell { set; get; }
        public string BWCell { set; get; }
        public string BXCell { set; get; }
        public string BYCell { set; get; }
        public string BZCell { set; get; }
        #endregion

        #region C
        public string CACell { set; get; }
        public string CBCell { set; get; }
        public string CCCell { set; get; }
        public string CDCell { set; get; }

        #endregion



    }




      public class GeneRows 
      {

          string GeneName {get;set;}
          List<GeneRowsDetails>  GeneRowsDetailList {get;set;}

     }



     public class GeneRowsDetails 
      {

          string AssaySymbol {get;set;}
          string snpRef {get;set;}
          string GeneSymbol {get;set;}
          string AssayIdName {get;set;}

     }






}
