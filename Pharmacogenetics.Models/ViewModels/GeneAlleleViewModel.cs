﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pharmacogenetics.Models.ViewModels
{
    public class GeneAlleleViewModel
    {
        public GeneAlleleViewModel()
        {
        }
        public GeneAlleleViewModel(int projectId, string projectName, int geneId,string geneName, string parentGeneName, string alleleName,string copyNumber,bool probability)
        {
            this.projectId = projectId;
            this.projectName = projectName;
            this.geneId=geneId;
            this.geneName=geneName;
            this.parentGeneName = parentGeneName;
            this.alleleName= alleleName;
            this.copyNumber = copyNumber;
            this.probability = probability;
        }
        public int projectId;
        public string projectName;
        public int geneId;
        public string geneName;
        public string parentGeneName;
        public string alleleName;
        public string copyNumber;
        public bool probability;
    }
}
