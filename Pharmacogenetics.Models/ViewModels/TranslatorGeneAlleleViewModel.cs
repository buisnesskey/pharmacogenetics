﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pharmacogenetics.Models.ViewModels
{
    public class TranslatorGeneAlleleViewModel
    {
        public TranslatorGeneAlleleViewModel()
        {
        }

        public TranslatorGeneAlleleViewModel(int projectId, string projectName, int geneId, string geneName, int alleleId, string alleleName)
        {
            this.projectId = projectId;
            this.projectName = projectName;
            this.geneId = geneId;
            this.geneName = geneName;
            this.alleleId = alleleId;
            this.alleleName = alleleName;
        }

        public int projectId;
        public string projectName;
        public int geneId;
        public string geneName;
        public int alleleId;
        public string alleleName;
    }
}

