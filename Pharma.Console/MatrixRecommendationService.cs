﻿using Pharmacogenetics.Business.Bases;
using Pharmacogenetics.Entities.MainModel;
using Pharmacogenetics.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pharma.Console
{
    class MatrixRecommendationService
    {



        public static void SaveMatrixRecommendation(OpenFileDialog openFileDialog1)
        {

            try
            {




                AppService _AppService = new AppService();


                var allRecommendationList = _AppService.TheUnitOfWork.Recommendation.GetAll().ToList();
                var allPhenoTypeList = _AppService.TheUnitOfWork.PhenoType.GetAll().ToList();


                List<Recommendation> NewRecommendationList = new List<Recommendation>();
                List<PhenoType> NewPhenoTypeList = new List<PhenoType>();

                List<Pharmacogenetics.Entities.MainModel.MatrixRecommendation> NewMatrixRecommendationList = new List<Pharmacogenetics.Entities.MainModel.MatrixRecommendation>();

                openFileDialog1.ShowDialog();
                string fileName = openFileDialog1.FileName;

                var resultRecommendationList = ParseMatrixRecommendation(fileName).GroupBy(g => new { g.MatrixRecordNo}).ToList();
                //                var translatorListGroupedByAllell = translatorList.GroupBy(p => new { p.AlleleRowIndex ,p.AlleleID, p.GeneID}).ToList();

                foreach (var recommendationLine in resultRecommendationList)
                {



                    foreach (var recommendationLineItem in recommendationLine)
                    {
                        PhenoType phenoType = new PhenoType();
                        Recommendation recommendation = new Recommendation();
                        string recommendationKeyName = recommendationLineItem.RecommendationKey;
                        var recommendationValue = recommendationLineItem.RecommendationValue;

                        if (string.IsNullOrEmpty(recommendationKeyName))
                            continue;

                        if (!CheckIfRecommendationExistBefore(allRecommendationList, NewRecommendationList, recommendationKeyName, ref recommendation))
                        {
                            recommendation.RecommendationKeyName = recommendationKeyName;

                            _AppService.TheUnitOfWork.Recommendation.Insert(recommendation);
                            _AppService.TheUnitOfWork.Commit();
                            NewRecommendationList.Add(recommendation);
                        }

                        //Recommendation recommendation = new Recommendation()
                        //{

                        //    RecommendationKeyName=recommendationLineItem.RecommendationKey , 
                        //};

                        //_AppService.TheUnitOfWork.Recommendation.Insert(recommendation);




                        if (recommendationKeyName.Contains("Coded Genotype/Phenotype Summary"))
                        {
                            if (!CheckIfPhenoTypeExistBefore(allPhenoTypeList, NewPhenoTypeList, recommendationValue, ref phenoType))
                            {
                                phenoType.PhenoTypeName = recommendationValue;

                                _AppService.TheUnitOfWork.PhenoType.Insert(phenoType);
                                _AppService.TheUnitOfWork.Commit();
                                NewPhenoTypeList.Add(phenoType);
                            }


                        }




                     

                        Pharmacogenetics.Entities.MainModel.MatrixRecommendation item = new Pharmacogenetics.Entities.MainModel.MatrixRecommendation()
                        {
                            Recommendation = recommendation,
                            RecommendationValue = recommendationLineItem.RecommendationValue,
                            MatrixRecordNo = int.Parse(recommendationLineItem.MatrixRecordNo)

                        };

                        


                        //GeneMatrix geneMatrix = new GeneMatrix()
                        //{
                        //    GeneMatrixName = recommendationLineItem.GeneMatrixName,
                        //};

                        //if (_AppService.TheUnitOfWork.GeneMatrix.Insert(geneMatrix))
                        //{
                        //    item.GeneMatrix = geneMatrix;
                        //}


                        NewMatrixRecommendationList.Add(item);

                        // _AppService.TheUnitOfWork.MatrixRecommendation.Insert(item);



                        //if (_AppService.TheUnitOfWork.Commit() <= 0)
                        //{
                        //    MessageBox.Show("faild");

                        //} 

                    }






                    //public string GeneName { get; set; }
                    //public string AlelleName { get; set; }
                    //public string RecommendationKey { get; set; } // column 16
                    //public string RecommendationValue { get; set; } // column 16
                    //public string GeneMatrixName { get; set; }


                    //PhenoType phenoType = new PhenoType();
                    //Recommendation recommendation = new Recommendation();
                    //MatrixRecommendation matrixRecommendation = new MatrixRecommendation();


                    //phenoType.PhenoTypeName = GetPhenoTypeName(item);





                }


                StringBuilder sb = new StringBuilder();
                int result = 0;
                sb = ConvertMatrixRecommendationListToInsertsStrings(NewMatrixRecommendationList);
                result = _AppService.TheUnitOfWork.ExecSQLCommand(sb.ToString());
                int count = NewMatrixRecommendationList.Count;

                //MessageBox.Show(NewMatrixRecommendationList.Count + " records are added");
            }
            catch (Exception ex)
            {
                string ss = ex.Message;

                //   MessageBox.Show(ex.Message);
            }

        }


        public static bool CheckIfRecommendationExistBefore(List<Recommendation> RecommendationListExistInDB, List<Recommendation> NewRecommendationList, string RecommendationKey, ref Recommendation recommendation)
        {
            var RecommendationExistInDataBase = RecommendationListExistInDB.Where(g => g.RecommendationKeyName.ToLower() == RecommendationKey.ToLower()).FirstOrDefault();
            if (RecommendationExistInDataBase == null)
            {
                var RecommendationExistInNewList = NewRecommendationList.Where(g => g.RecommendationKeyName.ToLower() == RecommendationKey.ToLower()).FirstOrDefault();
                if (RecommendationExistInNewList == null)
                {
                    return false;
                }
                else
                {
                    recommendation = RecommendationExistInNewList;
                    return true;
                }
            }
            else
            {
                recommendation = RecommendationExistInDataBase;
                return true;
            }
        }
        public static bool CheckIfPhenoTypeExistBefore(List<PhenoType> PhenoTypeListExistInDB, List<PhenoType> NewPhenoTypeList, string PhenoTypeKey, ref PhenoType phenoType)
        {
            var PhenoTypeExistInDataBase = PhenoTypeListExistInDB.Where(g => g.PhenoTypeName.ToLower() == PhenoTypeKey.ToLower()).FirstOrDefault();
            if (PhenoTypeExistInDataBase == null)
            {
                var PhenoTypeExistInNewList = NewPhenoTypeList.Where(g => g.PhenoTypeName.ToLower() == PhenoTypeKey.ToLower()).FirstOrDefault();
                if (PhenoTypeExistInNewList == null)
                {
                    return false;
                }
                else
                {
                    phenoType = PhenoTypeExistInNewList;
                    return true;
                }
            }
            else
            {
                phenoType = PhenoTypeExistInDataBase;
                return true;
            }
        }



        private static StringBuilder ConvertMatrixRecommendationListToInsertsStrings(List<Pharmacogenetics.Entities.MainModel.MatrixRecommendation> result)
        {

            StringBuilder sb = new StringBuilder();
            foreach (var s in result)
            {


                string ss =
                        @"INSERT INTO dbo.MatrixRecommendation
                        ( 
                             MatrixRecordNo , "
                        + "  RecommendationID ,"
                        + "  RecommendationValue"
                        + " )"
                        + "VALUES  ("
                        + "'" + s.MatrixRecordNo + "',"
                        + "'" + s.Recommendation.RecommendationID + "'," +
                        "'" + s.RecommendationValue.Replace("'", "''") + "'" +
                        " )";
                sb.Append(ss);


            }
            return sb;


        }





        public static List<MatrixRecommendationModel> ParseMatrixRecommendation(string fileName)
        {
            try
            {
                bool operationStatus = false;
                ExcelReader excelReader = new ExcelReader();
                DataTable dt = excelReader.ReadExcelFileForTranslator(fileName);
                operationStatus = dt != null && dt.Rows.Count > new int();
                if (operationStatus)
                {
                    List<MatrixRecommendationModel> MatrixRecommendationList = PrepareMatrixRecommendationToInsertIntoDataBase(dt);
                    return MatrixRecommendationList;
                }
                return null;
            }
            catch (Exception ex)
            {

                string ss = ex.Message;
                return null;
            }
        }
        public static List<MatrixRecommendationModel> PrepareMatrixRecommendationToInsertIntoDataBase(DataTable dt)
        {


            #region Read Assay


            #region 4 Rows For Assay without any upper rows

            int startRowIndexForFirstDrugName = 1;
            int startColumnIndexForFirstDrugGene = 1;

            #endregion



            #endregion

            #region Read Gene Allele Call
            List<MatrixRecommendationModel> GeneDrugMatrixList = new List<MatrixRecommendationModel>();

            for (int i = startRowIndexForFirstDrugName; i < dt.Rows.Count; i++)
            {


                MatrixRecommendationModel GeneDrugMatrixItem = null;

                for (int j = startColumnIndexForFirstDrugGene; j < dt.Columns.Count; j++)
                {
                    if (readCell(dt.Rows[i][j]) != null && !string.IsNullOrEmpty((readCell(dt.Rows[i][j])).ToString()))
                    {
                        GeneDrugMatrixItem = new MatrixRecommendationModel();
                        //GeneDrugMatrixItem.GeneName = "SLC28A3 1381C>T";//readCell(dt.Rows[i][0]);
                        GeneDrugMatrixItem.MatrixRecordNo = readCell(dt.Rows[i][0]);
                        //GeneDrugMatrixItem.AlelleName = readCell(dt.Rows[i][2]);
                       // GeneDrugMatrixItem.GeneMatrixName = GeneDrugMatrixItem.GeneName;

                        GeneDrugMatrixItem.RecommendationKey = readCell(dt.Rows[0][j]);
                        GeneDrugMatrixItem.RecommendationValue = readCell(dt.Rows[i][j]);



                        GeneDrugMatrixList.Add(GeneDrugMatrixItem);
                    }
                }
            }


            return GeneDrugMatrixList;

            #endregion



        }

        public static string readCell(object o)
        {
            return o == null ? string.Empty : o.ToString();
        }


        public class MatrixRecommendationModel
        {

            public string RecommendationKey { get; set; } // column 16
            public string RecommendationValue { get; set; } // column 16
            public string GeneMatrixName { get; set; }

            public string MatrixRecordNo { get; set; }





        }

    }
}
