﻿using Pharmacogenetics.Business.Bases;
using Pharmacogenetics.Entities.MainModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Pharma.Console
{
  public  class PhenoTypeRecommendationService
    {



        public static void SavePhenoTypeRecommendation(OpenFileDialog openFileDialog1)
        {

            try
            {




                AppService _AppService = new AppService();


                var allRecommendationList = _AppService.TheUnitOfWork.Recommendation.GetAll().ToList();
                var allPhenoTypeList = _AppService.TheUnitOfWork.PhenoType.GetAll().ToList();


                List<Recommendation> NewRecommendationList = new List<Recommendation>();
                List<PhenoType> NewPhenoTypeList = new List<PhenoType>();

                List<Pharmacogenetics.Entities.MainModel.PhenoTypeRecommendation> NewPhenoTypeRecommendationList = new List<Pharmacogenetics.Entities.MainModel.PhenoTypeRecommendation>();





                openFileDialog1.ShowDialog();
                string fileName = openFileDialog1.FileName;

                var resultRecommendationList = ThermoFisherHelper.ParsePhenTypeData(fileName).GroupBy(g => new { g.AlelleName, g.GeneName }).ToList();
                //                var translatorListGroupedByAllell = translatorList.GroupBy(p => new { p.AlleleRowIndex ,p.AlleleID, p.GeneID}).ToList();

                foreach (var recommendationLine in resultRecommendationList)
                {

                    var GeneName = recommendationLine.Key.GeneName;
                    var AlelleName = recommendationLine.Key.AlelleName;

                    Pharmacogenetics.Entities.MainModel.Allele AlelleObject = _AppService.TheUnitOfWork.Allele.GetWhere(a => a.AlleleSymbol.ToLower() == AlelleName.ToLower()).FirstOrDefault();
                    Pharmacogenetics.Entities.MainModel.Gene GeneObject = _AppService.TheUnitOfWork.Gene.GetWhere(a => a.GeneSymbol.ToLower() == GeneName.ToLower()).FirstOrDefault();


                    foreach (var recommendationLineItem in recommendationLine)
                    {
                        PhenoType phenoType = new PhenoType();
                        Recommendation recommendation = new Recommendation();
                        string recommendationKeyName = recommendationLineItem.RecommendationKey;
                        var recommendationValue = recommendationLineItem.RecommendationValue;

                        if (string.IsNullOrEmpty(recommendationKeyName))
                            continue;

                        if (!CheckIfRecommendationExistBefore(allRecommendationList, NewRecommendationList, recommendationKeyName, ref recommendation))
                        {
                            recommendation.RecommendationKeyName = recommendationKeyName;

                            _AppService.TheUnitOfWork.Recommendation.Insert(recommendation);
                            _AppService.TheUnitOfWork.Commit();
                            NewRecommendationList.Add(recommendation);
                        }

                        //Recommendation recommendation = new Recommendation()
                        //{

                        //    RecommendationKeyName=recommendationLineItem.RecommendationKey , 
                        //};

                        //_AppService.TheUnitOfWork.Recommendation.Insert(recommendation);




                        if (recommendationKeyName.Contains("Coded Genotype/Phenotype Summary"))
                        {
                            if (!CheckIfPhenoTypeExistBefore(allPhenoTypeList, NewPhenoTypeList, recommendationValue, ref phenoType))
                            {
                                phenoType.PhenoTypeName = recommendationValue;

                                _AppService.TheUnitOfWork.PhenoType.Insert(phenoType);
                                _AppService.TheUnitOfWork.Commit();
                                NewPhenoTypeList.Add(phenoType);
                            }


                        }




                        if (AlelleObject == null)
                        {
                            AlelleObject = new Allele()
                            {
                                AlleleSymbol = AlelleName,
                                CreateDate = DateTime.Now,
                                DeleteStatus = 0,

                            };

                            _AppService.TheUnitOfWork.Allele.Insert(AlelleObject);
                            _AppService.TheUnitOfWork.Commit();
                        }

                        Pharmacogenetics.Entities.MainModel.PhenoTypeRecommendation item = new Pharmacogenetics.Entities.MainModel.PhenoTypeRecommendation()
                        {
                            AlelleID = (AlelleObject != null ? AlelleObject.AlleleID : 6090),
                            GeneID = GeneObject.GeneID,
                            Recommendation = recommendation,
                            RecommendationValue = recommendationLineItem.RecommendationValue                         
                        };

                        if (phenoType != null && phenoType.PhenoTypeId > 0)
                        {

                            item.PhenoTypeId = phenoType.PhenoTypeId;
                        }


                        //GeneMatrix geneMatrix = new GeneMatrix()
                        //{
                        //    GeneMatrixName = recommendationLineItem.GeneMatrixName,
                        //};

                        //if (_AppService.TheUnitOfWork.GeneMatrix.Insert(geneMatrix))
                        //{
                        //    item.GeneMatrix = geneMatrix;
                        //}


                        NewPhenoTypeRecommendationList.Add(item);

                        // _AppService.TheUnitOfWork.PhenoTypeRecommendation.Insert(item);



                        //if (_AppService.TheUnitOfWork.Commit() <= 0)
                        //{
                        //    MessageBox.Show("faild");

                        //} 

                    }






                    //public string GeneName { get; set; }
                    //public string AlelleName { get; set; }
                    //public string RecommendationKey { get; set; } // column 16
                    //public string RecommendationValue { get; set; } // column 16
                    //public string GeneMatrixName { get; set; }


                    //PhenoType phenoType = new PhenoType();
                    //Recommendation recommendation = new Recommendation();
                    //PhenoTypeRecommendation phenoTypeRecommendation = new PhenoTypeRecommendation();


                    //phenoType.PhenoTypeName = GetPhenoTypeName(item);





                }


                StringBuilder sb = new StringBuilder();
                int result = 0;
                sb = ConvertPhenoTypeRecommendationListToInsertsStrings(NewPhenoTypeRecommendationList);
                result = _AppService.TheUnitOfWork.ExecSQLCommand(sb.ToString());
                int count = NewPhenoTypeRecommendationList.Count;

                //MessageBox.Show(NewPhenoTypeRecommendationList.Count + " records are added");
            }
            catch (Exception ex)
            {
                string ss = ex.Message;

             //   MessageBox.Show(ex.Message);
            }

        }


        public static bool CheckIfRecommendationExistBefore(List<Recommendation> RecommendationListExistInDB, List<Recommendation> NewRecommendationList, string RecommendationKey, ref Recommendation recommendation)
        {
            var RecommendationExistInDataBase = RecommendationListExistInDB.Where(g => g.RecommendationKeyName.ToLower() == RecommendationKey.ToLower()).FirstOrDefault();
            if (RecommendationExistInDataBase == null)
            {
                var RecommendationExistInNewList = NewRecommendationList.Where(g => g.RecommendationKeyName.ToLower() == RecommendationKey.ToLower()).FirstOrDefault();
                if (RecommendationExistInNewList == null)
                {
                    return false;
                }
                else
                {
                    recommendation = RecommendationExistInNewList;
                    return true;
                }
            }
            else
            {
                recommendation = RecommendationExistInDataBase;
                return true;
            }
        }
        public static bool CheckIfPhenoTypeExistBefore(List<PhenoType> PhenoTypeListExistInDB, List<PhenoType> NewPhenoTypeList, string PhenoTypeKey, ref PhenoType phenoType)
        {
            var PhenoTypeExistInDataBase = PhenoTypeListExistInDB.Where(g => g.PhenoTypeName.ToLower() == PhenoTypeKey.ToLower()).FirstOrDefault();
            if (PhenoTypeExistInDataBase == null)
            {
                var PhenoTypeExistInNewList = NewPhenoTypeList.Where(g => g.PhenoTypeName.ToLower() == PhenoTypeKey.ToLower()).FirstOrDefault();
                if (PhenoTypeExistInNewList == null)
                {
                    return false;
                }
                else
                {
                    phenoType = PhenoTypeExistInNewList;
                    return true;
                }
            }
            else
            {
                phenoType = PhenoTypeExistInDataBase;
                return true;
            }
        }



        private static StringBuilder ConvertPhenoTypeRecommendationListToInsertsStrings(List<Pharmacogenetics.Entities.MainModel.PhenoTypeRecommendation> result)
        {

            StringBuilder sb = new StringBuilder();
            foreach (var s in result)
            {


                string ss =
                @"INSERT INTO dbo.PhenoTypeRecommendation
        ( 
MatrixRecordNo , 
GeneID ,
          AlelleID ,"
        + (s.PhenoType != null && s.PhenoType.PhenoTypeId > 0 ? "  PhenoTypeId ," : "")
        + "  RecommendationID ,"
        + "  RecommendationValue"
       + " )"
+ "VALUES  ("
               + "'" + s.MatrixRecordNo + "',"
               + "'" + s.GeneID + "'," +
                "'" + s.AlelleID + "'," +

             (s.PhenoType != null && s.PhenoType.PhenoTypeId > 0 ? "'" + s.PhenoType.PhenoTypeId + "'," : "")

                + "'" + s.Recommendation.RecommendationID + "'," +
                "'" + s.RecommendationValue.Replace("'", "''") + "'" +

       " )";
                sb.Append(ss);


            }
            return sb;


        }


    }
}
