﻿using Pharmacogenetics.Business.AppServices;
using Pharmacogenetics.Business.Bases;
using Pharmacogenetics.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pharma.Console
{
    public class ThermoFisherHelper
    {


        #region ThermoFisherTest
        public static List<ThermoResult> ParseTranslatorData(string fileName)
        {
            bool operationStatus = false;
            ExcelReader excelReader = new ExcelReader();
            DataTable dt = excelReader.ReadExcelFileForTranslator(fileName);
            operationStatus = dt != null && dt.Rows.Count > new int();
            if (operationStatus)
            {
                List<ThermoResult> TranslatorExcelItemList = Parsing(dt);
                return TranslatorExcelItemList;
            }
            return null;
        }


        public static List<ThermoResult> Parsing(DataTable dt)
        {

            string KeyCellName = "";
            List<string> KeyAssayNameList = new List<string>();
            string keyAssayName = "";

            #region Read Assay
            List<AssayListNamesHeader> assayListNamesHeaderList = new List<AssayListNamesHeader>();
            AssayListNamesHeader assayListNamesHeaderItem = null;


            #region 4 Rows For Assay without any upper rows
            int startRowIndexForAssay = 0;
            int endRowIndexForAssay = 0;
            int startRowIndexForFirstCall = endRowIndexForAssay + 1;


            int startColumnIndexForFirstGene = 2;
            int endColumnIndexForLastGene = 18;

            #endregion



            #endregion

            #region Read Gene Allele Call
            List<ThermoResult> ThermoResultList = new List<ThermoResult>();

            for (int j = startColumnIndexForFirstGene; j <= endColumnIndexForLastGene; j++)
            {


                ThermoResult ThermoResult = null;
                for (int i = startRowIndexForFirstCall; i < dt.Rows.Count; i++)
                {
                    ThermoResult = new ThermoResult();
                    ThermoResult.GeneName = readCell(dt.Rows[0][j]);
                    ThermoResult.PateintName = readCell(dt.Rows[i][0]);
                    string alleleString = readCell(dt.Rows[i][j]);
                    ThermoResult.AlleleValueList = ConvertCommaToList(alleleString);
                    ThermoResultList.Add(ThermoResult);

                }
            }


            return ThermoResultList;

            #endregion

            #region Build Translator
            //var listGroupAssayWithCellKey = assayListNamesHeaderList.GroupBy(k => k.KeyExcelColmnName).ToList();
            ////var TranslatorItemListGroup = TranslatorItemList.GroupBy(k => k.AlleleValue).ToList();
            //List<TranslatorExcelItem> TranslatorItemListNew = new List<TranslatorExcelItem>();
            //TranslatorExcelItem TranslatorItemNew = null;
            //foreach (var item in TranslatorItemList)
            //{

            //    var dedicatedAssaylist = listGroupAssayWithCellKey.Where(aa => aa.Key == item.KeyExcelColmnName).ToList();
            //    TranslatorItemNew = new TranslatorExcelItem()
            //    {
            //        AlleleValue = item.AlleleValue,
            //        CallValue = item.CallValue,
            //        GeneValue = item.GeneValue,
            //        KeyExcelColmnName = item.KeyExcelColmnName,
            //        AlleleRowIndex = item.AlleleRowIndex,
            //        AssayList = dedicatedAssaylist[0].Select(s => s.KeyExcelAssayName).ToList(),
            //    };
            //    TranslatorItemListNew.Add(TranslatorItemNew);
            //}
            #endregion

            //            return TranslatorItemListNew;

        }

        private static List<string> ConvertCommaToList(string alleleString)
        {
            List<string> alleleList = new List<string>();
            var alleleStringSplited = alleleString.Split(',');
            foreach (var dd in alleleStringSplited)
            {
                alleleList.Add(dd.Replace("{", "").Replace("}", ""));
            }
            return alleleList;
        }


        #endregion

        #region Inject Recommendation To DataBase
        public static List<GeneDrugMatrix> ParseRecommenationData(string fileName)
        {
            bool operationStatus = false;
            ExcelReader excelReader = new ExcelReader();
            DataTable dt = excelReader.ReadExcelFileForTranslator(fileName);
            operationStatus = dt != null && dt.Rows.Count > new int();
            if (operationStatus)
            {
                List<GeneDrugMatrix> TranslatorExcelItemList = ParsingRecommenationData(dt);
                return TranslatorExcelItemList;
            }
            return null;
        }
        public static List<GeneDrugMatrix> ParsingRecommenationData(DataTable dt)
        {


            #region Read Assay


            #region 4 Rows For Assay without any upper rows

            int startRowIndexForFirstDrugName = 1;


            int startColumnIndexForFirstDrugGene = 1;

            #endregion



            #endregion

            #region Read Gene Allele Call
            List<GeneDrugMatrix> GeneDrugMatrixList = new List<GeneDrugMatrix>();

            for (int i = startRowIndexForFirstDrugName; i < dt.Rows.Count; i++)
            {


                    GeneDrugMatrix GeneDrugMatrixItem = null;

                    for (int j = startColumnIndexForFirstDrugGene; j < dt.Columns.Count; j++)
                    {
                    if (readCell(dt.Rows[i][j])!=null &&   !string.IsNullOrEmpty((readCell(dt.Rows[i][j])).ToString()) &&
                        
                        (readCell(dt.Rows[i][j])).ToString().ToLower().Contains("x"))
                    {
                        GeneDrugMatrixItem = new GeneDrugMatrix();
                        GeneDrugMatrixItem.GeneName = readCell(dt.Rows[0][j]);
                        GeneDrugMatrixItem.DrugName = readCell(dt.Rows[i][0]);
                        GeneDrugMatrixItem.MatrixName = readCell(dt.Rows[i][dt.Columns.Count-1]);
                            GeneDrugMatrixList.Add(GeneDrugMatrixItem);
                    }
                 }
            }


            return GeneDrugMatrixList;

            #endregion

            #region Build Translator
            //var listGroupAssayWithCellKey = assayListNamesHeaderList.GroupBy(k => k.KeyExcelColmnName).ToList();
            ////var TranslatorItemListGroup = TranslatorItemList.GroupBy(k => k.AlleleValue).ToList();
            //List<TranslatorExcelItem> TranslatorItemListNew = new List<TranslatorExcelItem>();
            //TranslatorExcelItem TranslatorItemNew = null;
            //foreach (var item in TranslatorItemList)
            //{

            //    var dedicatedAssaylist = listGroupAssayWithCellKey.Where(aa => aa.Key == item.KeyExcelColmnName).ToList();
            //    TranslatorItemNew = new TranslatorExcelItem()
            //    {
            //        AlleleValue = item.AlleleValue,
            //        CallValue = item.CallValue,
            //        GeneValue = item.GeneValue,
            //        KeyExcelColmnName = item.KeyExcelColmnName,
            //        AlleleRowIndex = item.AlleleRowIndex,
            //        AssayList = dedicatedAssaylist[0].Select(s => s.KeyExcelAssayName).ToList(),
            //    };
            //    TranslatorItemListNew.Add(TranslatorItemNew);
            //}
            #endregion

            //            return TranslatorItemListNew;

        }
        #endregion




        #region Inject Recommendation To DataBase
        public static List<PhenoTypeRecommendation> ParsePhenTypeData(string fileName)
        {
            try
            {
                bool operationStatus = false;
                ExcelReader excelReader = new ExcelReader();
                DataTable dt = excelReader.ReadExcelFileForTranslator(fileName);
                operationStatus = dt != null && dt.Rows.Count > new int();
                if (operationStatus)
                {
                    List<PhenoTypeRecommendation> PhenoTypeRecommendationList = PreparePhenoTypeToInsertIntoDataBase(dt);
                    return PhenoTypeRecommendationList;
                }
                return null;
            }
            catch (Exception ex)
            {

                string ss = ex.Message;
                return null;
            }
        }
        public static List<PhenoTypeRecommendation> PreparePhenoTypeToInsertIntoDataBase(DataTable dt)
        {


            #region Read Assay


            #region 4 Rows For Assay without any upper rows

            int startRowIndexForFirstDrugName = 1;
            int startColumnIndexForFirstDrugGene = 2;

            #endregion



            #endregion

            #region Read Gene Allele Call
            List<PhenoTypeRecommendation> GeneDrugMatrixList = new List<PhenoTypeRecommendation>();

            for (int i = startRowIndexForFirstDrugName; i < dt.Rows.Count; i++)
            {


                PhenoTypeRecommendation GeneDrugMatrixItem = null;

                for (int j = startColumnIndexForFirstDrugGene; j < dt.Columns.Count; j++)
                {
                    if (readCell(dt.Rows[i][j]) != null && !string.IsNullOrEmpty((readCell(dt.Rows[i][j])).ToString()))
                    {
                        GeneDrugMatrixItem = new PhenoTypeRecommendation();
                        GeneDrugMatrixItem.GeneName = readCell(dt.Rows[i][0]);
                   //     GeneDrugMatrixItem.MatrixRecordNo = readCell(dt.Rows[i][0]);
                        GeneDrugMatrixItem.AlelleName = readCell(dt.Rows[i][1]);


                       // GeneDrugMatrixItem.GeneMatrixName = GeneDrugMatrixItem.GeneName;
                        GeneDrugMatrixItem.RecommendationKey = readCell(dt.Rows[0][j]);
                        GeneDrugMatrixItem.RecommendationValue = readCell(dt.Rows[i][j]);

                        

                        GeneDrugMatrixList.Add(GeneDrugMatrixItem);
                    }
                }
            }


            return GeneDrugMatrixList;

            #endregion

            

        }
        #endregion




        #region Common
        public static String GetColumnName(long columnNumber)
        {
            StringBuilder retVal = new StringBuilder();
            int x = 0;

            for (int n = (int)(Math.Log(25 * (columnNumber + 1)) / Math.Log(26)) - 1; n >= 0; n--)
            {
                x = (int)((Math.Pow(26, (n + 1)) - 1) / 25 - 1);
                if (columnNumber > x)
                    retVal.Append(System.Convert.ToChar((int)(((columnNumber - x - 1) / Math.Pow(26, n)) % 26 + 65)));
            }

            return retVal.ToString();
        }

        public static string readCell(object o)
        {
            return o == null ? string.Empty : o.ToString();
        } 
        #endregion

    }







    public class ThermoResult
    {
        public string PateintName { get; set; }

        public  List <string> AlleleValueList { get; set; }


        public string GeneName { get; set; }


    }


    public class GeneDrugMatrix
    {
        public string GeneName { get; set; }
        public string DrugName { get; set; }
        public string MatrixName { get; set; } // column 16
    }



    public class PhenoTypeRecommendation
    {

 //       [GeneID]
 //       [int] NULL,
	//[AlelleID]
 //       [int] NULL,
	//[PhenoTypeId]
 //       [int] NULL,
	//[RecommendationID]
 //       [int] NULL,
	//[GeneMatrixID]
 //       [int] NULL,

            
        public string GeneName { get; set; }
        public string AlelleName { get; set; }

        public string RecommendationKey { get; set; } // column 16
        public string RecommendationValue { get; set; } // column 16


        public string GeneMatrixName { get; set; }

        public string MatrixRecordNo { get; set; }


        


    }





}
