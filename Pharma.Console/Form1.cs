﻿using Pharmacogenetics.Business.Bases;
using Pharmacogenetics.Entities.MainModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pharma.Console
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {



                openFileDialog1.ShowDialog();


                //  MessageBox.Show(openFileDialog1.FileName);

                string fileName = openFileDialog1.FileName;
                var resut = ThermoFisherHelper.ParseTranslatorData(fileName);

                AppService _AppService = new AppService();

                StringBuilder sb = new StringBuilder();
                int result = 0;
                sb = ConvertListToInsertsStrings(resut);
                result = _AppService.TheUnitOfWork.ExecSQLCommand(sb.ToString());

                int count = resut.Count;



            }
            catch (Exception ex)
            {
                string ss = ex.Message;
            }

        }

        private StringBuilder ConvertListToInsertsStrings(List<ThermoResult> resut)
        {
            StringBuilder sb = new StringBuilder();
            foreach ( var s in resut)
            {
                foreach ( var d in s.AlleleValueList)
                {
                    string ss =
                        @"INSERT INTO dbo.ThermoResult
        ( 
          PateintName ,
          GeneName ,
          AlleleSymbol
        )
VALUES  ( 
                N'" + s.PateintName + "'," +
                "'" + s.GeneName + "'," +
                "'" + d + "')";
                    sb.Append(ss);
                }
            }
            return sb;
        }

        private void btnInjectDatabase_Click(object sender, EventArgs e)
        {


            try
            {

                openFileDialog1.ShowDialog();
                string fileName = openFileDialog1.FileName;

                var resut = ThermoFisherHelper.ParseRecommenationData(fileName);

                AppService _AppService = new AppService();

                StringBuilder sb = new StringBuilder();
                int result = 0;
                //sb = ConvertGeneDrugListListToInsertsStrings(resut);
                //result = _AppService.TheUnitOfWork.ExecSQLCommand(sb.ToString());

                //int count = resut.Count;



            }
            catch (Exception ex)
            {
                string ss = ex.Message;
            }


        }

        private StringBuilder ConvertGeneDrugListListToInsertsStrings(List<GeneDrugMatrix> resut)
        {

            StringBuilder sb = new StringBuilder();
            foreach (var s in resut)
            {


                    //string ss =
                    //@"INSERT INTO dbo.ThermoResult
                    //( 
                    //PateintName ,
                    //GeneName ,
                    //AlleleSymbol
                    //)
                    //VALUES  ( 
                    //N'" + s.PateintName + "'," +
                    //"'" + s.GeneName + "'," +
                    //"'" + d + "')";
                    //sb.Append(ss);


            }
            return sb;


        }





        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                PhenoTypeRecommendationService.SavePhenoTypeRecommendation(openFileDialog1);
                MessageBox.Show("records are added");
            }
            catch (Exception ex)
            {
                string ss = ex.Message;

                MessageBox.Show(ex.Message);
            }
        }

        private string GetPhenoTypeName(PhenoTypeRecommendation item)
        {

            //if (item.RecommendationKey.Contains("Coded Genotype/Phenotype Summary"))
                return item.RecommendationValue;
        }



        private void button3_Click(object sender, EventArgs e)
       
        {
            try
            {
                MatrixRecommendationService.SaveMatrixRecommendation(openFileDialog1);
                MessageBox.Show( " records are added");
            }
            catch (Exception ex)
            {
                string ss = ex.Message;

                MessageBox.Show(ex.Message);
            }
        }

    }
}
