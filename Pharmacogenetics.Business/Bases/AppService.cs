﻿using Pharmacogenetics.BaseClasses.Classes;
using Pharmacogenetics.Business.AppServices;
using Pharmacogenetics.EntityService.Bases;
//using Alterna.Data.Entities.AlternaEntityService.IRepositories;
//using Pharmacogenetics.Data.Entities.PharmaEntitySvcInterface.IBases;
using Pharmacogenetics.EntityService.Repositories;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Pharmacogenetics.Business.Bases
{
    public class AppService : BaseAppService, System.IDisposable
    {
        public UnitOfWork TheUnitOfWork { get; set; }
        AppServiceManager _appServiceManger;

        public AppServiceManager AppServiceManager
        {
            get
            {
                if (_appServiceManger == null)
                    _appServiceManger = new AppServiceManager(TheUnitOfWork);
                return _appServiceManger;
            }
        }
        public AppService()
        {
            TheUnitOfWork = new UnitOfWork();
        }

        public AppService(UnitOfWork unitOfWork)
        {
            TheUnitOfWork = unitOfWork;
        }

        #region Dispose
        public void Dispose()
        {
            TheUnitOfWork.Dispose();
        }
        #endregion
    }
}
