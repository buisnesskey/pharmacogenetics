﻿using Pharmacogenetics.Business.AppServices;
using Pharmacogenetics.EntityService.Bases;
//using Pharmacogenetics.Data.Entities.PharmaEntitySvcInterface.IBases;
//using Alterna.Data.Entities.AlternaEntityService.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pharmacogenetics.Business.Interfaces
{
    public interface IAppServiceManager
    {
        #region Props

        UnitOfWork TheUnitOfWork { get; set; }
        #endregion


        #region Services Props
        PatientAppService PatientAppService { get;  }
        #endregion
    }
}
