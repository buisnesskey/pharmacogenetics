﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Pharmacogenetics.Business.Bases;
using Pharmacogenetics.Entities.MainModel;
using Pharmacogenetics.EntityService.Bases;
using Pharmacogenetics.Models.ViewModels;
using Pharmacogenetics.BaseClasses.Enums;
using Newtonsoft.Json;
using System.Data.Entity.Infrastructure;
using System.Data.Entity;
using System.IO;
using Pharmacogenetics.Business.Containers;
using System.Web;
using System.Data;
using Pharmacogenetics.Helper;
using System.Data.SqlClient;
using System.ComponentModel;

namespace Pharmacogenetics.Business.AppServices
{
    public class ProjectAppService : AppService
    {
        AppServiceManager appServiceManager = new AppServiceManager();

        #region CTOR

        public ProjectAppService()
            : base()
        {
        }

        public ProjectAppService(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        #endregion

        #region Methods
        public List<ProjectViewModel> GetAllProjects()
        {
            List<ProjectViewModel> projects = appServiceManager.TheUnitOfWork.Project.GetWhere(c => c.DeleteStatus == 1).Select(p => new ProjectViewModel { ProjectID = p.ProjectID, ProjectName = p.ProjectName, ProjectDescription = p.ProjectDescription, ProjectNotes = p.ProjectNotes, CreateDate = p.CreateDate }).ToList();
            return projects;
        }

        public bool CreateProject(ProjectViewModel projectViewModel)
        {
            bool operationStatus = false;
            //create one time value to all records (Project and files)
            DateTime CreateDateTime = DateTime.Now;
            //create project object
            Project project = new Project()
            {
                CreateDate = CreateDateTime,
                ProjectName = projectViewModel.ProjectName,
                ProjectDescription = projectViewModel.ProjectDescription,
                ProjectNotes = projectViewModel.ProjectNotes,
                DeleteStatus = (byte)CommonEnums.DeleteStatus.NotDeleted,
            };
            //create files objetcs and inject it in the project object
            projectViewModel.File.ForEach(x =>
            {
                project.File.Add(
                    new Pharmacogenetics.Entities.MainModel.File()
                    {
                        CreateDate = CreateDateTime,
                        FilePath = x.FilePath,
                        FileType = x.FileType,
                        DeleteStatus = (byte)CommonEnums.DeleteStatus.NotDeleted,
                    }
                );
            });
            //store it in the database
            appServiceManager.TheUnitOfWork.Project.Insert(project);
            operationStatus = appServiceManager.TheUnitOfWork.Commit() > 0;
            //add new id of the project in the passed project view model
            if (operationStatus)
                projectViewModel.ProjectID = project.ProjectID;
            return operationStatus;
        }

        public bool EditProject(ProjectViewModel projectViewModel)
        {
            bool operationStatus = false;
            if (projectViewModel.ProjectID > 0)
            {
                DateTime ModifyDateTime = DateTime.Now;
                Project project = TheUnitOfWork.Project.GetWhere(x => x.ProjectID == projectViewModel.ProjectID, "File").FirstOrDefault();
                project.ModifyDate = ModifyDateTime;
                project.ProjectName = projectViewModel.ProjectName;
                project.ProjectDescription = projectViewModel.ProjectDescription;
                project.ProjectNotes = projectViewModel.ProjectNotes;

                if (projectViewModel.File != null && projectViewModel.File.Count > 0)
                {
                    projectViewModel.File.ForEach(x =>
                    {
                        Pharmacogenetics.Entities.MainModel.File file = project.File.Where(y => y.FileType == y.FileType).FirstOrDefault();
                        if (file != null)
                        {
                            file.ModifyDate = ModifyDateTime;
                            file.FilePath = x.FilePath;
                            //TheUnitOfWork.File.Delete(file);
                            //project.File.Add(
                            //    new File()
                            //    {
                            //        ModifyDate = ModifyDateTime,
                            //        FilePath = x.FilePath,
                            //        FileType = x.FileType,
                            //        DeleteStatus = (byte)CommonEnums.DeleteStatus.NotDeleted,
                            //    }
                            //);
                        }
                    });
                }
                TheUnitOfWork.Project.Update(project);
                operationStatus = TheUnitOfWork.Commit() > 0;
            }
            return operationStatus;
        }
        #endregion

        public ProjectViewModel GetProject(int id)
        {
            Project project = appServiceManager.TheUnitOfWork.Project.GetWhere(x => x.ProjectID == id, "File").FirstOrDefault();
            ProjectViewModel model = JsonConvert.DeserializeObject<ProjectViewModel>(JsonConvert.SerializeObject(project, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        })
                        );
            return model;
        }

        public bool DeleteProject(int projectId)
        {
            bool operationStatus = false;
            Project project = TheUnitOfWork.Project.GetById(projectId);
            if (project != null)
            {
                //var PatientTranslatorList = TheUnitOfWork.PatientTranslator.GetWhere(p => p.ProjectID == projectId).ToList();
                //TheUnitOfWork.PatientTranslator.DeleteList(PatientTranslatorList);

                //var TranslatorList = TheUnitOfWork.Translator.GetWhere(p => p.ProjectID == projectId).ToList();
                //TheUnitOfWork.Translator.DeleteList(TranslatorList);

                //var PatientLabList = TheUnitOfWork.PatientLab.GetWhere(p => p.ProjectID == projectId).ToList();
                //TheUnitOfWork.PatientLab.DeleteList(PatientLabList);



                //var FileList = TheUnitOfWork.File.GetWhere(p => p.ProjectID == projectId).ToList();
                //TheUnitOfWork.File.DeleteList(FileList);


                int result = 0;

                result += TheUnitOfWork.ExecSQLCommand("DELETE  FROM dbo.Translator WHERE ProjectID="+projectId);
                result += TheUnitOfWork.ExecSQLCommand("DELETE  FROM dbo.PatientLab WHERE ProjectID=" + projectId);
                result += TheUnitOfWork.ExecSQLCommand("DELETE  FROM dbo.PatientTranslator WHERE ProjectID=" + projectId);
                result += TheUnitOfWork.ExecSQLCommand("DELETE  FROM dbo.[File] WHERE ProjectID=" + projectId);
                result += TheUnitOfWork.ExecSQLCommand("DELETE  FROM dbo.Project WHERE ProjectID=" + projectId);



                // db.SaveChanges();

                //project.DeleteStatus = (byte)Pharmacogenetics.BaseClasses.Enums.CommonEnums.DeleteStatus.Deleted;
                //TheUnitOfWork.Project.Update(project);


                //operationStatus = TheUnitOfWork.Commit() > 0;
            }
            return operationStatus;
        }
        /// <summary>
        /// parsing, analysis, comparing and storing the project's data files
        /// </summary>
        /// <param name="projectViewModel"></param>
        /// <param name="uploadStatusKey"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public bool CreateProjectDetails(ProjectViewModel projectViewModel, string uploadStatusKey, List<string> status)
        {
            bool operationStatus = false;
            try
            {
                //Get patient file from the files the included in the project
                string filePathPatientData = projectViewModel.File.Where(x => x.FileType == (byte)CommonEnums.ProjectFilesEnums.filePatient).Select(x => x.FilePath).FirstOrDefault();
                //Get copy file from the files the included in the project
                string filePathCopyNumber = projectViewModel.File.Where(x => x.FileType == (byte)CommonEnums.ProjectFilesEnums.fileCopyNumber).Select(x => x.FilePath).FirstOrDefault();
                //Get patient file from the files the included in the project
                List<string> filePathTranslatorList = projectViewModel.File.Where(x => x.FileType == (byte)CommonEnums.ProjectFilesEnums.fileTranslator).Select(x => x.FilePath).ToList();
                //Add status list in the application context to make any process to log in this status
                System.Web.HttpContext.Current.Application.Add("listStatus" + uploadStatusKey, new List<string>());


                //prepare object contain all stored data (Patients, Gene, Assay, calls and allele) from the database and Patient lab and translator data for the current project in (Edit mode)
                ProjectEntities projectEntities = new ProjectEntities();
                projectEntities.allPateintList = TheUnitOfWork.Patient.GetAll().ToList();
                projectEntities.allGeneList = TheUnitOfWork.Gene.GetAll().ToList();
                projectEntities.allAssayList = TheUnitOfWork.Assay.GetAll("Gene").ToList();
                projectEntities.allCallList = TheUnitOfWork.Call.GetAll().ToList();
                projectEntities.allPatientLabList = projectViewModel.ProjectID > 0 ? TheUnitOfWork.PatientLab.GetWhere(x => x.ProjectID == projectViewModel.ProjectID).ToList() : new List<PatientLab>();

                projectEntities.allTranslatorLabList = TheUnitOfWork.Translator.GetWhere(x => x.ProjectID == projectViewModel.ProjectID).ToList();
                projectEntities.allAlleleList = TheUnitOfWork.Allele.GetAll().ToList();

                //create object with new lists (empty) to record the new data from (Patients, Gene, Assay, calls, allele, Patient lab and translator data) and 
                ProjectEntities NewProjectEntities = new ProjectEntities();
                NewProjectEntities.allPateintList = new List<Patient>();
                NewProjectEntities.allGeneList = new List<Gene>();
                NewProjectEntities.allAssayList = new List<Assay>();
                NewProjectEntities.allCallList = new List<Call>();
                NewProjectEntities.allTranslatorLabList = new List<Translator>();
                NewProjectEntities.allAlleleList = new List<Allele>();
                NewProjectEntities.allPatientLabList = new List<PatientLab>();



                //Process translator
                #region  Translator

                System.Web.HttpContext ctx = System.Web.HttpContext.Current;
                foreach (var currentFile in filePathTranslatorList)
                {
                    AppServiceManager.TranslatorAppService.ProcessFile(currentFile, uploadStatusKey, ctx, projectViewModel, ref status, projectEntities, ref NewProjectEntities, TheUnitOfWork);

                }

                #region Inserts as strings  without pagesize
                InsertWithSqlCommandsApproach(status, uploadStatusKey, NewProjectEntities.allTranslatorLabList);
                #endregion

                #region Insert With OpenRowset Approach
                //InsertWithOpenRowsetApproach(NewProjectEntities.allTranslatorLabList);

                #endregion

                #region Inserts as strings  with page size
                //InsertWithSqlCommandsWithPageSizeApproach(status, NewProjectEntities.allTranslatorLabList, uploadStatusKey);
                #endregion

                #region Stored Procedure

                //List<SqlParameter> paramList = new List<SqlParameter>();
                //SqlParameter param1 = new SqlParameter("@ExcelPath", newFilePathAndName);
                //paramList.Add(param1);

                //var param = new SqlParameter("@ExcelPath", SqlDbType.VarChar) { Value = newFilePathAndName };

                //var result = TheUnitOfWork.ExecSQLCommand(@"INSERT INTO Translator
                //        (GeneID,AlleleID,AssayID,CallID,DeleteStatus,CreateDate,ProjectID)
                //        SELECT GeneID,AlleleID,AssayID,CallID,DeleteStatus,GETDATE(),ProjectID
                //        FROM OPENROWSET('Microsoft.ACE.OLEDB.12.0',
                //        'Excel 12.0; Database=" + newFilePathAndName + @"', 'SELECT * FROM [Sheet0$]')");
                //projectEntities.allTranslatorLabList = TheUnitOfWork.Translator.GetWhere(t => t.ProjectID == projectViewModel.ProjectID).ToList();


                #endregion

                #region for loop defualt approach 
                // InsertWithAddListApproach(NewProjectEntities.allTranslatorLabList);
                #endregion



                #region Retrieving Translator Data

                updateSessionStatus(status, string.Format("Retrieving Translator Data........"), uploadStatusKey);
               // projectEntities.allTranslatorLabList = TheUnitOfWork.Translator.GetWhere(t => t.ProjectID == projectViewModel.ProjectID).ToList();


                projectEntities.allTranslatorLabList = null;
                projectEntities.allTranslatorLabList = NewProjectEntities.allTranslatorLabList; 

                #endregion



                // after that all projectEntities properties must be intialized and loaded with data

                #endregion

                //Process patients data 
                #region Patient Lap
                List<PatientResultViewModel> patientList = AppServiceManager.PatientAppService.ParsePatientData(filePathPatientData, status, uploadStatusKey);
                updateSessionStatus(status, "Analysis patients", uploadStatusKey);
                updateSessionStatus(status, "Analysis patients", uploadStatusKey);
                List<string> allStatusLog = new List<string>();
                List<string> statusLog = new List<string>();
                //comparing the new data of patients, genes, assays and calls with the actual stored data in the database
               AppServiceManager.PatientAppService.StoreData(patientList, statusLog, status, uploadStatusKey, projectViewModel, projectEntities);

                #endregion

                operationStatus = true;

                #region Execute Comparing and copy number
                if (operationStatus)
                {

                    ProjectEntities _ProjectEntities = new ProjectEntities();

                    _ProjectEntities.allAlleleList = TheUnitOfWork.Allele.GetAll().ToList();
                    _ProjectEntities.allAssayList = TheUnitOfWork.Assay.GetAll().ToList();
                    _ProjectEntities.allCallList = TheUnitOfWork.Call.GetAll().ToList();
                    _ProjectEntities.allGeneList = TheUnitOfWork.Gene.GetAll().ToList();
                    _ProjectEntities.allPateintList = TheUnitOfWork.Patient.GetAll().ToList();

                    _ProjectEntities.allPatientLabList = TheUnitOfWork.PatientLab.GetWhere(p => p.ProjectID == projectViewModel.ProjectID).ToList();
                    _ProjectEntities.allTranslatorLabList = TheUnitOfWork.Translator.GetWhere(p => p.ProjectID == projectViewModel.ProjectID).ToList();

                    //Updated-Khaled

                    #region CopyNumber

                    if (!string.IsNullOrWhiteSpace(filePathCopyNumber) && System.IO.File.Exists(filePathCopyNumber))
                    {
                        List<PatientCopyNumberAnalysisViewModel> patientCopyNumberList = AppServiceManager.PatientAppService.ParseCopyNumberData(filePathCopyNumber, status, uploadStatusKey);
                        AppServiceManager.PatientAppService.StoreCopyNumberData(projectViewModel.ProjectID, projectEntities, patientCopyNumberList, status, uploadStatusKey);

                    }
                    #endregion



                    operationStatus = AppServiceManager.PatientTranslatorAppService.ExecuteComparing(status, System.Web.HttpContext.Current, uploadStatusKey, projectViewModel, ref _ProjectEntities);

                    projectEntities.allPatientTranslatorList = _ProjectEntities.allPatientTranslatorList;
                }


                #endregion


                updateSessionStatus(status, string.Format("Project link: <a href='../../PatientManagement/Patient/PatientOutput?projectId=" + projectViewModel.ProjectID + "'>" + projectViewModel.ProjectName + "</a>"), uploadStatusKey);
                updateSessionStatus(status, string.Format("Complete"), uploadStatusKey);
                operationStatus = true;
            }
            catch (Exception ex)
            {
                updateSessionStatus(status, ex.Message, uploadStatusKey);
                updateSessionStatus(status, "Error", uploadStatusKey);
            }
            return operationStatus;
        }

        private void InsertWithSqlCommandsWithPageSizeApproach(List<string> status , List<Translator> allTranslatorLabList , string uploadStatusKey)
        {

            StringBuilder sbWithPageSize = new StringBuilder();
            int resultWithPageSize = 0;

            updateSessionStatus(status, string.Format("Saving Translator........"), uploadStatusKey);


            #region Loop

            int pageSize = 10000;

            if (allTranslatorLabList.Count >= pageSize)
            {

                int startIndex = 0;
                int endIndex = 0;

                int total = allTranslatorLabList.Count;
                int remainder = 0;




                remainder = total % pageSize;
                int iterationNumber = total / pageSize;

                for (int i = 0; i < iterationNumber; i++)
                {

                    startIndex = endIndex;
                    endIndex = startIndex + pageSize;

                    var listIteration = allTranslatorLabList.Skip(startIndex).Take(endIndex - startIndex).ToList();


                    sbWithPageSize = ConvertTranslatorListToInsertsStrings(listIteration);
                    resultWithPageSize = TheUnitOfWork.ExecSQLCommand(sbWithPageSize.ToString());


                }


                startIndex = endIndex;
                endIndex = startIndex + remainder + 1;


                var listReaminder = allTranslatorLabList.Skip(startIndex).Take(endIndex - startIndex).ToList();

                sbWithPageSize = ConvertTranslatorListToInsertsStrings(listReaminder);
                resultWithPageSize = TheUnitOfWork.ExecSQLCommand(sbWithPageSize.ToString());



            }
            else
            {
                sbWithPageSize = ConvertTranslatorListToInsertsStrings(allTranslatorLabList);
                resultWithPageSize = TheUnitOfWork.ExecSQLCommand(sbWithPageSize.ToString());

            }


            #endregion

            updateSessionStatus(status, string.Format("Saving Translator Complete........"), uploadStatusKey);
        }

        private void InsertWithOpenRowsetApproach(List<Translator> allTranslatorLabList)
        {

            #region Convert List To Datatable
            DataTable dt = new DataTable();

            dt.Columns.Add("GeneID", typeof(string));
            dt.Columns.Add("AlleleID", typeof(string));
            dt.Columns.Add("AssayID", typeof(string));
            dt.Columns.Add("CallID", typeof(string));
            dt.Columns.Add("DeleteStatus", typeof(string));
            dt.Columns.Add("CreateDate", typeof(string));
            dt.Columns.Add("ModifyDate", typeof(string));
            dt.Columns.Add("DeleteDate", typeof(string));
            dt.Columns.Add("ProjectID", typeof(string));
            //
            //
            //
            if (allTranslatorLabList != null)
            {
                foreach (Translator translator in allTranslatorLabList)
                {
                    dt.Rows.Add(translator.GeneID,
                    translator.AlleleID,
                    translator.AssayID,
                    translator.CallID,
                    translator.DeleteStatus,
                    translator.CreateDate,
                    translator.ModifyDate,
                    translator.DeleteDate,
                    translator.ProjectID);
                }
            }
            #endregion

            #region Generate File Name

            var uploadDirectory = "~/uploads";
            var newFileName = "ExportedTranslator" + DateTime.Now.ToFileTime() + ".xlsx";
            var newFilePathAndName = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(uploadDirectory), newFileName);
            string exportedFileName = "";
            bool exportOperationStatus = ExportFile.GenerateExcelSheetWithoutDownload(dt, newFilePathAndName, out exportedFileName);

            #endregion



        }


        //List<string> status, string currentStatus, string uploadStatusKey, bool removeLastRecord = false
        private void InsertWithSqlCommandsApproach(List<string> status, string uploadStatusKey, List<Translator> allTranslatorLabList)
        {

            StringBuilder sb = new StringBuilder();
            int result = 0;

            updateSessionStatus(status, string.Format("Saving Translator........"), uploadStatusKey);

            sb = ConvertTranslatorListToInsertsStrings(allTranslatorLabList);
            result = TheUnitOfWork.ExecSQLCommand(sb.ToString());

            updateSessionStatus(status, string.Format("Saving Translator Complete........"), uploadStatusKey);
        }

        private void InsertWithAddListApproach(List<Translator> allTranslatorLabList)
        {

            int pageSize = 10000;

            if (allTranslatorLabList.Count >= pageSize)
            {

                int startIndex = 0;
                int endIndex = 0;

                int total = allTranslatorLabList.Count;
                int remainder = 0;




                remainder = total % pageSize;
                int iterationNumber = total / pageSize;

                for (int i = 0; i < iterationNumber; i++)
                {

                    startIndex = endIndex;
                    endIndex = startIndex + pageSize;

                    var listIteration = allTranslatorLabList.Skip(startIndex).Take(endIndex - startIndex).ToList();

                    //TestCountTranslatorLabList.AddRange(listIteration);

                    TheUnitOfWork.Translator.InsertList(listIteration);
                    TheUnitOfWork.Commit();

                }


                startIndex = endIndex;
                endIndex = startIndex + remainder + 1;


                var listReaminder = allTranslatorLabList.Skip(startIndex).Take(endIndex - startIndex).ToList();
                //TestCountTranslatorLabList.AddRange(listReaminder);

                TheUnitOfWork.Translator.InsertList(listReaminder);
                TheUnitOfWork.Commit();

            }
            else
            {
                TheUnitOfWork.Translator.InsertList(allTranslatorLabList);
                TheUnitOfWork.Commit();
            }



        }

        private StringBuilder ConvertTranslatorListToInsertsStrings(List<Translator> allTranslatorLabList)
        {

            StringBuilder sb = new StringBuilder();


            foreach (var t in allTranslatorLabList)
            {

                    string insertStatment =
                    @"INSERT INTO Translator
                    (GeneID,
                    AlleleID,
                    AssayID,
                    CallID,
                    DeleteStatus,
                    CreateDate,
                    ProjectID ,
                    AlleleRowIndex) 
                    Values 
                    (" +
                    t.GeneID.ToString() +','+
                    t.AlleleID.ToString() + ',' +
                    t.AssayID.ToString() + ',' +
                    t.CallID + ',' +
                    t.DeleteStatus.ToString() + ',' +
                    t.CreateDate.ToString("yyyy-MM-dd") + ',' +
                    t.ProjectID + ',' +
                    t.AlleleRowIndex
                    +")";

                    sb.Append(insertStatment);

            }

            return sb;





        }

        public void updateSessionStatus(List<string> status, string currentStatus, string uploadStatusKey, bool removeLastRecord = false)
        {
            if (removeLastRecord)
                status.RemoveAt(status.Count - 1);
            status.Add(currentStatus);
            System.Web.HttpContext.Current.Application["listStatus" + uploadStatusKey] = status;
        }



        public static void InsertIntoMembers(DataTable dataTable)
        {
                    using (var connection = new SqlConnection(@"data source=;persist security info=True;user id=sa;password=root;initial catalog=PharmacoGeneticsDB;
                    MultipleActiveResultSets=True;App=EntityFramework"))
            {
                SqlTransaction transaction = null;
                connection.Open();
                try
                {
                    transaction = connection.BeginTransaction();
                    using (var sqlBulkCopy = new SqlBulkCopy(connection, SqlBulkCopyOptions.TableLock, transaction))
                    {
                        sqlBulkCopy.DestinationTableName = "Members";
                        sqlBulkCopy.ColumnMappings.Add("Firstname", "Firstname");
                        sqlBulkCopy.ColumnMappings.Add("Lastname", "Lastname");
                        sqlBulkCopy.ColumnMappings.Add("DOB", "DOB");
                        sqlBulkCopy.ColumnMappings.Add("Gender", "Gender");
                        sqlBulkCopy.ColumnMappings.Add("Email", "Email");

                        sqlBulkCopy.ColumnMappings.Add("Address1", "Address1");
                        sqlBulkCopy.ColumnMappings.Add("Address2", "Address2");
                        sqlBulkCopy.ColumnMappings.Add("Address3", "Address3");
                        sqlBulkCopy.ColumnMappings.Add("Address4", "Address4");
                        sqlBulkCopy.ColumnMappings.Add("Postcode", "Postcode");

                        sqlBulkCopy.ColumnMappings.Add("MobileNumber", "MobileNumber");
                        sqlBulkCopy.ColumnMappings.Add("TelephoneNumber", "TelephoneNumber");

                        sqlBulkCopy.ColumnMappings.Add("Deleted", "Deleted");

                        sqlBulkCopy.WriteToServer(dataTable);
                    }
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                }

            }
        }

    }
}
