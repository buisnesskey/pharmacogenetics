﻿using Newtonsoft.Json;
using Pharmacogenetics.Models.ViewModels;
using Pharmacogenetics.Models.ViewModels.ImportViewModels;
using Pharmacogenetics.Entities.MainModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Web;
using System.Diagnostics;
using Pharmacogenetics.EntityService.Bases;
using Pharmacogenetics.Business.Bases;
using Pharmacogenetics.Business.Containers;
using Pharmacogenetics.BaseClasses.Enums;

namespace Pharmacogenetics.Business.AppServices
{
    public class PatientTranslatorService : AppService
    {

        #region CTOR

        public PatientTranslatorService() : base()
        {
        }

        public PatientTranslatorService(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        #endregion
        AppServiceManager appManager = new AppServiceManager();

        Pharmacogenetics.Business.AppServices.AppServiceManager appServiceManager = new Pharmacogenetics.Business.AppServices.AppServiceManager();

        public bool Result(out int PatientTranslatorInsertedCounts, out int PatientTranslatorUpdatedCounts, HttpContext ctx, List<string> status, string uploadStatusKey, ProjectViewModel _projectViewModels, ref ProjectEntities projectEntities)
        {
            PatientTranslatorInsertedCounts = 0;
            PatientTranslatorUpdatedCounts = 0;
            bool operationStatus = false;

            try
            {

                #region Test Comparing for Specific Gene
               // List<PatientLab> pationlap = appServiceManager.TheUnitOfWork.PatientLab.GetWhere(p => p.ProjectID == _projectViewModels.ProjectID && p.PatientID==17).ToList();                
                #endregion
                List<PatientLab> pationlap = appServiceManager.TheUnitOfWork.PatientLab.GetWhere(p => p.ProjectID == _projectViewModels.ProjectID   ).ToList();
                List<int> GeneIdsInpationlap = pationlap.Select(p => p.GeneID.Value).Distinct().ToList();

                var myInClause = "";

                foreach (int g in GeneIdsInpationlap)
                {
                    myInClause += g.ToString()+" ,";
                }
                 myInClause=  myInClause.Remove(myInClause.Length - 1);


                 List<Translator> translatorAll = projectEntities.allTranslatorLabList.Where(x => myInClause.Contains(x.GeneID.ToString()) && x.ProjectID == _projectViewModels.ProjectID).ToList();

                 #region Test Comparing for Specific Gene
                 //List<Translator> translatorAll = projectEntities.allTranslatorLabList.Where(x =>x.ProjectID == _projectViewModels.ProjectID&& x.GeneID==16).ToList(); 
                 //List<Translator> translatorAll = projectEntities.allTranslatorLabList.Where(x => x.GeneID == 35 && x.AlleleID == 359 && x.ProjectID == _projectViewModels.ProjectID).ToList();

                 #endregion



               
                var pationlapGroupByPatientAndGene = pationlap.OrderBy(s=>s.GeneID).GroupBy(p => new { p.PatientID, p.GeneID }).ToList();

                PatientTranslatorViewModel _PatientTranslatorViewModel;
                int PatientTranslatorIter = 1;
                
                List<PatientTranslator> PatientTranslatorListToInsert = new List<PatientTranslator>();
                List<PatientTranslator> PatientTranslatorListToUpdate = new List<PatientTranslator>();


                List<Call> CallListOfDatabase = projectEntities.allCallList;  //appServiceManager.TheUnitOfWork.Call.GetAll().ToList();


                foreach (var patientlapObject in pationlapGroupByPatientAndGene)
                {

                    PatientTranslatorIter++;
                    // clearSessionStatus();

                    //string patientName = appServiceManager.TheUnitOfWork.Patient.GetWhere(p => p.PatientID == patientlapObject.Key.PatientID).FirstOrDefault().PatientName;
                    string patientName = projectEntities.allPateintList.Where(p => p.PatientID == patientlapObject.Key.PatientID).FirstOrDefault().PatientName;
                    string geneName_patient = projectEntities.allGeneList.Where(g => g.GeneID == patientlapObject.Key.GeneID).FirstOrDefault().GeneSymbol;  //appServiceManager.TheUnitOfWork.Gene.GetWhere(g => g.GeneID == patientlapObject.Key.GeneID).FirstOrDefault().GeneSymbol;

                    string textStatus = string.Format("Progress {0} %", Math.Round(((double)(PatientTranslatorIter - 1) / pationlapGroupByPatientAndGene.Count) * 100, 2));
                    textStatus += "\t" + string.Format("Compare  data for Patient {0} Gene {1}", patientName, geneName_patient);
                    updateSessionStatus(status, textStatus, ctx, uploadStatusKey, true);
                    List<PatientTranslatorViewModel> PatientTranslatorProbability = null;
                    List<PatientTranslatorViewModel> PatientTranslatorForAllAlleles = null;
                    List<Translator> translatorList = translatorAll.Where(t => t.GeneID == patientlapObject.Key.GeneID).ToList();


                    if (translatorList == null || translatorList.Count == 0)
                    {
                        var geneNamesList = projectEntities.allGeneList.Where(g => g.GeneSymbol.ToUpper().Contains(geneName_patient.ToUpper())).ToList();
                        foreach (var gene in geneNamesList)
                        {
                            List<Translator> translatorListModifiedGene = translatorAll.Where(t => t.GeneID == gene.GeneID).ToList();
                            if (translatorListModifiedGene != null && translatorListModifiedGene.Count > 0)
                            {
                                #region code 
                                //_PatientTranslatorViewModel = BuildPatientTranslator(patientlapObject.ToList(), translatorListModifiedGene, CallListOfDatabase, patientlapObject.Key.GeneID, patientlapObject.Key.PatientID, out PatientTranslatorProbability, _projectViewModels);
                                _PatientTranslatorViewModel = BuildPatientTranslatorModified(patientlapObject.ToList(), translatorListModifiedGene, CallListOfDatabase, patientlapObject.Key.GeneID, patientlapObject.Key.PatientID, out PatientTranslatorProbability, _projectViewModels , out PatientTranslatorForAllAlleles);


                                    if (PatientTranslatorForAllAlleles != null && PatientTranslatorForAllAlleles.Count > 0)
                                {
                                    foreach (var pt in PatientTranslatorForAllAlleles)
                                    {
                                        try
                                        {
                                            InsertInDataBasePatientTranslator(pt, ref PatientTranslatorInsertedCounts, ref PatientTranslatorUpdatedCounts, ref PatientTranslatorListToInsert, ref PatientTranslatorListToUpdate);
                                        }
                                        catch (Exception ex) { continue; }
                                    }
                                }


                              else  if (_PatientTranslatorViewModel != null && _PatientTranslatorViewModel.AlleleID.HasValue && !_PatientTranslatorViewModel.Probability)
                                {
                                    try
                                    {
                                        InsertInDataBasePatientTranslator(_PatientTranslatorViewModel, ref PatientTranslatorInsertedCounts, ref PatientTranslatorUpdatedCounts, ref PatientTranslatorListToInsert, ref PatientTranslatorListToUpdate);
                                    }
                                    catch (Exception ex) { continue; }
                                }

                            

                                else if (PatientTranslatorProbability != null && PatientTranslatorProbability.Count > 0)
                                {
                                    foreach (var pt in PatientTranslatorProbability)
                                    {
                                        try
                                        {
                                            InsertInDataBasePatientTranslator(pt, ref PatientTranslatorInsertedCounts, ref PatientTranslatorUpdatedCounts, ref PatientTranslatorListToInsert, ref PatientTranslatorListToUpdate);
                                        }
                                        catch (Exception ex){continue;}
                                    }
                                }
                                else if (_PatientTranslatorViewModel != null && !_PatientTranslatorViewModel.AlleleID.HasValue)
                                {
                                    try
                                    {
                                        InsertInDataBasePatientTranslator(_PatientTranslatorViewModel, ref PatientTranslatorInsertedCounts, ref PatientTranslatorUpdatedCounts, ref PatientTranslatorListToInsert, ref PatientTranslatorListToUpdate);
                                    }
                                    catch (Exception ex) {continue;}
                                }
                                #endregion
                            }
                        }
                    }
                                else
                                {
                        #region BuildAndInsert
                        //_PatientTranslatorViewModel = BuildPatientTranslator(patientlapObject.ToList(), translatorList, CallListOfDatabase, patientlapObject.Key.GeneID, patientlapObject.Key.PatientID, out PatientTranslatorProbability, _projectViewModels);
                        _PatientTranslatorViewModel = BuildPatientTranslatorModified(patientlapObject.ToList(), translatorList, CallListOfDatabase, patientlapObject.Key.GeneID, patientlapObject.Key.PatientID, out PatientTranslatorProbability, _projectViewModels, out PatientTranslatorForAllAlleles);

                        if (PatientTranslatorForAllAlleles != null && PatientTranslatorForAllAlleles.Count > 0)
                        {
                            foreach (var pt in PatientTranslatorForAllAlleles)
                            {
                                try
                                {
                                    InsertInDataBasePatientTranslator(pt, ref PatientTranslatorInsertedCounts, ref PatientTranslatorUpdatedCounts, ref PatientTranslatorListToInsert, ref PatientTranslatorListToUpdate);
                                }
                                catch (Exception ex) { continue; }
                            }
                        }


                      else  if (_PatientTranslatorViewModel != null && _PatientTranslatorViewModel.AlleleID.HasValue && ! _PatientTranslatorViewModel.Probability)

                        {
                            try
                            {
                                InsertInDataBasePatientTranslator(_PatientTranslatorViewModel, ref PatientTranslatorInsertedCounts, ref PatientTranslatorUpdatedCounts, ref PatientTranslatorListToInsert, ref PatientTranslatorListToUpdate);
                            }
                            catch (Exception ex) { continue; }
                        }



                        else  if (PatientTranslatorProbability != null && PatientTranslatorProbability.Count > 0)
                                {
                                    foreach (var pt in PatientTranslatorProbability)
                                    {
                                        try
                                        {
                                            InsertInDataBasePatientTranslator(pt, ref PatientTranslatorInsertedCounts, ref PatientTranslatorUpdatedCounts, ref PatientTranslatorListToInsert, ref PatientTranslatorListToUpdate);
                                        }
                                        catch (Exception ex)
                                        {
                                            continue;
                                        }
                                    }
                                }
                        else if (_PatientTranslatorViewModel != null && !_PatientTranslatorViewModel.AlleleID.HasValue)
                        {
                                    try
                                    {
                                        InsertInDataBasePatientTranslator(_PatientTranslatorViewModel, ref PatientTranslatorInsertedCounts, ref PatientTranslatorUpdatedCounts, ref PatientTranslatorListToInsert, ref PatientTranslatorListToUpdate);
                                    }
                                    catch (Exception ex)
                                    {
                                        continue;
                                    }
                                } 
                                #endregion
                                }
                 }
                #region Insert
                int countPatientTranslatorListToInsert = PatientTranslatorListToInsert.Count;
                int indexPatientTranslatorInsertIter = 0;

                #region Commented code
                //foreach (var _PatientTranslator in PatientTranslatorListToInsert)
                //{
                //    indexPatientTranslatorInsertIter++;

                //    string textStatusInsert = string.Format("Progress {0} %", Math.Round(((double)(indexPatientTranslatorInsertIter - 1) / countPatientTranslatorListToInsert) * 100, 2));
                //    textStatusInsert += "\t" + string.Format("insert  data for item {0}", indexPatientTranslatorInsertIter.ToString());
                //    updateSessionStatus(status, textStatusInsert, ctx, uploadStatusKey, true);

                //    appServiceManager.TheUnitOfWork.PatientTranslator.Insert(_PatientTranslator);
                //} 
                
                #endregion
                #endregion

                #region Update
                int countPatientTranslatorListToUpdate = PatientTranslatorListToUpdate.Count;
                int indexPatientTranslatorUpdateIter = 0;

                #region Commented code
                //foreach (var _PatientTranslator in PatientTranslatorListToUpdate)
                //{
                //    indexPatientTranslatorUpdateIter++;
                //    string textStatusInsert = string.Format("Progress {0} %", Math.Round(((double)(indexPatientTranslatorUpdateIter - 1) / countPatientTranslatorListToUpdate) * 100, 2));
                //    textStatusInsert += "\t" + string.Format("Update  data for item {0}", indexPatientTranslatorUpdateIter.ToString());
                //    updateSessionStatus(status, textStatusInsert, ctx, uploadStatusKey, true);


                //var obj=    appServiceManager.TheUnitOfWork.PatientTranslator.GetById(_PatientTranslator.PatientTranslatorID);
                // appServiceManager.TheUnitOfWork.PatientTranslator.Delete(obj);
                // appServiceManager.TheUnitOfWork.PatientTranslator.Insert(_PatientTranslator);

                //}
                
                #endregion


                updateSessionStatus(status, string.Format("Saving Patient Translator ........"), ctx, uploadStatusKey , true);



                #region Insert record by record Debug Mode Only
                //foreach (var pt in PatientTranslatorListToInsert)
                //{
                //    try
                //    {
                //        appServiceManager.TheUnitOfWork.PatientTranslator.Insert(pt);
                //        appServiceManager.TheUnitOfWork.Commit();

                //    }
                //    catch (Exception ex)
                //    {

                //        string ss = ex.Message;
                //    
                //}

                #endregion
                try
                {
                    //appServiceManager.TheUnitOfWork.PatientTranslator.InsertList(PatientTranslatorListToInsert);
                    //appServiceManager.TheUnitOfWork.Commit();


                    

                    InsertWithSqlCommandsApproach(status, uploadStatusKey, PatientTranslatorListToInsert);

                }
                catch (Exception ex)
                {

                   string ss = ex.Message;

                }
                updateSessionStatus(status, string.Format("Saving PatientTranslator Complete........"), ctx, uploadStatusKey);

                #endregion


                updateSessionStatus(status, string.Format("Progress 100%"), ctx, uploadStatusKey);
                updateSessionStatus(status, string.Format("Count Patient Translator Records: " + countPatientTranslatorListToInsert), ctx, uploadStatusKey, true);
                updateSessionStatus(status, string.Format("Complete Patient Translator"), ctx, uploadStatusKey);
                updateSessionStatus(status, "----------------------------------------------------------------", ctx, uploadStatusKey);

                operationStatus = true;


            }
            catch (Exception ex)
            {

                string ss = ex.Message;
                operationStatus = false;
            }



            return operationStatus;



        }


        private void InsertWithSqlCommandsApproach(List<string> status, string uploadStatusKey, List<PatientTranslator> allPatientTranslatorList)
        {
            StringBuilder sb = new StringBuilder();
            int result = 0;
            sb = ConvertPatientTranslatorListToInsertsStrings(allPatientTranslatorList);
            result = TheUnitOfWork.ExecSQLCommand(sb.ToString());
        }

        private StringBuilder ConvertPatientTranslatorListToInsertsStrings(List<PatientTranslator> allPatientTranslatorList)
        {



            StringBuilder sb = new StringBuilder();


            foreach (var t in allPatientTranslatorList)
            {

                    string insertStatment =
                    @"INSERT INTO PatientTranslator
                    ( PatientID 
                    , GeneID 
                    , AlleleID 
                    , Swap 
                    , DeleteStatus 
                    , CreateDate 
                    , ProjectID 
                    , Probability 
                    , CopyNumber )
                    Values 
                    (" +
                    t.PatientID.ToString() + ',' +
                    t.GeneID.ToString() + ',' +
                    (t.AlleleID.HasValue ? t.AlleleID.Value.ToString() : "NULL") +
                    ',' +
                     "'"+ t.Swap +"'"+ ',' +
                    t.DeleteStatus.ToString() + ',' +
                    t.CreateDate.ToString("yyyy-MM-dd") + ',' +
                    t.ProjectID +","+
                    "'" + t.Probability + "'" + ',' +
                   ( t.CopyNumber.HasValue?t.CopyNumber.Value.ToString():"NULL") +
                    ")";

                sb.Append(insertStatment);

            }

            return sb;
        }



        //private void BuildAndInsert(List<PatientLab> PatientLabAssayCallList, List<Translator> translatorList,     List<Call> CallListOfDatabase ,  int? PatientGeneId, int? PateintId, out  List<PatientTranslatorViewModel> PatientTranslatorProbability, ProjectViewModel _projectViewModels)
        //{

        //    _PatientTranslatorViewModel = BuildPatientTranslator(patientlapObject.ToList(), translatorList, CallListOfDatabase, patientlapObject.Key.GeneID, patientlapObject.Key.PatientID, out PatientTranslatorProbability, _projectViewModels);
        //    if (PatientTranslatorProbability != null && PatientTranslatorProbability.Count > 0)
        //    {
        //        foreach (var pt in PatientTranslatorProbability)
        //        {
        //            try
        //            {
        //                InsertInDataBasePatientTranslator(pt, ref PatientTranslatorInsertedCounts, ref PatientTranslatorUpdatedCounts, ref PatientTranslatorListToInsert, ref PatientTranslatorListToUpdate);
        //            }
        //            catch (Exception ex)
        //            {
        //                continue;
        //            }
        //        }
        //    }0
        //    else
        //    {
        //        try
        //        {
        //            InsertInDataBasePatientTranslator(_PatientTranslatorViewModel, ref PatientTranslatorInsertedCounts, ref PatientTranslatorUpdatedCounts, ref PatientTranslatorListToInsert, ref PatientTranslatorListToUpdate);
        //        }
        //        catch (Exception ex)
        //        {
        //            continue;
        //        }
        //    }

        //}

        private void InsertInDataBasePatientTranslator(PatientTranslatorViewModel _PatientTranslatorViewModel, ref int PatientTranslatorInsertedCounts, ref int PatientTranslatorUpdatedCounts, ref List<PatientTranslator> PatientTranslatorListToInsert

            , ref List<PatientTranslator> PatientTranslatorListToUpdate)
        {
            if (_PatientTranslatorViewModel != null)
            {

                PatientTranslator _PatientTranslator = new Pharmacogenetics.Entities.MainModel.PatientTranslator();
                _PatientTranslator.PatientID = _PatientTranslatorViewModel.PatientID;
                _PatientTranslator.GeneID = _PatientTranslatorViewModel.GeneID;
                _PatientTranslator.AlleleID = _PatientTranslatorViewModel.AlleleID;
                _PatientTranslator.Swap = _PatientTranslatorViewModel.Swap;
                _PatientTranslator.Probability = _PatientTranslatorViewModel.Probability;
                _PatientTranslator.ProjectID = _PatientTranslatorViewModel.ProjectID;
                _PatientTranslator.CopyNumber = _PatientTranslatorViewModel.CopyNumber;
                _PatientTranslator.CreateDate = DateTime.Now;
                _PatientTranslator.DeleteStatus = 1;



                PatientTranslator isExistPatientTranslatorObject = null;
                //appServiceManager.TheUnitOfWork.PatientTranslator.GetWhere
                //     (x => ((x.PatientID == _PatientTranslator.PatientID) && (x.GeneID == _PatientTranslator.GeneID)
                //         && (x.ProjectID == _PatientTranslator.ProjectID))).FirstOrDefault();


                //appServiceManager.TheUnitOfWork.PatientTranslator.Insert(_PatientTranslator);
                //appServiceManager.TheUnitOfWork.Commit();
                //if (!PatientTranslatorListToInsert.Contains(_PatientTranslator))
                //{
                //    PatientTranslatorListToInsert.Add(_PatientTranslator);
                //    PatientTranslatorInsertedCounts++;
                //}


                     //insert 
                    if (isExistPatientTranslatorObject == null)
                    {
                        //appServiceManager.TheUnitOfWork.PatientTranslator.Insert(_PatientTranslator);
                        //appServiceManager.TheUnitOfWork.Commit();
                            if (!PatientTranslatorListToInsert.Contains(_PatientTranslator))
                            {
                                PatientTranslatorListToInsert.Add(_PatientTranslator);
                                PatientTranslatorInsertedCounts++;
                            }
                        
                    }
                    else
                    {
                        // update
                        if (isExistPatientTranslatorObject.AlleleID != _PatientTranslator.AlleleID)
                        {
                            isExistPatientTranslatorObject.AlleleID = _PatientTranslator.AlleleID;

                            //appServiceManager.TheUnitOfWork.PatientTranslator.Update(isExistPatientTranslatorObject);
                            //appServiceManager.TheUnitOfWork.Commit();

                            if (!PatientTranslatorListToUpdate.Contains(_PatientTranslator))
                            {
                                PatientTranslatorListToUpdate.Add(_PatientTranslator);
                                PatientTranslatorUpdatedCounts++;
                            }


                        }
                    }



              

            }
        }


        public void updateSessionStatus(List<string> status, string currentStatus, HttpContext ctx, string uploadStatusKey, bool removeLastRecord = false)
        {
            if (removeLastRecord)
                status.RemoveAt(status.Count - 1);
            status.Add(currentStatus);
            System.Web.HttpContext.Current.Application["listStatus" + uploadStatusKey] = status;
        }



        #region BuildPatientTranslator Modified
        private PatientTranslatorViewModel BuildPatientTranslatorModified(List<PatientLab> PatientLabAssayCallList, 
            List<Translator> translatorList, List<Call> CallListOfDatabase, int? PatientGeneId, int? PateintId, out List<PatientTranslatorViewModel> PatientTranslatorProbability, ProjectViewModel _projectViewModels ,
            
            out List<PatientTranslatorViewModel> PatientTranslatorForAllAlleles)

        {
            try
            {
                PatientTranslatorProbability = new List<PatientTranslatorViewModel>();
                PatientTranslatorForAllAlleles = new List<PatientTranslatorViewModel>();
                bool NoAmpExist =false;
                List<Translator> TranslatorAssayCallListOrderd = new List<Translator>();
                List<int> TranslatorAssaySequence = new List<int>();
                List<int> TranslatorCallSequence = new List<int>();


                List<PatientLab> PatientLabAssayCallListOrderd = new List<PatientLab>();
                List<int> PatientAssaySequence = new List<int>();
                List<int> PatientCallSequence = new List<int>();

                PatientTranslatorViewModel _PatientTranslatorViewModel = null;
                if (translatorList == null || translatorList.Count == 0)
                {
                    _PatientTranslatorViewModel = new PatientTranslatorViewModel();
                    _PatientTranslatorViewModel.GeneID = PatientGeneId.Value;
                    _PatientTranslatorViewModel.AlleleID = null;
                    _PatientTranslatorViewModel.PatientID = PateintId.Value;
                    _PatientTranslatorViewModel.ProjectID = _projectViewModels.ProjectID;
                    return _PatientTranslatorViewModel;
                }
                var translatorListGroupedByAllell = translatorList.GroupBy(p => new { p.AlleleRowIndex ,p.AlleleID, p.GeneID}).ToList();

                int TranslatorAlleleID;
                int TranslatorGeneID;
                int TranslatorAlleleRowIndex;

                int result_TranslatorAlleleID = -1;
                int result_TranslatorGeneID = -1;


                int? CallIdForCopyNumber = PatientLabAssayCallList.Where(ct => ct.CallType == (byte)CommonEnums.CallTypeEnum.CopyNumber).Select(ct => ct.CallID).FirstOrDefault();
                string CallValueForCopyNumber = (CallIdForCopyNumber.HasValue ? CallListOfDatabase.Where(c => c.CallID == int.Parse(CallIdForCopyNumber.Value.ToString())).Select(v => v.CallValue).FirstOrDefault() : DBNull.Value.ToString());




                bool compareSucceed = false;


                for (int k = 0; k < translatorListGroupedByAllell.Count; k++)

                {

                    var translatorObject = translatorListGroupedByAllell[k];
                    TranslatorGeneID = translatorObject.Key.GeneID;
                    TranslatorAlleleRowIndex = translatorObject.Key.AlleleRowIndex.Value;
                    TranslatorAlleleID = translatorObject.Key.AlleleID;
                    var TranslatorAssayCallList = translatorObject.ToList();

                    #region Proceed Comparing


                    TranslatorAssayCallListOrderd = TranslatorAssayCallList.OrderBy(p => p.AssayID).ThenBy(p => p.CallID).ToList();
                    TranslatorAssaySequence = TranslatorAssayCallListOrderd.Select(p => p.AssayID).ToList();

                    PatientLabAssayCallListOrderd = PatientLabAssayCallList.Where(c=>c.CallID.HasValue).OrderBy(p => p.AssayID.Value).ThenBy(p => p.CallID).ToList();
                    PatientAssaySequence = PatientLabAssayCallListOrderd.Select(p => p.AssayID.Value).ToList();

                    #region Get Intersect List Between TranslatorAssaySequence and PatientAssaySequence
                    if (PatientAssaySequence.Count > TranslatorAssaySequence.Count || PatientAssaySequence.Count < TranslatorAssaySequence.Count)
                    {
                        var AssaySequenceIntersectList = PatientAssaySequence.Intersect(TranslatorAssaySequence).ToList();
                        PatientAssaySequence = TranslatorAssaySequence = AssaySequenceIntersectList;


                        List<Translator> TranslatorAssayCallListOrderdModified = new List<Translator>();
                        List<PatientLab> PatientLabAssayCallListOrderdModified = new List<PatientLab>();

                        foreach (var item in AssaySequenceIntersectList)
                        {

                            try
                            {

                                var singleTranslator = TranslatorAssayCallListOrderd.FirstOrDefault(a => a.AssayID == item);
                                var singlePateint = PatientLabAssayCallListOrderd.FirstOrDefault(a => a.AssayID == item);

                                TranslatorAssayCallListOrderdModified.Add(singleTranslator);
                                PatientLabAssayCallListOrderdModified.Add(singlePateint);
                            }
                            catch (Exception ex)
                            {


                                string sss = ex.Message;
                            }

                        }

                        TranslatorAssayCallListOrderd = TranslatorAssayCallListOrderdModified;
                        PatientLabAssayCallListOrderd = PatientLabAssayCallListOrderdModified;


                    }

                    TranslatorCallSequence = TranslatorAssayCallListOrderd.Select(p => p.CallID).ToList();
                    PatientCallSequence = PatientLabAssayCallListOrderd.Select(p => p.CallID.Value).ToList(); // fix bug , if callid is null in patient lab 

                    #endregion

                    if (PatientAssaySequence.SequenceEqual(TranslatorAssaySequence))
                    {


                        if (PatientCallSequence.SequenceEqual(TranslatorCallSequence))
                        {

                            result_TranslatorAlleleID = TranslatorAlleleID;
                            result_TranslatorGeneID = TranslatorGeneID;
                            _PatientTranslatorViewModel = new PatientTranslatorViewModel();
                            _PatientTranslatorViewModel.GeneID = PatientGeneId.Value; //result_TranslatorGeneID;
                            _PatientTranslatorViewModel.AlleleID = result_TranslatorAlleleID;
                            _PatientTranslatorViewModel.PatientID = PateintId.Value;
                            _PatientTranslatorViewModel.ProjectID = _projectViewModels.ProjectID;
                            _PatientTranslatorViewModel.Swap = false;
                            _PatientTranslatorViewModel.Probability = false;

                            if (CallValueForCopyNumber != ""){_PatientTranslatorViewModel.CopyNumber = int.Parse(CallValueForCopyNumber);}                          
                            compareSucceed = true;
                            PatientTranslatorForAllAlleles.Add(_PatientTranslatorViewModel);
                            continue;
                        }


                        #region  Translator Sequence


                        string LeftTranslatorOneCallSequence = "";
                        string RightTranslatorOneCallSequenc = "";

                        List<int> NOAMPLocationInTranslatorCallList = null;
                        BuildCallSequenceStringModified(TranslatorCallSequence, CallListOfDatabase, out LeftTranslatorOneCallSequence, out RightTranslatorOneCallSequenc, out NOAMPLocationInTranslatorCallList, false);
                        #endregion

                        #region PatientLab Sequence

                        string LeftPatientOneCallSequence = "";
                        string RightPatientOneCallSequenc = "";

                        List<int> NOAMPLocationInPatientCallList = null;
                        BuildCallSequenceStringModified(PatientCallSequence, CallListOfDatabase, out LeftPatientOneCallSequence, out RightPatientOneCallSequenc, out NOAMPLocationInPatientCallList, false);
                        #endregion

                        
                        #region Compare

                        if (PatientAssaySequence.SequenceEqual(TranslatorAssaySequence))
                        {
                            #region Normal Sequence Comparing
                            if (LeftTranslatorOneCallSequence.Trim() == LeftPatientOneCallSequence.Trim() && RightTranslatorOneCallSequenc.Trim() == RightPatientOneCallSequenc.Trim())
                            {
                                result_TranslatorAlleleID = TranslatorAlleleID;
                                result_TranslatorGeneID = TranslatorGeneID;
                                // swap false

                                _PatientTranslatorViewModel = new PatientTranslatorViewModel();
                                _PatientTranslatorViewModel.GeneID = PatientGeneId.Value; //result_TranslatorGeneID;
                                _PatientTranslatorViewModel.AlleleID = result_TranslatorAlleleID;
                                _PatientTranslatorViewModel.PatientID = PateintId.Value;
                                _PatientTranslatorViewModel.ProjectID = _projectViewModels.ProjectID;
                                _PatientTranslatorViewModel.Swap = false;
                                _PatientTranslatorViewModel.Probability = false;
                                if (CallValueForCopyNumber != "")
                                { _PatientTranslatorViewModel.CopyNumber = int.Parse(CallValueForCopyNumber); }
                                compareSucceed = true;
                                break;
                                //  return _PatientTranslatorViewModel;

                                //PatientTranslatorForAllAlleles.Add(_PatientTranslatorViewModel);
                                //continue;
                            }
                            else if (LeftTranslatorOneCallSequence == RightPatientOneCallSequenc && RightTranslatorOneCallSequenc == LeftPatientOneCallSequence)
                            {
                                result_TranslatorAlleleID = TranslatorAlleleID;
                                result_TranslatorGeneID = TranslatorGeneID;
                                // swap true

                                _PatientTranslatorViewModel = new PatientTranslatorViewModel();
                                _PatientTranslatorViewModel.GeneID = PatientGeneId.Value; //result_TranslatorGeneID;
                                _PatientTranslatorViewModel.AlleleID = result_TranslatorAlleleID;
                                _PatientTranslatorViewModel.PatientID = PateintId.Value;
                                _PatientTranslatorViewModel.ProjectID = _projectViewModels.ProjectID;
                                _PatientTranslatorViewModel.Swap = true;
                                _PatientTranslatorViewModel.Probability = false;
                                if (CallValueForCopyNumber != "")
                                { _PatientTranslatorViewModel.CopyNumber = int.Parse(CallValueForCopyNumber); }
                                compareSucceed = true;
                                break;

                                //PatientTranslatorForAllAlleles.Add(_PatientTranslatorViewModel);
                                //continue;

                                //   return _PatientTranslatorViewModel;
                            }

                            #endregion


                            // Stage two:::Get Probability For Noamp Call In Patient Sequence Comparing

                            #region Probability
                            else if (NOAMPLocationInPatientCallList != null && NOAMPLocationInPatientCallList.Count > 0)
                            {

                                if (NOAMPLocationInTranslatorCallList != null && NOAMPLocationInTranslatorCallList.Count > 0)
                                {
                                    if (NOAMPLocationInPatientCallList.Count == NOAMPLocationInTranslatorCallList.Count)
                                    {
                                        //  no action
                                    }
                                    else if (NOAMPLocationInPatientCallList.Count > NOAMPLocationInTranslatorCallList.Count)
                                    {

                                        // what the action ?

                                        if (PatientCallSequence.Count > NOAMPLocationInPatientCallList.Count)
                                        {
                                            NOAMPLocationInPatientCallList = NOAMPLocationInPatientCallList.Except(NOAMPLocationInTranslatorCallList).ToList();
                                        }

                                        else
                                        {
                                            var ss = NOAMPLocationInPatientCallList;
                                        }
                                        BuildCallExcludedSequenceString(PatientCallSequence, CallListOfDatabase, out LeftPatientOneCallSequence, out RightPatientOneCallSequenc, NOAMPLocationInPatientCallList);
                                        BuildCallExcludedSequenceString(TranslatorCallSequence, CallListOfDatabase, out LeftTranslatorOneCallSequence, out RightTranslatorOneCallSequenc, NOAMPLocationInPatientCallList);

                                        if (LeftTranslatorOneCallSequence == LeftPatientOneCallSequence && RightTranslatorOneCallSequenc == RightPatientOneCallSequenc)
                                        {
                                            result_TranslatorAlleleID = TranslatorAlleleID;
                                            result_TranslatorGeneID = TranslatorGeneID;
                                            // swap false

                                            _PatientTranslatorViewModel = new PatientTranslatorViewModel();
                                            _PatientTranslatorViewModel.GeneID = PatientGeneId.Value; //result_TranslatorGeneID;
                                            _PatientTranslatorViewModel.AlleleID = result_TranslatorAlleleID;
                                            _PatientTranslatorViewModel.PatientID = PateintId.Value;
                                            _PatientTranslatorViewModel.ProjectID = _projectViewModels.ProjectID;
                                            _PatientTranslatorViewModel.Swap = false;
                                            _PatientTranslatorViewModel.Probability = true;
                                            if (CallValueForCopyNumber != "")
                                            { _PatientTranslatorViewModel.CopyNumber = int.Parse(CallValueForCopyNumber); }
                                            PatientTranslatorProbability.Add(_PatientTranslatorViewModel);
                                            compareSucceed = true;
                                        }

                                        else if (LeftTranslatorOneCallSequence == RightPatientOneCallSequenc && RightTranslatorOneCallSequenc == LeftPatientOneCallSequence)
                                        {
                                            result_TranslatorAlleleID = TranslatorAlleleID;
                                            result_TranslatorGeneID = TranslatorGeneID;
                                            // swap true
                                            _PatientTranslatorViewModel = new PatientTranslatorViewModel();
                                            _PatientTranslatorViewModel.GeneID = PatientGeneId.Value; //result_TranslatorGeneID;
                                            _PatientTranslatorViewModel.AlleleID = result_TranslatorAlleleID;
                                            _PatientTranslatorViewModel.PatientID = PateintId.Value;
                                            _PatientTranslatorViewModel.ProjectID = _projectViewModels.ProjectID;
                                            _PatientTranslatorViewModel.Swap = true;
                                            _PatientTranslatorViewModel.Probability = true;
                                            if (CallValueForCopyNumber != "")
                                            { _PatientTranslatorViewModel.CopyNumber = int.Parse(CallValueForCopyNumber); }
                                            PatientTranslatorProbability.Add(_PatientTranslatorViewModel);
                                            compareSucceed = true;

                                        }
                                    }

                                }
                                else
                                {
                                    // exclude mode

                                    BuildCallExcludedSequenceString(PatientCallSequence, CallListOfDatabase, out LeftPatientOneCallSequence, out RightPatientOneCallSequenc, NOAMPLocationInPatientCallList);
                                    BuildCallExcludedSequenceString(TranslatorCallSequence, CallListOfDatabase, out LeftTranslatorOneCallSequence, out RightTranslatorOneCallSequenc, NOAMPLocationInPatientCallList);

                                    if (LeftTranslatorOneCallSequence == LeftPatientOneCallSequence && RightTranslatorOneCallSequenc == RightPatientOneCallSequenc)
                                    {
                                        result_TranslatorAlleleID = TranslatorAlleleID;
                                        result_TranslatorGeneID = TranslatorGeneID;
                                        // swap false

                                        _PatientTranslatorViewModel = new PatientTranslatorViewModel();
                                        _PatientTranslatorViewModel.GeneID = PatientGeneId.Value; //result_TranslatorGeneID;
                                        _PatientTranslatorViewModel.AlleleID = result_TranslatorAlleleID;
                                        _PatientTranslatorViewModel.PatientID = PateintId.Value;
                                        _PatientTranslatorViewModel.ProjectID = _projectViewModels.ProjectID;
                                        _PatientTranslatorViewModel.Swap = false;
                                        _PatientTranslatorViewModel.Probability = true;
                                        if (CallValueForCopyNumber != "")
                                        { _PatientTranslatorViewModel.CopyNumber = int.Parse(CallValueForCopyNumber); }
                                        PatientTranslatorProbability.Add(_PatientTranslatorViewModel);
                                        compareSucceed = true;
                                    }

                                    else if (LeftTranslatorOneCallSequence == RightPatientOneCallSequenc && RightTranslatorOneCallSequenc == LeftPatientOneCallSequence)
                                    {
                                        result_TranslatorAlleleID = TranslatorAlleleID;
                                        result_TranslatorGeneID = TranslatorGeneID;
                                        // swap true
                                        _PatientTranslatorViewModel = new PatientTranslatorViewModel();
                                        _PatientTranslatorViewModel.GeneID = PatientGeneId.Value; //result_TranslatorGeneID;
                                        _PatientTranslatorViewModel.AlleleID = result_TranslatorAlleleID;
                                        _PatientTranslatorViewModel.PatientID = PateintId.Value;
                                        _PatientTranslatorViewModel.ProjectID = _projectViewModels.ProjectID;
                                        _PatientTranslatorViewModel.Swap = true;
                                        _PatientTranslatorViewModel.Probability = true;

                                        if (CallValueForCopyNumber != "")
                                        { _PatientTranslatorViewModel.CopyNumber = int.Parse(CallValueForCopyNumber); }
                                        PatientTranslatorProbability.Add(_PatientTranslatorViewModel);
                                        compareSucceed = true;

                                    }

                                }


                            }
                            #endregion






                            #region swap per call
                            if (!compareSucceed)
                            {

                                // get call list for patient 
                                List<int> currentPatientCallIdsList = PatientCallSequence; // PatientLabAssayCallListOrderd
                                List<SelectItemViewModel> currentPatientCallAssayList =
                                                        PatientLabAssayCallListOrderd.Select(x => new SelectItemViewModel() { key = x.AssayID.Value.ToString(), value = x.CallID.Value.ToString() }).ToList();


                                // get call list for translator 
                                List<int> currentTranslatorCallIdsList = TranslatorCallSequence; // TranslatorAssayCallListOrderd
                                List<SelectItemViewModel> currentTranslatorCallAssayList =
                                                        TranslatorAssayCallListOrderd.Select(x => new SelectItemViewModel() { key = x.AssayID.ToString(), value = x.CallID.ToString() }).ToList();


                                int detectedCount = 0;
                                int counter = 0;

                                #region call assay 

                                foreach (var CallAssayPatient in currentPatientCallAssayList)
                                {

                                    string callValuePatient = CallListOfDatabase.Where(c => c.CallID == int.Parse(CallAssayPatient.value)).Select(v => v.CallValue).FirstOrDefault();
                                    string CurrentAssayIdPatient = CallAssayPatient.key;

                                    var split = callValuePatient.Split('/');
                                    int result = -1;


                                     if (int.TryParse(split[0].ToString(), out result))
                                    {

                                        var dedicatedCallAssayTranslator = currentTranslatorCallAssayList.Where(a => a.key == CurrentAssayIdPatient).FirstOrDefault();
                                        if (dedicatedCallAssayTranslator.value == CallAssayPatient.value)
                                        {
                                            detectedCount++;
                                        }

                                    }

                                    else  if (split.Length == 1  && !int.TryParse(split[0].ToString(), out result)) // noampExist
                                    {
                                        detectedCount++;
                                        NoAmpExist = true;
                                    }
                                   
                                    else
                                    {
                                        string callValuePatientSwaped = SwapCallValue(callValuePatient);
                                        int callValuePatientSwapedId = CallListOfDatabase.Where(c => c.CallValue == callValuePatientSwaped).Select(i => i.CallID).FirstOrDefault();
                                        var dedicatedCallAssayTranslator = currentTranslatorCallAssayList.Where(a => a.key == CurrentAssayIdPatient).FirstOrDefault();
                                        if (dedicatedCallAssayTranslator.value == CallAssayPatient.value || dedicatedCallAssayTranslator.value == callValuePatientSwapedId.ToString())
                                        {
                                            detectedCount++;
                                        }
                                    }

                                    counter++;
                                }


                                #endregion


                                #region call bug
                                // foreach call patient  to get matched translator call in normal form or swap form 
                                //foreach (var CallIdPatient in currentPatientCallIdsList)
                                //{

                                //    string callValuePatient = CallListOfDatabase.Where(c => c.CallID == CallIdPatient).Select(v => v.CallValue).FirstOrDefault();

                                //    var split = callValuePatient.Split('/');
                                //    if (split.Length == 1)
                                //    {
                                //        detectedCount++;
                                //    }
                                //    else
                                //    {
                                //        string callValuePatientSwaped = SwapCallValue(callValuePatient);
                                //        int callValuePatientSwapedId = CallListOfDatabase.Where(c => c.CallValue == callValuePatientSwaped).Select(i => i.CallID).FirstOrDefault();

                                //        if (currentTranslatorCallIdsList.Contains(CallIdPatient) || currentTranslatorCallIdsList.Contains(callValuePatientSwapedId))
                                //        {
                                //            detectedCount++;
                                //        }
                                //    }

                                //} 
                                #endregion

                                if (detectedCount == currentTranslatorCallAssayList.Count)
                                {
                                    result_TranslatorAlleleID = TranslatorAlleleID;
                                    result_TranslatorGeneID = TranslatorGeneID;
                                    _PatientTranslatorViewModel = new PatientTranslatorViewModel();
                                    _PatientTranslatorViewModel.GeneID = result_TranslatorGeneID;
                                    _PatientTranslatorViewModel.AlleleID = result_TranslatorAlleleID;
                                    _PatientTranslatorViewModel.PatientID = PateintId.Value;
                                    _PatientTranslatorViewModel.ProjectID = _projectViewModels.ProjectID;
                                    _PatientTranslatorViewModel.Probability = NoAmpExist;
                                    if (CallValueForCopyNumber != "")
                                    { _PatientTranslatorViewModel.CopyNumber = int.Parse(CallValueForCopyNumber); }
                                    //_PatientTranslatorViewModel.Swap = ;
                                    //compareSucceed = true;
                                    //break;

                                    PatientTranslatorForAllAlleles.Add(_PatientTranslatorViewModel);
                                    continue;
                                  
                                }
                            }


                            #endregion




                        }


                        #endregion

                        #region old  comparing approach
                        //foreach (var AssayCallTransltor in TranslatorAssayCallList)
                        //{
                        //    foreach (var AssayCallPatient in PatientLabAssayCallList)
                        //    {
                        //        if (AssayCallPatient.AssayID.Value == AssayCallTransltor.AssayID && AssayCallPatient.CallID.Value == AssayCallTransltor.CallID)
                        //        {
                        //            detectedCount++;
                        //            result_TranslatorAlleleID = AssayCallTransltor.AlleleID;
                        //            result_TranslatorGeneID = AssayCallTransltor.GeneID;
                        //        }
                        //    }
                        //}

                        //if (detectedCount == TranslatorAssayCallList.Count)
                        //{
                        //    _PatientTranslatorViewModel = new PatientTranslatorViewModel();
                        //    _PatientTranslatorViewModel.GeneID = result_TranslatorGeneID;
                        //    _PatientTranslatorViewModel.AlleleID = result_TranslatorAlleleID;
                        //    _PatientTranslatorViewModel.PatientID = PateintId.Value;
                        //    _PatientTranslatorViewModel.Swap = false;
                        //    break;
                        //}

                        //else
                        //{
                        //    //_PatientTranslatorViewModel = new PatientTranslatorViewModel();
                        //    //_PatientTranslatorViewModel.GeneID = PatientGeneId.Value;
                        //    //_PatientTranslatorViewModel.AlleleID = null;
                        //    //_PatientTranslatorViewModel.Swap = false;
                        //    //_PatientTranslatorViewModel.PatientID = PateintId.Value;


                        //} 
                        #endregion
                    }

                    #endregion
                }




                #region undefind allele
                if (!compareSucceed)
                {


                    _PatientTranslatorViewModel = new PatientTranslatorViewModel();
                    _PatientTranslatorViewModel.GeneID = PatientGeneId.Value;
                    _PatientTranslatorViewModel.AlleleID = null;
                    _PatientTranslatorViewModel.Swap = false;
                    _PatientTranslatorViewModel.PatientID = PateintId.Value;
                    _PatientTranslatorViewModel.ProjectID = _projectViewModels.ProjectID;

                    if (CallValueForCopyNumber != "")
                    { _PatientTranslatorViewModel.CopyNumber = int.Parse(CallValueForCopyNumber); }


                    //if (PatientTranslatorProbability != null && PatientTranslatorProbability.Count > 0)
                    //{
                    //}
                    //else
                    //{
                    //    // undefind allele
                    //}

                }
                #endregion

                return _PatientTranslatorViewModel;
            }
            catch (Exception ex)
            {

                string ee = ex.Message;
                PatientTranslatorProbability = null;
                PatientTranslatorForAllAlleles = null;
                return null;
            }

        }

        #endregion


        #region BuildPatientTranslatorModifiedEnhancement
        //private PatientTranslatorViewModel BuildPatientTranslatorModifiedEnhancement(List<PatientLab> PatientLabAssayCallList, List<Translator> translatorList, List<Call> CallListOfDatabase, int? PatientGeneId, int? PateintId, out List<PatientTranslatorViewModel> PatientTranslatorProbability, ProjectViewModel _projectViewModels)

        //{
        //    try
        //    {
        //        PatientTranslatorProbability = new List<PatientTranslatorViewModel>();

        //        List<Translator> TranslatorAssayCallListOrderd = new List<Translator>();
        //        List<int> TranslatorAssaySequence = new List<int>();
        //        List<int> TranslatorCallSequence = new List<int>();

        //        List<PatientLab> PatientLabAssayCallListOrderd = new List<PatientLab>();
        //        List<int> PatientAssaySequence = new List<int>();
        //        List<int> PatientCallSequence = new List<int>();

        //        PatientTranslatorViewModel _PatientTranslatorViewModel = null;
        //        if (translatorList == null || translatorList.Count == 0)
        //        {
        //            _PatientTranslatorViewModel = new PatientTranslatorViewModel();
        //            _PatientTranslatorViewModel.GeneID = PatientGeneId.Value;
        //            _PatientTranslatorViewModel.AlleleID = null;
        //            _PatientTranslatorViewModel.PatientID = PateintId.Value;
        //            _PatientTranslatorViewModel.ProjectID = _projectViewModels.ProjectID;
        //            return _PatientTranslatorViewModel;
        //        }
        //        var translatorListGroupedByAllell = translatorList.GroupBy(p => new { p.AlleleID, p.GeneID }).ToList();

        //        int TranslatorAlleleID;
        //        int TranslatorGeneID;
        //        int result_TranslatorAlleleID = -1;
        //        int result_TranslatorGeneID = -1;


        //        bool compareSucceed = false;

        //        //Loop For Each Allele In Translator to Get Matched Allele/s For cutrent assay sequence For Patient
        //        #region Loop For Each Allele In Translator 
        //        for (int k = 0; k < translatorListGroupedByAllell.Count; k++)
        //        {

        //            var translatorObject = translatorListGroupedByAllell[k];
        //            TranslatorGeneID = translatorObject.Key.GeneID;
        //            TranslatorAlleleID = translatorObject.Key.AlleleID;
        //            var TranslatorAssayCallList = translatorObject.ToList();

        //            #region Proceed Comparing

        //            TranslatorAssayCallListOrderd = TranslatorAssayCallList.OrderBy(p => p.AssayID).ThenBy(p => p.CallID).ToList();
        //            TranslatorAssaySequence = TranslatorAssayCallListOrderd.Select(p => p.AssayID).ToList();



        //            PatientLabAssayCallListOrderd = PatientLabAssayCallList.OrderBy(p => p.AssayID.Value).ThenBy(p => p.CallID).ToList();
        //            PatientAssaySequence = PatientLabAssayCallListOrderd.Select(p => p.AssayID.Value).ToList();

        //            //Get Intersect List Between TranslatorAssaySequence and PatientAssaySequence
        //            #region Get Intersect List Between TranslatorAssaySequence and PatientAssaySequence
        //            if (PatientAssaySequence.Count > TranslatorAssaySequence.Count || PatientAssaySequence.Count < TranslatorAssaySequence.Count)
        //            {
        //                var AssaySequenceIntersectList = PatientAssaySequence.Intersect(TranslatorAssaySequence).ToList();
        //                PatientAssaySequence = TranslatorAssaySequence = AssaySequenceIntersectList;


        //                List<Translator> TranslatorAssayCallListOrderdModified = new List<Translator>();
        //                List<PatientLab> PatientLabAssayCallListOrderdModified = new List<PatientLab>();

        //                foreach (var item in AssaySequenceIntersectList)
        //                {

        //                    TranslatorAssayCallListOrderdModified.Add(TranslatorAssayCallListOrderd.SingleOrDefault(a => a.AssayID == item));
        //                    PatientLabAssayCallListOrderdModified.Add(PatientLabAssayCallListOrderd.SingleOrDefault(a => a.AssayID == item));

        //                }

        //                TranslatorAssayCallListOrderd = TranslatorAssayCallListOrderdModified;
        //                PatientLabAssayCallListOrderd = PatientLabAssayCallListOrderdModified;


        //            }

        //            TranslatorCallSequence = TranslatorAssayCallListOrderd.Select(p => p.CallID).ToList();
        //            PatientCallSequence = PatientLabAssayCallListOrderd.Select(p => p.CallID.Value).ToList();

        //            #endregion


        //            if (PatientAssaySequence.SequenceEqual(TranslatorAssaySequence))
        //            {
        //                // speed compare if PatientCallsList is equal to  TranslatorCallList  , current allele is result and no need to complete rest of code
        //                if (PatientCallSequence.SequenceEqual(TranslatorCallSequence))
        //                {

        //                    result_TranslatorAlleleID = TranslatorAlleleID;
        //                    result_TranslatorGeneID = TranslatorGeneID;
        //                    _PatientTranslatorViewModel = new PatientTranslatorViewModel();
        //                    _PatientTranslatorViewModel.GeneID = PatientGeneId.Value; //result_TranslatorGeneID;
        //                    _PatientTranslatorViewModel.AlleleID = result_TranslatorAlleleID;
        //                    _PatientTranslatorViewModel.PatientID = PateintId.Value;
        //                    _PatientTranslatorViewModel.ProjectID = _projectViewModels.ProjectID;
        //                    _PatientTranslatorViewModel.Swap = false;
        //                    _PatientTranslatorViewModel.Probability = false;
        //                    compareSucceed = true;
        //                    break; // after break next step is  return  

        //                }



        //                //-----------------------------prepare CallSequence for translator and patient------------------------------------------//

        //                #region  Translator Sequence

        //                string LeftTranslatorOneCallSequence = "";
        //                string RightTranslatorOneCallSequenc = "";

        //                List<int> NOAMPLocationInTranslatorCallList = null;
        //                BuildCallSequenceStringModified(TranslatorCallSequence, CallListOfDatabase, out LeftTranslatorOneCallSequence, out RightTranslatorOneCallSequenc, out NOAMPLocationInTranslatorCallList, false);
        //                #endregion

        //                #region PatientLab Sequence


        //                string LeftPatientOneCallSequence = "";
        //                string RightPatientOneCallSequenc = "";

        //                List<int> NOAMPLocationInPatientCallList = null;
        //                BuildCallSequenceStringModified(PatientCallSequence, CallListOfDatabase, out LeftPatientOneCallSequence, out RightPatientOneCallSequenc, out NOAMPLocationInPatientCallList, false);
        //                #endregion
        //                //--------------------------------------------------------------------------------------------------------------------//


        //                #region Compare

        //                // Stage one Normal Sequence Comparing
        //                #region Normal Sequence Comparing

        //                #region Left With Lift  //swap false
        //                if (LeftTranslatorOneCallSequence == LeftPatientOneCallSequence && RightTranslatorOneCallSequenc == RightPatientOneCallSequenc)
        //                    {
        //                        result_TranslatorAlleleID = TranslatorAlleleID;
        //                        result_TranslatorGeneID = TranslatorGeneID;
        //                        _PatientTranslatorViewModel = new PatientTranslatorViewModel();
        //                        _PatientTranslatorViewModel.GeneID = PatientGeneId.Value; //result_TranslatorGeneID;
        //                        _PatientTranslatorViewModel.AlleleID = result_TranslatorAlleleID;
        //                        _PatientTranslatorViewModel.PatientID = PateintId.Value;
        //                        _PatientTranslatorViewModel.ProjectID = _projectViewModels.ProjectID;
        //                        _PatientTranslatorViewModel.Swap = false;
        //                        _PatientTranslatorViewModel.Probability = false;
        //                        compareSucceed = true;
        //                        break;
        //                    }
        //                #endregion

        //                #region Swap //swap  true
        //                else if (LeftTranslatorOneCallSequence == RightPatientOneCallSequenc && RightTranslatorOneCallSequenc == LeftPatientOneCallSequence)
        //                    {
        //                        result_TranslatorAlleleID = TranslatorAlleleID;
        //                        result_TranslatorGeneID = TranslatorGeneID;
        //                        _PatientTranslatorViewModel = new PatientTranslatorViewModel();
        //                        _PatientTranslatorViewModel.GeneID = PatientGeneId.Value; //result_TranslatorGeneID;
        //                        _PatientTranslatorViewModel.AlleleID = result_TranslatorAlleleID;
        //                        _PatientTranslatorViewModel.PatientID = PateintId.Value;
        //                        _PatientTranslatorViewModel.ProjectID = _projectViewModels.ProjectID;
        //                        _PatientTranslatorViewModel.Swap = true;
        //                        _PatientTranslatorViewModel.Probability = false;
        //                        compareSucceed = true;
        //                        break;
        //                    }

        //                    #endregion

        //                    #endregion

        //                    // Stage two:::Get Probability For Noamp Call In Patient Sequence Comparing                        
        //                    #region Probability
        //                    else if (NOAMPLocationInPatientCallList != null && NOAMPLocationInPatientCallList.Count > 0)
        //                    {

        //                    //------------------ if  NoAmp is  exist in Translator Sequence 
        //                    //------------------so we get the noamp locations in patientlist that not exist in translator list  
        //                    //-------------------then we will exclude any noamp in patient and after that we make compare 

        //                    #region NOAMPLocationInTranslatorCallList
        //                    if (NOAMPLocationInTranslatorCallList != null && NOAMPLocationInTranslatorCallList.Count > 0)
        //                        {
        //                            if (NOAMPLocationInPatientCallList.Count == NOAMPLocationInTranslatorCallList.Count)
        //                            {
        //                                //  no action
        //                            }
        //                            else if (NOAMPLocationInPatientCallList.Count > NOAMPLocationInTranslatorCallList.Count)
        //                            {

        //                                // what the action ?

        //                                NOAMPLocationInPatientCallList = NOAMPLocationInPatientCallList.Except(NOAMPLocationInTranslatorCallList).ToList();
        //                                BuildCallExcludedSequenceString(PatientCallSequence, CallListOfDatabase, out LeftPatientOneCallSequence, out RightPatientOneCallSequenc, NOAMPLocationInPatientCallList);
        //                                BuildCallExcludedSequenceString(TranslatorCallSequence, CallListOfDatabase, out LeftTranslatorOneCallSequence, out RightTranslatorOneCallSequenc, NOAMPLocationInPatientCallList);

        //                                if (LeftTranslatorOneCallSequence == LeftPatientOneCallSequence && RightTranslatorOneCallSequenc == RightPatientOneCallSequenc)
        //                                {
        //                                    result_TranslatorAlleleID = TranslatorAlleleID;
        //                                    result_TranslatorGeneID = TranslatorGeneID;
        //                                    _PatientTranslatorViewModel = new PatientTranslatorViewModel();
        //                                    _PatientTranslatorViewModel.GeneID = PatientGeneId.Value; //result_TranslatorGeneID;
        //                                    _PatientTranslatorViewModel.AlleleID = result_TranslatorAlleleID;
        //                                    _PatientTranslatorViewModel.PatientID = PateintId.Value;
        //                                    _PatientTranslatorViewModel.ProjectID = _projectViewModels.ProjectID;
        //                                    _PatientTranslatorViewModel.Swap = false;
        //                                    _PatientTranslatorViewModel.Probability = true;
        //                                    PatientTranslatorProbability.Add(_PatientTranslatorViewModel);
        //                                    compareSucceed = true;
        //                                }

        //                                else if (LeftTranslatorOneCallSequence == RightPatientOneCallSequenc && RightTranslatorOneCallSequenc == LeftPatientOneCallSequence)
        //                                {
        //                                    result_TranslatorAlleleID = TranslatorAlleleID;
        //                                    result_TranslatorGeneID = TranslatorGeneID;
        //                                    _PatientTranslatorViewModel = new PatientTranslatorViewModel();
        //                                    _PatientTranslatorViewModel.GeneID = PatientGeneId.Value; //result_TranslatorGeneID;
        //                                    _PatientTranslatorViewModel.AlleleID = result_TranslatorAlleleID;
        //                                    _PatientTranslatorViewModel.PatientID = PateintId.Value;
        //                                    _PatientTranslatorViewModel.ProjectID = _projectViewModels.ProjectID;
        //                                    _PatientTranslatorViewModel.Swap = true;
        //                                    _PatientTranslatorViewModel.Probability = true;
        //                                    PatientTranslatorProbability.Add(_PatientTranslatorViewModel);
        //                                    compareSucceed = true;

        //                                }
        //                            }
        //                        }
        //                        #endregion

        //                        //------------------ no amp is not exist in Translator Sequence but exist in Pateint only 
        //                        //-------------------so we will exclude all of noamp locatoins in patient list and make compare normaly 
        //                        #region exclude mode 
        //                        else
        //                        {

        //                            BuildCallExcludedSequenceString(PatientCallSequence, CallListOfDatabase, out LeftPatientOneCallSequence, out RightPatientOneCallSequenc, NOAMPLocationInPatientCallList);
        //                            BuildCallExcludedSequenceString(TranslatorCallSequence, CallListOfDatabase, out LeftTranslatorOneCallSequence, out RightTranslatorOneCallSequenc, NOAMPLocationInPatientCallList);

        //                            if (LeftTranslatorOneCallSequence == LeftPatientOneCallSequence && RightTranslatorOneCallSequenc == RightPatientOneCallSequenc)
        //                            {
        //                                result_TranslatorAlleleID = TranslatorAlleleID;
        //                                result_TranslatorGeneID = TranslatorGeneID;
        //                                _PatientTranslatorViewModel = new PatientTranslatorViewModel();
        //                                _PatientTranslatorViewModel.GeneID = PatientGeneId.Value; //result_TranslatorGeneID;
        //                                _PatientTranslatorViewModel.AlleleID = result_TranslatorAlleleID;
        //                                _PatientTranslatorViewModel.PatientID = PateintId.Value;
        //                                _PatientTranslatorViewModel.ProjectID = _projectViewModels.ProjectID;
        //                                _PatientTranslatorViewModel.Swap = false;
        //                                _PatientTranslatorViewModel.Probability = true;
        //                                PatientTranslatorProbability.Add(_PatientTranslatorViewModel);
        //                                compareSucceed = true;
        //                            }

        //                            else if (LeftTranslatorOneCallSequence == RightPatientOneCallSequenc && RightTranslatorOneCallSequenc == LeftPatientOneCallSequence)
        //                            {
        //                                result_TranslatorAlleleID = TranslatorAlleleID;
        //                                result_TranslatorGeneID = TranslatorGeneID;
        //                                _PatientTranslatorViewModel = new PatientTranslatorViewModel();
        //                                _PatientTranslatorViewModel.GeneID = PatientGeneId.Value; //result_TranslatorGeneID;
        //                                _PatientTranslatorViewModel.AlleleID = result_TranslatorAlleleID;
        //                                _PatientTranslatorViewModel.PatientID = PateintId.Value;
        //                                _PatientTranslatorViewModel.ProjectID = _projectViewModels.ProjectID;
        //                                _PatientTranslatorViewModel.Swap = true;
        //                                _PatientTranslatorViewModel.Probability = true;
        //                                PatientTranslatorProbability.Add(_PatientTranslatorViewModel);
        //                                compareSucceed = true;

        //                            }

        //                        } 
        //                        #endregion

        //                    }
        //                    #endregion

        //                    // Stage Three 
        //                    // if all of above compare methods not succeed  , we use swap per call method 
        //                    #region swap per call
        //                    if (!compareSucceed)
        //                    {

        //                        // get call list for patient 
        //                        List<int> currentPatientCallIdsList = PatientCallSequence; // PatientLabAssayCallListOrderd
        //                        List<SelectItemViewModel> currentPatientCallAssayList =
        //                                                PatientLabAssayCallListOrderd.Select(x => new SelectItemViewModel() { key = x.AssayID.Value.ToString(), value = x.CallID.Value.ToString() }).ToList();


        //                        // get call list for translator 
        //                        List<int> currentTranslatorCallIdsList = TranslatorCallSequence; // TranslatorAssayCallListOrderd
        //                        List<SelectItemViewModel> currentTranslatorCallAssayList =
        //                                                TranslatorAssayCallListOrderd.Select(x => new SelectItemViewModel() { key = x.AssayID.ToString(), value = x.CallID.ToString() }).ToList();


        //                        int detectedCount = 0;


        //                        #region call assay 

        //                        foreach (var CallAssayPatient in currentPatientCallAssayList)
        //                        {

        //                            string callValuePatient = CallListOfDatabase.Where(c => c.CallID == int.Parse(CallAssayPatient.value)).Select(v => v.CallValue).FirstOrDefault();
        //                            string CurrentAssayIdPatient = CallAssayPatient.key;

        //                            var split = callValuePatient.Split('/');
        //                            if (split.Length == 1)
        //                            {
        //                                detectedCount++;
        //                            }
        //                            else
        //                            {
        //                                string callValuePatientSwaped = SwapCallValue(callValuePatient);
        //                                int callValuePatientSwapedId = CallListOfDatabase.Where(c => c.CallValue == callValuePatientSwaped).Select(i => i.CallID).FirstOrDefault();
        //                                var dedicatedCallAssayTranslator = currentTranslatorCallAssayList.Where(a => a.key == CurrentAssayIdPatient).FirstOrDefault();
        //                                if (dedicatedCallAssayTranslator.value == CallAssayPatient.value || dedicatedCallAssayTranslator.value == callValuePatientSwapedId.ToString())
        //                                {
        //                                    detectedCount++;
        //                                }
        //                            }

        //                        }


        //                        #endregion

        //                        if (detectedCount == TranslatorAssayCallList.Count)
        //                        {
        //                            result_TranslatorAlleleID = TranslatorAlleleID;
        //                            result_TranslatorGeneID = TranslatorGeneID;
        //                            _PatientTranslatorViewModel = new PatientTranslatorViewModel();
        //                            _PatientTranslatorViewModel.GeneID = result_TranslatorGeneID;
        //                            _PatientTranslatorViewModel.AlleleID = result_TranslatorAlleleID;
        //                            _PatientTranslatorViewModel.PatientID = PateintId.Value;
        //                            _PatientTranslatorViewModel.ProjectID = _projectViewModels.ProjectID;
        //                            _PatientTranslatorViewModel.Swap = false;
        //                            compareSucceed = true;
        //                            break;
        //                        }
        //                    }


        //                    #endregion


                        


        //                #endregion


        //            }

        //            #endregion
        //        }

        //        #endregion

        //        //if there is not any matched allele in loop  , so the allele for current patient for current gene is null
        //        #region undefind allele 

        //        if (!compareSucceed)
        //        {
        //            _PatientTranslatorViewModel = new PatientTranslatorViewModel();
        //            _PatientTranslatorViewModel.GeneID = PatientGeneId.Value;
        //            _PatientTranslatorViewModel.AlleleID = null;
        //            _PatientTranslatorViewModel.Swap = false;
        //            _PatientTranslatorViewModel.PatientID = PateintId.Value;
        //            _PatientTranslatorViewModel.ProjectID = _projectViewModels.ProjectID;

        //        }
        //        #endregion

        //        return _PatientTranslatorViewModel;
        //    }
        //    catch (Exception ex)
        //    {

        //        string ee = ex.Message;
        //        PatientTranslatorProbability = null;
        //        return null;
        //    }

        //}


        #endregion

        #region BuildPatientTranslator Basic
        //private PatientTranslatorViewModel BuildPatientTranslator(List<PatientLab> PatientLabAssayCallList, List<Translator> translatorList, List<Call> CallListOfDatabase, int? PatientGeneId, int? PateintId, out List<PatientTranslatorViewModel> PatientTranslatorProbability, ProjectViewModel _projectViewModels)

        //{
        //    try
        //    {
        //        PatientTranslatorProbability = new List<PatientTranslatorViewModel>();


        //        List<Translator> TranslatorAssayCallListOrderd = new List<Translator>();
        //        List<int> TranslatorAssaySequence = new List<int>();
        //        List<int> TranslatorCallSequence = new List<int>();


        //        List<PatientLab> PatientLabAssayCallListOrderd = new List<PatientLab>();
        //        List<int> PatientAssaySequence = new List<int>();
        //        List<int> PatientCallSequence = new List<int>();








        //        PatientTranslatorViewModel _PatientTranslatorViewModel = null;
        //        if (translatorList == null || translatorList.Count == 0)
        //        {
        //            _PatientTranslatorViewModel = new PatientTranslatorViewModel();
        //            _PatientTranslatorViewModel.GeneID = PatientGeneId.Value;
        //            _PatientTranslatorViewModel.AlleleID = null;
        //            _PatientTranslatorViewModel.PatientID = PateintId.Value;
        //            _PatientTranslatorViewModel.ProjectID = _projectViewModels.ProjectID;
        //            return _PatientTranslatorViewModel;
        //        }
        //        var translatorListGroupedByAllell = translatorList.GroupBy(p => new { p.AlleleID, p.GeneID }).ToList();

        //        int TranslatorAlleleID;
        //        int TranslatorGeneID;
        //        int result_TranslatorAlleleID = -1;
        //        int result_TranslatorGeneID = -1;


        //        bool compareSucceed = false;


        //        for (int k = 0; k < translatorListGroupedByAllell.Count; k++)

        //        {

        //            var translatorObject = translatorListGroupedByAllell[k];
        //            TranslatorGeneID = translatorObject.Key.GeneID;
        //            TranslatorAlleleID = translatorObject.Key.AlleleID;
        //            var TranslatorAssayCallList = translatorObject.ToList();

        //            #region Proceed Comparing
        //            if (TranslatorAssayCallList.Count == PatientLabAssayCallList.Count)
        //            {

        //                #region  Translator Sequence
        //                TranslatorAssayCallListOrderd = TranslatorAssayCallList.OrderBy(p => p.AssayID).ThenBy(p => p.CallID).ToList();
        //                TranslatorAssaySequence = TranslatorAssayCallListOrderd.Select(p => p.AssayID).ToList();
        //                TranslatorCallSequence = TranslatorAssayCallListOrderd.Select(p => p.CallID).ToList();


        //                string LeftTranslatorOneCallSequence = "";
        //                string RightTranslatorOneCallSequenc = "";

        //                List<int> NOAMPLocationInTranslatorCallList = null;
        //                BuildCallSequenceString(TranslatorCallSequence, CallListOfDatabase, out LeftTranslatorOneCallSequence, out RightTranslatorOneCallSequenc, out NOAMPLocationInTranslatorCallList, false);
        //                #endregion

        //                #region PatientLab Sequence

        //                PatientLabAssayCallListOrderd = PatientLabAssayCallList.OrderBy(p => p.AssayID.Value).ThenBy(p => p.CallID).ToList();
        //                PatientAssaySequence = PatientLabAssayCallListOrderd.Select(p => p.AssayID.Value).ToList();
        //                PatientCallSequence = PatientLabAssayCallListOrderd.Select(p => p.CallID.Value).ToList();

        //                string LeftPatientOneCallSequence = "";
        //                string RightPatientOneCallSequenc = "";

        //                List<int> NOAMPLocationInPatientCallList = null;
        //                BuildCallSequenceString(PatientCallSequence, CallListOfDatabase, out LeftPatientOneCallSequence, out RightPatientOneCallSequenc, out NOAMPLocationInPatientCallList, false);
        //                #endregion


        //                #region Compare

        //                if (PatientAssaySequence.SequenceEqual(TranslatorAssaySequence))
        //                {
        //                    #region Normal Sequence Comparing
        //                    if (LeftTranslatorOneCallSequence == LeftPatientOneCallSequence && RightTranslatorOneCallSequenc == RightPatientOneCallSequenc)
        //                    {
        //                        result_TranslatorAlleleID = TranslatorAlleleID;
        //                        result_TranslatorGeneID = TranslatorGeneID;
        //                        // swap false

        //                        _PatientTranslatorViewModel = new PatientTranslatorViewModel();
        //                        _PatientTranslatorViewModel.GeneID = PatientGeneId.Value; //result_TranslatorGeneID;
        //                        _PatientTranslatorViewModel.AlleleID = result_TranslatorAlleleID;
        //                        _PatientTranslatorViewModel.PatientID = PateintId.Value;
        //                        _PatientTranslatorViewModel.ProjectID = _projectViewModels.ProjectID;
        //                        _PatientTranslatorViewModel.Swap = false;
        //                        _PatientTranslatorViewModel.Probability = false;
        //                        compareSucceed = true;
        //                        break;
        //                        //  return _PatientTranslatorViewModel;


        //                    }
        //                    else if (LeftTranslatorOneCallSequence == RightPatientOneCallSequenc && RightTranslatorOneCallSequenc == LeftPatientOneCallSequence)
        //                    {
        //                        result_TranslatorAlleleID = TranslatorAlleleID;
        //                        result_TranslatorGeneID = TranslatorGeneID;
        //                        // swap true

        //                        _PatientTranslatorViewModel = new PatientTranslatorViewModel();
        //                        _PatientTranslatorViewModel.GeneID = PatientGeneId.Value; //result_TranslatorGeneID;
        //                        _PatientTranslatorViewModel.AlleleID = result_TranslatorAlleleID;
        //                        _PatientTranslatorViewModel.PatientID = PateintId.Value;
        //                        _PatientTranslatorViewModel.ProjectID = _projectViewModels.ProjectID;
        //                        _PatientTranslatorViewModel.Swap = true;
        //                        _PatientTranslatorViewModel.Probability = false;
        //                        compareSucceed = true;
        //                        break;
        //                        //   return _PatientTranslatorViewModel;
        //                    }

        //                    #endregion






        //                    #region Probability
        //                    else if (NOAMPLocationInPatientCallList != null && NOAMPLocationInPatientCallList.Count > 0)
        //                    {

        //                        if (NOAMPLocationInTranslatorCallList != null && NOAMPLocationInTranslatorCallList.Count > 0)
        //                        {
        //                            if (NOAMPLocationInPatientCallList.Count == NOAMPLocationInTranslatorCallList.Count)
        //                            {
        //                                //  no action
        //                            }
        //                            else if (NOAMPLocationInPatientCallList.Count > NOAMPLocationInTranslatorCallList.Count)
        //                            {

        //                                // what the actoin ?
        //                            }

        //                        }
        //                        else
        //                        {
        //                            // exclude mode

        //                            BuildCallExcludedSequenceString(PatientCallSequence, CallListOfDatabase, out LeftPatientOneCallSequence, out RightPatientOneCallSequenc, NOAMPLocationInPatientCallList);
        //                            BuildCallExcludedSequenceString(TranslatorCallSequence, CallListOfDatabase, out LeftTranslatorOneCallSequence, out RightTranslatorOneCallSequenc, NOAMPLocationInPatientCallList);

        //                            if (LeftTranslatorOneCallSequence == LeftPatientOneCallSequence && RightTranslatorOneCallSequenc == RightPatientOneCallSequenc)
        //                            {
        //                                result_TranslatorAlleleID = TranslatorAlleleID;
        //                                result_TranslatorGeneID = TranslatorGeneID;
        //                                // swap false

        //                                _PatientTranslatorViewModel = new PatientTranslatorViewModel();
        //                                _PatientTranslatorViewModel.GeneID = PatientGeneId.Value; //result_TranslatorGeneID;
        //                                _PatientTranslatorViewModel.AlleleID = result_TranslatorAlleleID;
        //                                _PatientTranslatorViewModel.PatientID = PateintId.Value;
        //                                _PatientTranslatorViewModel.ProjectID = _projectViewModels.ProjectID;
        //                                _PatientTranslatorViewModel.Swap = false;
        //                                _PatientTranslatorViewModel.Probability = true;
        //                                PatientTranslatorProbability.Add(_PatientTranslatorViewModel);
        //                                compareSucceed = true;
        //                            }

        //                            else if (LeftTranslatorOneCallSequence == RightPatientOneCallSequenc && RightTranslatorOneCallSequenc == LeftPatientOneCallSequence)
        //                            {
        //                                result_TranslatorAlleleID = TranslatorAlleleID;
        //                                result_TranslatorGeneID = TranslatorGeneID;
        //                                // swap true
        //                                _PatientTranslatorViewModel = new PatientTranslatorViewModel();
        //                                _PatientTranslatorViewModel.GeneID = PatientGeneId.Value; //result_TranslatorGeneID;
        //                                _PatientTranslatorViewModel.AlleleID = result_TranslatorAlleleID;
        //                                _PatientTranslatorViewModel.PatientID = PateintId.Value;
        //                                _PatientTranslatorViewModel.ProjectID = _projectViewModels.ProjectID;
        //                                _PatientTranslatorViewModel.Swap = true;
        //                                _PatientTranslatorViewModel.Probability = true;
        //                                PatientTranslatorProbability.Add(_PatientTranslatorViewModel);
        //                                compareSucceed = true;

        //                            }

        //                        }


        //                    }
        //                    #endregion




        //                    #region swap per call
        //                    if (!compareSucceed)
        //                    {

        //                        // get call list for patient 
        //                        List<int> currentPatientCallIdsList = PatientCallSequence; // PatientLabAssayCallListOrderd
        //                        List<SelectItemViewModel> currentPatientCallAssayList =
        //                                                PatientLabAssayCallListOrderd.Select(x => new SelectItemViewModel() { key = x.AssayID.Value.ToString(), value = x.CallID.Value.ToString() }).ToList();


        //                        // get call list for translator 
        //                        List<int> currentTranslatorCallIdsList = TranslatorCallSequence; // TranslatorAssayCallListOrderd
        //                        List<SelectItemViewModel> currentTranslatorCallAssayList =
        //                                                TranslatorAssayCallListOrderd.Select(x => new SelectItemViewModel() { key = x.AssayID.ToString(), value = x.CallID.ToString() }).ToList();


        //                        int detectedCount = 0;


        //                        #region call assay 

        //                        foreach (var CallAssayPatient in currentPatientCallAssayList)
        //                        {

        //                            string callValuePatient = CallListOfDatabase.Where(c => c.CallID == int.Parse(CallAssayPatient.value)).Select(v => v.CallValue).FirstOrDefault();
        //                            string CurrentAssayIdPatient = CallAssayPatient.key;

        //                            var split = callValuePatient.Split('/');
        //                            if (split.Length == 1)
        //                            {
        //                                detectedCount++;
        //                            }
        //                            else
        //                            {
        //                                string callValuePatientSwaped = SwapCallValue(callValuePatient);
        //                                int callValuePatientSwapedId = CallListOfDatabase.Where(c => c.CallValue == callValuePatientSwaped).Select(i => i.CallID).FirstOrDefault();
        //                                var dedicatedCallAssayTranslator = currentTranslatorCallAssayList.Where(a => a.key == CurrentAssayIdPatient).FirstOrDefault();
        //                                if (dedicatedCallAssayTranslator.value == CallAssayPatient.value || dedicatedCallAssayTranslator.value == callValuePatientSwapedId.ToString())
        //                                {
        //                                    detectedCount++;
        //                                }
        //                            }

        //                        }


        //                        #endregion


        //                        #region call bug
        //                        // foreach call patient  to get matched translator call in normal form or swap form 
        //                        //foreach (var CallIdPatient in currentPatientCallIdsList)
        //                        //{

        //                        //    string callValuePatient = CallListOfDatabase.Where(c => c.CallID == CallIdPatient).Select(v => v.CallValue).FirstOrDefault();

        //                        //    var split = callValuePatient.Split('/');
        //                        //    if (split.Length == 1)
        //                        //    {
        //                        //        detectedCount++;
        //                        //    }
        //                        //    else
        //                        //    {
        //                        //        string callValuePatientSwaped = SwapCallValue(callValuePatient);
        //                        //        int callValuePatientSwapedId = CallListOfDatabase.Where(c => c.CallValue == callValuePatientSwaped).Select(i => i.CallID).FirstOrDefault();

        //                        //        if (currentTranslatorCallIdsList.Contains(CallIdPatient) || currentTranslatorCallIdsList.Contains(callValuePatientSwapedId))
        //                        //        {
        //                        //            detectedCount++;
        //                        //        }
        //                        //    }

        //                        //} 
        //                        #endregion

        //                        if (detectedCount == TranslatorAssayCallList.Count)
        //                        {
        //                            result_TranslatorAlleleID = TranslatorAlleleID;
        //                            result_TranslatorGeneID = TranslatorGeneID;
        //                            _PatientTranslatorViewModel = new PatientTranslatorViewModel();
        //                            _PatientTranslatorViewModel.GeneID = result_TranslatorGeneID;
        //                            _PatientTranslatorViewModel.AlleleID = result_TranslatorAlleleID;
        //                            _PatientTranslatorViewModel.PatientID = PateintId.Value;
        //                            _PatientTranslatorViewModel.ProjectID = _projectViewModels.ProjectID;
        //                            _PatientTranslatorViewModel.Probability = true;
        //                            _PatientTranslatorViewModel.Swap = false;
        //                            compareSucceed = true;
        //                        }
        //                    }


        //                    #endregion




        //                }


        //                #endregion

        //                #region old  comparing approach
        //                //foreach (var AssayCallTransltor in TranslatorAssayCallList)
        //                //{
        //                //    foreach (var AssayCallPatient in PatientLabAssayCallList)
        //                //    {
        //                //        if (AssayCallPatient.AssayID.Value == AssayCallTransltor.AssayID && AssayCallPatient.CallID.Value == AssayCallTransltor.CallID)
        //                //        {
        //                //            detectedCount++;
        //                //            result_TranslatorAlleleID = AssayCallTransltor.AlleleID;
        //                //            result_TranslatorGeneID = AssayCallTransltor.GeneID;
        //                //        }
        //                //    }
        //                //}

        //                //if (detectedCount == TranslatorAssayCallList.Count)
        //                //{
        //                //    _PatientTranslatorViewModel = new PatientTranslatorViewModel();
        //                //    _PatientTranslatorViewModel.GeneID = result_TranslatorGeneID;
        //                //    _PatientTranslatorViewModel.AlleleID = result_TranslatorAlleleID;
        //                //    _PatientTranslatorViewModel.PatientID = PateintId.Value;
        //                //    _PatientTranslatorViewModel.Swap = false;
        //                //    break;
        //                //}

        //                //else
        //                //{
        //                //    //_PatientTranslatorViewModel = new PatientTranslatorViewModel();
        //                //    //_PatientTranslatorViewModel.GeneID = PatientGeneId.Value;
        //                //    //_PatientTranslatorViewModel.AlleleID = null;
        //                //    //_PatientTranslatorViewModel.Swap = false;
        //                //    //_PatientTranslatorViewModel.PatientID = PateintId.Value;


        //                //} 
        //                #endregion
        //            }

        //            #endregion
        //        }




        //        #region Propabilty Stage
        //        if (!compareSucceed)
        //        {




        //            if (PatientTranslatorProbability != null && PatientTranslatorProbability.Count > 0)
        //            {
        //            }
        //            else
        //            {



        //                // undefind allele
        //                _PatientTranslatorViewModel = new PatientTranslatorViewModel();
        //                _PatientTranslatorViewModel.GeneID = PatientGeneId.Value;
        //                _PatientTranslatorViewModel.AlleleID = null;
        //                _PatientTranslatorViewModel.Swap = false;
        //                _PatientTranslatorViewModel.PatientID = PateintId.Value;
        //                _PatientTranslatorViewModel.ProjectID = _projectViewModels.ProjectID;

        //            }
        //        }
        //        #endregion



        //        return _PatientTranslatorViewModel;
        //    }
        //    catch (Exception ex)
        //    {

        //        string ee = ex.Message;
        //        PatientTranslatorProbability = null;
        //        return null;
        //    }

        //}

        #endregion


        private string SwapCallValue(string callValuePatient)
        {
            var callPatientArray = callValuePatient.ToString().Split('/');
            var callValueSwap = callPatientArray[1] + "/" + callPatientArray[0];
            return callValueSwap;
        }

        private bool CompareCallValues(int callIdTransltor, int callIdPatient , List<Call> CallListOfDatabase)
        {

            string callValuePatient = CallListOfDatabase.Where(c => c.CallID == callIdPatient).Select(v => v.CallValue).FirstOrDefault();
            string callValueTransltor = CallListOfDatabase.Where(c => c.CallID == callIdTransltor).Select(v => v.CallValue).FirstOrDefault();

            var callPatientArray = callValuePatient.ToString().Split('/');
            var callTranslatorArray = callValueTransltor.ToString().Split('/');

            if (
                callIdTransltor ==callIdPatient ||
                callPatientArray.Length==1 ||
                (callPatientArray[0] == callTranslatorArray[0] && callPatientArray[1] == callTranslatorArray[1]) ||
                (callPatientArray[0] == callTranslatorArray[1] && callPatientArray[1] == callTranslatorArray[0])
                )
            {
                return true;
            }
            else
                return false;



        }

        #region BuildCallSequenceString
        private List<int> BuildCallSequenceString(List<int> ListCallSequence, List<Call> CallListOfDatabase, out string LeftOneCallSequence, out string RightOneCallSequenc, out List<int> NOAMPLocationInCallList, bool ExcludeNoampMode)
        {
            LeftOneCallSequence = "";
            RightOneCallSequenc = "";



            NOAMPLocationInCallList = new List<int>();

            ArrayList callValueList = new ArrayList();

            int id = -1;
            for (int i = 0; i < ListCallSequence.Count; i++)
            {

                id = ListCallSequence[i];
                var call = CallListOfDatabase.Where(c => c.CallID == id).Select(c => c.CallValue).SingleOrDefault();
                callValueList.Add(call);
            }

            int j = 0;
            foreach (var singleCall in callValueList)
            {


                var callArray = singleCall.ToString().Split('/');

                if (callArray.Length > 1)
                {
                    LeftOneCallSequence += callArray[0];
                    RightOneCallSequenc += callArray[1];
                }
                else
                {

                    if (!ExcludeNoampMode)
                    {
                        LeftOneCallSequence += callArray[0];
                    }
                    NOAMPLocationInCallList.Add(j);
                }

                j++;
            }

            return NOAMPLocationInCallList;

        }

        #endregion


        #region BuildCallSequenceString
        private List<int> BuildCallSequenceStringModified(List<int> ListCallSequence, List<Call> CallListOfDatabase, out string LeftOneCallSequence, out string RightOneCallSequenc, out List<int> NOAMPLocationInCallList, bool ExcludeNoampMode)
        {
            LeftOneCallSequence = "";
            RightOneCallSequenc = "";



            NOAMPLocationInCallList = new List<int>();

            ArrayList callValueList = new ArrayList();

            int id = -1;
            for (int i = 0; i < ListCallSequence.Count; i++)
            {

                id = ListCallSequence[i];
                var call = CallListOfDatabase.Where(c => c.CallID == id).Select(c => c.CallValue).SingleOrDefault();
                callValueList.Add(call);
            }

            int j = 0;
            foreach (var singleCall in callValueList)
            {


                var callArray = singleCall.ToString().Split('/');

                if (callArray.Length > 1)
                {
                    LeftOneCallSequence += callArray[0];
                    RightOneCallSequenc += callArray[1];
                }
                else
                {

                    if (!ExcludeNoampMode)
                    {
                        LeftOneCallSequence += callArray[0];
                        RightOneCallSequenc += callArray[0];
                    }
                    NOAMPLocationInCallList.Add(j);
                }

                j++;
            }

            return NOAMPLocationInCallList;

        }

        #endregion


        private void BuildCallExcludedSequenceString(List<int> ListCallSequence, List<Call> CallListOfDatabase, out string LeftOneCallSequence, out string RightOneCallSequenc, List<int> NOAMPLocationInCallList)
        {
            LeftOneCallSequence = "";
            RightOneCallSequenc = "";


            ArrayList callValueList = new ArrayList();

            int id = -1;
            for (int i = 0; i < ListCallSequence.Count; i++)
            {

                id = ListCallSequence[i];
                var call = CallListOfDatabase.Where(c => c.CallID == id).Select(c => c.CallValue).SingleOrDefault();
                callValueList.Add(call);
            }



            int k = 0;
            foreach (var singleCall in callValueList)
            {

                if (!NOAMPLocationInCallList.Contains(k))
                {

                    var callArray = singleCall.ToString().Split('/');
                    if (callArray.Length > 1)
                    {
                        LeftOneCallSequence += callArray[0];
                        RightOneCallSequenc += callArray[1];
                    }
                    else 
                    {
                        LeftOneCallSequence += callArray[0];

                    }

               }

                k++;
            }



        }






        public bool ExecuteComparing(List<string> status, HttpContext ctx, string uploadStatusKey, ProjectViewModel _projectViewModels, ref ProjectEntities projectEntities)
        {
            bool operationStatus = false;
            updateSessionStatus(status, "----------------------------------------------------------------", ctx, uploadStatusKey);

            updateSessionStatus(status, string.Format("Comparing Stage"), ctx, uploadStatusKey);
            updateSessionStatus(status, "", ctx, uploadStatusKey);

            int PatientTranslatorInsertedCounts = 0;
            int PatientTranslatorUpdatedCounts = 0;


            operationStatus = Result(out PatientTranslatorInsertedCounts, out PatientTranslatorUpdatedCounts, ctx, status, uploadStatusKey, _projectViewModels  , ref  projectEntities);

           //    updateSessionStatus(status, string.Format("{0} new records in Patient Translator  ", PatientTranslatorInsertedCounts), ctx, uploadStatusKey);
           // updateSessionStatus(status, string.Format("{0} updated records in Patient Translator  ", PatientTranslatorUpdatedCounts), ctx, uploadStatusKey);

            return operationStatus;
        }
    }
}
