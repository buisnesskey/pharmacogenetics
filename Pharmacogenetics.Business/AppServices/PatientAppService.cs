﻿using Pharmacogenetics.Business.Bases;
using Pharmacogenetics.Models.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Pharmacogenetics.Helper;
using System.Data;
using Pharmacogenetics.Models.ViewModels.ImportViewModels;
using Pharmacogenetics.Entities.MainModel;
using Pharmacogenetics.BaseClasses.Enums;
using Pharmacogenetics.EntityService.Bases;
using Pharmacogenetics.Business.Containers;
using System.Text;

namespace Pharmacogenetics.Business.AppServices
{
    public class PatientAppService : AppService
    {
        #region CTOR
        public PatientAppService() : base() { }
        public PatientAppService(UnitOfWork unitOfWork) : base(unitOfWork) { }
        #endregion
        #region Methods
        /// <summary>
        /// Get all patients
        /// </summary>
        /// <returns>List of patient view model</returns>
        //public List<PatientViewModel> GetAllPatients()
        //{
        //    List<PatientViewModel> patientsViewModelList = null;
        //    var result = AppServiceManager.PatientAppService.TheUnitOfWork.Patient.GetAll().ToList();
        //    patientsViewModelList = JsonConvert.DeserializeObject<List<PatientViewModel>>(JsonConvert.SerializeObject(TheUnitOfWork.Patient.GetAll().ToList()));
        //    return patientsViewModelList;
        //}
        /// <summary>
        /// Get patients for grid with patient viewmodel for search
        /// </summary>
        /// <param name="model"></param>
        /// <param name="jtStartIndex"></param>
        /// <param name="jtPageSize"></param>
        /// <param name="jtSorting"></param>
        /// <param name="totalCount"></param>
        /// <returns>List of patient view model</returns>
        public List<PatientViewModel> GetAllPatients(PatientViewModel model, int jtStartIndex, int jtPageSize, string jtSorting, out int totalCount)
        {
            List<PatientViewModel> usersViewModelList = null;
            totalCount = new int();
            var result = AppServiceManager.PatientAppService.TheUnitOfWork.Patient.GetAll().ToList();
            usersViewModelList = JsonConvert.DeserializeObject<List<PatientViewModel>>(JsonConvert.SerializeObject(TheUnitOfWork.Patient.GetAllPatients(string.Empty, string.Empty, 1, jtStartIndex, jtPageSize, jtSorting, out totalCount).ToList()));
            return usersViewModelList;
        }
        /// <summary>
        /// Get patient by id
        /// </summary>
        /// <param name="patientId"></param>
        /// <returns>Patient view model</returns>
        public PatientViewModel GetPatient(int patientId)
        {
            PatientViewModel PatientViewModel = null;
            PatientViewModel = JsonConvert.DeserializeObject<PatientViewModel>(JsonConvert.SerializeObject(TheUnitOfWork.Patient.GetById(patientId)));
            return PatientViewModel;
        }
        /// <summary>
        /// Parse training data sheet (Excel file)
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="sheetName">sheet name in the excel file</param>
        /// <param name="status">out put status for operation progress</param>
        /// <returns></returns>
        public List<PatientResultViewModel> ParsePatientData(string fileName, List<string> status, string uploadStatusKey)
        {
            bool operationStatus = false;
            ExcelReader excelReader = new ExcelReader();
            //extract the data from the patient sheet as DataTable
            DataTable dt = excelReader.ReadExcelFile(fileName);
            //check if the file contain any rows
            operationStatus = dt != null && dt.Rows.Count > new int();
            if (operationStatus)
            {
                updateSessionStatusInner(status, string.Format("{0} Records in Patient Lab File", dt.Rows.Count), uploadStatusKey);
                updateSessionStatusInner(status, "Convert records to valid entities", uploadStatusKey);
                //convert the dataTable that readed from the sheet to real objects model (patient data genes, allele and assays)
                List<TrainingAnalysisViewModel> tList = ParseToModel(dt);
                //order to avoid null in the first
                tList =
                    tList.OrderByDescending(x => x.SampleID)
                        .OrderByDescending(x => x.GeneSymbol)
                        .OrderByDescending(x => x.AssayName)
                        .OrderByDescending(x => x.AssayID)
                        .ToList();
                updateSessionStatusInner(status, "Order patients records", uploadStatusKey);
                operationStatus = tList != null && tList.Count > new int();
                if (operationStatus)
                {
                    updateSessionStatusInner(status, "Group patients records", uploadStatusKey);
                    //grouping the data to  (patient and it's data genes, allele and assays)
                    List<PatientResultViewModel> finalResult = GroupData(tList);
                    operationStatus = finalResult != null && finalResult.Count > new int();
                    if (operationStatus)
                    {
                        updateSessionStatusInner(status, string.Format("{0} Patients in Patient Lab File", finalResult.Count), uploadStatusKey);
                        return finalResult;
                    }
                }
            }
            return null;
        }

        //public List<PatientResultViewModel> ParseAllPatientData(string fileName, List<string> status, string uploadStatusKey)
        //{
        //    bool operationStatus = false;
        //    ExcelReader excelReader = new ExcelReader();
        //    DataTable dt = excelReader.ReadExcelFile(fileName);
        //    operationStatus = dt != null && dt.Rows.Count > new int();
        //    if (operationStatus)
        //    {
        //        updateSessionStatusInner(status, string.Format("{0} Records in the Sheet File", dt.Rows.Count), uploadStatusKey);
        //        updateSessionStatusInner(status, "Convert records to valid entities", uploadStatusKey);
        //        List<TrainingAnalysisViewModel> tList = ParseToModel(dt);
        //        //order to avoid null in the first
        //        tList =
        //            tList.OrderByDescending(x => x.SampleID)
        //                .OrderByDescending(x => x.GeneSymbol)
        //                .OrderByDescending(x => x.AssayName)
        //                .OrderByDescending(x => x.AssayID)
        //                .ToList();

        //        updateSessionStatusInner(status, "Order patients records", uploadStatusKey);
        //        operationStatus = tList != null && tList.Count > new int();
        //        if (operationStatus)
        //        {
        //            updateSessionStatusInner(status, "Group patients records", uploadStatusKey);
        //            List<PatientResultViewModel> finalResult = GroupData(tList);
        //            operationStatus = finalResult != null && finalResult.Count > new int();
        //            if (operationStatus)
        //            {
        //                updateSessionStatusInner(status, string.Format("{0} Patient in the Sheet File", finalResult.Count), uploadStatusKey);
        //                operationStatus = StoreData(finalResult, status, uploadStatusKey);
        //                if (operationStatus)
        //                {
        //                    //PatientTranslatorService PatientTranslatorService = new PatientTranslatorService();
        //                    //int PatientTranslatorCounts = 0;
        //                    //operationStatus = PatientTranslatorService.Result(out PatientTranslatorCounts);
        //                    //updateSessionStatus(status, string.Format("{0} new records in Patient Translator  ", PatientTranslatorCounts), uploadStatusKey);

        //                    PatientTranslatorService PatientTranslatorService = new PatientTranslatorService();

        //                    int PatientTranslatorInsertedCounts = 0;
        //                    int PatientTranslatorUpdatedCounts = 0;

        //                    operationStatus = PatientTranslatorService.Result(out PatientTranslatorInsertedCounts, out  PatientTranslatorUpdatedCounts);
        //                    updateSessionStatusInner(status, string.Format("{0} new records in Patient Translator  ", PatientTranslatorInsertedCounts), uploadStatusKey);
        //                    updateSessionStatusInner(status, string.Format("{0} updated records in Patient Translator  ", PatientTranslatorUpdatedCounts), uploadStatusKey);


        //                }
        //            }
        //        }
        //    }
        //    return null;
        //}

        //public bool StorePatientData(PatientResultViewModel patient, List<string> status, List<string> prevStatus, string uploadStatusKey)
        //{
        //    updateSessionStatus(prevStatus, string.Format("Progress {0} %", Guid.NewGuid()), uploadStatusKey, true);
        //    bool operationStatus = false;
        //    clearSessionStatus();
        //    updateSessionStatusInner(status, string.Format("Analysis patient {0} records", patient.PatientID), uploadStatusKey);
        //    operationStatus = StoreData(patient, status, uploadStatusKey);
        //    return operationStatus;

        //}
        /// <summary>
        /// Convert datatable to list of traing analysis view model
        /// </summary>
        /// <param name="dt"></param>
        /// <returns>list of traing analysis view model</returns>
        public List<TrainingAnalysisViewModel> ParseToModel(DataTable dt)
        {
            #region put in list model

            List<TrainingAnalysisViewModel> tList = new List<TrainingAnalysisViewModel>();
            int j = 0;
            //iterate over the rows and columns and print to the console as it appears in the file
            //excel is not zero based!!
            for (int i = 1; i < dt.Rows.Count; i++)
            {
                //read cell cell
                j = 0;
                TrainingAnalysisViewModel tpatient = new TrainingAnalysisViewModel();
                tpatient.AssayName = readCell(dt.Rows[i][j++]);
                tpatient.AssayID = readCell(dt.Rows[i][j++]);
                tpatient.GeneSymbol = readCell(dt.Rows[i][j++]);
                tpatient.NCBISNPReference = readCell(dt.Rows[i][j++]);
                tpatient.SampleID = readCell(dt.Rows[i][j++]);
                if (!string.IsNullOrEmpty(tpatient.SampleID))
                {
                    tpatient.Call = readCell(dt.Rows[i][j++]);
                    tpatient.Manual = readCell(dt.Rows[i][j++]);
                    tpatient.Quality = readCell(dt.Rows[i][j++]);
                    tpatient.VIC_Rn = readCell(dt.Rows[i][j++]);
                    tpatient.FAM_Rn = readCell(dt.Rows[i][j++]);
                    tpatient.ROX = readCell(dt.Rows[i][j++]);
                    tpatient.Task = readCell(dt.Rows[i][j++]);
                    tpatient.Gender = readCell(dt.Rows[i][j++]);
                    tpatient.Population = readCell(dt.Rows[i][j++]);
                    tpatient.Well = readCell(dt.Rows[i][j++]);
                    tpatient.ExperimentName = readCell(dt.Rows[i][j++]);
                    tpatient.PlateBarcode = readCell(dt.Rows[i][j++]);
                    tpatient.Annotation = readCell(dt.Rows[i][j++]);
                    tpatient.LowROXIntensity = readCell(dt.Rows[i][j++]);
                    tpatient.NTCFAMIntensityHigh = readCell(dt.Rows[i][j++]);
                    tpatient.NTCVICIntensityHigh = readCell(dt.Rows[i][j++]);
                    tpatient.GenotypeQualityLow = readCell(dt.Rows[i][j++]);
                    tpatient.FailedControl = readCell(dt.Rows[i][j++]);
                    tpatient.ReferenceSampleDiscordance = readCell(dt.Rows[i][j++]);
                    tpatient.ReplicateSampleDiscordance = readCell(dt.Rows[i][j++]);
                    tpatient.Chromosome = readCell(dt.Rows[i][j++]);
                    tpatient.Position = readCell(dt.Rows[i][j++]);
                    tList.Add(tpatient);
                }
            }

            #endregion

            return tList;
        }
        /// <summary>
        /// Group list of traing analysis view model by patient,gene, assays and calls
        /// </summary>
        /// <param name="tList"></param>
        /// <returns>list of patient result view model</returns>
        public List<PatientResultViewModel> GroupData(List<TrainingAnalysisViewModel> tList)
        {
            return tList
                .GroupBy(a => a.SampleID)
                .Select(
                    g =>
                        new PatientResultViewModel
                        {
                            //PatientID = string.IsNullOrWhiteSpace(g.Key) ? 0 : Int32.Parse(g.Key),
                            PatientID = g.Key,
                            //number = g.Sum(n => n.number),
                            GeneList =
                                g.GroupBy(b => b.GeneSymbol)
                                    .Select(
                                        gg =>
                                            new GeneResultViewModel
                                            {
                                                GeneSymbol = gg.Key,
                                                //otherNumber = gg.Sum(b => b.otherNumber)
                                                AssayList =
                                                    gg.GroupBy(b => new { b.AssayName, b.AssayID, b.NCBISNPReference })
                                                        .Select(
                                                            ggg =>
                                                                new AssayResultViewModel
                                                                {
                                                                    AssayName = ggg.Key.AssayName,
                                                                    //otherNumber = gg.Sum(b => b.otherNumber)
                                                                    AssaySNPRef = ggg.Key.NCBISNPReference,
                                                                    AssayID = ggg.Key.AssayID,
                                                                    Call =
                                                                        new CallViewModel()
                                                                        {
                                                                            CallValue =
                                                                                ggg.Select(x => x.Call).FirstOrDefault()
                                                                        }
                                                                }).ToList()
                                            }).ToList()
                        }).ToList();
        }
        /// <summary>
        /// store list of patient result view model to the database after checking existing of patient , gene and assay
        /// </summary>
        /// <param name="finalResult"></param>
        /// <param name="status"></param>
        /// <returns>boolean for operation status</returns>
        //public bool StoreData(List<PatientResultViewModel> finalResult, List<string> status, string uploadStatusKey)
        //{
        //    bool operationStatus = false;
        //    #region store in database
        //    Pharmacogenetics.Business.AppServices.AppServiceManager appManager =
        //        new Pharmacogenetics.Business.AppServices.AppServiceManager();
        //    DateTime curDate = DateTime.Now;
        //    int newGene = new int(),
        //        newAssay = new int(),
        //        newPatient = new int(),
        //        updateGene = new int(),
        //        updateAssay = new int(),
        //        updatePatient = new int(),
        //        updateCall = new int();

        //    #region check patients data
        //    int patientIter = 0;
        //    foreach (var curPatient in finalResult)
        //    {
        //        try
        //        {
        //            patientIter++;
        //            updateSessionStatusInner(status, string.Format("Check patient {0} {1}/{2} records", curPatient.PatientID, patientIter, finalResult.Count), uploadStatusKey);
        //            #region checkPatient
        //            var patient =
        //                appManager.TheUnitOfWork.Patient.GetWhere(x => x.PatientName == curPatient.PatientID.ToString())
        //                    .FirstOrDefault();
        //            bool patientExist = patient != null;
        //            if (!patientExist)
        //            {
        //                patient = new Patient()
        //                {
        //                    //MedicalID = curPatient.ID,
        //                    PatientName = curPatient.PatientID.ToString(),
        //                    CreateDate = curDate,
        //                    DeleteStatus = (byte)CommonEnums.DeleteStatus.NotDeleted
        //                };
        //            }

        //            #endregion
        //            #region check genes list
        //            int geneIter = 0;
        //            foreach (var curGene in curPatient.GeneList)
        //            {
        //                geneIter++;
        //                updateSessionStatusInner(status, string.Format("Check Gene {0} {1}/{2} records", curGene.GeneSymbol, geneIter, curPatient.GeneList.Count()), uploadStatusKey, geneIter > 1 ? true : false);
        //                #region check gene
        //                var gene =
        //                    appManager.TheUnitOfWork.Gene.GetWhere(x => x.GeneSymbol == curGene.GeneSymbol)
        //                        .FirstOrDefault();
        //                bool geneExist = gene != null;
        //                if (!geneExist)
        //                {
        //                    gene = new Gene()
        //                    {
        //                        GeneSymbol = curGene.GeneSymbol,
        //                        CreateDate = curDate,
        //                        DeleteStatus = (byte)CommonEnums.DeleteStatus.NotDeleted
        //                    };
        //                }

        //                #endregion
        //                #region Assays list
        //                foreach (var curAssay in curGene.AssayList)
        //                {
        //                    if (!string.IsNullOrWhiteSpace(curAssay.AssayID))
        //                    {
        //                        #region check assay item
        //                        var assay =
        //                            appManager.TheUnitOfWork.Assay.GetWhere(x => x.AssayID == curAssay.AssayID)
        //                                .FirstOrDefault();
        //                        bool assayToGeneExist = false; //gene symbol not empty
        //                        if (!geneExist && string.IsNullOrWhiteSpace(curGene.GeneSymbol) && assay != null)
        //                        //gene not exist and gene symbol is empty
        //                        {
        //                            var translatorRecord = appManager.TheUnitOfWork.Translator.GetWhere(x => x.AssayID == assay.ID, "Gene").FirstOrDefault();
        //                            gene = translatorRecord.Gene;
        //                            //var translatorRecord = appManager.TheUnitOfWork.Translator.GetWhere(x => x.AssayID == assay.ID, "TranslatorMapping.Gene").FirstOrDefault();
        //                            //gene = translatorRecord.TranslatorMapping.Gene;
        //                            assayToGeneExist = gene != null;
        //                            //assay exist and related to gene in translator table
        //                        }
        //                        if (!string.IsNullOrWhiteSpace(curGene.GeneSymbol) || assayToGeneExist)
        //                        {
        //                            bool assayExist = assay != null;
        //                            if (!assayExist)
        //                            {
        //                                assay = new Assay()
        //                                {
        //                                    AssayID = curAssay.AssayID,
        //                                    AssayName = curAssay.AssayName,
        //                                    AssaySNPRef = curAssay.AssaySNPRef,
        //                                    GeneID = gene.GeneID,
        //                                    //Gene = gene,
        //                                    CreateDate = curDate,
        //                                    DeleteStatus = (byte)CommonEnums.DeleteStatus.NotDeleted,
        //                                };
        //                            }

        //                            #region check call
        //                            var call =
        //                                appManager.TheUnitOfWork.Call.GetWhere(
        //                                    x => x.CallValue == curAssay.Call.CallValue).FirstOrDefault();
        //                            bool callExist = call != null;
        //                            if (callExist)
        //                            {
        //                                if (!patientExist)
        //                                {
        //                                    appManager.TheUnitOfWork.Patient.Insert(patient);
        //                                    newPatient++;
        //                                }
        //                                if (!geneExist && !assayToGeneExist)
        //                                {
        //                                    appManager.TheUnitOfWork.Gene.Insert(gene);
        //                                    newGene++;
        //                                }
        //                                if (!assayExist)
        //                                {
        //                                    appManager.TheUnitOfWork.Assay.Insert(assay);
        //                                    newAssay++;
        //                                }
        //                                #region insert patient lab result
        //                                var patientLabRecord =
        //                                    appManager.TheUnitOfWork.PatientLab.GetWhere(
        //                                        x =>
        //                                            x.PatientID == patient.PatientID && x.GeneID == gene.GeneID &&
        //                                            x.AssayID == assay.ID).FirstOrDefault();
        //                                if (patientLabRecord == null)
        //                                {
        //                                    appManager.TheUnitOfWork.PatientLab.Insert(new PatientLab()
        //                                    {
        //                                        PatientID = patient.PatientID,
        //                                        AssayID = assay.ID,
        //                                        CallID = call.CallID,
        //                                        GeneID = gene.GeneID,
        //                                        //Patient = patient,
        //                                        //Assay = assay,
        //                                        //Call = call,
        //                                        //Gene = gene,
        //                                        CreateDate = curDate,
        //                                        DeleteStatus = (byte)CommonEnums.DeleteStatus.NotDeleted,
        //                                    });
        //                                }
        //                                else
        //                                {
        //                                    if (patientLabRecord.CallID != call.CallID)
        //                                    {
        //                                        patientLabRecord.CallID = call.CallID;
        //                                        appManager.TheUnitOfWork.PatientLab.Update(patientLabRecord);
        //                                        updateCall++;
        //                                    }
        //                                }
        //                                #endregion
        //                                appManager.TheUnitOfWork.Commit();
        //                                patientExist = true;
        //                                geneExist = true;
        //                            }
        //                            #endregion
        //                        }
        //                        #endregion
        //                    }
        //                }
        //                #endregion
        //            }
        //            #endregion
        //        }
        //        catch (Exception ex)
        //        {
        //            continue;
        //        }
        //    }
        //    #endregion
        //    #endregion
        //    //
        //    updateSessionStatusInner(status, string.Format("{0} new Patients", newPatient), uploadStatusKey);
        //    updateSessionStatusInner(status, string.Format("{0} new Genes", newGene), uploadStatusKey);
        //    updateSessionStatusInner(status, string.Format("{0} new Assays", newAssay), uploadStatusKey);
        //    updateSessionStatusInner(status, string.Format("{0} update Calls", updateCall), uploadStatusKey);
        //    operationStatus = newPatient > new int() || newGene > new int() || newAssay > new int() || updateCall > new int();
        //    return operationStatus;
        //}


        public bool StoreData(List<PatientResultViewModel> finalResult, List<string> status, List<string> prevStatus, string uploadStatusKey, ProjectViewModel projectViewModel, ProjectEntities projectEntities)
        {
            bool operationStatus = false;

            #region store in database
            //Pharmacogenetics.Business.AppServices.AppServiceManager appServiceManager = new Pharmacogenetics.Business.AppServices.AppServiceManager();
            DateTime curDate = DateTime.Now;
            int newGene = new int(),
                newAssay = new int(),
                newPatient = new int(),
                updateGene = new int(),
                updateAssay = new int(),
                updatePatient = new int(),
                updateCall = new int(),
                invalidCalls = new int();

            //var allPateintList = appServiceManager.TheUnitOfWork.Patient.GetAll().ToList();
            //var allGeneList = appServiceManager.TheUnitOfWork.Gene.GetAll().ToList();
            //var allAssayList = appServiceManager.TheUnitOfWork.Assay.GetAll("Gene").ToList();
            //var allCallList = appServiceManager.TheUnitOfWork.Call.GetAll().ToList();
            //var allPatientLabList = projectViewModel.ProjectID > 0 ? appServiceManager.TheUnitOfWork.PatientLab.GetWhere(x => x.ProjectID == projectViewModel.ProjectID).ToList() : new List<PatientLab>();

            #region check patients data
            int patientIter = 0;

            foreach (var curPatient in finalResult)
            {
                try
                {
                    patientIter++;
                    //formula to calculate the progress
                    updateSessionStatus(prevStatus, string.Format("Progress {0} %", Math.Round(((double)(patientIter - 1) / finalResult.Count) * 100, 2)), uploadStatusKey, true);

                    #region check Patient if exist else create it
                    var patient = projectEntities.allPateintList.Where(x => x.PatientName == curPatient.PatientID.ToString()).FirstOrDefault();
                    bool patientExist = patient != null;
                    if (!patientExist)
                    {
                        patient = new Patient()
                        {
                            //MedicalID = curPatient.ID,
                            PatientName = curPatient.PatientID.ToString(),
                            CreateDate = curDate,
                            DeleteStatus = (byte)CommonEnums.DeleteStatus.NotDeleted
                        };
                    }

                    #endregion
                    #region check genes list
                    int geneIter = 0;
                    foreach (var curGene in curPatient.GeneList)
                    {
                        geneIter++;
                        #region check gene
                        var gene = projectEntities.allGeneList.Where(x => x.GeneSymbol.ToLower() == curGene.GeneSymbol.ToLower()).FirstOrDefault();
                        var parentGeneId = new int();
                        var parentGene = new Gene();
                        bool geneExist = gene != null;
                        if (!geneExist && !string.IsNullOrWhiteSpace(curGene.GeneSymbol))
                        {
                            gene = new Gene()
                            {
                                GeneSymbol = curGene.GeneSymbol,
                                CreateDate = curDate,
                                DeleteStatus = (byte)CommonEnums.DeleteStatus.NotDeleted
                            };
                        }

                        #endregion
                        #region Assays list
                        foreach (var curAssay in curGene.AssayList)
                        {
                            if (string.IsNullOrWhiteSpace(curGene.GeneSymbol))
                                geneExist = false;
                            if (!string.IsNullOrWhiteSpace(curAssay.AssayID))
                            {
                                #region check assay item
                                var assay = projectEntities.allAssayList.Where(x => x.AssayID.ToLower() == curAssay.AssayID.ToLower()).FirstOrDefault();
                                if (assay != null && assay.Gene != null)
                                {
                                    //gene that in the patent lab exist and is different than translator
                                    if (geneExist && gene.GeneSymbol != assay.Gene.GeneSymbol)
                                    {
                                        if (gene.GeneID > 0)
                                            parentGeneId = gene.GeneID;
                                        gene = assay.Gene;
                                        geneExist = true;
                                    }//gene that in the patent lab not exist and the patientlab's gene symbol not equal assay's gene
                                    else if (!geneExist && !string.IsNullOrWhiteSpace(curGene.GeneSymbol) && curGene.GeneSymbol != assay.Gene.GeneSymbol)
                                    {
                                        parentGeneId = -1;
                                        parentGene = new Gene()
                                        {
                                            GeneSymbol = curGene.GeneSymbol,
                                            CreateDate = curDate,
                                            DeleteStatus = (byte)CommonEnums.DeleteStatus.NotDeleted
                                        };
                                    }
                                }
                                //gene symbol is empty but assay already have gene
                                if (!geneExist && string.IsNullOrWhiteSpace(curGene.GeneSymbol) && assay != null && assay.Gene != null)
                                {
                                    //gene not exist and gene symbol is empty
                                    gene = assay.Gene;
                                    //assayToGeneExist = gene != null;
                                    geneExist = true;
                                    //assay exist and related to gene in translator table
                                }
                                //gene symbol 
                                //else if (!geneExist && assay != null && assay.Gene != null)
                                //{
                                //    gene = assay.Gene;
                                //    geneExist = true;
                                //}

                                if (!string.IsNullOrWhiteSpace(curGene.GeneSymbol) || geneExist)
                                {
                                    bool assayExist = assay != null;
                                    if (!assayExist)
                                    {
                                        assay = new Assay()
                                        {
                                            AssayID = curAssay.AssayID,
                                            AssayName = curAssay.AssayName,
                                            AssaySNPRef = curAssay.AssaySNPRef,
                                            //GeneID = gene.GeneID,
                                            Gene = gene,
                                            CreateDate = curDate,
                                            DeleteStatus = (byte)CommonEnums.DeleteStatus.NotDeleted,
                                        };
                                    }

                                    #region check call
                                    var call = projectEntities.allCallList.Where(x => x.CallValue.ToLower() == curAssay.Call.CallValue.ToLower()).FirstOrDefault();
                                    bool callExist = call != null;
                                    if (!callExist)
                                    {
                                        call = new Call()
                                        {
                                            CallValue = curAssay.Call.CallValue,
                                            CreateDate = curDate,
                                            DeleteStatus = (byte)CommonEnums.DeleteStatus.NotDeleted,
                                        };
                                        projectEntities.allCallList.Add(call);
                                        //appManager.TheUnitOfWork.Call.Insert(call);
                                        //invalidCalls++;
                                    }
                                    if (!patientExist)
                                    {
                                        projectEntities.allPateintList.Add(patient);
                                        //appManager.TheUnitOfWork.Patient.Insert(patient);
                                        newPatient++;
                                    }
                                    if (!geneExist)
                                    {
                                        projectEntities.allGeneList.Add(gene);
                                        //appManager.TheUnitOfWork.Gene.Insert(gene);
                                        newGene++;
                                    }
                                    if (!assayExist)
                                    {
                                        projectEntities.allAssayList.Add(assay);
                                        //appManager.TheUnitOfWork.Assay.Insert(assay);
                                        newAssay++;
                                    }

                                    #region insert patient lab result
                                    //var patientLabRecord =
                                    //    appManager.TheUnitOfWork.PatientLab.GetWhere(
                                    //        x =>
                                    //            x.PatientID == patient.PatientID && x.GeneID == gene.GeneID &&
                                    //            x.AssayID == assay.ID &&
                                    //            x.PositionID == (int)currentPosition).FirstOrDefault();
                                    var patientLabRecord = projectEntities.allPatientLabList.Where(
                                            x =>
                                                x.ProjectID == projectViewModel.ProjectID &&
                                                x.PatientID == patient.PatientID &&
                                                x.GeneID == gene.GeneID &&
                                                x.AssayID == assay.ID
                                                ).FirstOrDefault();
                                    //appManager.TheUnitOfWork.PatientLab.GetWhere(
                                    //    x =>
                                    //        x.ProjectID == projectViewModel.ProjectID &&
                                    //        x.PatientID == patient.PatientID && 
                                    //        x.GeneID == gene.GeneID &&
                                    //        x.AssayID == assay.ID).FirstOrDefault();



                                    bool patientLabRecordExist = patientLabRecord != null;
                                    if (!patientLabRecordExist)
                                    {
                                        var newPatientLabRecord = new PatientLab()
                                        {
                                            //PatientID = patient.PatientID,
                                            Patient = patient,
                                            //AssayID = assay.ID,
                                            Assay = assay,
                                            //CallID = call.CallID,
                                            //GeneID = gene.GeneID,
                                            Gene = gene,
                                            //ProjectID = projectViewModel.ProjectID,
                                            ProjectID = projectViewModel.ProjectID,
                                            //Patient = patient,
                                            //Assay = assay,
                                            //Call = call,
                                            //Gene = gene,
                                            CreateDate = curDate,
                                            DeleteStatus = (byte)CommonEnums.DeleteStatus.NotDeleted,
                                            CallType = (byte)CommonEnums.CallTypeEnum.Normal,
                                        };

                                        int testNumber = 0;
                                        if (int.TryParse(curAssay.Call.CallValue, out testNumber))
                                            newPatientLabRecord.CallType = (byte)CommonEnums.CallTypeEnum.CopyNumber;
                                        //Need to fix
                                        //if (callExist)
                                        newPatientLabRecord.Call = call;

                                        if (parentGeneId > 0)
                                            newPatientLabRecord.ParentGeneID = parentGeneId;
                                        else if (parentGeneId == -1)
                                            newPatientLabRecord.Gene1 = parentGene;
                                        else
                                            newPatientLabRecord.Gene1 = gene;
                                        //newPatientLabRecord.CallID = call.CallID;
                                        //   newPatientLabRecord.PositionID = (int)currentPosition;
                                        //else
                                        //    newPatientLabRecord.InvalidCall = curAssay.Call.CallValue;
                                        projectEntities.allPatientLabList.Add(newPatientLabRecord);
                                        //appManager.TheUnitOfWork.PatientLab.Insert(newPatientLabRecord);
                                    }
                                    else
                                    {
                                        //update call value for patient lab record
                                        //if (patientLabRecord.CallID != call.CallID)
                                        //{
                                        //    //avoid overwrite invalid call
                                        //    if (callExist)
                                        //    {
                                        //        patientLabRecord.CallID = call.CallID;
                                        //        //appManager.TheUnitOfWork.PatientLab.Update(patientLabRecord);
                                        //        updateCall++;
                                        //    }
                                        //}
                                    }

                                    #endregion
                                    //appManager.TheUnitOfWork.Commit();
                                    patientExist = true;
                                    geneExist = true;
                                    assayExist = true;
                                    callExist = true;

                                    #endregion
                                }
                                #endregion
                            }
                        }
                        #endregion
                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    continue;
                }
            }
            TheUnitOfWork.Patient.InsertList(projectEntities.allPateintList.Where(x => x.PatientID <= 0).ToList());
            TheUnitOfWork.Gene.InsertList(projectEntities.allGeneList.Where(x => x.GeneID <= 0).ToList());
            TheUnitOfWork.Assay.InsertList(projectEntities.allAssayList.Where(x => x.ID <= 0).ToList());
            TheUnitOfWork.Call.InsertList(projectEntities.allCallList.Where(x => x.CallID <= 0).ToList());
            //TheUnitOfWork.PatientLab.InsertList(projectEntities.allPatientLabList.Where(x => x.ID <= 0).ToList());

            updateSessionStatus(prevStatus, "Saving Patient lab..........", uploadStatusKey);
            TheUnitOfWork.Commit();

            try
            {
                TranslatorHelper _TranslatorHelper = new TranslatorHelper();
                _TranslatorHelper.InsertPatientLabListWithSqlCommandsApproach(status, uploadStatusKey, projectEntities.allPatientLabList);

            }
            catch (Exception ex)
            {

                string ss = ex.Message;

            }


            #endregion
            #endregion
            updateSessionStatus(prevStatus, string.Format("Progress {0} %", Math.Round(((double)patientIter / finalResult.Count) * 100, 2)), uploadStatusKey, true);
            //updateSessionStatus(status, string.Format("{0} new Patients", newPatient), uploadStatusKey);
            updateSessionStatus(prevStatus, string.Format("{0} new Assays", newAssay), uploadStatusKey);
            updateSessionStatus(prevStatus, string.Format("{0} new Genes", newGene), uploadStatusKey);
            //updateSessionStatus(prevStatus, string.Format("{0} update Calls", updateCall), uploadStatusKey);
            //updateSessionStatus(prevStatus, string.Format("{0} Invalid Calls", invalidCalls), uploadStatusKey);
            operationStatus = newPatient > new int() || newGene > new int() || newAssay > new int() || updateCall > new int();
            return operationStatus;
        }

        /// <summary>
        /// extract value form the excel sheet cell
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public string readCell(object o)
        {
            return o == null ? string.Empty : o.ToString();
        }

        public void updateSessionStatusInner(List<string> status, string currentStatus, string uploadStatusKey, bool removeLastRecord = false)
        {
            //if (removeLastRecord)
            //    status.RemoveAt(status.Count - 1);
            status.Add(currentStatus);
            System.Web.HttpContext.Current.Application["listStatusInner" + uploadStatusKey] = status;
        }

        public void updateSessionStatus(List<string> status, string currentStatus, string uploadStatusKey, bool removeLastRecord = false)
        {
            if (removeLastRecord && status.Count > 0)
                status.RemoveAt(status.Count - 1);
            status.Add(currentStatus);
            System.Web.HttpContext.Current.Application["listStatus" + uploadStatusKey] = status;
        }

        public void clearSessionStatus()
        {
            System.Web.HttpContext.Current.Application["listStatusInner"] = new List<string>();
        }
        #endregion

        #region PatientOutput
        /// <summary>
        /// Get list of Patients , Genes and Alleles from database filtered by Project Id , Patient Id , Gene Id , Undefined Allele ,  Allele 1 , Allele 2
        /// </summary>
        /// <param name="projectIdFilter"></param>
        /// <param name="patientIdFilter"></param>
        /// <param name="geneIdFilter"></param>
        /// <param name="undefinedAllele"></param>
        /// <param name="alleleProbability"></param>
        /// <param name="allele1NameFilter"></param>
        /// <param name="allele2NameFilter"></param>
        /// <returns>List of PatientGeneAlleleViewModel</returns>
        public List<PatientGeneAlleleViewModel> GetPatientGenesAndAlleles(List<int> projectIdFilter, List<int> patientIdFilter,List<int> geneIdFilter, bool undefinedAllele,bool alleleProbability, List<string> allele1NameFilter,
            List<string> allele2NameFilter)
        {
            List<PatientGeneAlleleViewModel> patientGeneAlleles = new List<PatientGeneAlleleViewModel>();
            try
            {
                //Get All Patients after filtering
                List<Patient> patients = null;

                bool projectIdCondition = projectIdFilter != null && projectIdFilter.All(x => x != 0);
                bool patientIdCondition = patientIdFilter != null && patientIdFilter.All(x => x != 0);
                bool geneCondition = geneIdFilter != null && geneIdFilter.All(x => x != 0);
                bool allele1Condition = allele1NameFilter != null && allele1NameFilter.All(x => x != "All");
                bool allele2Condition = allele2NameFilter != null && allele2NameFilter.All(x => x != "All");
                bool alleleCondition = (allele1Condition || allele2Condition)&&!undefinedAllele;

                if (projectIdFilter == null)
                    projectIdFilter = new List<int>();
                if (patientIdFilter == null)
                    patientIdFilter = new List<int>();
                if (geneIdFilter == null)
                    geneIdFilter = new List<int>();
                if (allele1NameFilter == null)
                    allele1NameFilter = new List<string>();
                if (allele2NameFilter == null)
                    allele2NameFilter = new List<string>();

                List<int> alleleIdFilter = new List<int>();

                if (undefinedAllele == false)
                {
                    alleleIdFilter =
                        TheUnitOfWork.Allele.GetWhere(
                                x =>
                                    (!allele1Condition ||
                                     allele1NameFilter.Any(
                                         a1 => x.AlleleSymbol.StartsWith(a1 + "/") || x.AlleleSymbol.EndsWith("/" + a1))) &&
                                    (!allele2Condition ||
                                     allele2NameFilter.Any(
                                         a2 => x.AlleleSymbol.StartsWith(a2 + "/") || x.AlleleSymbol.EndsWith("/" + a2))) &&
                                    (!(allele1Condition && allele2Condition) ||
                                     (allele1NameFilter.Any(a1 => x.AlleleSymbol.StartsWith(a1 + "/")) &&
                                      allele2NameFilter.Any(a2 => x.AlleleSymbol.EndsWith("/" + a2))) ||
                                     (allele2NameFilter.Any(a2 => x.AlleleSymbol.StartsWith(a2 + "/")) &&
                                      allele1NameFilter.Any(a1 => x.AlleleSymbol.EndsWith("/" + a1))))
                            )
                            .Select(a => a.AlleleID).ToList();
                }

                patients = TheUnitOfWork.Patient.GetWhere(
                    x =>
                    (!projectIdCondition || x.PatientTranslator.Any(y => projectIdFilter.Any(j => j == y.ProjectID))) &&
                    (x.PatientTranslator.Any(y => y.Project.DeleteStatus == 1)) &&
                        (!patientIdCondition || patientIdFilter.Any(p => p == x.PatientID)) &&
                        (!geneCondition || x.PatientTranslator.Any(y => geneIdFilter.Any(g => g == y.GeneID))) &&
                        (!undefinedAllele || x.PatientTranslator.Any(y => y.AlleleID == null)) &&
                        (!alleleProbability || x.PatientTranslator.Any(y => y.Probability == true)) &&

                        //(!alleleCondition || x.PatientTranslator.Any(y => alleleIdFilter.Any(g =>  g == y.AlleleID))) &&

                        ((allele1NameFilter.All(a1 => a1 != "All") && allele2NameFilter.All(a2 => a2 != "All")) ||
                         (x.PatientTranslator.Any(y => y.AlleleID != null)))



                    , "PatientTranslator.Project,PatientTranslator.Gene,PatientTranslator.Allele,PatientLab.Gene1,PatientLab.Call").ToList();

                //,PatientLab.Gene1,PatientLab.Call
                // Edit by Amr Alsayd to avoid exceptions
                patients = patients.Where(x => !alleleCondition || x.PatientTranslator.Any(y => alleleIdFilter.Any(g => g == y.AlleleID))).ToList();



                if (patients.Count() != 0)
                {
                    foreach (var patient in patients)
                    {
                        PatientGeneAlleleViewModel patientGeneAllele = new PatientGeneAlleleViewModel();
                        patientGeneAllele.patientName = patient.PatientName;
                        patientGeneAllele.patientId = patient.PatientID;
                        var patientTranslatorGroup =
                            patient.PatientTranslator.Where(
                             x => (!projectIdCondition || projectIdFilter.Any(j => j == x.ProjectID)) &&
                                 (x.Project.DeleteStatus == 1) &&
                                 (!geneCondition || geneIdFilter.Any(g => g == x.GeneID)))
                                .GroupBy(p => new { p.Project, p.Gene })
                                .ToList();





                        #region paging using take  , skip 
                        //int startIndex = 3;
                        //int pageSize = 20;

                        //var patientTranslatorList =
                        //patient.PatientTranslator.Where(
                        //x => (!projectIdCondition || projectIdFilter.Any(j => j == x.ProjectID)) &&
                        //(x.Project.DeleteStatus == 1) &&
                        //(!geneCondition || geneIdFilter.Any(g => g == x.GeneID))).OrderBy(o => o.ProjectID).ThenBy(g => g.GeneID).Take(pageSize).Skip(pageSize * startIndex).ToList();
                        //var patientTranslatorGroup = patientTranslatorList.GroupBy(p => new { p.Project, p.Gene }).ToList();

                        #endregion



                        var patientTranslatorsGroupFiltered =
                            patientTranslatorGroup.Where(
                                r =>
                                    (!undefinedAllele || (r.Any(y => y.AlleleID == null))) &&
                                    (!alleleProbability || (r.Any(y => y.Probability == true))) &&
                                    ((allele1NameFilter.All(a1 => a1 != "All") &&
                                      allele2NameFilter.All(a2 => a2 != "All")) ||
                                     (r.Any(y => y.AlleleID != null))) &&
                                    (!alleleCondition ||
                                     (r.Any(y => alleleIdFilter.Any(g => g == y.AlleleID))))).ToList();




                        if (patientTranslatorsGroupFiltered.Count != 0)
                        {
                            //var patientTranslatorsGroup=patientTranslators.;
                            foreach (var patientTranslatorsItem in patientTranslatorsGroupFiltered)
                            {
                                GeneAlleleViewModel geneAllele = new GeneAlleleViewModel(0,"-",0, "-",null, "-","-",false);
                                if (patientTranslatorsItem != null && patientTranslatorsItem.Key.Project != null && patientTranslatorsItem.Key.Gene != null)
                                {
                                    geneAllele = new GeneAlleleViewModel(patientTranslatorsItem.Key.Project.ProjectID, patientTranslatorsItem.Key.Project.ProjectName,patientTranslatorsItem.Key.Gene.GeneID,
                                        patientTranslatorsItem.Key.Gene.GeneSymbol, null, "UND","-",false);
                                    string alleleName="-";
                                    bool first=true;
                                    foreach (var patientTranslator in patientTranslatorsItem)
                                    {
                                        if (patientTranslator.Allele != null)
                                        {
                                            if (first)
                                            {
                                                if (patientTranslator.Swap &&
                                                    patientTranslator.Allele.AlleleSymbol.Contains('/'))
                                                {
                                                    var alleleParts = patientTranslator.Allele.AlleleSymbol.Split('/');
                                                    alleleName = alleleParts[1] + "/" +
                                                                 alleleParts[0];
                                                    first = false;
                                                }
                                                else
                                                {
                                                    alleleName = patientTranslator.Allele.AlleleSymbol;
                                                    first = false;
                                                }
                                            }
                                            else
                                            {
                                                if (patientTranslator.Swap &&
                                                    patientTranslator.Allele.AlleleSymbol.Contains('/'))
                                                {
                                                    var alleleParts = patientTranslator.Allele.AlleleSymbol.Split('/');
                                                    alleleName = alleleName + "," + alleleParts[1] + "/" +
                                                                 alleleParts[0];
                                                }
                                                else
                                                {
                                                    alleleName = alleleName + "," +
                                                                 patientTranslator.Allele.AlleleSymbol;
                                                }
                                            }
                                            geneAllele.probability=(bool) (patientTranslator.Probability !=null ? patientTranslator.Probability:false);
                                        }
                                        var patientLabRecordNormal =
                                            patient.PatientLab.FirstOrDefault(
                                                p => p.ProjectID == patientTranslatorsItem.Key.Project.ProjectID &&
                                                    p.GeneID == patientTranslatorsItem.Key.Gene.GeneID &&
                                                    p.PatientID == patientTranslator.PatientID && p.CallType == (byte)CommonEnums.CallTypeEnum.Normal);
                                        geneAllele.parentGeneName = patientLabRecordNormal != null? patientLabRecordNormal.Gene1 != null ? patientLabRecordNormal.Gene1.GeneSymbol : "-":"-";

                                        var patientLabRecordCopyNumber =
                                            patient.PatientLab.FirstOrDefault(
                                                p => p.ProjectID == patientTranslatorsItem.Key.Project.ProjectID &&
                                                    p.GeneID == patientTranslatorsItem.Key.Gene.GeneID &&
                                                    p.PatientID == patientTranslator.PatientID && p.CallType == (byte)CommonEnums.CallTypeEnum.CopyNumber);
                                        geneAllele.copyNumber = patientLabRecordCopyNumber!=null ? patientLabRecordCopyNumber.Call != null ? patientLabRecordCopyNumber.Call.CallValue != null ? patientLabRecordCopyNumber.Call.CallValue : "-" : "-" : "-";

                                    }
                                    geneAllele.alleleName = alleleName;
                                }
                                patientGeneAllele.geneAllele.Add(geneAllele);
                            }
                        }
                        patientGeneAllele.geneAllele = patientGeneAllele.geneAllele.OrderBy(o => o.geneName).ToList();
                        patientGeneAlleles.Add(patientGeneAllele);
                    }
                }
                return patientGeneAlleles.OrderBy(o => o.patientName, new AlphanumComparatorFast()).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Get list of Assays and Calls from database filtered by Patient Id and Gene Id
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="patientId"></param>
        /// <param name="geneId"></param>
        /// <returns>List of AssayCallViewModel </returns>
        public List<AssayCallViewModel> GetAssaysAndCalls(int projectId,int patientId, int geneId)
        {
            List<AssayCallViewModel> assayCallList = new List<AssayCallViewModel>();
            var assaysCalls =
                TheUnitOfWork.PatientLab.GetWhere(p =>p.ProjectID == projectId && p.PatientID == patientId && p.GeneID == geneId,
                    "Assay,Call").ToList();
            if (assaysCalls.Count != 0)
            {
                foreach (var assayCall in assaysCalls)
                {
                    AssayCallViewModel assayCallViewModel = new AssayCallViewModel("-", "-", "-");
                    if (assayCall != null && assayCall.Assay != null)
                    {
                        assayCallViewModel = new AssayCallViewModel(string.IsNullOrEmpty(assayCall.Assay.AssayID)?"-": assayCall.Assay.AssayID, string.IsNullOrEmpty(assayCall.Assay.AssaySNPRef)?"-": assayCall.Assay.AssaySNPRef, "-");
                        if (assayCall.Call != null)
                        {
                            assayCallViewModel.callName = string.IsNullOrWhiteSpace(assayCall.Call.CallValue)?"-" :assayCall.Call.CallValue;
                        }
                    }
                    assayCallList.Add(assayCallViewModel);
                }
            }
            return assayCallList.OrderBy(o => o.assayName).ToList();
        }

        /// <summary>
        /// Get list of Patient Id and Patient Name
        /// </summary>
        /// <returns>List of SelectItemViewModel for Patient Id and Patient Name</returns>
        public List<SelectItemViewModel> GetAllPatients()
        {
            List<SelectItemViewModel> allPatients =
                TheUnitOfWork.Patient.GetAll()
                    .Select(x => new SelectItemViewModel() { key = x.PatientID.ToString(), value = x.PatientName })
                   .OrderBy(l => l.value.Length).ThenBy(o => o.value).ToList();
            return allPatients;
        }

        /// <summary>
        /// Get list of Projects
        /// </summary>
        /// <returns>List of SelectItemViewModel for Projects</returns>
        public List<SelectItemViewModel> GetAllProjects()
        {
            List<SelectItemViewModel> allProjects =
                TheUnitOfWork.Project.GetWhere(p=>p.DeleteStatus==1)
                    .Select(x => new SelectItemViewModel() { key = x.ProjectID.ToString(), value = x.ProjectName })
                   .OrderBy(o => o.value).ToList();
            return allProjects;
        }

        /// <summary>
        /// Get list of Gene Id and Gene Name
        /// </summary>
        /// <returns>List of SelectItemViewModel for Gene Id and Gene Name</returns>
        public List<SelectItemViewModel> GetAllGenes()
        {
            List<SelectItemViewModel> allGenes =
                TheUnitOfWork.Gene.GetAll()
                .Select(x => new SelectItemViewModel() { key = x.GeneID.ToString(), value = x.GeneSymbol })
                .OrderBy(o => o.value).ToList();
            return allGenes;
        }

        /// <summary>
        /// Get list of Single Allele Name
        /// </summary>
        /// <returns>List of SelectItemViewModel for Single Allele Name</returns>
        public List<SelectItemViewModel> GetAllSingleAlleles()
        {
            var alleles = TheUnitOfWork.Allele.GetAll().Select(s=>s.AlleleSymbol).ToList().Where(x => x.Contains('/')).SelectMany(y => y.Split('/')).Distinct().Select(s => new SelectItemViewModel() { key = s, value = s }).OrderBy(l => l.value,new AlphanumComparatorFast()).ToList();
            return alleles;
        }
        #endregion

        #region CopyNumberDataFile

        /// <summary>
        /// Parse CopyNumberData File to List of PatientCopyNumberAnalysisViewModel
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="status"></param>
        /// <param name="uploadStatusKey"></param>
        /// <returns>List of PatientCopyNumberAnalysisViewModel </returns>
        public List<PatientCopyNumberAnalysisViewModel> ParseCopyNumberData(string filePath, List<string> status,
            string uploadStatusKey)
        {
            updateSessionStatus(status, "Analysis Copy Number File", uploadStatusKey);
            ExcelReader excelReader = new ExcelReader();
            DataTable dt = excelReader.ReadExcelFileForPatientCopyNumber(filePath);
            List<PatientCopyNumberAnalysisViewModel> cList = null;
            if (dt != null)
            {
                cList = new List<PatientCopyNumberAnalysisViewModel>();
                if (dt.Rows.Count > new int())
                {
                    updateSessionStatusInner(status, string.Format("{0} Records in the Copy Number File", dt.Rows.Count),
                        uploadStatusKey);
                    updateSessionStatusInner(status, "Convert records to valid entities", uploadStatusKey);
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        PatientCopyNumberAnalysisViewModel cpatient = new PatientCopyNumberAnalysisViewModel();
                        cpatient.SampleName = readCell(dt.Rows[i]["Sample Name"]);
                        cpatient.Target = readCell(dt.Rows[i]["Target"]);
                        if (!string.IsNullOrEmpty(cpatient.SampleName) && !string.IsNullOrEmpty(cpatient.Target))
                        {
                            cpatient.CNPredicted = readCell(dt.Rows[i]["CN Predicted"]);
                            cList.Add(cpatient);
                        }
                    }
                }
            }
            return cList;
        }

        /// <summary>
        /// Store CopyNumberData in PatientTranslator Table
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="projectEntities"></param>
        /// <param name="patientCopyNumberList"></param>
        /// <param name="status"></param>
        /// <param name="uploadStatusKey"></param>
        /// <returns>List of PatientCopyNumberAnalysisViewModel</returns>
        public void StoreCopyNumberData(int projectId, ProjectEntities projectEntities,
            List<PatientCopyNumberAnalysisViewModel> patientCopyNumberList,
            List<string> status, string uploadStatusKey)
        {
            if (patientCopyNumberList != null && patientCopyNumberList.Count > 0)
            {
                updateSessionStatus(status, "Store Copy Number Data", uploadStatusKey);
                updateSessionStatus(status, "", uploadStatusKey);
                if (projectEntities != null && projectEntities.allPateintList != null &&
                    projectEntities.allTranslatorLabList != null)
                {
                    int patientLabInsertedCount = 0;
                    try
                    {
                        int patientCopyNumberIter = 0;
                        string AssaysNotFoundInTranslator = string.Empty;
                        foreach (var patientCopyNumberObject in patientCopyNumberList)
                        {
                            patientCopyNumberIter++;
                            Translator translatorRecord;
                            int patientId = 0;
                            int? geneId = null;
                            int? assayId = null;
                            int? callId = null;
                            string copyNumber = patientCopyNumberObject.CNPredicted.Trim();

                            int currentAssayId = projectEntities.allAssayList.Where(a => a.AssayID.ToUpper() == patientCopyNumberObject.Target.Trim().ToUpper()).Select(i => i.ID).FirstOrDefault();
                            patientId = projectEntities.allPateintList.Where( p => p.PatientName.ToUpper() == patientCopyNumberObject.SampleName.Trim().ToUpper()).Select(p => p.PatientID)
                                    .FirstOrDefault();
                            translatorRecord = projectEntities.allTranslatorLabList.FirstOrDefault(a => a.ProjectID == projectId && a.AssayID== currentAssayId);
                            if (translatorRecord != null)
                            {
                                assayId = translatorRecord.AssayID;
                                geneId = translatorRecord.GeneID;
                            }

                            string textStatus = string.Format("Progress {0} %",
                                Math.Round(((double)(patientCopyNumberIter) / patientCopyNumberList.Count) * 100, 2));
                            textStatus += "\t" +
                                          string.Format("Store Copy Number for Patient {0} Assay {1}",
                                              patientCopyNumberObject.SampleName, patientCopyNumberObject.Target);
                            updateSessionStatus(status, textStatus, uploadStatusKey, true);

                            if (patientId != 0 && geneId != null)
                            {
                                try
                                {
                                    var call = projectEntities.allCallList.FirstOrDefault(x => x.CallValue == copyNumber);
                                    if (call == null && !string.IsNullOrWhiteSpace(copyNumber))
                                    {
                                        call = new Call()
                                        {
                                            CallValue = patientCopyNumberObject.CNPredicted.Trim(),
                                            CreateDate = DateTime.Now,
                                            DeleteStatus = (byte)CommonEnums.DeleteStatus.NotDeleted,
                                        };
                                        projectEntities.allCallList.Add(call);
                                        TheUnitOfWork.Call.InsertList(
                                            projectEntities.allCallList.Where(x => x.CallID <= 0).ToList());
                                        TheUnitOfWork.Commit();
                                        callId = call.CallID;
                                    }

                                    var newPatientLabRecord = new PatientLab()
                                    {
                                        PatientID = patientId,
                                        AssayID = assayId,
                                        CallID = (call != null ? call.CallID : callId),  // khaled change
                                        GeneID = geneId,
                                        ProjectID = projectId,
                                        CallType = (byte)CommonEnums.CallTypeEnum.CopyNumber,
                                        CreateDate = DateTime.Now,
                                        DeleteStatus = (byte)CommonEnums.DeleteStatus.NotDeleted,
                                    };
                                    projectEntities.allPatientLabList.Add(newPatientLabRecord);
                                    patientLabInsertedCount++;
                                }
                                catch (Exception ex)
                                {
                                    string error = ex.Message;
                                }
                            }
                            else if (geneId == null || geneId == 0)
                            {
                                if (string.IsNullOrWhiteSpace(AssaysNotFoundInTranslator))
                                {
                                    AssaysNotFoundInTranslator += patientCopyNumberObject.Target;
                                }
                                else if (!string.IsNullOrWhiteSpace(AssaysNotFoundInTranslator) && !AssaysNotFoundInTranslator.Contains(patientCopyNumberObject.Target))
                                {
                                    AssaysNotFoundInTranslator += " , " + patientCopyNumberObject.Target;
                                }
                            }
                        }
                        TheUnitOfWork.PatientLab.InsertList(
                            projectEntities.allPatientLabList.Where(x => x.ID <= 0).ToList());
                        TheUnitOfWork.Commit();
                        updateSessionStatus(status, string.Format("Progress 100%"), uploadStatusKey);
                        if (!string.IsNullOrWhiteSpace(AssaysNotFoundInTranslator))
                        {
                            updateSessionStatus(status,
                            string.Format("Assays Not Found in Translator : {0}", AssaysNotFoundInTranslator),
                            uploadStatusKey);
                        }
                        updateSessionStatus(status,
                            string.Format("{0} Inserted records in Patient Lab", patientLabInsertedCount),
                            uploadStatusKey);
                    }
                    catch (Exception ex)
                    {
                        string ss = ex.Message;
                    }
                }
                else
                {
                    updateSessionStatus(status, "Storing Copy Number File Failed", uploadStatusKey);
                }
            }
            else if (patientCopyNumberList != null && patientCopyNumberList.Count == 0)
            {
                updateSessionStatus(status, "No records in Copy Number File to store", uploadStatusKey);
            }
            else
            {
                updateSessionStatus(status, "Analysis Copy Number File Failed", uploadStatusKey, false);
            }
        }

        #endregion

    }
}

