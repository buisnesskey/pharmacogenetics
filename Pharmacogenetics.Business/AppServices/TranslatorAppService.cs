﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Newtonsoft.Json;
using Pharmacogenetics.Business.Bases;
using Pharmacogenetics.Entities.MainModel;
using Pharmacogenetics.EntityService.Bases;
using Pharmacogenetics.Helper;
using Pharmacogenetics.Models.ViewModels;
using Pharmacogenetics.Models.ViewModels.ImportViewModels;
using System.Web;
using Pharmacogenetics.Business.Containers;

namespace Pharmacogenetics.Business.AppServices
{
    public class TranslatorAppService : AppService
    {

        AppServiceManager appServiceManager = new AppServiceManager();


        #region Constructor

        public TranslatorAppService()
            : base()
        {
        }

        public TranslatorAppService(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        #endregion

        #region Methods
        /// <summary>
        /// Get list of Genes and Alleles from database filtered Gene Id and Allele 
        /// </summary>
        /// <param name="projectIdFilter"></param>
        /// <param name="geneIdFilter"></param>
        /// <param name="allele1NameFilter"></param>
        /// <param name="allele2NameFilter"></param>
        /// <returns>List of TranslatorGeneAlleleViewModel</returns>
        public List<TranslatorGeneAlleleViewModel> GetGenesAndAlleles(List<int> projectIdFilter, List<int> geneIdFilter,
            List<string> allele1NameFilter, List<string> allele2NameFilter)
        {
            List<TranslatorGeneAlleleViewModel> geneAlleles = new List<TranslatorGeneAlleleViewModel>();
            try
            {
                //Get All Genes and Alleles after filtering
                bool projectIdCondition = projectIdFilter != null && projectIdFilter.All(x => x != 0);
                bool geneCondition = geneIdFilter != null && geneIdFilter.All(x => x != 0);
                bool allele1Condition = allele1NameFilter != null && allele1NameFilter.All(x => x != "All");
                bool allele2Condition = allele2NameFilter != null && allele2NameFilter.All(x => x != "All");

                if (projectIdFilter == null)
                    projectIdFilter = new List<int>();
                if (geneIdFilter == null)
                    geneIdFilter = new List<int>();
                if (allele1NameFilter == null)
                    allele1NameFilter = new List<string>();
                if (allele2NameFilter == null)
                    allele2NameFilter = new List<string>();

                var alleleIdFilter =
                    TheUnitOfWork.Allele.GetWhere(
                            x =>
                                (!allele1Condition ||
                                 allele1NameFilter.Any(
                                     a1 => x.AlleleSymbol.StartsWith(a1 + "/") || x.AlleleSymbol.EndsWith("/" + a1))) &&
                                (!allele2Condition ||
                                 allele2NameFilter.Any(
                                     a2 => x.AlleleSymbol.StartsWith(a2 + "/") || x.AlleleSymbol.EndsWith("/" + a2))) &&
                                (!(allele1Condition && allele2Condition) ||
                                 (allele1NameFilter.Any(a1 => x.AlleleSymbol.StartsWith(a1 + "/")) &&
                                  allele2NameFilter.Any(a2 => x.AlleleSymbol.EndsWith("/" + a2))) ||
                                 (allele2NameFilter.Any(a2 => x.AlleleSymbol.StartsWith(a2 + "/")) &&
                                  allele1NameFilter.Any(a1 => x.AlleleSymbol.EndsWith("/" + a1))))
                        )
                        .Select(a => a.AlleleID).ToList();

                if (alleleIdFilter.Count != 0)
                {
                    var translatorsGroupGenesList =
                        TheUnitOfWork.Translator.GetWhere(
                            x => (!projectIdCondition || projectIdFilter.Any(j => j == x.ProjectID)) &&
                                 (x.Project.DeleteStatus == 1) &&
                                 (!geneCondition || geneIdFilter.Any(p => p == x.GeneID)),
                            "Project,Gene,Allele").ToList();
                    if (translatorsGroupGenesList.Count != 0)
                    {
                        translatorsGroupGenesList =
                            translatorsGroupGenesList.Where(x => alleleIdFilter.Any(m => m == x.AlleleID)).ToList();
                    }
                    if (translatorsGroupGenesList.Count != 0)
                    {
                        var translatorsGroupGenes =
                            translatorsGroupGenesList.GroupBy(p => new {p.Project, p.Gene}).ToList();
                        if (translatorsGroupGenes.Count != 0)
                        {
                            foreach (var translatorsGroupGene in translatorsGroupGenes)
                            {
                                if (translatorsGroupGene.Key != null && translatorsGroupGene.Key.Project != null &&
                                    translatorsGroupGene.Key.Gene != null)
                                {
                                    var translatorsGroupGeneAllele =
                                        translatorsGroupGene.GroupBy(a => a.Allele).ToList();
                                    if (translatorsGroupGeneAllele.Count != 0)
                                    {
                                        foreach (var translatorsGrouped in translatorsGroupGeneAllele)
                                        {
                                            TranslatorGeneAlleleViewModel geneAllele =
                                                new TranslatorGeneAlleleViewModel();
                                            geneAllele.projectId = translatorsGroupGene.Key.Project.ProjectID;
                                            geneAllele.projectName = translatorsGroupGene.Key.Project.ProjectName;
                                            geneAllele.geneId = translatorsGroupGene.Key.Gene.GeneID;
                                            geneAllele.geneName = translatorsGroupGene.Key.Gene.GeneSymbol;
                                            if (translatorsGrouped.Key != null)
                                            {
                                                geneAllele.alleleId = translatorsGrouped.Key.AlleleID;
                                                geneAllele.alleleName = translatorsGrouped.Key.AlleleSymbol;
                                            }
                                            else
                                            {
                                                geneAllele.alleleId = 0;
                                                geneAllele.alleleName = "UND";
                                            }
                                            geneAlleles.Add(geneAllele);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                return
                    geneAlleles.OrderBy(o => o.geneName)
                        .ThenBy(a => a.alleleName.Split('/')[0], new AlphanumComparatorFast())
                        .ThenBy(a => a.alleleName.Split('/')[1], new AlphanumComparatorFast())
                        .ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Get list of Assays and Calls from database filtered by Gene Id and Allele Id
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="geneId"></param>
        /// <param name="alleleId"></param>
        /// <returns>List of AssayCallViewModel </returns>
        public List<AssayCallViewModel> GetAssaysAndCalls(int projectId, int geneId, int alleleId)
        {
            List<AssayCallViewModel> assayCallList = new List<AssayCallViewModel>();
            var assaysCalls =
                TheUnitOfWork.Translator.GetWhere(t => t.ProjectID == projectId && t.GeneID == geneId && t.AlleleID == alleleId,
                    "Assay,Call").ToList();
            if (assaysCalls.Count != 0)
            {
                foreach (var assayCall in assaysCalls)
                {
                    AssayCallViewModel assayCallViewModel = new AssayCallViewModel("-", "-", "-");
                    if (assayCall != null && assayCall.Assay != null)
                    {
                        assayCallViewModel = new AssayCallViewModel(string.IsNullOrEmpty(assayCall.Assay.AssayID)?"-": assayCall.Assay.AssayID, string.IsNullOrEmpty(assayCall.Assay.AssaySNPRef) ? "-" : assayCall.Assay.AssaySNPRef, "-");
                        if (assayCall.Call != null)
                        {
                            assayCallViewModel.callName = string.IsNullOrEmpty(assayCall.Call.CallValue)?"-": assayCall.Call.CallValue;
                        }
                    }
                    assayCallList.Add(assayCallViewModel);
                }

            }
            return assayCallList.OrderBy(o => o.assayName).ToList();
        }

        /// <summary>
        /// Get list of Projects
        /// </summary>
        /// <returns>List of SelectItemViewModel for Projects</returns>
        public List<SelectItemViewModel> GetAllProjects()
        {
            List<SelectItemViewModel> allProjects =
                TheUnitOfWork.Project.GetWhere(p => p.DeleteStatus == 1)
                    .Select(x => new SelectItemViewModel() { key = x.ProjectID.ToString(), value = x.ProjectName })
                   .OrderBy(o => o.value).ToList();
            return allProjects;
        }

        /// <summary>
        /// Get list of Gene Id and Gene Name
        /// </summary>
        /// <returns>List of SelectItemViewModel for Gene Id and Gene Name</returns>
        public List<SelectItemViewModel> GetAllGenes()
        {
            List<SelectItemViewModel> allGenes =
                TheUnitOfWork.Gene.GetAll()
                .Select(x => new SelectItemViewModel() { key = x.GeneID.ToString(), value = x.GeneSymbol })
                .OrderBy(o => o.value).ToList();
            return allGenes;
        }

        /// <summary>
        /// Get list of Single Allele Name
        /// </summary>
        /// <returns>List of SelectItemViewModel for Single Allele Name</returns>
        public List<SelectItemViewModel> GetAllSingleAlleles()
        {
            var alleles = TheUnitOfWork.Allele.GetAll().Select(s => s.AlleleSymbol).ToList().Where(x => x.Contains('/')).SelectMany(y => y.Split('/')).Distinct().Select(s => new SelectItemViewModel() { key = s, value = s }).OrderBy(l => l.value, new AlphanumComparatorFast()).ToList();
            return alleles;
        }
        #endregion

        #region Upload And Parsing

        /// <summary>
        /// Parse training data sheet (Excel file)
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="status"></param>
        /// <param name="uploadStatusKey"></param>
        /// <returns></returns>
        //public List<ExeclAssayAlleleTranslator> ParseTranslatorData(string fileName, List<string> status,
        //    string uploadStatusKey)
        //{
        //    bool operationStatus = false;
        //    ExcelReader excelReader = new ExcelReader();
        //    DataTable dt = excelReader.ReadExcelFileForTranslator(fileName);
        //    operationStatus = dt != null && dt.Rows.Count > new int();
        //    if (operationStatus)
        //    {
        //        List<ExeclAssayAlleleTranslator> tList = ParseToModel(dt);
        //        List<TranslatorExcelItem> TranslatorExcelItemList  =TranslatorHelper.Parsing(dt);
                

        //       return tList;
        //    }
        //    return null;
        //}


        #region Parse translator



        public static string readCell(object o)
        {
            return o == null ? string.Empty : o.ToString();
        }



        #region InsertIntoDatabase unused code

        //public void InsertIntoDatabase(ExeclAssayAlleleTranslator Item, string GeneSymbol, ExeclAssayAlleleTranslator AllAssaySymbolCells, ExeclAssayAlleleTranslator AllSNPREFCells,

        //    ExeclAssayAlleleTranslator AllAssayIDsCells, ExeclAssayAlleleTranslator AllAssayTextCells, ProjectViewModel _projectViewModel, ProjectEntities projectEntities
        //    , ref ProjectEntities NewProjectEntities, UnitOfWork _UnitOfWork
        //    )
        //{

        //    //GeneViewModel _GeneViewModel = new GeneViewModel();
        //    //_GeneViewModel.GeneSymbol = GeneSymbol;







        //    Gene GeneEntity = new Gene();
        //    if (!CheckIfGeneExistBefore(projectEntities.allGeneList, NewProjectEntities.allGeneList, GeneSymbol, ref  GeneEntity))
        //    {
        //        // inser into Gene table and return GeneID
        //        // insert into GeneListExistInDB List
        //        #region Feedback Issues (you can use DateTime.Now one time you don't need to call it every time)
        //        // good performance and all recoreds insterted in the database with same value of times
        //        #endregion
        //        GeneEntity.GeneSymbol = GeneSymbol;
        //        GeneEntity.CreateDate = DateTime.Now;
        //        GeneEntity.DeleteStatus = 0;


        //        _UnitOfWork.Gene.Insert(GeneEntity);
        //        _UnitOfWork.Commit();
        //        //_GeneViewModel.GeneID = GeneEntity.GeneID;
        //        //_geneID = GeneEntity.GeneID;

        //        NewProjectEntities.allGeneList.Add(GeneEntity);
        //    }


        //    string alleleSymbol = Item.BCell; //   check if exist gene and allele  then  insert  gene and allele  if not exit and get ids 

        //    //ref List<AlleleViewModel> AlleleListExistInDB, 

        //    Allele AlleleEntity = new Allele();

        //    if (!CheckIfAlleleExistBefore(projectEntities.allAlleleList, NewProjectEntities.allAlleleList, alleleSymbol, ref AlleleEntity))
        //    {
        //        AlleleEntity.AlleleSymbol = alleleSymbol;
        //        AlleleEntity.CreateDate = DateTime.Now;
        //        AlleleEntity.DeleteStatus = 0;
        //        _UnitOfWork.Allele.Insert(AlleleEntity);
        //        _UnitOfWork.Commit();
        //        //_AlleleViewModel.AlleleID = AlleleEntity.AlleleID;
        //        //_alleleID = AlleleEntity.AlleleID;
        //        //AlleleListExistInDB.Add(_AlleleViewModel);
        //        NewProjectEntities.allAlleleList.Add(AlleleEntity);

        //    }

        //    Assay AssayEntity;
        //    CellCharachter currentCellChar;

        //    #region Feedback Issues (I think this unmaintainable code if we have change somting in the code or logic we need to change all of them)
        //    // I think if we seprate these properties in list we avoid  these lines
        //    #endregion

        //    #region CCell

        //    if (!string.IsNullOrEmpty(Item.CCell)) // call
        //    {
        //        currentCellChar = CellCharachter.C;
        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);

        //        string currentCallCellValue = Item.CCell;
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);

        //        //ref CallListExistInDB, ref TranslatorListExistInDB,
        //        //InsertRowData(currentCallCellValue, _AssayViewModel, _alleleID, _geneID, ref AssayListExistInDB, ref CallListExistInDB, ref TranslatorListExistInDB  ,_projectViewModel);
        //        //, ref List<TranslatorViewModel> TranslatorListExistInDB, ref List<TranslatorMappingViewModel> TranslatorMaapingListExistInDB

        //    }
        //    #endregion

        //    #region Group A




        //    #region DCell


        //    if (!string.IsNullOrEmpty(Item.DCell)) // call
        //    {
        //        currentCellChar = CellCharachter.D;
        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);



        //        string currentCallCellValue = Item.DCell;
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //    }

        //    #endregion

        //    #region ECell

        //    if (!string.IsNullOrEmpty(Item.ECell)) // call
        //    {
        //        currentCellChar = CellCharachter.E;
        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);

        //        string currentCallCellValue = Item.ECell;
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //    }
        //    #endregion

        //    #region FCell

        //    if (!string.IsNullOrEmpty(Item.FCell)) // call
        //    {
        //        currentCellChar = CellCharachter.F;
        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);


        //        string currentCallCellValue = Item.FCell;
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);

        //    }
        //    #endregion

        //    #region GCell
        //    if (!string.IsNullOrEmpty(Item.GCell)) // call
        //    {
        //        currentCellChar = CellCharachter.G;
        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);


        //        string currentCallCellValue = Item.GCell;


        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);

        //    }
        //    #endregion

        //    #region HCell

        //    if (!string.IsNullOrEmpty(Item.HCell)) // call
        //    {
        //        currentCellChar = CellCharachter.H;

        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);


        //        string currentCallCellValue = Item.HCell;
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);

        //    }
        //    #endregion


        //    #region ICell



        //    if (!string.IsNullOrEmpty(Item.ICell)) // call
        //    {
        //        currentCellChar = CellCharachter.I;
        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);


        //        string currentCallCellValue = Item.ICell;

        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //        // insert into assay table with gene id 

        //    }
        //    #endregion

        //    #region JCell


        //    if (!string.IsNullOrEmpty(Item.JCell)) // call
        //    {
        //        currentCellChar = CellCharachter.J;
        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);


        //        string currentCallCellValue = Item.JCell;

        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //        // insert into assay table with gene id 

        //    }
        //    #endregion

        //    #region kCell


        //    if (!string.IsNullOrEmpty(Item.kCell)) // call
        //    {
        //        currentCellChar = CellCharachter.K;
        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);

        //        string currentCallCellValue = Item.kCell;

        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);

        //    }
        //    #endregion

        //    #region LCell

        //    if (!string.IsNullOrEmpty(Item.LCell)) // call
        //    {
        //        currentCellChar = CellCharachter.L;
        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);

        //        string currentCallCellValue = Item.LCell;
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);

        //    }
        //    #endregion

        //    #region MCell

        //    if (!string.IsNullOrEmpty(Item.MCell)) // call
        //    {
        //        currentCellChar = CellCharachter.M;
        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);

        //        string currentCallCellValue = Item.MCell;
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //    }
        //    #endregion

        //    #region NCell

        //    if (!string.IsNullOrEmpty(Item.NCell)) // call
        //    {
        //        currentCellChar = CellCharachter.N;
        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);


        //        string currentCallCellValue = Item.NCell;
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //    }
        //    #endregion

        //    #region OCell

        //    if (!string.IsNullOrEmpty(Item.OCell)) // call
        //    {
        //        currentCellChar = CellCharachter.O;
        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);


        //        string currentCallCellValue = Item.OCell;
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //    }
        //    #endregion

        //    #region PCell

        //    if (!string.IsNullOrEmpty(Item.PCell)) // call
        //    {
        //        currentCellChar = CellCharachter.P;
        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);


        //        string currentCallCellValue = Item.PCell;
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //    }
        //    #endregion

        //    #region QCell


        //    if (!string.IsNullOrEmpty(Item.QCell)) // call
        //    {
        //        currentCellChar = CellCharachter.Q;
        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);

        //        string currentCallCellValue = Item.QCell;
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //    }
        //    #endregion

        //    #region RCell


        //    if (!string.IsNullOrEmpty(Item.RCell)) // call
        //    {
        //        currentCellChar = CellCharachter.R;
        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);

        //        string currentCallCellValue = Item.RCell;
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //    }
        //    #endregion

        //    #region SCell

        //    if (!string.IsNullOrEmpty(Item.SCell)) // call
        //    {
        //        currentCellChar = CellCharachter.S;
        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);

        //        string currentCallCellValue = Item.SCell;
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //    }
        //    #endregion

        //    #region TCell


        //    if (!string.IsNullOrEmpty(Item.TCell)) // call
        //    {
        //        currentCellChar = CellCharachter.T;
        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);

        //        string currentCallCellValue = Item.TCell;



        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //    }
        //    #endregion

        //    #region UCell


        //    if (!string.IsNullOrEmpty(Item.UCell)) // call
        //    {
        //        currentCellChar = CellCharachter.U;
        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);

        //        string currentCallCellValue = Item.UCell;
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //    }
        //    #endregion

        //    #region VCell

        //    if (!string.IsNullOrEmpty(Item.VCell)) // call
        //    {
        //        currentCellChar = CellCharachter.V;
        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);

        //        string currentCallCellValue = Item.VCell;
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //    }
        //    #endregion

        //    #region WCell

        //    if (!string.IsNullOrEmpty(Item.WCell)) // call
        //    {
        //        currentCellChar = CellCharachter.W;
        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);

        //        string currentCallCellValue = Item.WCell;
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //    }
        //    #endregion

        //    #region XCell


        //    if (!string.IsNullOrEmpty(Item.XCell)) // call
        //    {
        //        currentCellChar = CellCharachter.X;
        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);

        //        string currentCallCellValue = Item.XCell;
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //    }
        //    #endregion

        //    #region YCell



        //    if (!string.IsNullOrEmpty(Item.YCell)) // call
        //    {
        //        currentCellChar = CellCharachter.Y;
        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);

        //        string currentCallCellValue = Item.YCell;

        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //    }
        //    #endregion

        //    #region ZCell



        //    if (!string.IsNullOrEmpty(Item.ZCell)) // call
        //    {
        //        currentCellChar = CellCharachter.Z;
        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);


        //        string currentCallCellValue = Item.ZCell;
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //    }
        //    #endregion

        //    #endregion


        //    #region MyRegion
        //    #region Group AA

        //    #region ACell
        //    if (!string.IsNullOrEmpty(Item.AACell)) // call
        //    {


        //        string currentCallCellValue = Item.AACell;
        //        currentCellChar = CellCharachter.AA;

        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);


        //    }
        //    #endregion

        //    #region BCell
        //    if (!string.IsNullOrEmpty(Item.ABCell)) // call
        //    {


        //        string currentCallCellValue = Item.ABCell;
        //        currentCellChar = CellCharachter.AB;


        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);


        //    }
        //    #endregion

        //    #region CCell
        //    if (!string.IsNullOrEmpty(Item.ACCell)) // call
        //    {


        //        string currentCallCellValue = Item.ACCell;

        //        currentCellChar = CellCharachter.AC;



        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);


        //    }
        //    #endregion

        //    #region DCell
        //    if (!string.IsNullOrEmpty(Item.ADCell)) // call
        //    {

        //        string currentCallCellValue = Item.ADCell;
        //        currentCellChar = CellCharachter.AD;

        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);





        //    }

        //    #endregion

        //    #region ECell
        //    if (!string.IsNullOrEmpty(Item.AECell)) // call
        //    {


        //        string currentCallCellValue = Item.AECell;
        //        currentCellChar = CellCharachter.AE;

        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);


        //    }
        //    #endregion

        //    #region FCell
        //    if (!string.IsNullOrEmpty(Item.AFCell)) // call
        //    {
        //        string currentCallCellValue = Item.AFCell;
        //        currentCellChar = CellCharachter.AF;

        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);

        //    }
        //    #endregion

        //    #region GCell
        //    if (!string.IsNullOrEmpty(Item.AGCell)) // call
        //    {
        //        string currentCallCellValue = Item.AGCell;
        //        currentCellChar = CellCharachter.AG;

        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);

        //    }
        //    #endregion

        //    #region HCell

        //    if (!string.IsNullOrEmpty(Item.AHCell)) // call
        //    {
        //        currentCellChar = CellCharachter.AH;
        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);


        //        string currentCallCellValue = Item.AHCell;
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);

        //    }
        //    #endregion

        //    #region ICell

        //    if (!string.IsNullOrEmpty(Item.AICell)) // call
        //    {
        //        currentCellChar = CellCharachter.AI;
        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);



        //        string currentCallCellValue = Item.AICell;


        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //        // insert into assay table with gene id 

        //    }
        //    #endregion

        //    #region JCell


        //    if (!string.IsNullOrEmpty(Item.AJCell)) // call
        //    {
        //        currentCellChar = CellCharachter.AJ;
        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);


        //        string currentCallCellValue = Item.AJCell;

        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //        // insert into assay table with gene id 

        //    }
        //    #endregion

        //    #region kCell


        //    if (!string.IsNullOrEmpty(Item.AkCell)) // call
        //    {
        //        currentCellChar = CellCharachter.AK;
        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);

        //        string currentCallCellValue = Item.AkCell;
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);

        //    }
        //    #endregion

        //    #region LCell

        //    if (!string.IsNullOrEmpty(Item.ALCell)) // call
        //    {
        //        currentCellChar = CellCharachter.AL;
        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);

        //        string currentCallCellValue = Item.ALCell;
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);

        //    }
        //    #endregion

        //    #region MCell


        //    if (!string.IsNullOrEmpty(Item.AMCell)) // call
        //    {
        //        currentCellChar = CellCharachter.AM;
        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);

        //        string currentCallCellValue = Item.AMCell;

        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //    }
        //    #endregion

        //    #region NCell

        //    if (!string.IsNullOrEmpty(Item.ANCell)) // call
        //    {
        //        currentCellChar = CellCharachter.AN;
        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);


        //        string currentCallCellValue = Item.ANCell;

        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //    }
        //    #endregion

        //    #region OCell





        //    if (!string.IsNullOrEmpty(Item.AOCell)) // call
        //    {
        //        currentCellChar = CellCharachter.AO;
        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);


        //        string currentCallCellValue = Item.AOCell;
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //    }
        //    #endregion

        //    #region PCell
        //    if (!string.IsNullOrEmpty(Item.APCell)) // call
        //    {
        //        string currentCallCellValue = Item.APCell;
        //        currentCellChar = CellCharachter.AP;

        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //    }
        //    #endregion

        //    #region QCell
        //    if (!string.IsNullOrEmpty(Item.AQCell)) // call
        //    {
        //        string currentCallCellValue = Item.AQCell;
        //        currentCellChar = CellCharachter.AQ;

        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //    }
        //    #endregion

        //    #region RCell
        //    if (!string.IsNullOrEmpty(Item.ARCell)) // call
        //    {
        //        string currentCallCellValue = Item.ARCell;
        //        currentCellChar = CellCharachter.AR;

        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //    }
        //    #endregion

        //    #region SCell
        //    if (!string.IsNullOrEmpty(Item.ASCell)) // call
        //    {
        //        string currentCallCellValue = Item.ASCell;
        //        currentCellChar = CellCharachter.AS;

        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //    }
        //    #endregion

        //    #region TCell
        //    if (!string.IsNullOrEmpty(Item.ATCell)) // call
        //    {
        //        string currentCallCellValue = Item.ATCell;
        //        currentCellChar = CellCharachter.AT;

        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //    }
        //    #endregion

        //    #region UCell
        //    if (!string.IsNullOrEmpty(Item.AUCell)) // call
        //    {
        //        string currentCallCellValue = Item.AUCell;
        //        currentCellChar = CellCharachter.AU;

        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //    }
        //    #endregion

        //    #region VCell
        //    if (!string.IsNullOrEmpty(Item.AVCell)) // call
        //    {
        //        string currentCallCellValue = Item.AVCell;
        //        currentCellChar = CellCharachter.AV;

        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //    }
        //    #endregion

        //    #region WCell
        //    if (!string.IsNullOrEmpty(Item.AWCell)) // call
        //    {
        //        string currentCallCellValue = Item.AWCell;
        //        currentCellChar = CellCharachter.AW;

        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //    }
        //    #endregion

        //    #region XCell
        //    if (!string.IsNullOrEmpty(Item.AXCell)) // call
        //    {
        //        string currentCallCellValue = Item.AXCell;
        //        currentCellChar = CellCharachter.AX;

        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //    }
        //    #endregion

        //    #region YCell
        //    if (!string.IsNullOrEmpty(Item.AYCell)) // call
        //    {
        //        string currentCallCellValue = Item.AYCell;
        //        currentCellChar = CellCharachter.AY;

        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //    }
        //    #endregion

        //    #region ZCell
        //    if (!string.IsNullOrEmpty(Item.AZCell)) // call
        //    {
        //        string currentCallCellValue = Item.AZCell;
        //        currentCellChar = CellCharachter.AZ;

        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //    }
        //    #endregion

        //    #endregion

        //    #region Group BB

        //    #region ACell
        //    if (!string.IsNullOrEmpty(Item.BACell)) // call
        //    {


        //        string currentCallCellValue = Item.BACell;
        //        currentCellChar = CellCharachter.BA;

        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);


        //    }
        //    #endregion

        //    #region BCell
        //    if (!string.IsNullOrEmpty(Item.BBCell)) // call
        //    {


        //        string currentCallCellValue = Item.BBCell;
        //        currentCellChar = CellCharachter.BB;


        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);


        //    }
        //    #endregion

        //    #region CCell
        //    if (!string.IsNullOrEmpty(Item.BCCell)) // call
        //    {


        //        string currentCallCellValue = Item.BCCell;

        //        currentCellChar = CellCharachter.BC;



        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);


        //    }
        //    #endregion

        //    #region DCell
        //    if (!string.IsNullOrEmpty(Item.BDCell)) // call
        //    {

        //        string currentCallCellValue = Item.BDCell;
        //        currentCellChar = CellCharachter.BD;

        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);





        //    }

        //    #endregion

        //    #region ECell
        //    if (!string.IsNullOrEmpty(Item.BECell)) // call
        //    {


        //        string currentCallCellValue = Item.BECell;
        //        currentCellChar = CellCharachter.BE;

        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);


        //    }
        //    #endregion

        //    #region FCell
        //    if (!string.IsNullOrEmpty(Item.BFCell)) // call
        //    {
        //        string currentCallCellValue = Item.BFCell;
        //        currentCellChar = CellCharachter.BF;

        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);

        //    }
        //    #endregion

        //    #region GCell
        //    if (!string.IsNullOrEmpty(Item.BGCell)) // call
        //    {
        //        string currentCallCellValue = Item.BGCell;
        //        currentCellChar = CellCharachter.BG;

        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);

        //    }
        //    #endregion

        //    #region HCell
        //    if (!string.IsNullOrEmpty(Item.BHCell)) // call
        //    {
        //        string currentCallCellValue = Item.BHCell;
        //        currentCellChar = CellCharachter.BH;

        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);

        //    }
        //    #endregion

        //    #region ICell
        //    if (!string.IsNullOrEmpty(Item.BICell)) // call
        //    {
        //        string currentCallCellValue = Item.BICell;
        //        currentCellChar = CellCharachter.BI;

        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //        // insert into assay table with gene id 

        //    }
        //    #endregion

        //    #region JCell
        //    if (!string.IsNullOrEmpty(Item.BJCell)) // call
        //    {
        //        string currentCallCellValue = Item.BJCell;
        //        currentCellChar = CellCharachter.BJ;

        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //        // insert into assay table with gene id 

        //    }
        //    #endregion

        //    #region kCell
        //    if (!string.IsNullOrEmpty(Item.BkCell)) // call
        //    {
        //        string currentCallCellValue = Item.BkCell;
        //        currentCellChar = CellCharachter.BK;

        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);

        //    }
        //    #endregion

        //    #region LCell
        //    if (!string.IsNullOrEmpty(Item.BLCell)) // call
        //    {
        //        string currentCallCellValue = Item.BLCell;
        //        currentCellChar = CellCharachter.BL;

        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);

        //    }
        //    #endregion

        //    #region MCell
        //    if (!string.IsNullOrEmpty(Item.BMCell)) // call
        //    {
        //        string currentCallCellValue = Item.BMCell;
        //        currentCellChar = CellCharachter.BM;

        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //    }
        //    #endregion

        //    #region NCell
        //    if (!string.IsNullOrEmpty(Item.BNCell)) // call
        //    {
        //        string currentCallCellValue = Item.BNCell;
        //        currentCellChar = CellCharachter.BN;

        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //    }
        //    #endregion

        //    #region OCell
        //    if (!string.IsNullOrEmpty(Item.BOCell)) // call
        //    {
        //        string currentCallCellValue = Item.BOCell;
        //        currentCellChar = CellCharachter.BO;

        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //    }
        //    #endregion

        //    #region PCell
        //    if (!string.IsNullOrEmpty(Item.BPCell)) // call
        //    {
        //        string currentCallCellValue = Item.BPCell;
        //        currentCellChar = CellCharachter.BP;

        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //    }
        //    #endregion

        //    #region QCell



        //    if (!string.IsNullOrEmpty(Item.BQCell)) // call
        //    {
        //        currentCellChar = CellCharachter.BQ;
        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);


        //        string currentCallCellValue = Item.BQCell;
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //    }
        //    #endregion

        //    #region RCell
        //    if (!string.IsNullOrEmpty(Item.BRCell)) // call
        //    {
        //        string currentCallCellValue = Item.BRCell;
        //        currentCellChar = CellCharachter.BR;

        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //    }
        //    #endregion

        //    #region SCell
        //    if (!string.IsNullOrEmpty(Item.BSCell)) // call
        //    {
        //        string currentCallCellValue = Item.BSCell;
        //        currentCellChar = CellCharachter.BS;

        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //    }
        //    #endregion

        //    #region TCell
        //    if (!string.IsNullOrEmpty(Item.BTCell)) // call
        //    {
        //        string currentCallCellValue = Item.BTCell;
        //        currentCellChar = CellCharachter.BT;

        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //    }
        //    #endregion

        //    #region UCell
        //    if (!string.IsNullOrEmpty(Item.BUCell)) // call
        //    {
        //        string currentCallCellValue = Item.BUCell;
        //        currentCellChar = CellCharachter.BU;

        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //    }
        //    #endregion

        //    #region VCell
        //    if (!string.IsNullOrEmpty(Item.BVCell)) // call
        //    {
        //        string currentCallCellValue = Item.BVCell;
        //        currentCellChar = CellCharachter.BV;

        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //    }
        //    #endregion

        //    #region WCell
        //    if (!string.IsNullOrEmpty(Item.BWCell)) // call
        //    {
        //        string currentCallCellValue = Item.BWCell;
        //        currentCellChar = CellCharachter.BW;

        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //    }
        //    #endregion

        //    #region XCell
        //    if (!string.IsNullOrEmpty(Item.BXCell)) // call
        //    {
        //        string currentCallCellValue = Item.BXCell;
        //        currentCellChar = CellCharachter.BX;

        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //    }
        //    #endregion

        //    #region YCell
        //    if (!string.IsNullOrEmpty(Item.BYCell)) // call
        //    {
        //        string currentCallCellValue = Item.BYCell;
        //        currentCellChar = CellCharachter.BY;

        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //    }
        //    #endregion

        //    #region ZCell
        //    if (!string.IsNullOrEmpty(Item.BZCell)) // call
        //    {
        //        string currentCallCellValue = Item.BZCell;
        //        currentCellChar = CellCharachter.BZ;

        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);
        //    }
        //    #endregion

        //    #endregion

        //    #region CC
        //    #region ACell
        //    if (!string.IsNullOrEmpty(Item.CACell)) // call
        //    {


        //        string currentCallCellValue = Item.CACell;
        //        currentCellChar = CellCharachter.CA;

        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);


        //    }
        //    #endregion

        //    #region BCell
        //    if (!string.IsNullOrEmpty(Item.CBCell)) // call
        //    {


        //        string currentCallCellValue = Item.CBCell;
        //        currentCellChar = CellCharachter.CB;


        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);


        //    }
        //    #endregion

        //    #region CCell
        //    if (!string.IsNullOrEmpty(Item.CCCell)) // call
        //    {


        //        string currentCallCellValue = Item.CCCell;

        //        currentCellChar = CellCharachter.CC;



        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);


        //    }
        //    #endregion

        //    #region DCell
        //    if (!string.IsNullOrEmpty(Item.CDCell)) // call
        //    {

        //        string currentCallCellValue = Item.CDCell;
        //        currentCellChar = CellCharachter.CD;

        //        AssayEntity = BuildAssayEntity(projectEntities, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, currentCellChar, GeneEntity, ref NewProjectEntities, _UnitOfWork);
        //        InsertRowData(ref  projectEntities, currentCallCellValue, AssayEntity, AlleleEntity, GeneEntity, _projectViewModel, ref NewProjectEntities, _UnitOfWork);





        //    }

        //    #endregion
        //    #endregion

        //    #endregion

        //    string ss = "";
        //}


        #endregion

        #region BuildAssayViewModel unused
        //private Assay BuildAssayEntity(ProjectEntities projectEntities, ExeclAssayAlleleTranslator AllAssaySymbolCells, ExeclAssayAlleleTranslator AllSNPREFCells, ExeclAssayAlleleTranslator AllAssayIDsCells, ExeclAssayAlleleTranslator AllAssayTextCells, CellCharachter cellCharachter, Gene geneEntity, ref ProjectEntities NewProjectEntities, UnitOfWork _UnitOfWork)
        //{

        //    //AssayViewModel _AssayViewModel = new AssayViewModel();
        //    #region Feedback Issues (You delete status enums inasted of write 0)
        //    //Pharmacogenetics.BaseClasses.Enums.CommonEnums.DeleteStatus.NotDeleted;
        //    #endregion
        //    Assay _Assay = new Assay();
        //    _Assay.CreateDate = DateTime.Now;
        //    _Assay.DeleteStatus = 0;
        //    #region A
        //    #region C
        //    if (cellCharachter == CellCharachter.C)
        //    {
        //        _Assay.AssayName = AllAssaySymbolCells.CCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.CCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.CCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.CCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;

        //    }
        //    #endregion

        //    #region D
        //    if (cellCharachter == CellCharachter.D)
        //    {
        //        _Assay.AssayName = AllAssaySymbolCells.DCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.DCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.DCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.DCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region E
        //    if (cellCharachter == CellCharachter.E)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.ECell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.ECell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.ECell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.ECell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region F
        //    if (cellCharachter == CellCharachter.F)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.FCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.FCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.FCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.FCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region G
        //    if (cellCharachter == CellCharachter.G)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.GCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.GCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.GCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.GCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region H
        //    if (cellCharachter == CellCharachter.H)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.HCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.HCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.HCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.HCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region I
        //    if (cellCharachter == CellCharachter.I)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.ICell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.ICell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.ICell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.ICell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion


        //    #region L
        //    if (cellCharachter == CellCharachter.L)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.LCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.LCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.LCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.LCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion


        //    #region J
        //    if (cellCharachter == CellCharachter.J)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.JCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.JCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.JCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.JCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region K
        //    if (cellCharachter == CellCharachter.K)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.kCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.kCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.kCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.kCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region L
        //    if (cellCharachter == CellCharachter.L)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.LCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.LCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.LCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.LCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion


        //    #region M
        //    if (cellCharachter == CellCharachter.M)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.MCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.MCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.MCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.MCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region N
        //    if (cellCharachter == CellCharachter.N)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.NCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.NCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.NCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.NCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region O
        //    if (cellCharachter == CellCharachter.O)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.OCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.OCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.OCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.OCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region P
        //    if (cellCharachter == CellCharachter.P)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.PCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.PCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.PCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.PCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region Q
        //    if (cellCharachter == CellCharachter.Q)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.QCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.QCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.QCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.QCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region R
        //    if (cellCharachter == CellCharachter.R)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.RCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.RCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.RCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.RCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region S
        //    if (cellCharachter == CellCharachter.S)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.SCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.SCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.SCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.SCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region T
        //    if (cellCharachter == CellCharachter.T)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.TCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.TCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.TCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.TCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region U
        //    if (cellCharachter == CellCharachter.U)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.UCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.UCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.UCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.UCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region V
        //    if (cellCharachter == CellCharachter.V)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.VCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.VCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.VCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.VCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region W
        //    if (cellCharachter == CellCharachter.W)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.WCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.WCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.WCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.WCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region X
        //    if (cellCharachter == CellCharachter.X)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.XCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.XCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.XCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.XCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region Y
        //    if (cellCharachter == CellCharachter.Y)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.YCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.YCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.YCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.YCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region Z
        //    if (cellCharachter == CellCharachter.Z)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.ZCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.ZCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.ZCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.ZCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion





        //    #endregion


        //    #region MyRegion
        //    #region AA

        //    #region A


        //    if (cellCharachter == CellCharachter.AA)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.AACell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.AACell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.AACell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.AACell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region B


        //    if (cellCharachter == CellCharachter.AB)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.ABCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.ABCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.ABCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.ABCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion


        //    #region C


        //    if (cellCharachter == CellCharachter.AC)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.ACCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.ACCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.ACCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.ACCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region D
        //    if (cellCharachter == CellCharachter.AD)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.ADCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.ADCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.ADCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.ADCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region E
        //    if (cellCharachter == CellCharachter.AE)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.AECell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.AECell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.AECell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.AECell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region F
        //    if (cellCharachter == CellCharachter.AF)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.AFCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.AFCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.AFCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.AFCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region G
        //    if (cellCharachter == CellCharachter.AG)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.AGCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.AGCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.AGCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.AGCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region H
        //    if (cellCharachter == CellCharachter.AH)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.AHCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.AHCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.AHCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.AHCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region I
        //    if (cellCharachter == CellCharachter.AI)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.AICell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.AICell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.AICell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.AICell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion


        //    #region L
        //    if (cellCharachter == CellCharachter.AL)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.ALCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.ALCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.ALCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.ALCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion


        //    #region J
        //    if (cellCharachter == CellCharachter.AJ)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.AJCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.AJCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.AJCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.AJCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region K
        //    if (cellCharachter == CellCharachter.AK)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.AkCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.AkCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.AkCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.AkCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region L
        //    if (cellCharachter == CellCharachter.AL)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.ALCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.ALCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.ALCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.ALCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion


        //    #region M
        //    if (cellCharachter == CellCharachter.AM)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.AMCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.AMCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.AMCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.AMCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region N
        //    if (cellCharachter == CellCharachter.AN)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.ANCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.ANCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.ANCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.ANCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region O
        //    if (cellCharachter == CellCharachter.AO)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.AOCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.AOCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.AOCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.AOCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region P
        //    if (cellCharachter == CellCharachter.AP)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.APCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.APCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.APCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.APCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region Q
        //    if (cellCharachter == CellCharachter.AQ)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.AQCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.AQCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.AQCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.AQCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region R
        //    if (cellCharachter == CellCharachter.AR)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.ARCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.ARCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.ARCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.ARCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region S
        //    if (cellCharachter == CellCharachter.AS)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.ASCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.ASCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.ASCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.ASCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region T
        //    if (cellCharachter == CellCharachter.AT)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.ATCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.ATCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.ATCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.ATCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region U
        //    if (cellCharachter == CellCharachter.AU)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.AUCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.AUCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.AUCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.AUCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region V
        //    if (cellCharachter == CellCharachter.AV)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.AVCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.AVCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.AVCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.AVCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region W
        //    if (cellCharachter == CellCharachter.AW)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.AWCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.AWCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.AWCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.AWCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region X
        //    if (cellCharachter == CellCharachter.AX)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.AXCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.AXCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.AXCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.AXCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region Y
        //    if (cellCharachter == CellCharachter.AY)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.AYCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.AYCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.AYCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.AYCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region Z
        //    if (cellCharachter == CellCharachter.AZ)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.AZCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.AZCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.AZCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.AZCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion





        //    #endregion

        //    #region B

        //    #region A


        //    if (cellCharachter == CellCharachter.BA)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.BACell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.BACell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.BACell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.BACell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region B


        //    if (cellCharachter == CellCharachter.BB)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.BBCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.BBCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.BBCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.BBCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion


        //    #region C


        //    if (cellCharachter == CellCharachter.BC)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.BCCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.BCCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.BCCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.BCCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region D
        //    if (cellCharachter == CellCharachter.BD)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.BDCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.BDCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.BDCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.BDCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region E
        //    if (cellCharachter == CellCharachter.BE)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.BECell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.BECell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.BECell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.BECell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region F
        //    if (cellCharachter == CellCharachter.BF)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.BFCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.BFCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.BFCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.BFCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region G
        //    if (cellCharachter == CellCharachter.BG)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.BGCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.BGCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.BGCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.BGCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region H
        //    if (cellCharachter == CellCharachter.BH)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.BHCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.BHCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.BHCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.BHCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region I
        //    if (cellCharachter == CellCharachter.BI)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.BICell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.BICell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.BICell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.BICell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion


        //    #region L
        //    if (cellCharachter == CellCharachter.BL)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.BLCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.BLCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.BLCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.BLCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion


        //    #region J
        //    if (cellCharachter == CellCharachter.BJ)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.BJCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.BJCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.BJCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.BJCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region K
        //    if (cellCharachter == CellCharachter.BK)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.BkCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.BkCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.BkCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.BkCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region L
        //    if (cellCharachter == CellCharachter.BL)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.BLCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.BLCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.BLCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.BLCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion


        //    #region M
        //    if (cellCharachter == CellCharachter.BM)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.BMCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.BMCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.BMCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.BMCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region N
        //    if (cellCharachter == CellCharachter.BN)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.BNCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.BNCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.BNCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.BNCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region O
        //    if (cellCharachter == CellCharachter.BO)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.BOCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.BOCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.BOCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.BOCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region P
        //    if (cellCharachter == CellCharachter.BP)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.BPCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.BPCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.BPCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.BPCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region Q
        //    if (cellCharachter == CellCharachter.BQ)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.BQCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.BQCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.BQCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.BQCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region R
        //    if (cellCharachter == CellCharachter.BR)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.BRCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.BRCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.BRCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.BRCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region S
        //    if (cellCharachter == CellCharachter.BS)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.BSCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.BSCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.BSCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.BSCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region T
        //    if (cellCharachter == CellCharachter.BT)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.BTCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.BTCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.BTCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.BTCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region U
        //    if (cellCharachter == CellCharachter.BU)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.BUCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.BUCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.BUCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.BUCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region V
        //    if (cellCharachter == CellCharachter.BV)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.BVCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.BVCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.BVCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.BVCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region W
        //    if (cellCharachter == CellCharachter.BW)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.BWCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.BWCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.BWCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.BWCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region X
        //    if (cellCharachter == CellCharachter.BX)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.BXCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.BXCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.BXCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.BXCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region Y
        //    if (cellCharachter == CellCharachter.BY)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.BYCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.BYCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.BYCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.BYCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region Z
        //    if (cellCharachter == CellCharachter.BZ)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.BZCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.BZCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.BZCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.BZCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion





        //    #endregion


        //    #region CC
        //    #region A


        //    if (cellCharachter == CellCharachter.CA)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.CACell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.CACell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.CACell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.CACell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region B


        //    if (cellCharachter == CellCharachter.CB)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.CBCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.CBCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.CBCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.CBCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion


        //    #region C


        //    if (cellCharachter == CellCharachter.CC)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.CCCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.CCCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.CCCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.CCCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion

        //    #region D
        //    if (cellCharachter == CellCharachter.CD)
        //    {

        //        _Assay.AssayName = AllAssaySymbolCells.CDCell; // AllAssaySymbolCells
        //        _Assay.AssaySNPRef = AllSNPREFCells.CDCell; // AllSNPREFCells
        //        _Assay.AssayID = AllAssayIDsCells.CDCell; // AllAssayIDsCells
        //        _Assay.GeneTestSymbol = AllAssayTextCells.CDCell;  // AllAssayTextCells 
        //        _Assay.Gene = geneEntity;
        //    }
        //    #endregion
        //    #endregion

        //    #endregion



        //    #region _assayID


        //    //ref  List<Assay> AssayListExistInDB, 

        //    if (!CheckIfAssayExistBefore(projectEntities.allAssayList, NewProjectEntities.allAssayList, ref  _Assay))
        //    {
        //        // insert into assay table with current gene id 
        //        // insert into AssayListExistInDB

        //        //Assay AssayEntity = new Assay();

        //        //AssayEntity.AssayID = _AssayViewModel.AssayID;
        //        //AssayEntity.GeneID = _geneID.Value;
        //        //AssayEntity.AssayName = _AssayViewModel.AssayName;
        //        //AssayEntity.AssaySNPRef = _AssayViewModel.AssaySNPRef;
        //        //AssayEntity.GeneTestSymbol = _AssayViewModel.GeneTestSymbol;

        //        //AssayEntity.CreateDate = DateTime.Now;
        //        //AssayEntity.DeleteStatus = 0;

        //        try
        //        {

        //            if (_Assay.AssayID != null && !string.IsNullOrEmpty(_Assay.AssayID.ToString()))
        //            {
        //                NewProjectEntities.allAssayList.Add(_Assay);
        //                _UnitOfWork.Assay.Insert(_Assay);
        //                _UnitOfWork.Commit();
        //            }

        //        }
        //        catch (Exception ex)
        //        {


        //            string ss = ex.Message;
        //        }

        //        //_AssayViewModel.ID = AssayEntity.ID;
        //        //_assayID = AssayEntity.ID;
        //        //AssayListExistInDB.Add(_AssayViewModel);
        //    }
        //    else
        //    {
        //        //_AssayViewModel.ID = _assayID.Value;
        //    }
        //    #endregion
        //    return _Assay;

        //}

        #endregion

        #region InsertRowData unused
        //private void InsertRowData(ref ProjectEntities ProjectEntities, string currentCallValue, Assay _Assay, Allele AlleleEntity, Gene GeneEntity, ProjectViewModel _ProjectViewModel, ref ProjectEntities NewProjectEntities, UnitOfWork _UnitOfWork)
        //{

        //    if (_Assay.AssayID == null || string.IsNullOrEmpty(_Assay.AssayID.ToString()))
        //        return;


        //    DateTime currentDate = DateTime.Now;

        //    Call _Call = new Call();
        //    _Call.CallValue = currentCallValue;
        //    _Call.CreateDate = currentDate;
        //    _Call.DeleteStatus = 0;

        //    #region _callID
        //    if (!CheckIfCallExsitBefore(ProjectEntities.allCallList, NewProjectEntities.allCallList, currentCallValue, ref _Call))
        //    {
        //        // insert into assay table with current gene id 
        //        // insert into AssayListExistInDB

        //        _UnitOfWork.Call.Insert(_Call);
        //        _UnitOfWork.Commit();

        //        //_Call.CallID = CallEntity.CallID;
        //        //_callID = CallEntity.CallID;

        //        NewProjectEntities.allCallList.Add(_Call);

        //    }
        //    #endregion

        //    #region Insert Into Translator

        //    //int? _TranslatorID = null;

        //    //TranslatorViewModel _TranslatorViewModel = new TranslatorViewModel();

        //    //_TranslatorViewModel.AssayID = _AssayViewModel.ID;
        //    //_TranslatorViewModel.CallID = _callID.Value;

        //    //_TranslatorViewModel.GeneID = _geneID.Value;
        //    //_TranslatorViewModel.AlleleID = _alleleID.Value;
        //    //_TranslatorViewModel.ProjectID = _ProjectViewModel.ProjectID;

        //    Translator _Translator = new Translator();


        //    //_Translator.Gene = GeneEntity;
        //    //_Translator.Allele = AlleleEntity;
        //    //_Translator.Assay = _Assay;
        //    //_Translator.Call = _Call;


        //    _Translator.AlleleID = AlleleEntity.AlleleID;//_assayID.Value;
        //    _Translator.AssayID = _Assay.ID;//_assayID.Value;
        //    _Translator.CallID = _Call.CallID;
        //    _Translator.GeneID = GeneEntity.GeneID;//_assayID.Value;
        //    _Translator.ProjectID = _ProjectViewModel.ProjectID;
        //    _Translator.CreateDate = currentDate;
        //    _Translator.DeleteStatus = 0;
        //    Translator TranslatorByRef = new Translator();
        //    if (!CheckIfTranslatorExsitBefore(ProjectEntities.allTranslatorLabList, NewProjectEntities.allTranslatorLabList, _Translator, ref TranslatorByRef))
        //    {
        //        NewProjectEntities.allTranslatorLabList.Add(_Translator);

        //    }








        //    #region commented code
        //    //appServiceManager.TheUnitOfWork.Translator.Insert(_Translator);
        //    //appServiceManager.TheUnitOfWork.Commit();
        //    //_TranslatorViewModel.TranslatorID = _Translator.TranslatorID;
        //    //  TranslatorListExistInDB.Add(_TranslatorViewModel);
        //    //_TranslatorID = _Translator.TranslatorID;




        //    //if (!CheckIfTranslatorExsitBefore(ref TranslatorViewModelListExistInDB, _TranslatorViewModel, out _TranslatorID))
        //    //{

        //    //    Translator _Translator = new Translator();

        //    //    _Translator.AssayID = _TranslatorViewModel.AssayID;//_assayID.Value;
        //    //    _Translator.CallID = _TranslatorViewModel.CallID;

        //    //    _Translator.GeneID = _TranslatorViewModel.GeneID;//_assayID.Value;
        //    //    _Translator.AlleleID = _TranslatorViewModel.AlleleID;
        //    //    _Translator.ProjectID = _TranslatorViewModel.ProjectID;

        //    //    _Translator.CreateDate = DateTime.Now;
        //    //    _Translator.DeleteStatus = 0;
        //    //    appServiceManager.TheUnitOfWork.Translator.Insert(_Translator);
        //    //    appServiceManager.TheUnitOfWork.Commit();
        //    //   _TranslatorViewModel.TranslatorID = _Translator.TranslatorID;
        //    //    TranslatorListExistInDB.Add(_TranslatorViewModel);
        //    //    _TranslatorID = _Translator.TranslatorID;
        //    //}

        //    #endregion



        //    #endregion

        //}
        #endregion

        #region Checking Exsitance


        private bool CheckIfTranslatorExsitBefore(List<Translator> TranslatorListExistInDB, List<Translator> NewTranslatorList, Translator translatorValue, ref Translator translator)
        {
            var TranslatorExist = TranslatorListExistInDB.Where(t =>
                t.GeneID == translatorValue.GeneID && t.AlleleID == translatorValue.AlleleID && t.AssayID == translatorValue.AssayID && t.CallID == translatorValue.CallID
                ).FirstOrDefault();
            if (TranslatorExist == null)
            {
                var TranslatorExistInNew = NewTranslatorList.Where(t =>
                t.GeneID == translatorValue.GeneID && t.AlleleID == translatorValue.AlleleID && t.AssayID == translatorValue.AssayID && t.CallID == translatorValue.CallID
                    ).FirstOrDefault();
                if (TranslatorExistInNew == null)
                {
                    return false;
                }
                else
                {
                    translator = TranslatorExistInNew;
                    return true;
                }
            }
            else
            {
                translator = TranslatorExist;
                return true;
            }
        }


        //private bool CheckIfTranslatorExsitBefore(ref List<TranslatorViewModel> TranslatorListExistInDB, TranslatorViewModel _TranslatorViewModel, out int? _TranslatorID)
        //{

        //    _TranslatorID = null;

        //    var TranslatorExist = TranslatorListExistInDB.Where(t =>

        //        t.GeneID == _TranslatorViewModel.GeneID &&
        //        t.AlleleID == _TranslatorViewModel.AlleleID &&

        //        t.AssayID == _TranslatorViewModel.AssayID &&
        //        t.CallID == _TranslatorViewModel.CallID &&
        //        t.ProjectID == _TranslatorViewModel.ProjectID

        //        ).FirstOrDefault();

        //    if (TranslatorExist == null)
        //    {

        //        var TranslatorExistInFullDB = appServiceManager.TheUnitOfWork.Translator.GetWhere
        //                                (t =>
        //                                    //t.TranslatorMappingID == _TranslatorViewModel.TranslatorMappingID &&
        //        t.GeneID == _TranslatorViewModel.GeneID &&
        //        t.AlleleID == _TranslatorViewModel.AlleleID &&
        //        t.AssayID == _TranslatorViewModel.AssayID &&
        //        t.CallID == _TranslatorViewModel.CallID &&
        //        t.ProjectID == _TranslatorViewModel.ProjectID

        //        )
        //        .FirstOrDefault();





        //        if (TranslatorExistInFullDB != null)
        //        {
        //            _TranslatorID = TranslatorExistInFullDB.TranslatorID;

        //            TranslatorViewModel newTranslatorViewModel = new TranslatorViewModel();

        //            newTranslatorViewModel.TranslatorID = TranslatorExistInFullDB.TranslatorID;
        //            newTranslatorViewModel.GeneID = TranslatorExistInFullDB.GeneID;
        //            newTranslatorViewModel.AlleleID = TranslatorExistInFullDB.AlleleID;
        //            newTranslatorViewModel.AssayID = TranslatorExistInFullDB.AssayID;
        //            newTranslatorViewModel.CallID = TranslatorExistInFullDB.CallID;
        //            newTranslatorViewModel.ProjectID = TranslatorExistInFullDB.ProjectID;
        //            TranslatorListExistInDB.Add(newTranslatorViewModel);
        //            return true;
        //        }
        //        else
        //        {
        //            return false;
        //        }

        //    }

        //    else
        //    {
        //        _TranslatorID = TranslatorExist.TranslatorID;
        //        return true;
        //    }
        //}

        private bool CheckIfAlleleExistBefore(List<Allele> AlleleListExistInDB, List<Allele> NewAlleleList, string alleleSymbol, ref Allele AlleleEntity)
        {
            var AlleleExist = AlleleListExistInDB.Where(a => a.AlleleSymbol.ToLower() == alleleSymbol.ToLower()).FirstOrDefault();
            if (AlleleExist == null)
            {
                var AlleleExistInNew = NewAlleleList.Where(a => a.AlleleSymbol.ToLower() == alleleSymbol.ToLower()).FirstOrDefault();
                if (AlleleExistInNew == null)
                {
                    return false;
                }
                else
                {
                    AlleleEntity = AlleleExistInNew;
                    return true;
                }
            }
            else
            {
                AlleleEntity = AlleleExist;
                return true;
            }
        }
        private bool CheckIfCallExsitBefore(List<Call> CallListExistInDB, List<Call> NewCallList, string CallValue, ref Call call)
        {
            var CallExist = CallListExistInDB.Where(c => c.CallValue.ToLower() == CallValue.ToLower()).FirstOrDefault();
            if (CallExist == null)
            {
                var CallExistInNew = NewCallList.Where(c => c.CallValue.ToLower() == CallValue.ToLower()).FirstOrDefault();
                if (CallExistInNew == null)
                {
                    return false;
                }
                else
                {
                    call = CallExistInNew;
                    return true;
                }
            }
            else
            {
                call = CallExist;
                return true;
            }
        }

        public static bool CheckIfGeneExistBefore(List<Gene> GeneListExistInDB, List<Gene> NewGeneList, string GeneSymbol, ref Gene gene)
        {
            var GeneExist = GeneListExistInDB.Where(g => g.GeneSymbol.ToLower() == GeneSymbol.ToLower()).FirstOrDefault();
            if (GeneExist == null)
            {
                var GeneExistInNew = NewGeneList.Where(g => g.GeneSymbol.ToLower() == GeneSymbol.ToLower()).FirstOrDefault();
                if (GeneExistInNew == null)
                {
                    return false;
                }
                else
                {
                    gene = GeneExistInNew;
                    return true;
                }
            }
            else
            {
                gene = GeneExist;
                return true;
            }
        }

        private bool CheckIfAssayExistBefore(List<Assay> AssayListExistInDB, List<Assay> NewAssayList, ref Assay AssayEntity)
        {
            var id = AssayEntity.AssayID;
            if (id == null || string.IsNullOrEmpty(id.ToString()))
                return false;
            var assayExist = AssayListExistInDB.Where(g => g.AssayID.ToLower() == id.ToLower()).FirstOrDefault();
            if (assayExist == null)
            {
                var assayExistInNew = NewAssayList.Where(g => g.AssayID.ToLower() == id.ToLower()).FirstOrDefault();
                if (assayExistInNew == null)
                {
                    return false;
                }
                {
                    AssayEntity = null;
                    AssayEntity = new Assay();
                    AssayEntity = assayExistInNew;
                    return true;
                }
            }
            else
            {
                AssayEntity = assayExist;
                return true;
            }
        }

        #endregion

        #endregion

        #region Feedback Issues (Why you don't put this in Global Enums class)
        #endregion
        #region CellCharachter  enum
        //public enum CellCharachter
        //{

        //    #region A
        //    A = 1,
        //    B,
        //    C,
        //    D,
        //    E,
        //    F,
        //    G,
        //    H,
        //    I,
        //    J,
        //    K,
        //    L,
        //    M,
        //    N,
        //    O,
        //    P,
        //    Q,
        //    R,
        //    S,
        //    T,
        //    U,
        //    V,
        //    W,
        //    X,
        //    Y,
        //    Z,
        //    #endregion

        //    #region AA
        //    AA,
        //    AB,
        //    AC,
        //    AD,
        //    AE,
        //    AF,
        //    AG,
        //    AH,
        //    AI,
        //    AJ,
        //    AK,
        //    AL,
        //    AM,
        //    AN,
        //    AO,
        //    AP,
        //    AQ,
        //    AR,
        //    AS,
        //    AT,
        //    AU,
        //    AV,
        //    AW,
        //    AX,
        //    AY,
        //    AZ,
        //    #endregion

        //    #region B
        //    BA,
        //    BB,
        //    BC,
        //    BD,
        //    BE,
        //    BF,
        //    BG,
        //    BH,
        //    BI,
        //    BJ,
        //    BK,
        //    BL,
        //    BM,
        //    BN,
        //    BO,
        //    BP,
        //    BQ,
        //    BR,
        //    BS,
        //    BT,
        //    BU,
        //    BV,
        //    BW,
        //    BX,
        //    BY,
        //    BZ,
        //    #endregion

        //    #region C
        //    CA,
        //    CB,
        //    CC,
        //    CD,
        //    CE,
        //    CF,
        //    CG,
        //    CH,
        //    CI,
        //    CJ,
        //    CK,
        //    CL,
        //    CM,
        //    CN,
        //    CO,
        //    CP,
        //    CQ,
        //    CR,
        //    CS,
        //    CT,
        //    CU,
        //    CV,
        //    CW,
        //    CX,
        //    CY,
        //    CZ
        //    #endregion

        //}

        #endregion


        #endregion

        #region ProcessFile
        public void ProcessFile(string filePath, string uploadStatusKey, HttpContext ctx, ProjectViewModel projectViewModel, ref  List<string> status, ProjectEntities projectEntities, ref ProjectEntities NewProjectEntities, UnitOfWork _UnitOfWork)
        {
            //  Pharmacogenetics.Business.AppServices.AppServiceManager appServiceManager = new Pharmacogenetics.Business.AppServices.AppServiceManager();

            TranslatorAppService translatorAppService = new TranslatorAppService();
            System.Web.HttpContext.Current.Application.Add("listStatus" + uploadStatusKey, status);
            bool operationStatus = false;
            List<Translator> TranslatorListExistInDB = new List<Translator>();

            ExeclAssayAlleleTranslator AllAssaySymbolCells = new ExeclAssayAlleleTranslator();
            ExeclAssayAlleleTranslator AllSNPREFCells = new ExeclAssayAlleleTranslator();
            ExeclAssayAlleleTranslator AllAssayIDsCells = new ExeclAssayAlleleTranslator();
            ExeclAssayAlleleTranslator AllAssayTextCells = new ExeclAssayAlleleTranslator();

            #region Parsing Translator File
           // List<ExeclAssayAlleleTranslator> ExeclAssayAlleleTranslatorList = translatorAppService.ParseTranslatorData(filePath, status, uploadStatusKey);

            List<TranslatorExcelItem> NewTranslatorExcelItemList = TranslatorHelper.ParseTranslatorData(filePath, status, uploadStatusKey , _UnitOfWork);
            #endregion

            //var ExeclAssayAlleleTranslatorListPerGene = ExeclAssayAlleleTranslatorList.GroupBy(x => x.ACell).ToList();
            UpdateSessionStatus(status, "Analysis Translator", ctx, uploadStatusKey);
            int translatorIter = 1;
            List<string> allStatusLog = new List<string>();
            List<string> statusLog = new List<string>();

            #region  Prepare Gene ,Allele , Assays , Calls And Translator Lists To Insert In DataBase 


            #region Feedback Issues (Why uou don't create container conatin these variables to avoid to pass it one by one)
            // You can pass one container contain all variables like projectEntities
            #endregion
           // PrepareTranslatorListsForInsertInDataBase(ExeclAssayAlleleTranslatorListPerGene  , AllAssaySymbolCells , AllSNPREFCells , AllAssayIDsCells , AllAssayTextCells , ref NewProjectEntities ,ref translatorIter , statusLog  ,
             //   uploadStatusKey,  ctx,  projectViewModel, ref  status , projectEntities , ExeclAssayAlleleTranslatorList ,  _UnitOfWork);

            projectEntities.projectViewModel = projectViewModel;
            TranslatorHelper.SaveTranslatorInDatabase(NewTranslatorExcelItemList, projectEntities , ref NewProjectEntities, ref translatorIter, statusLog,uploadStatusKey, ctx, ref status , _UnitOfWork);
            UpdateSessionStatus(status, string.Format("Saving Translator........"), ctx, uploadStatusKey);

            try
            {


                if (NewProjectEntities.allGeneList != null && NewProjectEntities.allGeneList.Count > 0)
                    projectEntities.allGeneList.AddRange(NewProjectEntities.allGeneList);

                if (NewProjectEntities.allAlleleList != null && NewProjectEntities.allAlleleList.Count > 0)
                    projectEntities.allAlleleList.AddRange(NewProjectEntities.allAlleleList);


                if (NewProjectEntities.allAssayList != null && NewProjectEntities.allAssayList.Count > 0)
                    projectEntities.allAssayList.AddRange(NewProjectEntities.allAssayList);

                

                if (NewProjectEntities.allCallList != null && NewProjectEntities.allCallList.Count > 0)
                    projectEntities.allCallList.AddRange(NewProjectEntities.allCallList);

                if (NewProjectEntities.allTranslatorLabList != null && NewProjectEntities.allTranslatorLabList.Count > 0)
                    projectEntities.allTranslatorLabList.AddRange(NewProjectEntities.allTranslatorLabList);
            }
            catch (Exception ex)
            {

                string ss = ex.Message;
            }




            UpdateSessionStatus(status, string.Format("Saving Translator Complete........"), ctx, uploadStatusKey);
            #endregion

            #region Update Status with final summary
            operationStatus = true;

            if (operationStatus)
                {
                int genesListCountsInSheet = NewProjectEntities.allGeneList.Count; string textGene = string.Format("Gene Counts : {0} In Translator  ", genesListCountsInSheet);
                int AssaysCountsInSheet = NewProjectEntities.allAssayList.Count; string textAssay = string.Format("Assay Counts : {0} In Translator  ", AssaysCountsInSheet);
                int CallsCountsInSheet = NewProjectEntities.allCallList.Count; string textCall = string.Format("Call Counts : {0} In Translator  ", CallsCountsInSheet);
                int AlleleCountsInSheet = NewProjectEntities.allAlleleList.Count; string textAlleleStatus = string.Format("Bi Allele Counts : {0} In Translator  ", AlleleCountsInSheet);
                int TranslatorCountsInSheet = NewProjectEntities.allTranslatorLabList.Count; string textTranlator = string.Format("Translator Counts : {0} In Translator  ", TranslatorCountsInSheet);

                UpdateSessionStatus(status, textGene, ctx, uploadStatusKey, false);
                UpdateSessionStatus(status, textAssay, ctx, uploadStatusKey, false);
                UpdateSessionStatus(status, textCall, ctx, uploadStatusKey, false);
                UpdateSessionStatus(status, textAlleleStatus, ctx, uploadStatusKey, false);
                UpdateSessionStatus(status, textTranlator, ctx, uploadStatusKey, false);
                UpdateSessionStatus(status, string.Format("Parsing Translator Complete  , Records number in  Translator File: " + TranslatorCountsInSheet.ToString()), ctx, uploadStatusKey);
                UpdateSessionStatus(status, "----------------------------------------------------------------", ctx, uploadStatusKey);

            }
            #endregion

        }

        #region Unused
//        private void PrepareTranslatorListsForInsertInDataBase(List<IGrouping<string, ExeclAssayAlleleTranslator>> ExeclAssayAlleleTranslatorListPerGene,
//ExeclAssayAlleleTranslator AllAssaySymbolCells, ExeclAssayAlleleTranslator AllSNPREFCells, ExeclAssayAlleleTranslator AllAssayIDsCells, ExeclAssayAlleleTranslator AllAssayTextCells, ref ProjectEntities NewProjectEntities, ref int translatorIter, List<string> statusLog,
//string uploadStatusKey, HttpContext ctx, ProjectViewModel projectViewModel, ref List<string> status, ProjectEntities projectEntities, List<ExeclAssayAlleleTranslator> ExeclAssayAlleleTranslatorList, UnitOfWork _UnitOfWork)
//        {


//            foreach (var obj in ExeclAssayAlleleTranslatorListPerGene)
//            {
//                statusLog = new List<string>();

//                if (obj.Key.Contains("***"))
//                {
//                    continue;
//                }
//                else
//                {
//                    if (obj.Key == null || string.IsNullOrEmpty(obj.Key.ToString()) || obj.Key.Contains("##"))
//                    {

//                        #region Prepare Assays
//                        if (obj.Key == null || string.IsNullOrEmpty(obj.Key.ToString()))
//                        {
//                            if (obj.ToList().Count == 3)
//                            {
//                                AllAssaySymbolCells = obj.ToList()[0];
//                                AllSNPREFCells = obj.ToList()[1];
//                                AllAssayIDsCells = obj.ToList()[2];
//                            }

//                            else if (obj.ToList().Count == 2)
//                            {

//                                AllAssayIDsCells = obj.ToList()[1];
//                            }
//                            else if (obj.ToList().Count == 1)
//                            {

//                                AllAssayIDsCells = obj.ToList()[0];
//                            }
//                        }
//                        else if (obj.Key.Contains("##"))
//                        {
//                            AllAssayTextCells = obj.ToList().FirstOrDefault();
//                        }
//                        continue;
//                        #endregion
//                    }

//                    else
//                    {
//                        #region Feedback Issues (I think we need to improve the performance in this code)
//                        // I trace the code and found we make 
//                        // 1440 (InsertIntoDatabase) * 82 (BuildAssayEntity) if conditions * 82 if conditions= 9,702,732 check operations
//                        // enhancment use 'if elseif' not 'if if if' to avoid check over all cells every time
//                        // if we use list contain cells char. it is easy than write cells name hard coded
//                        #endregion
//                        #region Prepare Calls  , Gene And Allele Are Known Before  , Key is Gene  , Value is Allele
//                        foreach (var item in obj.ToList())
//                        {
//                            translatorIter++;
//                            string textStatus = string.Format("Progress {0} %", Math.Round(((double)(translatorIter - 1) / (ExeclAssayAlleleTranslatorList.Count - 10)) * 100, 2));
//                            textStatus += "\t" + string.Format("Analysis translator [Gene] {0} ", obj.Key);
//                            UpdateSessionStatus(status, textStatus, ctx, uploadStatusKey, true);
//                            this.InsertIntoDatabase(item, obj.Key, AllAssaySymbolCells, AllSNPREFCells, AllAssayIDsCells, AllAssayTextCells, projectViewModel, projectEntities, ref NewProjectEntities, _UnitOfWork);

//                        }
//                        #endregion
//                    }
//                }
//            }

//        }

        #endregion

        #endregion

        #region update Session Status  Methods

        public void clearSessionStatus()
        {
            System.Web.HttpContext.Current.Application["listStatusInner"] = new List<string>();
        }

        public void UpdateSessionStatus(List<string> status, string currentStatus, HttpContext ctx, string uploadStatusKey, bool removeLastRecord = false)
        {
            if (removeLastRecord)
                status.RemoveAt(status.Count - 1);
            status.Add(currentStatus);
            System.Web.HttpContext.Current.Application["listStatus" + uploadStatusKey] = status;
        }


        #endregion


    }
}
