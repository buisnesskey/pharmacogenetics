﻿using Pharmacogenetics.Business.Bases;
using Pharmacogenetics.Business.Interfaces;
using Pharmacogenetics.EntityService.Bases;
//using Pharmacogenetics.EntitySvcInterface.IBases;
//using Alterna.Data.Entities.AlternaEntityService.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pharmacogenetics.Business.AppServices
{
    public class AppServiceManager : AppService//, IAppServiceManager
    {
        #region Props

        #endregion
        public AppServiceManager() : base() { }
        public AppServiceManager(UnitOfWork unitOfWork)
        {
            TheUnitOfWork = unitOfWork;
        }

        #region Services Props
        PatientAppService _patientManagementAppService;
        public PatientAppService PatientAppService
        {
            get
            {
                if (_patientManagementAppService == null)
                    _patientManagementAppService = new PatientAppService(TheUnitOfWork);
                return _patientManagementAppService;
            }
        }

        ProjectAppService _projectManagementAppService;
        public ProjectAppService ProjectAppService
        {
            get
            {
                if (_projectManagementAppService == null)
                    _projectManagementAppService = new ProjectAppService(TheUnitOfWork);
                return _projectManagementAppService;
            }
        }

        TranslatorAppService _translatorManagementAppService;
        public TranslatorAppService TranslatorAppService
        {
            get
            {
                if (_translatorManagementAppService == null)
                    _translatorManagementAppService = new TranslatorAppService(TheUnitOfWork);
                return _translatorManagementAppService;
            }
        }

        PatientTranslatorService _patientTranslatorManagementAppService;
        public PatientTranslatorService PatientTranslatorAppService
        {
            get
            {
                if (_patientTranslatorManagementAppService == null)
                    _patientTranslatorManagementAppService = new PatientTranslatorService(TheUnitOfWork);
                return _patientTranslatorManagementAppService;
            }
        }
        #endregion
    }
}
