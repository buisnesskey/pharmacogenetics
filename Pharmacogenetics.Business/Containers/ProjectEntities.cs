﻿using Pharmacogenetics.Entities.MainModel;
using Pharmacogenetics.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pharmacogenetics.Business.Containers
{
    public class ProjectEntities
    {
        public List<Patient> allPateintList { set; get; }
        public List<Gene> allGeneList { set; get; }
        public List<Assay> allAssayList { set; get; }
        public List<Call> allCallList { set; get; }
        public List<PatientLab> allPatientLabList { set; get; }


        public List<Allele> allAlleleList { set; get; }
        public List<Translator> allTranslatorLabList { set; get; }

        public List<PatientTranslator> allPatientTranslatorList { set; get; }
        public ProjectViewModel projectViewModel { set;get;}

        public ProjectEntities()
        {
            allPateintList = new List<Patient>();
            allGeneList = new List<Gene>();
            allAssayList = new List<Assay>();
            allCallList = new List<Call>();
            allPatientLabList = new List<PatientLab>();
            allAlleleList = new List<Allele>();
            allTranslatorLabList = new List<Translator>();
            allPatientTranslatorList = new List<PatientTranslator>();
            projectViewModel = new ProjectViewModel();
        }
    }
}
